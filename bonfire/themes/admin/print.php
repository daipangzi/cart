<?php
	Assets::add_css( array(
        'css/print.css'
	));
?>

<html>
<head>
<meta charset="utf-8">

<title><?php echo isset($toolbar_title) ? $toolbar_title: $this->settings_lib->item('site.title'); ?></title>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
<?php echo Assets::css(null, false); ?>
</head>
  
<body class="<?php echo isset($class)?$class:''; ?>">

<!--main content-->
<?php echo Template::yield(); ?>
<!--end-->

</body>
</html>