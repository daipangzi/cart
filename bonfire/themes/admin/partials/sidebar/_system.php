<?php if (has_permission('Site.System.View')):?>
<li>
    <a href="#" hasTab="1"><span class="icon16 icomoon-icon-screen-2"></span>System</a>
    <ul class="sub">
        <?php if (has_permission('Bonfire.Users.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/system/users'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>User Management</a></li>
        <?php endif;?>
        
        <?php if (has_permission('Bonfire.Roles.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/system/roles'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>Role Management</a></li>
        <?php endif;?>
        
        <?php if (has_permission('Bonfire.Permissions.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/system/permissions'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>Permissions</a></li>
        <?php endif;?>
        
        <?php if (has_permission('Bonfire.Activities.View')):?>
        <li>
            <a href="<?php echo site_url(SITE_AREA.'/reports/activities'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>Site Activities</a>
        </li>
        <?php endif;?>
        
        <?php if (has_permission('Bonfire.Emailer.Manage')):?>
        <li>
            <a href="<?php echo site_url(SITE_AREA.'/system/emailer'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>Emailer</a>
        </li>
        <?php endif;?>
    </ul>
</li>
<?php endif;?>