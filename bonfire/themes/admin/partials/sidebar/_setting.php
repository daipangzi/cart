<?php if (has_permission('Site.Setting.View')):?>
<li>
    <a href="#" hasTab="1"><span class="icon16 icomoon-icon-cog"></span>Setting</a>
    <ul class="sub">
        <?php if (has_permission('Currencies.Settings.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/settings/currencies'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_currency_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Languages.Settings.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/settings/languages'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_langauge_mng'); ?></a></li>
        <?php endif;?>
    </ul>
</li>
<?php endif;?>