<?php if (has_permission('Site.Developer.View')):?>
<li>
    <a href="#" hasTab="1"><span class="icon16 icomoon-icon-folder"></span>Developer</a>
    <ul class="sub">
        <?php if (has_permission('Bonfire.Sysinfo.View')):?>
        <li>
            <a href="<?php echo site_url(SITE_AREA.'/developer/sysinfo'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>System Information</a>
        </li>
        <?php endif;?>
        
        <?php if (has_permission('Bonfire.Logs.View')):?>
        <li>
            <a href="<?php echo site_url(SITE_AREA.'/developer/logs'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>System Log</a>
        </li>
        <?php endif;?>
        
        <?php if (has_permission('Bonfire.Translate.Manage') && has_permission('Bonfire.Translate.View')):?>
        <!--<li>
            <a href="#"><span class="icon16 icomoon-icon-pencil"></span>Translate</a>
            <ul class="sub">
                <li><a href="<?php echo site_url(SITE_AREA.'/developer/translate'); ?>"><span class="icon16 icomoon-icon-arrow-right-3"></span>Translate Files</a></li>
                <li><a href="<?php echo site_url(SITE_AREA.'/developer/translate/export'); ?>"><span class="icon16 icomoon-icon-arrow-right-3"></span>Expert</a></li>
            </ul>
        </li>-->
        <?php endif;?>
        
        <li>
            <a href="<?php echo site_url(SITE_AREA.'/developer/builder'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>Code Builder</a>
        </li>
       
       <?php if (has_permission('Bonfire.Database.Manage')):?>
        <li>
            <a href="<?php echo site_url(SITE_AREA.'/developer/database'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>Database Tools</a>
        </li>
        <li>
            <a href="<?php echo site_url(SITE_AREA.'/developer/migrations'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span>Migrations</a>
        </li>
        <?php endif;?>
    </ul>
</li>
<?php endif;?>