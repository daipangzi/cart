<?php if (has_permission('Site.Content.View')):?>
<li>
    <a href="#" hasTab="1"><span class="icon16 icomoon-icon-file-4"></span>Content</a>
    <ul class="sub">
        <?php if (has_permission('Banners.Content.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/content/banners'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_banner_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Pages.Content.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/content/pages'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_page_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Posts.Content.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/content/posts'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_post_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Post_Categories.Content.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/content/post_categories'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_post_category_mng'); ?></a></li>
        <?php endif;?>
        
        
    </ul>
</li>
<?php endif;?>