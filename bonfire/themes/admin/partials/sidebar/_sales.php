<?php if (has_permission('Site.Sales.View')):?>
<li>
    <a href="#" hasTab="1"><span class="icon16 icomoon-icon-cart-4"></span>Sales</a>
    <ul class="sub">
        <?php if (has_permission('Orders.Sales.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/sales/orders'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_order_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Invoices.Sales.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/sales/invoices'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_invoice_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Shipments.Sales.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/sales/shipments'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_shipment_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Refunds.Sales.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/sales/refunds'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_refund_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Coupons.Sales.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/sales/coupons'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_coupon_mng'); ?></a></li>
        <?php endif;?>
    </ul>
</li>
<?php endif;?>