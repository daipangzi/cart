<?php if (has_permission('Site.Catalog.View')):?>
<li>
    <a href="#" hasTab="1"><span class="icon16 icomoon-icon-cube"></span>Catalog</a>
    <ul class="sub">
        <?php if (has_permission('Manufacturers.Catalog.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/catalog/manufacturers'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_manufacturer_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Categories.Catalog.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/catalog/categories'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_category_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Products.Catalog.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/catalog/products'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_product_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Customers.Catalog.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/catalog/customers'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_customer_mng'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Shipping_Methods.Catalog.View')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/catalog/shipping_methods'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_shipping_methods'); ?></a></li>
        <?php endif;?>
        
        <?php if (has_permission('Payment_Methods.Catalog.Edit')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/catalog/payment_methods'); ?>" hasTab="1"><span class="icon16 icomoon-icon-arrow-right-3"></span><?php echo lang('ed_payment_methods'); ?></a></li>
        <?php endif;?>
    </ul>
</li>
<?php endif;?>