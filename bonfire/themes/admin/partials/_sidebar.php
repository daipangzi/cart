<!--Responsive navigation button-->  
<div class="resBtn">
    <a href="#"><span class="icon16 minia-icon-list-3"></span></a>
</div>

<!--Left Sidebar collapse button-->  
<div class="collapseBtn leftbar">
     <a href="#" class="tipR" title="Hide Left Sidebar"><span class="icon12 minia-icon-layout"></span></a>
</div>

<!--Sidebar background-->
<div id="sidebarbg"></div>
<!--Sidebar content-->
<div id="sidebar">

    <div class="shortcuts">
        <ul>
            <li><a href="support.html" title="Support section" class="tip"><span class="icon24 icomoon-icon-support"></span></a></li>
            <li><a href="#" title="Database backup" class="tip"><span class="icon24 icomoon-icon-database"></span></a></li>
            <li><a href="charts.html" title="Sales statistics" class="tip"><span class="icon24 icomoon-icon-pie-2"></span></a></li>
            <li><a href="#" title="Write post" class="tip"><span class="icon24 icomoon-icon-pencil"></span></a></li>
        </ul>
    </div><!-- End search -->            

    <div class="sidenav">
        <div class="sidebar-widget" style="margin: -1px 0 0 0;">
            <h5 class="title" style="margin-bottom:0">Catalog</h5>
        </div><!-- End .sidenav-widget -->
        <div class="mainnav">
            <ul>
                <?php echo theme_view('partials/sidebar/_catalog'); ?>            
                <?php echo theme_view('partials/sidebar/_sales'); ?>            
                <?php echo theme_view('partials/sidebar/_setting'); ?>          
                <?php echo theme_view('partials/sidebar/_cms'); ?>              
                <?php echo theme_view('partials/sidebar/_system'); ?>            
                <?php echo theme_view('partials/sidebar/_developer'); ?>            
            </ul>
        </div>
    </div><!-- End sidenav -->

    <div class="sidebar-widget">
        <h5 class="title">Right now</h5>
        <div class="content">
            <div class="rightnow">
                <ul class="unstyled">
                    <li><span class="number">34</span><span class="icon16 icomoon-icon-new"></span>Posts</li>
                    <li><span class="number">7</span><span class="icon16 icomoon-icon-file"></span>Pages</li>
                    <li><span class="number">14</span><span class="icon16 icomoon-icon-list-2"></span>Categories</li>
                    <li><span class="number">201</span><span class="icon16 icomoon-icon-tag"></span>Tags</li>
                </ul>
            </div>
        </div>

    </div><!-- End .sidenav-widget -->

</div><!-- End #sidebar -->