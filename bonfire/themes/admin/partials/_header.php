<?php
	Assets::add_css( array(
		'bootstrap.css',
		'bootstrap-responsive.css',
        'css/supr-theme/jquery.ui.supr.css',
        'css/icons.css',
        plugin_path().'misc/qtip/jquery.qtip.css',
        //plugin_path().'misc/fullcalendar/fullcalendar.css',
        //plugin_path().'misc/search/tipuesearch.css',
        plugin_path().'forms/uniform/uniform.default.css',
        plugin_path().'forms/togglebutton/toggle-buttons.css',
        plugin_path().'tables/dataTables/jquery.dataTables.css',
        plugin_path().'forms/inputlimiter/jquery.inputlimiter.css',
        'css/main.css'
	));

	if (isset($shortcut_data) && is_array($shortcut_data['shortcut_keys'])) {
		Assets::add_js($this->load->view('ui/shortcut_keys', $shortcut_data, true), 'inline');
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo isset($toolbar_title) ? $toolbar_title .' : ' : ''; ?> <?php echo $this->settings_lib->item('site.title') ?></title>

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="favicon.ico" />

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="robots" content="noindex" />

<!-- Force IE9 to render in normla mode -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />-->
<?php echo Assets::css(null, true); ?>

<!--[if IE 8]><link href="<?php echo css_path(); ?>ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script type="text/javascript" src="<?php echo js_path(); ?>libs/excanvas.min.js"></script>
  <script type="text/javascript" src="<?php echo js_path(); ?>html5.js"></script>
  <script type="text/javascript" src="<?php echo js_path(); ?>libs/respond.min.js"></script>
<![endif]-->

<!-- Load modernizr first -->

<script type="text/javascript" src="<?php echo js_path(); ?>libs/modernizr.js"></script>
</head>
  
<body>
<!-- loading animation -->
<div id="qLoverlay"></div>
<div id="qLbar"></div>

<div id="loading-mask">
    <p id="loading_mask_loader" class="loader"><img alt="Loading..." src="<?php echo themes_path(); ?>/admin/images/ajax-loader-tr.gif"><br>Please wait...</p>
</div>
    
<div id="header">
    <?php echo theme_view('partials/_topbar'); ?>
</div><!-- End #header -->

<div id="wrapper">

    <?php echo theme_view('partials/_sidebar'); ?>

    <!--Body content-->
    <div id="content" class="clearfix">
        <div class="contentwrapper"><!--Content wrapper-->
            <div class="heading">
                <div class="span6 noLeftMargin">
                    <?php simple_breadcrumb(array(1,2,5,6), TRUE, TRUE, TRUE); ?>
                </div>
                
                <div class="right marginT10">
                    <?php echo form_open($this->uri->uri_string(), 'id="language-form"'); ?>
                    <?php echo form_dropdown2('interface_language', $bar_languages, set_value('interface_language', $current_language), 'id="interface_language" class="nostyle"'); ?>
                    <?php echo form_close(); ?>
                </div>
            </div><!-- End .heading-->

            
                
                