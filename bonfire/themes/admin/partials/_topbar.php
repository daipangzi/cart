<div class="navbar">
    <div class="navbar-inner">
      <div class="container-fluid">
        <a class="manufacturer" href="<?php echo site_url('/'); ?>"><?php echo $this->settings_lib->item('site.title'); ?>.<span class="slogan">admin</span></a>
        <div class="nav-no-collapse">
            <ul class="nav">
                <li class=""><a href="<?php echo site_url(SITE_AREA); ?>"><span class="icon16 icomoon-icon-screen-2"></span> <span class="txt">Dashboard</span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="icon16 icomoon-icon-cog"></span><span class="txt"> Settings</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="menu">
                            <ul>
                                <li>                                                    
                                    <a href="<?php echo site_url(SITE_AREA.'/settings'); ?>"><span class="icon16 icomoon-icon-equalizer"></span>Site config</a>
                                </li>
                                <li>                                                    
                                    <a href="#"><span class="icon16 icomoon-icon-wrench"></span>Plugins</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="icon16 icomoon-icon-envelop"></span><span class="txt">Messages</span><span class="notification">8</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="menu">
                            <ul class="messages">    
                                <li class="header"><strong>Messages</strong> (10) emails and (2) PM</li>
                                <li>
                                   <span class="icon"><span class="icon16 icomoon-icon-user-plus"></span></span>
                                    <span class="name"><a data-toggle="modal" href="#myModal1"><strong>Sammy Morerira</strong></a><span class="time">35 min ago</span></span>
                                    <span class="msg">I have question about new function ...</span>
                                </li>
                                <li>
                                    <span class="icon"><span class="icon16 icomoon-icon-envelop"></span></span>
                                    <span class="name"><a data-toggle="modal" href="#myModal1"><strong>Ivanovich</strong></a><span class="time">1 day ago</span></span>
                                    <span class="msg">I send you my suggestion, please look and ...</span>
                                </li>
                                <li class="view-all"><a href="#">View all messages <span class="icon16 icomoon-icon-arrow-right-8"></span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
          
            <ul class="nav pull-right usernav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="icon16 icomoon-icon-bell"></span><span class="notification">3</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="menu">
                            <ul class="notif">
                                <li class="header"><strong>Notifications</strong> (3) items</li>
                                <li>
                                    <a href="#">
                                        <span class="icon"><span class="icon16 icomoon-icon-user-plus"></span></span>
                                        <span class="event">1 User is registred</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon"><span class="icon16 icomoon-icon-bubble-3"></span></span>
                                        <span class="event">Jony add 1 comment</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon"><span class="icon16 icomoon-icon-new"></span></span>
                                        <span class="event">admin Julia added post with a long description</span>
                                    </a>
                                </li>
                                <li class="view-all"><a href="#">View all notifications <span class="icon16 icomoon-icon-arrow-right-8"></span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle avatar" data-toggle="dropdown">
                        <img src="<?php echo media_file($current_user->avatar, 'avatar', 36, 32); ?>" alt="" class="image" /> 
                        <span class="txt"><?php echo $current_user->email; ?></span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="menu">
                            <ul>
                                <li>
                                    <a href="<?php echo site_url(SITE_AREA.'/system/users/edit'); ?>"><span class="icon16 icomoon-icon-profile"></span>Edit profile</a>
                                </li>
                                <li>
                                    <a href="#"><span class="icon16 icomoon-icon-bubble-2"></span>Approve comments</a>
                                </li>
                                <li>
                                    <a href="#"><span class="icon16 icomoon-icon-plus"></span>Add user</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="<?php echo site_url('users/logout'); ?>"><span class="icon16 icomoon-icon-exit"></span><span class="txt"> <?php echo lang('bf_action_logout')?></span></a></li>
            </ul>
        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
  </div><!-- /navbar --> 