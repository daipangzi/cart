<?php
	Assets::add_js( array( 'bootstrap.min.js', 'jwerty.js'), 'external', true);
?>
<?php echo theme_view('partials/_header'); ?>

<div class="body-content">
    <div class="row-fluid messages-area"><div class="span6">
        <?php echo Template::message(); ?>
    </div></div>

    <?php echo Template::yield(); ?>
</div>

<?php echo theme_view('partials/_footer'); ?>
