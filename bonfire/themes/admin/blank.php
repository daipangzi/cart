<?php
	Assets::add_css( array(
		'bootstrap.css',
		'bootstrap-responsive.css',
        'css/icons.css',
        'css/main.css'
	));
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo isset($toolbar_title) ? $toolbar_title .' : ' : ''; ?> <?php echo $this->settings_lib->item('site.title') ?></title>

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="favicon.ico" />

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="robots" content="noindex" />

<!-- Force IE9 to render in normla mode -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />-->
<?php echo Assets::css(null, true); ?>

<!--[if IE 8]><link href="<?php echo css_path(); ?>ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script type="text/javascript" src="<?php echo js_path(); ?>libs/excanvas.min.js"></script>
  <script type="text/javascript" src="<?php echo js_path(); ?>html5.js"></script>
  <script type="text/javascript" src="<?php echo js_path(); ?>libs/respond.min.js"></script>
<![endif]-->
</head>
  
<body class="<?php echo isset($class)?$class:''; ?>">

<!--main content-->
<?php echo Template::yield(); ?>
<!--end-->

<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>-->
<script>window.jQuery || document.write('<script src="<?php echo js_path(); ?>jquery.min.js"><\/script>')</script>
<script type="text/javascript" src="<?php echo js_path(); ?>bootstrap.js"></script>  
<script type="text/javascript" src="<?php echo themes_path(); ?>admin/js/blank.js"></script>  
</body>
</html>