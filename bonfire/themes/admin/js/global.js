// document ready function
$(document).ready(function() { 
    //--------------- Accordion ------------------//
    var acc = $('.accordion'); //get all accordions
    var accHeading = acc.find('.accordion-heading');
    var accBody = acc.find('.accordion-body');

    //function to put icons
    accPutIcon = function () {
        acc.each(function(index) {
           accExp = $(this).find('.accordion-body.in');
           accExp.prev().find('a.accordion-toggle').append($('<span class="icon12 entypo-icon-plus gray"></span>'));

           accNor = $(this).find('.accordion-body').not('.accordion-body.in');
           accNor.prev().find('a.accordion-toggle').append($('<span class="icon12 entypo-icon-plus-2 gray"></span>'));


        });
    }

    //function to update icons
    accUpdIcon = function() {
        acc.each(function(index) {
           accExp = $(this).find('.accordion-body.in');
           accExp.prev().find('span').remove();
           accExp.prev().find('a.accordion-toggle').append($('<span class="icon12 entypo-icon-plus gray"></span>'));

           accNor = $(this).find('.accordion-body').not('.accordion-body.in');
           accNor.prev().find('span').remove();
           accNor.prev().find('a.accordion-toggle').append($('<span class="icon12 entypo-icon-plus-2 gray"></span>'));


        });
    }

    accPutIcon();

    $('.accordion').on('shown', function () {
        accUpdIcon();
    }).on('hidden', function () {
        accUpdIcon();
    })
    //--------------- End Accordion ------------------//
    
    //top tooltip
    $('.error input, .error textarea').qtip({
        content: false,
        position: {
            my: 'left center',
            at: 'right center',
            viewport: $(window)
        },
        style: {
            classes: 'qtip-red'
        }
    });
});

//--------------- Tinymce ------------------//
$('textarea.tinymce').tinymce({
    // Location of TinyMCE script
    script_url : plugin_path+'forms/tiny_mce/tiny_mce.js',

    // General options
    theme : "advanced",
    plugins : "autolink,lists,pagebreak,style,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,fullscreen,visualchars,nonbreaking,xhtmlxtras,advlist",

    // Theme options
    //theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,advhr,|,print,|,fullscreen,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : false,
    height: "400",
    width: "100%",

    // Example content CSS (should be your site CSS)
    //content_css : "css/main.css",

    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js",

    // Replace values for the template plugin
    template_replace_values : {
        //username : "SuprUser",
        //staffid : "991234"
    }
});
    
//setup pagination with search fields
if($('#list-form').length == 1) {
$('.pagination a, table th.sortable a').click(function(event){
    event.preventDefault();
    
    form = $('#list-form');
    form.attr('action', $(this).attr('href'));
    form.submit();
});
}

// language switch
$('#interface_language').change(function() {
    $('#language-form').submit();
}); 

// data control
$('.date').datepicker({ dateFormat: 'yy-mm-dd'});

//check all checkboxes in table
$('.check-all').on('click', function () {
    $(this).closest('.checkAll').find('input:checkbox').prop('checked', this.checked).closest('.checker>span').toggleClass('checked')
});

$('#province').change(function() {
    if(typeof base_path ==="undefined")
    {
        base_path = '';
    }
    
    $.ajax({
      url: base_path+"customers/get_city_list",
      type: "post",
      data: {'province': $(this).val(), "ci_csrf_token": ci_csrf_token()},
      success: function(res){
          $('#city').html(res);
      }   
    });    
});
////////////////////////////////////////////////////////////
//global functions
////////////////////////////////////////////////////////////
function setLocation(url) {
    document.location.href = url;
}

function resetForm() {
    $("form").reset();
}

function generate_temp_id(len)
{
    random_string = Math.random().toString(36);
    return random_string.substr(2, len);
}

function showLoading() {
    $('#loading-mask').show();
}

function hideLoading() {
    $('#loading-mask').hide();
}

function applySearchRow(form_id, url)
{
    $('#search_row input, #search_row select').change(function() {
        orderby = $('#param_orderby').val();
        order   = $('#param_order').val();
        if(orderby && order)
        {
            url += '?orderby='+orderby+'&order='+order;
        }
        $('#'+form_id).attr("action", url);
    });  
}