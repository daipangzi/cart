jQuery(document).ready( function() {
    new AnimOnScroll( document.getElementById( 'grid' ), {
        minDuration : 0.4,
        maxDuration : 0.7,
        viewportFactor : 0.2
    });
    
    jQuery('.bxslider').bxSlider({
      auto: true,
      mode: 'fade'
    });
    
    jQuery(".banner .in_cont").height(jQuery(".banner .slide").height());
    
    jQuery(window).resize(function() {
        jQuery(".banner .in_cont").height(jQuery(".banner .slide").height());
    });
});