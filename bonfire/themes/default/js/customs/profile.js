jQuery(document).ready( function() {
    var content_height = jQuery(".profile_content").height();
    var sidebar_height = jQuery(".profile_sidebar").height();
    var max_height = 0;
    
    if( content_height > sidebar_height )
        max_height = content_height;
    else
        max_height = sidebar_height;
        
    jQuery(".profile_content").height(max_height);
    jQuery(".profile_sidebarwrap").height(max_height);
    
    jQuery(window).resize(function() {
        var content_height = jQuery(".profile_content").height();
        var sidebar_height = jQuery(".profile_sidebar").height();
        var max_height = 0;
        
        if( content_height > sidebar_height )
            max_height = content_height;
        else
            max_height = sidebar_height;
            
        jQuery(".profile_content").height(max_height);
        jQuery(".profile_sidebarwrap").height(max_height);
    });
});