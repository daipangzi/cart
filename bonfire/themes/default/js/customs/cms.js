jQuery(document).ready( function() {
    var menu_height = jQuery(".cms_menu").height();
    var content_height = jQuery(".cms_page").height();
    var max_height = 0;
    
    if( menu_height > content_height )
        max_height = menu_height;
    else
        max_height = content_height;
        
    jQuery(".cms_menu").height(max_height);
    jQuery(".cms_page").height(max_height);
    
    jQuery(window).resize(function() {
        var menu_height = jQuery(".cms_menu").height();
        var content_height = jQuery(".cms_page").height();
        var max_height = 0;
        
        if( menu_height > content_height )
            max_height = menu_height;
        else
            max_height = content_height;
            
        jQuery(".cms_menu").height(max_height);
        jQuery(".cms_page").height(max_height);
    });
});