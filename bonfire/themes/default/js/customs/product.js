var is_ok = 1;
jQuery(document).ready( function() {
    $('.fancybox').fancybox();
    
    if( is_related == 0 ) {
        new AnimOnScroll( document.getElementById( 'grid' ), {
            minDuration : 0.4,
            maxDuration : 0.7,
            viewportFactor : 0.2
        });
    }
    jQuery('.product-form').submit(function(event) { 
        jQuery(this).validate();    
        return true;
    });
    
    jQuery('.required').val('');
    jQuery('.unrequired').val('');
    
    jQuery('.price .view_btn').click(function() {
        is_ok = 1;
        jQuery('.required').each(function() {
            if( jQuery(this).val() == '' )
                is_ok = 0;
        });
        if( is_ok == 0 ) {
            alert("Please select option.");
            return false;
        }
    });
    jQuery('.more_detail a[href^="#"]').click(function(event) {
        var id = jQuery(this).attr("href");
        var offset = 20;
        var target = jQuery(id).offset().top - offset;
        jQuery('html, body').animate({scrollTop:target}, 800, jQuery.easing.easeInOutExpo());
        event.preventDefault();
    });
    
    jQuery("#product_gallery a").click(function() {
        jQuery("#product_gallery a").removeClass("active");
        jQuery(this).addClass("active");
        var obj = jQuery(this);
        jQuery("#main_product_image").parent().attr("href", obj.attr("zoom-image"));
        jQuery("#main_product_image").fadeOut("fast", function() {
            jQuery("#main_product_image").attr("src", obj.attr("data-image"));
        }).fadeIn("normal");
    });
        
    jQuery(window).resize(function() {
        
    });
});

function onSelectOption(obj_id, value, obj) {
    jQuery("#options_"+obj_id).val(value);
    jQuery(".opt_"+obj_id).removeClass("selected");
    jQuery(obj).addClass("selected");
}
