jQuery(document).ready(function() {
    if( no_submenu == 0 ) {
	    if( jQuery(document).scrollTop() > 170 )
		    jQuery(".sub_nav").addClass("over_sub_nav");
	    else
		    jQuery(".sub_nav").removeClass("over_sub_nav");
    }
    
    jQuery(".no_popup h3").mouseover(function() {
        jQuery(this).parent().find("ul.main_bordercolor").stop().slideDown("normal");
    });
    
    jQuery(".no_popup").hover(function() {
        jQuery(this).find("ul.main_bordercolor").slideDown("normal");
    }, function() {
        jQuery(this).find("ul.main_bordercolor").stop().slideUp("fast");
    });
    		
	jQuery(window).scroll(function() {
        if( no_submenu == 0 ) {
		    if( jQuery(document).scrollTop() > 170 )
			    jQuery(".sub_nav").addClass("over_sub_nav");
		    else
			    jQuery(".sub_nav").removeClass("over_sub_nav");
        }
	});
    
    jQuery(".sub_nav ul li").hover(
        function(){
            jQuery(this).children('.sub_cats').hide();
            jQuery(this).children('.sub_cats').slideDown('normal');
        },
        function () {
            jQuery(this).children('.sub_cats').hide();
            jQuery('.sub_nav', this).slideUp('fast');
    });
	
	jQuery(".online_top").click( function() {
        var element_str;
        if ((navigator.appVersion.indexOf('1.') != -1))
            element_str = 'body';
        else
            element_str = 'html, body';
		if( jQuery(element_str).scrollTop() > 650 )
			jQuery(element_str).animate({scrollTop: 0}, 1300);
		else if( jQuery(element_str).scrollTop() > 250 )
			jQuery(element_str).animate({scrollTop: 0}, 1000);
		else if ( jQuery(element_str).scrollTop() > 10 )
			jQuery(element_str).animate({scrollTop: 0}, 700);
	});
});