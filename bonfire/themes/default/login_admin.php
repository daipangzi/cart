<?php
    Assets::add_css( array(
        'bootstrap.css',
        'bootstrap-responsive.css',
        themes_path().'admin/css/supr-theme/jquery.ui.supr.css',
        themes_path().'admin/css/icons.css',
        plugin_path().'forms/uniform/uniform.default.css',
        themes_path().'admin/css/main.css'
    ));
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo isset($toolbar_title) ? $toolbar_title .' : ' : ''; ?> <?php echo $this->settings_lib->item('site.title') ?></title>

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="favicon.ico" />

<!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
<meta name="application-name" content="Supr"/> 
<meta name="msapplication-TileColor" content="#3399cc"/> 

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="robots" content="noindex" />

<!-- Force IE9 to render in normla mode -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' />
<?php echo Assets::css(null, true); ?>

<!--[if IE 8]><link href="<?php echo css_path(); ?>ie8.css" rel="stylesheet" type="text/css" /><![endif]-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script type="text/javascript" src="<?php echo js_path(); ?>libs/excanvas.min.js"></script>
  <script type="text/javascript" src="<?php echo js_path(); ?>html5.js"></script>
  <script type="text/javascript" src="<?php echo js_path(); ?>libs/respond.min.js"></script>
<![endif]-->

<!-- Load modernizr first -->

<script type="text/javascript" src="<?php echo js_path(); ?>libs/modernizr.js"></script>
</head>
  
<body class="loginPage">

<?php echo isset($content) ? $content : Template::yield(); ?>

<?php
    Assets::add_js( array(
        'jquery.min.js',
        'bootstrap.js',
        plugin_path().'forms/validate/jquery.validate.min.js',
        plugin_path().'forms/uniform/jquery.uniform.min.js'
    ));  
?>
<?php echo Assets::js(); ?>
</body>
</html>