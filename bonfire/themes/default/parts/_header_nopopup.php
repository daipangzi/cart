<?php
	// Setup our default assets to load.
	Assets::add_js( 'bootstrap.min.js');
	Assets::add_css( array('component.css', 'bootstrap.min.css', 'jquery.bxslider.css', 'style.css', 'responsive.css'));

	Template::block('header', 'parts/head');

	Template::block('topbar', 'parts/topbar_nopopup');
?>