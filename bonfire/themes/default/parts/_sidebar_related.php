<div class="sidebar cat_side"><!-- Sidebar -->                    
    <div class="side main_bordercolor">
        <h3 class="main_bordercolor main_color">正在进行的特卖</h3>
        <div class="side_curpros"> <!-- Curretly special products -->
        <?php if( is_array($random_products) && count($random_products)>0 ) { ?>
            <?php foreach( $random_products as $pro ) { ?>
            <div class="item">
                <a href="<?php echo $pro->getUrl(); ?>" class="item_img sub_bordercolor"><img src="<?php echo media_file($pro->image, 'product', 400); ?>" /></a>
                <a href="<?php echo $pro->getUrl(); ?>" class="item_title"><?php echo $pro->name; ?></a><br />
                <span><?php echo format_price($pro->special_price); ?></span>&nbsp;(<?php echo format_sales_percent($pro->getSalesPercent()); ?>)
            </div>
            <?php } ?>
        <?php } else { ?>
            <div class="item"><br />&nbsp;&nbsp;There is no products.</div>
        <?php } ?>
        </div>
    </div>
</div><!-- Sidebar -->