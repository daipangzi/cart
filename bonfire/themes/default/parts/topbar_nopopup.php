<!-- header area -->
        <div class="header">
            <div class="top_header main_bgcolor"><!-- Top Bar -->
                <div class="leftarea">
                <?php if (isset($current_customer->email)) : ?>
                    <?php echo sprintf(lang('ed_welcome'), $current_customer->name); ?>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="<?php echo site_url('customers/profile');?>" title=""><?php echo lang('ed_my_account'); ?></a>&nbsp;|&nbsp;
                    <a href="<?php echo site_url('logout');?>" title=""><?php echo lang('ed_logout'); ?></a>&nbsp;|
                <?php else :  ?>
                    <a href="<?php echo site_url('customers/login');?>" title="" class="selected"><?php echo lang('ed_login'); ?></a>&nbsp;|&nbsp;
                    <a href="<?php echo site_url('customers/register');?>" title=""><?php echo lang('ed_register'); ?></a>&nbsp;|&nbsp;
                    <a href="<?php echo site_url('customers/login');?>" title=""><?php echo lang('ed_my_account'); ?></a>&nbsp;|
                <?php endif; ?>
                </div>
                <div class="rightarea">
                    |&nbsp;<a href="#this" title="" class="selected">中国语</a>
                    &nbsp;|&nbsp;<a href="#this" title="">English</a>
                    &nbsp;|&nbsp;<a href="#this" title="">조선어</a>
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="m_header">
                <div class="logo"><!-- Logo -->
                    <a href="<?php echo site_url('/'); ?>"><img src="<?php echo Template::theme_url('images/demo/logo.png') ?>" alt="<?php e($this->settings_lib->item('site.title')); ?>"/></a>
                </div>
                <div class="searchbar"><!-- Search bar -->
                    <div class="searchinput main_bgcolor">
                        <div class="l_area">
                            <div class="s_bg"><div class="main_bordercolor"><input type="text" id="search" name="search" placeholder="<?php echo lang('search_enter_keyword'); ?>"/></div></div>
                        </div>
                        <div class="searchbtn">
                            <a href="#this" title="Search Button">Search</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="searchkeys"><strong><?php echo lang('search_popular_terms'); ?>&nbsp;:</strong>&nbsp;羽绒服&nbsp;毛呢大衣&nbsp;爆款棉衣&nbsp;短靴&nbsp;秋冬连衣裙&nbsp;打底裤&nbsp;雪地靴</div>
                </div>
                <div class="cartarea"><!-- Cart button -->
                    <div class="cartbtn main_bordercolor sub_bgcolor">
                        <a href="<?php echo site_url('/cart'); ?>" title="Cart Button"><?php echo lang('ed_cart'); ?></a>
                    </div>
                    <div class="carttext"><?php echo sprintf(lang('ed_cart_items'), get_cart_qty()); ?></div>
                    <div class="cartnum"><?php echo get_cart_items(); ?></div>
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="nav sub_bgcolor main_bordercolor"><!-- Top Menu-->
                <div class="nav_wrap">
                    <div class="main_navwrap">
                        <ul class="main_nav"><!-- Main Menu -->
                            <li><a href="<?php echo site_url('/'); ?>" class="sub_bgcolor">今日特卖</a></li>
                            <li><a href="#this" class="sub_bgcolor">往期特卖</a></li>
                            <li><a href="#this" class="sub_bgcolor">评论口啤</a></li>
                            <li><a href="#this" class="sub_bgcolor">新闻公告</a></li>
                            <li><a href="#this" class="sub_bgcolor"><?php echo lang('help_center'); ?></a></li>
                        </ul>
                        <div class="menu_spacing">&nbsp;</div>
                        <div class="clear"></div>
                    </div>
                    <div class="sub_nav no_popup">
                        <h3 class="main_bgcolor"><?php echo lang('ed_categories'); ?></h3>
                        <ul class="main_bordercolor">
                            <?php foreach( $categories as $cat ) { ?>
                            <li class="main_bordercolor">
                                <div class="cat_img"><a href="<?php echo $cat->getUrl(); ?>"><img src="<?php echo Template::theme_url("images/demo/cat1.png"); ?>" width="28" height="28"/></a></div>
                                <div class="cat_title"><a href="<?php echo $cat->getUrl(); ?>"><?php echo $cat->name?></a></div>
                                <div class="clear"></div>
                                
                                <div class="sub_cats main_bordercolor">
                                    <h3 class="main_bordercolor"><?php echo $cat->name?></h3>
                                    
                                    <?php if( $cat->hasChildrens() ) { 
                                        $is_sub_cat = 0;
                                        
                                        foreach( $cat->getChildrens() as $child_cat ) {
                                            if( $child_cat->hasChildrens() )
                                                $is_sub_cat = 1;
                                        }
                                        
                                        if( $is_sub_cat == 0 ) { // Sub categories ?>
                                        <div>
                                            <ul>
                                                <?php foreach( $cat->getChildrens() as $child_cat ) { ?>
                                                 <li><a href="<?php echo $child_cat->getUrl(); ?>"><?php echo $child_cat->name; ?></a></li>
                                                 <?php } ?>
                                            </ul>
                                        </div>
                                        <?php } else { // is Sub > sub categories
                                            foreach( $cat->getChildrens() as $child_cat ) { ?>
                                            <div>
                                                <h4><a href="<?php echo $child_cat->getUrl(); ?>" class="main_color"><?php echo $child_cat->name; ?></a></h4>
                                                <?php if( $child_cat->hasChildrens() ) { ?>
                                                <ul>
                                                    <?php foreach( $child_cat->getChildrens() as $sub_cat ) { ?>
                                                    <li><a href="<?php echo $sub_cat->getUrl(); ?>"><?php echo $sub_cat->name; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                                <?php } ?>
                                                <div class="clear"></div>
                                            </div>
                                            <?php 
                                            }
                                        }
                                    } ?>
                                </div>
                            </li>
                            <?php } ?>
                            
                            <li class="main_bordercolor">
                                <div class="cat_img"><a href="#this"><img src="<?php echo Template::theme_url("images/demo/cat6.png"); ?>" width="28" height="28"/></a></div>
                                <div class="cat_title"><a href="#this"><?php echo lang('ed_manufacturers'); ?></a></div>
                                <div class="clear"></div>
                                
                                <div class="sub_cats main_bordercolor">
                                    <h3 class="main_bordercolor"><?php echo lang('ed_manufacturers'); ?></h3>
                                    <div class="brand">
                                        <ul>
                                        <?php foreach($manufacturers as $manufacturer) { ?>
                                            <li><a href="<?php echo get_clean_url($manufacturer['manufacturer_id'], 'manufacturer'); ?>"><img src="<?php echo media_file($manufacturer['image'], 'manufacturer', 120, 60); ?>"/></a></li>
                                        <?php } ?>
                                        </ul>
                                    </div>                                    
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- Top Menu -->
        </div>
        <!-- !header area -->
        <script>
        no_submenu = 1;
        </script>