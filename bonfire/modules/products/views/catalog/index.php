<div class="admin-box">
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" id="list-form"'); ?>
    <input type="hidden" id="param_orderby" name="orderby" value="<?php echo $params['orderby']; ?>"/>
    <input type="hidden" id="param_order" name="order" value="<?php echo $params['order']; ?>"/>
    
    <fieldset><legend><?php echo lang('products_list'); ?><small><?php echo $this->pagination->get_page_status();?></small></legend></fieldset>
        
    <div class="form-buttons">
        <?php if(!empty($search)) :?>
        <button type="submit" name="reset_search" id="reset-search" class="btn" value="reset"><?php echo lang('bf_action_reset_filter') ?></button>
        <?php endif; ?>
        
        <button type="submit" name="search_entries" id="search-entries" class="btn" value="search"><span class="icon-zoom-in"></span>&nbsp;<?php echo lang('bf_action_filter') ?></button>
        
        <?php if ($this->auth->has_permission('Products.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
        <button type="submit" name="delete" id="delete-me" class="btn" value="<?php echo lang('bf_action_delete') ?>" onclick="return confirm('<?php echo lang('products_delete_confirm'); ?>')"><span class="icon-remove-sign"></span>&nbsp;<?php echo lang('bf_action_delete') ?></button>            
        <?php endif;?>
        
        <?php if ($this->auth->has_permission('Products.Catalog.Create')) : ?>
        <?php echo anchor(SITE_AREA .'/catalog/products/create', '<span class="icon-plus-sign"></span>&nbsp;'.lang('bf_action_insert'), 'class="btn"'); ?>                    
        <?php endif;?>
    </div>
    
    <div class="responsive">
		<table class="table table-striped lrborder checkAll" style="min-width:1000px;">
            <colgroup>
                <?php if ($this->auth->has_permission('Products.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                <col width="40"/>
                <?php endif;?>
                <col width="100"/>
                <col width=""/>
                <col width="200"/>
                <col width="120"/>
                <col width="150"/>
                <col width="130"/>
                <col width="100"/>
            </colgroup>
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Products.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
                    <th class="<?php echo sort_classes($params['orderby'], "id", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/products?orderby=id&amp;order='.sort_direction($params['orderby'], "id", $params['order'])); ?>"><span><?php echo lang('ed_id'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "name", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/products?orderby=name&amp;order='.sort_direction($params['orderby'], "name", $params['order'])); ?>"><span><?php echo lang('ed_name'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "sku", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/products?orderby=sku&amp;order='.sort_direction($params['orderby'], "sku", $params['order'])); ?>"><span><?php echo lang('ed_sku'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "price", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/products?orderby=price&amp;order='.sort_direction($params['orderby'], "price", $params['order'])); ?>"><span><?php echo lang('ed_price'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "quantity", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/products?orderby=quantity&amp;order='.sort_direction($params['orderby'], "quantity", $params['order'])); ?>"><span><?php echo lang('ed_quantity'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
					<th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/products?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ed_status'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th><?php echo lang('ed_actions'); ?></th>
				</tr>
                <tr id="search_row">
                    <?php if ($this->auth->has_permission('Products.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                    <td>&nbsp;</td>
                    <?php endif;?>
                    
                    <td style="text-align:right;">
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="right wd30px" name="search[from_id]" value="<?php echo isset($search['from_id'])?$search['from_id']:''; ?>" />
                        <div class="clear"></div>
                        <hr/>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="right wd30px" name="search[to_id]" value="<?php echo isset($search['to_id'])?$search['to_id']:''; ?>"/>
                    </td>
                    <td><input type="text" name="search[name]" value="<?php echo isset($search['name'])?$search['name']:''; ?>" style="width:90%;"><br/></td>
                    <td><input type="text" name="search[sku]" value="<?php echo isset($search['sku'])?$search['sku']:''; ?>" style="width:90%;"><br/></td>
                    <td style="text-align:right;">
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="right wd50px" name="search[from_price]" value="<?php echo isset($search['from_price'])?$search['from_price']:''; ?>"/>                             <div class="clear"></div>
                        <hr/>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="right wd50px" name="search[to_price]" value="<?php echo isset($search['to_price'])?$search['to_price']:''; ?>"/>
                    </td>
                    <td style="text-align:right;">
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="right wd50px" name="search[from_qty]" value="<?php echo isset($search['from_qty'])?$search['from_qty']:''; ?>"/>                                   <div class="clear"></div>
                        <hr/>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="right wd50px" name="search[to_qty]" value="<?php echo isset($search['to_qty'])?$search['to_qty']:''; ?>"/>
                    </td>
                    <td><?php echo form_dropdown2('search[status]', array('-1' => '',"1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value('search[status]', isset($search['status']) ? $search['status'] : '-1'), 'class="nostyle autowidth"'); ?></td>
                    <td></td>
                </tr>
			</thead>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Products.Catalog.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record['product_id'] ?>" /></td>
					<?php endif;?>
				        
                    <td><?php echo $record['product_id'] ?></td>
				    <?php if ($this->auth->has_permission('Products.Catalog.Edit')) : ?>
				    <td><?php echo anchor(SITE_AREA .'/catalog/products/edit/'.$record['product_id'].'/'.md5($record['product_id']), $record['name']) ?></td>
				    <?php else: ?>
				    <td><?php echo $record['name'] ?></td>
				    <?php endif; ?>
			        <td><?php echo $record['sku'] ?></td>
				    <td><?php echo format_price($record['price']); ?></td>
                    <td><?php echo $record['qty'] ?></td>
				    <td><?php echo $record['status']=='1'?lang('ed_enable'):lang('ed_disable'); ?></td>
                    <td>
                        <?php if ($this->auth->has_permission('Products.Catalog.Edit')) : ?>
                        <a href="<?php echo site_url(SITE_AREA .'/catalog/products/edit/'.$record['product_id'].'/'.md5($record['product_id'])) ?>" title="<?php echo lang('bf_action_edit'); ?>" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                        <?php endif; ?>
                        
                        <?php if ($this->auth->has_permission('Products.Catalog.Delete')) : ?>
                        <a href="<?php echo site_url(SITE_AREA .'/catalog/products/delete/'. $record['product_id'].'/'.md5($record['product_id'])) ?>" title="<?php echo lang('bf_action_delete'); ?>" class="tip"><span class="icon12 icomoon-icon-remove"></span></a>
                        <?php endif; ?>
                    </td>
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="20"><?php echo lang('no_record'); ?></td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
    </div>
    
	<?php echo form_close(); ?>
    <?php echo $this->pagination->create_links(); ?>
</div>