<input type="hidden" id="offset" name="offset" value="<?php echo $offset; ?>"/>

<div class="responsive">
    <div class="row-fluid" style="padding-bottom:5px;">
        <div class="left" style="padding-top:5px;">
            <?php echo $this->pagination->get_page_status();?>
        </div>
        
        <div class="right">
            <input type="submit" name="reset_entries" id="reset_entries" class="btn nostyle" value="<?php echo lang('bf_action_reset') ?>">&nbsp;
            <input type="submit" name="search_entries" id="search_entries" class="btn nostyle" value="<?php echo lang('bf_action_filter') ?>"/>
        </div>
    </div>
    
    <table class="table table-striped lrborder checkAll" style="min-width:1000px;">
        <colgroup>
            <col width="40"/>
            <col width="100"/>
            <col width=""/>
            <col width="200"/>
            <col width="120"/>
            <col width="150"/>
            <col width="130"/>
        </colgroup>
        <thead>
            <tr id="sort_row">
                <th class="column-check"><input class="check-all" type="checkbox" /></th>
                <th class="sortable <?php echo sort_classes($params['orderby'], "id", $params['order'])?>" data-orderby="id">
                    <a href="javascript:void(0);"><span><?php echo lang('ed_id'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="sortable <?php echo sort_classes($params['orderby'], "name", $params['order'])?>" data-orderby="name">
                    <a href="javascript:void(0);"><span><?php echo lang('ed_name'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="sortable <?php echo sort_classes($params['orderby'], "sku", $params['order'])?>" data-orderby="sku">
                    <a href="javascript:void(0);"><span><?php echo lang('ed_sku'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="sortable <?php echo sort_classes($params['orderby'], "price", $params['order'])?>" data-orderby="price">
                    <a href="javascript:void(0);"><span><?php echo lang('ed_price'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="sortable <?php echo sort_classes($params['orderby'], "quantity", $params['order'])?>" data-orderby="quantity">
                    <a href="javascript:void(0);"><span><?php echo lang('ed_quantity'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="sortable <?php echo sort_classes($params['orderby'], "status", $params['order'])?>" data-orderby="status">
                    <a href="javascript:void(0);"><span><?php echo lang('ed_status'); ?></span><span class="sorting-indicator"</span></a>
                </th>
            </tr>
            <tr id="search_row">
                <?php $varr = array('-1'=>lang('ed_any'), '0'=>lang('ed_yes'), '1'=>lang('ed_no')); ?>
                <td><?php echo form_dropdown2('search[visible]', $varr, set_value('search[visible]', isset($search['visible']) ? $search['visible'] : '0'), 'class="nostyle autowidth"'); ?></td>
                <td style="text-align:right;">
                    <span class="lbl"><?php echo lang('ed_from'); ?></span>
                    <input type="text" class="right wd30px" name="search[from_id]" value="<?php echo isset($search['from_id'])?$search['from_id']:''; ?>" />
                    <div class="clear"></div>
                    <hr/>
                    <span class="lbl"><?php echo lang('ed_to'); ?></span>
                    <input type="text" class="right wd30px" name="search[to_id]" value="<?php echo isset($search['to_id'])?$search['to_id']:''; ?>"/>
                </td>
                <td><input type="text" name="search[name]" value="<?php echo isset($search['name'])?$search['name']:''; ?>" style="width:90%;"><br/></td>
                <td><input type="text" name="search[sku]" value="<?php echo isset($search['sku'])?$search['sku']:''; ?>" style="width:90%;"><br/></td>
                <td style="text-align:right;">
                    <span class="lbl"><?php echo lang('ed_from'); ?></span>
                    <input type="text" class="right wd50px" name="search[from_price]" value="<?php echo isset($search['from_price'])?$search['from_price']:''; ?>"/>                             <div class="clear"></div>
                    <hr/>
                    <span class="lbl"><?php echo lang('ed_to'); ?></span>
                    <input type="text" class="right wd50px" name="search[to_price]" value="<?php echo isset($search['to_price'])?$search['to_price']:''; ?>"/>
                </td>
                <td style="text-align:right;">
                    <span class="lbl"><?php echo lang('ed_from'); ?></span>
                    <input type="text" class="right wd50px" name="search[from_qty]" value="<?php echo isset($search['from_qty'])?$search['from_qty']:''; ?>"/>                                   <div class="clear"></div>
                    <hr/>
                    <span class="lbl"><?php echo lang('ed_to'); ?></span>
                    <input type="text" class="right wd50px" name="search[to_qty]" value="<?php echo isset($search['to_qty'])?$search['to_qty']:''; ?>"/>
                </td>
                    <td><?php echo form_dropdown2('search[status]', array('-1' => '',"1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value('search[status]', isset($search['status']) ? $search['status'] : '-1'), 'class="nostyle autowidth"'); ?></td>
            </tr>
        </thead>
        <tbody>
        <?php if (isset($records) && is_array($records) && count($records)) : ?>
        <?php foreach ($records as $record) : ?>
            <tr>
                <td>
                    <input type="checkbox" name="related[check][]" value="<?php echo $record['product_id'] ?>" <?php echo in_array($record['product_id'], $related)?'checked="checked"':'';?>/>
                    <input type="hidden" name="related[ids][]" value="<?php echo $record['product_id'] ?>"/>
                </td>
                <td><?php echo $record['product_id'] ?></td>
                <td><?php echo $record['name'] ?></td>
                <td><?php echo $record['sku'] ?></td>
                <td><?php echo format_price($record['price']); ?></td>
                <td><?php echo $record['qty'] ?></td>
                <td><?php echo $record['status']=='1'?lang('ed_enable'):lang('ed_disable'); ?></td>
            </tr>
        <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="20"><?php echo lang('no_record'); ?></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
<br/>
<?php echo $this->pagination->create_links(); ?>