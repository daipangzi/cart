<?php // Change the css classes to suit your needs

$title = lang('product_new');
$back_url = isset($prev_page)?$prev_page:SITE_AREA .'/catalog/customers';
if(isset($record)) {
    $info = $record['info'];
    $title = $record['info'][global_language_id()]['name'];
}

$category_ids = (isset($categories) && is_array($categories) && count($categories)>0)?(implode(',', $categories)):'';
?>

<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'id="product_form" class="form-horizontal" autocomplete="off"'); ?>
    <input type="hidden" name="key" value="<?php echo md5(isset($record['product_id']) ? $record['product_id'] : '0');?>"/>
    <input type="hidden" name="selected_tab" id="selected_tab" value="<?php echo $selected_tab; ?>"/>
    <input type="hidden" id="param_orderby" name="orderby" value="<?php echo $params['orderby']; ?>"/>
    <input type="hidden" id="param_order" name="order" value="<?php echo $params['order']; ?>"/>
    <input type="hidden" id="last_offset" name="last_offset" value="<?php echo $offset; ?>"/>

    <fieldset><legend><?php echo $title; ?></legend></fieldset>
        
    <div style="margin-bottom: 20px;">
        <ul id="product-tab" class="nav nav-tabs">
            <li class="active"><a href="#general-tab" data-toggle="tab"><?php echo lang('ed_general'); ?></a></li>
            <li><a href="#description-tab" data-toggle="tab">&nbsp;<?php echo lang('ed_description'); ?>&nbsp;</a></li>
            <li><a href="#categories-tab" data-toggle="tab">&nbsp;<?php echo lang('ed_categories'); ?>&nbsp;</a></li>
            <li><a href="#options-tab" data-toggle="tab"><?php echo lang('ed_options'); ?></a></li>
            <li><a href="#images-tab" data-toggle="tab"><?php echo lang('ed_images'); ?></a></li>
            <li><a href="#samples-tab" data-toggle="tab"><?php echo lang('ed_samples'); ?></a></li>
            <li><a href="#related-tab" data-toggle="tab"><?php echo lang('ed_related_products'); ?></a></li>
            <li><a href="#meta-tab" data-toggle="tab"><?php echo lang('ed_meta_info'); ?></a></li>
        </ul>
        
        <div class="tab-content">
            <div class="tab-pane fade in active" id="general-tab"> 
                <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; ?>
                <div class="control-group <?php echo form_error("info[{$c}][name]") ? 'error' : ''; ?>">
                    <?php 
                        if($i == 0)
                            echo form_label(lang('ed_name').lang('bf_form_label_required'), "info_{$c}_name", array('class' => "control-label") );
                        else
                            echo form_label('', "info_{$c}_name", array('class' => "control-label") );
                    ?>
                    <div class='controls'>
                        <input id="info_<?php echo $c; ?>_name" type="text" name="info[<?php echo $c; ?>][name]" class="span6" maxlength="255" value="<?php echo set_value("info[{$c}][name]", isset($info[$c]['name']) ? $info[$c]['name'] : ''); ?>" <?php echo form_error("info[{$c}][name]") ? 'title="'.form_error("info[{$c}][name]").'"' : ''; ?>/>
                        <span class="inline-help gray"><?php echo strtoupper($l['locale']); ?></span>
                    </div>
                </div>
                <?php $i++; endforeach; ?>
                         
                <div class="control-group <?php echo form_error('sku') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_sku').lang('bf_form_label_required'), 'sku', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="sku" type="text" name="sku" class="span4" maxlength="30" value="<?php echo set_value('sku', isset($record['sku']) ? $record['sku'] : ''); ?>" <?php echo form_error('sku') ? 'title="'.form_error('sku').'"' : ''; ?>/>
                    </div>
                </div>
                
                <div class="control-group <?php echo form_error('price') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_price').lang('bf_form_label_required'), 'price', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="price" type="text" name="price" class="span4" maxlength="10" value="<?php echo set_value('price', isset($record['price']) ? $record['price'] : ''); ?>" <?php echo form_error('price') ? 'title="'.form_error('price').'"' : ''; ?>/>
                        <span class="gray"><?php echo strtoupper(settings_item('site.default_currency')); ?></span>
                    </div>
                </div>
                
                <div class="control-group <?php echo form_error('special_price') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_special_price').lang('bf_form_label_required'), 'special_price', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="special_price" type="text" name="special_price" class="span4" maxlength="10" value="<?php echo set_value('special_price', isset($record['special_price']) ? $record['special_price'] : ''); ?>" <?php echo form_error('special_price') ? 'title="'.form_error('special_price').'"' : ''; ?>/>
                    </div>
                </div>
                
                <div class="control-group <?php echo form_error('qty') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_qty'), 'qty', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="qty" type="text" name="qty" class="span4" maxlength="10" value="<?php echo set_value('qty', isset($record['qty']) ? $record['qty'] : ''); ?>" <?php echo form_error('qty') ? 'title="'.form_error('qty').'"' : ''; ?>/>
                    </div>
                </div>
                
                <div class="control-group <?php echo form_error('start_date') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_start_date'), 'start_date', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="start_date" type="text" name="start_date" class="span4 date" maxlength="20" value="<?php echo set_value('start_date', isset($record['start_date']) ? $record['start_date'] : ''); ?>" <?php echo form_error('start_date') ? 'title="'.form_error('start_date').'"' : ''; ?>/>
                    </div>
                </div>
                
                <div class="control-group <?php echo form_error('end_date') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_end_date'), 'end_date', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="end_date" type="text" name="end_date" class="span4 date" maxlength="20" value="<?php echo set_value('end_date', isset($record['end_date']) ? $record['end_date'] : ''); ?>" <?php echo form_error('end_date') ? 'title="'.form_error('end_date').'"' : ''; ?>/>
                    </div>
                </div>
                
                <div class="control-group <?php echo form_error('slug') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_slug'), 'slug', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="slug" type="text" name="slug" class="span4" maxlength="100" value="<?php echo set_value('slug', isset($record['slug']) ? $record['slug'] : ''); ?>" <?php echo form_error('slug') ? 'title="'.form_error('slug').'"' : ''; ?>/>
                    </div>
                </div>
                
                <?php echo form_dropdown('manufacturer', $manufacturers, set_value('manufacturer', isset($record['manufacturer_id']) ? $record['manufacturer_id'] : ''), lang('ed_manufacturer'), 'id="manufacturer" class="nostyle"'); ?>
                
                <?php echo form_dropdown('status', array("1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value('status', isset($record['status']) ? $record['status'] : '1'), lang('ed_status'), 'id="status" class="nostyle"'); ?>
                    
            </div>
            <!--end general-->
            
            <div class="tab-pane fade subtab in" id="description-tab">                 
                <ul id="myTab1" class="nav nav-tabs">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <li <?php if($i==0){echo 'class="active"';} ?>><a href="#description-tab-<?php echo $cd; ?>" data-toggle="tab"><?php echo $l['name']; ?></a></li>
                    <?php $i++; endforeach; ?>
                </ul>   
                
                <div class="tab-content">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <div class="tab-pane fade in <?php if($i==0){echo 'active';} ?>" id="description-tab-<?php echo $cd; ?>">
                        
                        <div class="control-group <?php echo form_error("info[{$c}][short_description]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_short_description'), "info_{$c}_short_description", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <textarea id="info_<?php echo $c; ?>_short_description" name="info[<?php echo $c; ?>][short_description]" class="span8" rows="5" <?php echo form_error("info[{$c}][short_description]") ? 'title="'.form_error("info[{$c}][short_description]").'"' : ''; ?>><?php echo set_value("info[{$c}][short_description]", isset($info[$c]['short_description']) ? $info[$c]['short_description'] : ''); ?></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group <?php echo form_error("info[{$c}][description]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_description'), "info_{$c}_description", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <textarea id="info_<?php echo $c; ?>_description" name="info[<?php echo $c; ?>][description]" class="span8" rows="10" <?php echo form_error("info[{$c}][description]") ? 'title="'.form_error("info[{$c}][description]").'"' : ''; ?>><?php echo set_value("info[{$c}][description]", isset($info[$c]['description']) ? $info[$c]['description'] : ''); ?></textarea>
                            </div>
                        </div> 
                        
                        <div class="control-group <?php echo form_error("info[{$c}][keywords]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_keywords'), "info_{$c}_keywords", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <textarea id="info_<?php echo $c; ?>_keywords" name="info[<?php echo $c; ?>][keywords]" class="span8" rows="4" <?php echo form_error("info[{$c}][keywords]") ? 'title="'.form_error("info[{$c}][keywords]").'"' : ''; ?>><?php echo set_value("info[{$c}][keywords]", isset($info[$c]['keywords']) ? $info[$c]['keywords'] : ''); ?></textarea>
                            </div>
                        </div>
                        
                        <!--<div class="control-group <?php echo form_error("info[{$c}][tags]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_tags'), "info_{$c}_tags", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <input id="info_<?php echo $c; ?>_tags" type="text" name="info[<?php echo $c; ?>][tags]" class="span8" maxlength="255" value="<?php echo set_value("info[{$c}][tags]", isset($info[$c]['tags']) ? $info[$c]['tags'] : ''); ?>" <?php echo form_error("info[{$c}][tags]") ? 'title="'.form_error("info[{$c}][tags]").'"' : ''; ?>/>
                                <span class="help-block noTopMargin"><?php echo lang('ed_comma_seperated'); ?></span>
                            </div>
                        </div>-->
                        
                    </div> 
                    <?php $i++; endforeach; ?>
                </div>
            </div>
            <!--end attribute-->
            
            <div class="tab-pane fade in" id="categories-tab">
                <input type="hidden" id="categories" name="categories" value="<?php echo $category_ids; ?>"/>
                <div id="tree">
                </div>
            </div>
            
            <div class="tab-pane fade in" id="options-tab">    
                <input type="hidden" id="removed_options" name="options[removed]" value="<?php echo isset($removed)?$removed:''; ?>"/>
                             
                <div class="responsive">
                    <table class="table table-bordered" id="option_table" style="min-width:720px;">
                    <colgroup>
                        <col width="200px"/>
                        <col width=""/>
                        <col width="200px"/> 
                        <col width="120px"/> 
                        <col width="120px"/> 
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="text-center"><?php echo lang('ed_option_name').lang('bf_form_label_required'); ?></th>
                            <th class="text-center"><?php echo lang('ed_option_values').lang('bf_form_label_required'); ?></th>
                            <th class="text-center"><?php echo lang('ed_sort_order').lang('bf_form_label_required'); ?></th>
                            <th class="text-center"><?php echo lang('ed_required'); ?></th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($product_options) && !empty($product_options) && count($product_options) > 0) :?>
                        <?php foreach($product_options as $option) : ?>
                        <tr>
                            <td class="text-center <?php echo form_error("options[rows][{$option['option_id']}][name]")?'error':''; ?>">
                                <input type="text" name="options[rows][<?php echo $option['option_id']; ?>][name]" value="<?php echo $option['name']; ?>" class="option_name" style="width:90%;" <?php echo form_error("options[rows][{$option['option_id']}][name]") ? 'title="'.form_error("options[rows][{$option['option_id']}][name]").'"' : ''; ?>/>
                            </td>
                            <td class="text-center <?php echo form_error("options[rows][{$option['option_id']}][values]")?'error':''; ?>">
                                <input type="text" name="options[rows][<?php echo $option['option_id']; ?>][values]" value="<?php echo $option['values']; ?>" class="option_values" style="width:90%;" <?php echo form_error("options[rows][{$option['option_id']}][values]") ? 'title="'.form_error("options[rows][{$option['option_id']}][values]").'"' : ''; ?>/>
                            </td>
                            <td class="text-center <?php echo form_error("options[rows][{$option['option_id']}][sort_order]")?'error':''; ?>">
                                <input type="text" name="options[rows][<?php echo $option['option_id']; ?>][sort_order]" value="<?php echo $option['sort_order']; ?>" class="sort_order" style="width:90%;" <?php echo form_error("options[rows][{$option['option_id']}][sort_order]") ? 'title="'.form_error("options[rows][{$option['option_id']}][sort_order]").'"' : ''; ?>/>
                            </td>
                            <td class="text-center"><input type="checkbox" name="options[rows][<?php echo $option['option_id']; ?>][required]" value="<?php echo $option['option_id']; ?>" class="required" <?php echo (isset($option['required']) && $option['required']==1)?'checked="checked"':''; ?>/></td>
                            <td class="text-center"><button type="button" name="remove" class="remove btn" value="<?php echo $option['option_id']; ?>"><?php echo lang('ed_remove'); ?></button></td>
                        </tr>
                        <?php endforeach; ?>
                        <?php endif; ?>
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center"><span class="help-block"><?php echo lang('option_value_note'); ?></span></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"><button id="add_new_option" type="button" class="btn"><?php echo lang('ed_add_option'); ?></button></td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
            <!--end option-->
            
            <div class="tab-pane fade" id="images-tab">
                <div class="responsive">
                    <table class="table table-bordered" id="image_table" style="min-width:1000px;">
                    <colgroup>
                        <col width="120px"/>
                        <col width=""/>
                        <col width="200px"/>
                        <col width="120"/>
                        <col width="120"/>
                        <col width="120"/>
                        <col width="120"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="text-center"><?php echo lang('ed_image'); ?></th>
                            <th class="text-center"><?php echo lang('ed_label'); ?></th>
                            <th class="text-center"><?php echo lang('ed_sort_order'); ?></th>
                            <th class="text-center"><?php echo lang('ed_thumbnail'); ?></th>
                            <th class="text-center"><?php echo lang('ed_default'); ?></th>
                            <th class="text-center"><?php echo lang('ed_exclude'); ?></th>
                            <th class="text-center"><?php echo lang('ed_remove'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center"><?php echo lang('ed_no_image'); ?></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"><input type="radio" name="images[is_thumbnail]" value="" checked="checked"/></td>
                            <td class="text-center"><input type="radio" name="images[is_default]" value="" checked="checked"/></td>
                            <td class="text-center"><input type="checkbox"/></td>
                            <td class="text-center"><input type="checkbox"/></td>
                        </tr>
                        <?php if(isset($product_images) && !empty($product_images) && count($product_images) > 0) :?>
                        <?php foreach($product_images as $img) : ?>
                        <tr>
                            <td class="text-center">
                                <img src="<?php echo media_file($img['image'], !isset($img['tmp'])?'product':'tmp/product', 100, 50); ?>" width="100" height="50">
                                <input type="hidden" name="images[rows][<?php echo $img['image_id']; ?>][image]" value="<?php echo $img['image']; ?>" class="image_name"/>
                            </td>
                            <td class="text-center"><input type="text" name="images[rows][<?php echo $img['image_id']; ?>][label]" value="<?php echo $img['label']; ?>" class="image_label" style="width:90%;"/></td>
                            <td class="text-center"><input type="text" name="images[rows][<?php echo $img['image_id']; ?>][sort_order]" value="<?php echo $img['sort_order']; ?>" class="sort_order" style="width:90%;"/></td>
                            <td class="text-center"><input type="radio" name="images[is_thumbnail]" value="<?php echo $img['image']; ?>" class="is_thumbnail" <?php echo ($img['thumbnail']==1)?'checked="checked"':''; ?>/></td>
                            <td class="text-center"><input type="radio" name="images[is_default]" value="<?php echo $img['image']; ?>" class="is_default" <?php echo ($img['default']==1)?'checked="checked"':''; ?>/></td>
                            <td class="text-center"><input type="checkbox" name="images[rows][<?php echo $img['image_id']; ?>][exclude]" value="<?php echo $img['image']; ?>" class="exclude" <?php echo ($img['exclude']==1)?'checked="checked"':''; ?>/></td>
                            <td class="text-center"><input type="checkbox" name="images[rows][<?php echo $img['image_id']; ?>][remove]" value="<?php echo $img['image']; ?>" class="remove"/></td>    
                        </tr>                       
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                    </table>
                </div>
                
                <br/>
                <input type="file" name="upload_image" id="upload_image" class="" value=""/>
                <span id="loading" class="help-inline"></span>
                <span id="errors" class="help-inline" style="color:#B94A48;"></span>
            </div>
            <!--end images-->
            
            <div class="tab-pane fade" id="samples-tab">
                <div class="responsive">
                    <table class="table table-bordered" id="sample_table" style="min-width:840px;">
                    <colgroup>
                        <col width="220px"/>
                        <col width=""/>
                        <col width="200px"/>
                        <col width="120"/>
                        <col width="120"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th class="text-center"><?php echo lang('ed_image'); ?></th>
                            <th class="text-center"><?php echo lang('ed_label'); ?></th>
                            <th class="text-center"><?php echo lang('ed_sort_order'); ?></th>
                            <th class="text-center"><?php echo lang('ed_exclude'); ?></th>
                            <th class="text-center"><?php echo lang('ed_remove'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($product_samples) && !empty($product_samples) && count($product_samples) > 0) :?>
                        <?php foreach($product_samples as $img) : ?>
                        <tr>
                            <td class="text-center">
                                <img src="<?php echo media_file($img['image'], !isset($img['tmp'])?'product':'tmp/product', 100, 50); ?>" width="100" height="50">
                                <input type="hidden" name="samples[<?php echo $img['sample_id']; ?>][image]" value="<?php echo $img['image']; ?>" class="image_name"/>
                            </td>
                            <td class="text-center"><input type="text" name="samples[<?php echo $img['sample_id']; ?>][label]" value="<?php echo $img['label']; ?>" class="image_label" style="width:90%;"/></td>
                            <td class="text-center"><input type="text" name="samples[<?php echo $img['sample_id']; ?>][sort_order]" value="<?php echo $img['sort_order']; ?>" class="sort_order" style="width:90%;"/></td>
                            <td class="text-center"><input type="checkbox" name="samples[<?php echo $img['sample_id']; ?>][exclude]" value="<?php echo $img['image']; ?>" class="exclude" <?php echo ($img['exclude']==1)?'checked="checked"':''; ?>/></td>
                            <td class="text-center"><input type="checkbox" name="samples[<?php echo $img['sample_id']; ?>][remove]" value="<?php echo $img['image']; ?>" class="remove"/></td>    
                        </tr>                       
                        <?php endforeach; ?>
                        <?php else: ?>
                        <tr id="empty-row-sample">
                            <td colspan="10"><?php echo lang('no_record'); ?></td>
                        </tr>
                        <?php endif; ?>
                    </tbody>
                    </table>
                </div>
                
                <br/>
                <input type="file" name="upload_image_sample" id="upload_image_sample" class="" value=""/>
                <span id="loading_sample" class="help-inline"></span>
                <span id="errors_sample" class="help-inline" style="color:#B94A48;"></span>
            </div>
            <!--end images-->
            
            <div class="tab-pane fade subtab" id="related-tab" loaded="false">
                <div id="related-products">
                </div>    
            </div>
            
            <div class="tab-pane fade subtab" id="meta-tab">
                <ul id="myTab1" class="nav nav-tabs">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <li <?php if($i==0){echo 'class="active"';} ?>><a href="#meta-tab-<?php echo $cd; ?>" data-toggle="tab"><?php echo $l['name']; ?></a></li>
                    <?php $i++; endforeach; ?>
                </ul>   
                
                <div class="tab-content">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <div class="tab-pane fade in <?php if($i==0){echo 'active';} ?>" id="meta-tab-<?php echo $cd; ?>">
                        <div class="control-group <?php echo form_error("info[{$c}][page_title]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_page_title'), "info_{$c}_page_title", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <input id="info_<?php echo $c; ?>_page_title" type="text" name="info[<?php echo $c; ?>][page_title]" class="span5" maxlength="255" value="<?php echo set_value("info[{$c}][page_title]", isset($info[$c]['page_title']) ? $info[$c]['page_title'] : ''); ?>" <?php echo form_error("info[{$c}][page_title]") ? 'title="'.form_error("info[{$c}][page_title]").'"' : ''; ?>/>
                            </div>
                        </div>
                        
                        <div class="control-group <?php echo form_error("info[{$c}][meta_keywords]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_meta_keywords'), "info_{$c}_meta_keywords", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <textarea id="info_<?php echo $c; ?>_meta_keywords" name="info[<?php echo $c; ?>][meta_keywords]" class="span5 limit" rows="4" <?php echo form_error("info[{$c}][meta_keywords]") ? 'title="'.form_error("info[{$c}][meta_keywords]").'"' : ''; ?>><?php echo set_value("info[{$c}][meta_keywords]", isset($info[$c]['meta_keywords']) ? $info[$c]['meta_keywords'] : ''); ?></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group <?php echo form_error("info[{$c}][meta_description]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_meta_description'), "info_{$c}_meta_description", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <textarea id="info_<?php echo $c; ?>_meta_description" name="info[<?php echo $c; ?>][meta_description]" class="span5" rows="5" <?php echo form_error("info[{$c}][meta_description]") ? 'title="'.form_error("info[{$c}][meta_description]").'"' : ''; ?>><?php echo set_value("info[{$c}][meta_description]", isset($info[$c]['meta_description']) ? $info[$c]['meta_description'] : ''); ?></textarea>
                            </div>
                        </div> 
                    </div> 
                    <?php $i++; endforeach; ?>
                </div>
            </div>
            <!--end meta-info-->
        </div>
    </div>
    
    <div class="form-buttons">
        <?php echo anchor($back_url, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>
        <button type="reset" name="reset" class="btn" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
        <?php if ($this->auth->has_permission('Products.Catalog.Delete')) : ?>
        <button type="submit" name="delete" class="btn" id="delete-me" onclick="return confirm('<?php echo lang('product_delete_confirm'); ?>')"><i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete'); ?></button>
        <?php endif; ?>        
        <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
        <button type="submit" name="save_continue" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save_continue'); ?></button>
    </div>
    <?php echo form_close(); ?>
</div>

<div class="hidden-forever"> 
    <table>
        <tr id="image_template_row">
            <td class="text-center">
                <img src="" width="100" height="50">
                <input type="hidden" name="images[rows][][image]" value="" class="image_name"/>
            </td>
            <td class="text-center"><input type="text" name="images[rows][][label]" value="" class="nostyle image_label" style="width:90%;"/></td>
            <td class="text-center"><input type="text" name="images[rows][][sort_order]" value="0" class="nostyle sort_order" style="width:90%;"/></td>
            <td class="text-center"><input type="radio" name="images[is_thumbnail]" value="" class="nostyle is_thumbnail"/></td>
            <td class="text-center"><input type="radio" name="images[is_default]" value="" class="nostyle is_default"/></td>
            <td class="text-center"><input type="checkbox" name="images[rows][][exclude]" value="" class="nostyle exclude"/></td>
            <td class="text-center"><input type="checkbox" name="images[rows][][remove]" value="" class="nostyle remove"/></td>    
        </tr>
    </table>
    
    <table>
        <tr id="sample_template_row">
            <td class="text-center">
                <img src="" width="100" height="50">
                <input type="hidden" name="samples[][image]" value="" class="image_name"/>
            </td>
            <td class="text-center"><input type="text" name="samples[][label]" value="" class="nostyle image_label" style="width:90%;"/></td>
            <td class="text-center"><input type="text" name="samples[][sort_order]" value="0" class="nostyle sort_order" style="width:90%;"/></td>
            <td class="text-center"><input type="checkbox" name="samples[][exclude]" value="" class="nostyle exclude"/></td>
            <td class="text-center"><input type="checkbox" name="samples[][remove]" value="" class="nostyle remove"/></td>    
        </tr>
    </table>
    
    <table>
        <tr id="option_template_row">
            <td class="text-center"><input type="text" name="options[rows][][name]" value="" class="option_name" style="width:90%;"/></td>
            <td class="text-center"><input type="text" name="options[rows][][values]" value="" class="option_values" style="width:90%;"/></td>
            <td class="text-center"><input type="text" name="options[rows][][sort_order]" value="0" class="sort_order" style="width:90%;"/></td>
            <td class="text-center"><input type="checkbox" name="options[rows][required]" value="1" class="nostyle required" checked="checked"/></td>
            <td class="text-center"><button type="button" name="remove" class="remove btn"><?php echo lang('ed_remove'); ?></button></td>
        </tr>
    </table>
</div>

<script>
var treeData = <?php echo json_encode($category_tree); ?>;
</script>