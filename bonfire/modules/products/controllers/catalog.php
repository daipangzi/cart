<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class catalog extends Admin_Controller {

	//--------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();
          
		$this->auth->restrict('Products.Catalog.View');
        
        //load models
		$this->load->model('products_model', null, true);
        $this->load->model('manufacturers/manufacturers_model', null, true);
        $this->load->model('categories/categories_model', null, true);
		$this->lang->load('products');
        
        //get manufacturer list
        $manufacturers = $this->manufacturers_model->dropdown_list(true);
        Template::set('manufacturers', $manufacturers);
        
        Assets::add_js('jquery.form.js');
        Assets::add_module_js('products', 'products.js');

        $config = init_upload_config('product');   
        $this->upload->initialize($config);
        
        // for tree
        Assets::add_css( array(
            plugin_path().'misc/tree2/ui.dynatree.css',
        )); 
         
        Assets::add_js( array(
            'jquery.form.js',
            plugin_path().'misc/tree2/jquery.dynatree.js',
        ));    
        //end for tree
        
        if(!$this->session->userdata('product_index'))
        {
            $this->session->set_userdata('product_index', site_url(SITE_AREA .'/catalog/products'));
        }
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->products_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('products_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('products_delete_failure') . $this->products_model->error, 'error');
				}
			}
		}
        
        //setup where conditions
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('product_search', $search);
        } else if($this->input->post('reset_search')) {
        } else {
            $search = $this->session->userdata('product_search')?$this->session->userdata('product_search'):array();
        }
        if(!empty($search['from_id']))  $where['products.product_id >= '] = $search['from_id'];
        if(!empty($search['to_id']))    $where['products.product_id <= '] = $search['to_id'];
        if(!empty($search['name']))     $where['name LIKE '] = "%" . $search['name'] . "%";
        if(!empty($search['sku']))      $where['sku LIKE '] = "%" . $search['sku'] . "%";
        if(!empty($search['from_price']))  $where['products.price >= '] = $search['from_price'];
        if(!empty($search['to_price']))    $where['products.price <= '] = $search['to_price'];
        if(!empty($search['quantity']))    $where['quantity']           = $search['quantity'];
        if(!empty($search['status']) && $search['status'] != -1)    $where['products.status'] = $search['status'];
        Template::set('search', $search);

        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);

        $orders  = array();
        switch($params['orderby']) {
            case "id":
                $orders["products.product_id"]  = $params['order'];
                break;
            case "sku":
            case "price":
            case "slug":
                $orders[$params['orderby']]     = $params['order'];
                break;
            case "name":
                $orders["name"]                 = $params['order'];
                break;
            case "quantity": 
                $orders["qty"]                  = $params['order'];
                break;
            default:
                $orders["products.product_id"]  = "desc";
                break;
        }
        
        //get total counts
        $total_records = $this->products_model
                ->where($where)
                ->count_all();

        //get all products
		$records = $this->products_model
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all(1);
        Template::set('records', $records);
       
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/catalog/products/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/catalog/products') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);
                
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('product_index', $url);
        
		Template::set('toolbar_title', lang('products_manage'));
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: create()

		Creates a Products object.
	*/
	public function create()
	{
		$this->auth->restrict('Products.Catalog.Create');

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			if ($insert_id = $this->save_products())
			{
				Template::set_message(lang('products_create_success'), 'success');
				
                if($this->input->post('save')) 
                {
                    $this->redirect_to_index();
                }
                else
                {
                    Template::redirect(SITE_AREA .'/catalog/products/edit/'.$insert_id);
                }
			}
			else
			{
				Template::set_message(lang('products_create_failure') . $this->products_model->error, 'error');
			}
		}
        Assets::add_js($this->load->view('catalog/tree_js', null, true), 'inline');
        
        //product images
        if(isset($this->input->post('images')['rows'])) // when save failed
        {
            $temp_images = $this->input->post('images')['rows'];
            $product_images = array();
            foreach($temp_images as $c => $img)
            {
                $img['image_id'] = $c;
                
                if(isset($img['thumbnail'])) $img['thumbnail'] = 1;
                else $img['thumbnail'] = 0;
                
                if(isset($img['default'])) $img['default'] = 1;
                else $img['default'] = 0;
                
                if(isset($img['exclude'])) $img['exclude'] = 1;
                else $img['exclude'] = 0;
                
                if(isset($img['remove'])) $img['remove'] = 1;
                else $img['remove'] = 0;
                
                if(!is_numeric($c)) $img['tmp'] = 1;
                
                $product_images[$c] = $img;
            }
            Template::set('product_images', $product_images);
        }
        
        //product options
        if(isset($this->input->post('options')['rows'])) // when save failed
        {
            $temp_options = $this->input->post('options')['rows'];
            $product_options = array();
            foreach($temp_options as $c => $o)
            {
                $o['option_id'] = $c;
                
                if(isset($o['required'])) $o['required'] = 1;
                else $o['required'] = 0;
                
                $product_options[] = $o;
            }
            Template::set('product_options', $product_options);
            Template::set('removed', $this->input->post('options')['removed']);
        }
        
        //product samples
        if($this->input->post('samples')) // when save failed
        {
            $temp_images = $this->input->post('samples');
            $product_samples = array();
            foreach($temp_images as $c => $img)
            {
                $img['sample_id'] = $c;
                
                if(isset($img['exclude'])) $img['exclude'] = 1;
                else $img['exclude'] = 0;
                
                if(isset($img['remove'])) $img['remove'] = 1;
                else $img['remove'] = 0;
                
                if(!is_numeric($c)) $img['tmp'] = 1;
                
                $product_samples[$c] = $img;
            }
            Template::set('product_samples', $product_samples);
        }
        
        $category_tree = $this->categories_model->get_tree_root(2);
        Template::set('category_tree', $category_tree);
        
        Template::set('prev_page', $this->session->userdata('product_index'));
        Template::set('toolbar_title', lang('products_create'));
        Template::set_view('products/catalog/form');
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Products data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('products_invalid_id'), 'error');
			$this->redirect_to_index();
		}

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			$this->auth->restrict('Products.Catalog.Edit');

            //check id and key
            $key = $this->input->post('key');
            if ($key != md5($id))
            {
                Template::set_message(lang('invalid_action'), 'error');
                $this->redirect_to_index();
            } 
            
			if ($this->save_products('update', $id))
			{
				Template::set_message(lang('products_edit_success'), 'success');
                
                if($this->input->post('save')) 
                {
                    $this->redirect_to_index();
                }
                else
                {
                    unset($_POST['options']);
                    unset($_POST['images']);
                    unset($_POST['samples']);
                }
			}
			else
			{
				Template::set_message(lang('products_edit_failure') . $this->products_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Products.Catalog.Delete');

            //check id and key
            $key = $this->input->post('key');
            if ($key != md5($id))
            {
                Template::set_message(lang('invalid_action'), 'error');
                $this->redirect_to_index();
            } 
            
			if ($this->products_model->delete($id))
			{
				Template::set_message(lang('products_delete_success'), 'success');

				$this->redirect_to_index();
			} else
			{
				Template::set_message(lang('products_delete_failure') . $this->products_model->error, 'error');
			}
		}
        Assets::add_js($this->load->view('catalog/tree_js', null, true), 'inline');
        
        // product main info
        $record = $this->products_model->find($id, 1);     
        if(!$record)
        {
            Template::set_message(lang('products_invalid_id'), 'error');
            $this->redirect_to_index();   
        }
        Template::set('record', $record);
        
        //product images
        if(isset($this->input->post('images')['rows'])) // when save failed
        {
            $temp_images = $this->input->post('images')['rows'];
            $product_images = array();
            foreach($temp_images as $c => $img)
            {
                $img['image_id'] = $c;
                
                if(isset($img['thumbnail'])) $img['thumbnail'] = 1;
                else $img['thumbnail'] = 0;
                
                if(isset($img['default'])) $img['default'] = 1;
                else $img['default'] = 0;
                
                if(isset($img['exclude'])) $img['exclude'] = 1;
                else $img['exclude'] = 0;
                
                if(isset($img['remove'])) $img['remove'] = 1;
                else $img['remove'] = 0;
                
                if(!is_numeric($c)) $img['tmp'] = 1;
                
                $product_images[$c] = $img;
            }
            Template::set('product_images', $product_images);
        }
        else
        {
            $product_images = $this->products_model->get_images($id, 1);
            Template::set('product_images', $product_images);
        }
        //end product images
        
        //product options
        if(isset($this->input->post('options')['rows'])) // when save failed
        {
            $temp_options = $this->input->post('options')['rows'];
            $product_options = array();
            foreach($temp_options as $c => $o)
            {
                $o['option_id'] = $c;
                
                if(isset($o['required'])) $o['required'] = 1;
                else $o['required'] = 0;
                
                $product_options[] = $o;
            }
            Template::set('product_options', $product_options);
            Template::set('removed', $this->input->post('options')['removed']);
        }
        else
        {
            $product_options= $this->products_model->get_options($id, 1);
            Template::set('product_options', $product_options);
        }
        //end product options
        
        //build category tree
        $categories = $this->products_model->get_categories($id);                     
        if(empty($categories))
        {
            $category_tree = $this->categories_model->get_tree_root(2);  
        }
        else
        {
            $category_tree = $this->categories_model->get_tree_from_ids($categories);            
        }        
        Template::set('categories', $categories);
        Template::set('category_tree', $category_tree);
        //end build category tree
        
        //product samples
        if($this->input->post('samples')) // when save failed
        {
            $temp_images = $this->input->post('samples');
            $product_samples = array();
            foreach($temp_images as $c => $img)
            {
                $img['sample_id'] = $c;
                
                if(isset($img['exclude'])) $img['exclude'] = 1;
                else $img['exclude'] = 0;
                
                if(isset($img['remove'])) $img['remove'] = 1;
                else $img['remove'] = 0;
                
                if(!is_numeric($c)) $img['tmp'] = 1;
                
                $product_images[$c] = $img;
            }
            Template::set('product_samples', $product_samples);
        }
        else
        {
            $product_samples = $this->products_model->get_samples($id, 1);
            Template::set('product_samples', $product_samples);
        }
        
        Template::set('prev_page', $this->session->userdata('product_index'));
		Template::set('toolbar_title', lang('products_edit'));
        Template::set_view('products/catalog/form');
		Template::render();
	}
    
    public function delete()
    {
        $id = $this->uri->segment(5);
        $key = $this->uri->segment(6);
        
        if (empty($id))
        {
            Template::set_message(lang('products_invalid_id'), 'error');
            $this->redirect_to_index();
        }
        
        if(md5($id) != $key)
        {
            Template::set_message(lang('invalid_action'), 'error');
            $this->redirect_to_index();
        }
        
        $this->auth->restrict('Products.Catalog.Delete');
        
        if ($this->products_model->delete($id))
        {
            Template::set_message(lang('products_delete_success'), 'success');

            $this->redirect_to_index();
        }
    }
    //--------------------------------------------------------------------

	//--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_products()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_products($type='insert', $id=0)
	{
        $_POST['price']         = floatval($this->input->post('price'));
        $_POST['special_price'] = floatval($this->input->post('special_price'));
        $_POST['qty']           = intval($this->input->post('qty'));
        $_POST['sku']           = preg_replace("/[^A-Za-z0-9]/","", $this->input->post('sku'));
        $_POST['slug']          = url_title(convert_accented_characters($this->input->post('slug')), 'dash', TRUE);
                                                                                                                                        
        //set rules for manufacturer attributes             
        $languages = $this->languages_model->find_all(1);
        foreach($languages as $l)
        {
            $c = $l['language_id'];
            
            $this->form_validation->set_rules("info[{$c}][name]", 'lang:ed_name', 'required|trim|max_length[255]|strip_tags|xss_clean');
            //$this->form_validation->set_rules("info[{$c}][tags]", 'lang:ed_tags', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][keywords]", 'lang:ed_keywords', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][short_description]", 'lang:ed_short_description', 'trim|strip_tags|xss_clean');
            //$this->form_validation->set_rules("info[{$c}][description]", 'lang:ed_description', 'trim');
            $this->form_validation->set_rules("info[{$c}][page_title]", 'lang:ed_page_title', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_keywords]", 'lang:ed_meta_keyword', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_description]", 'lang:ed_meta_description', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][note]", 'lang:ed_note', 'trim|max_length[255]|strip_tags|xss_clean');
        }
        
        if($this->input->post('sku') != $this->products_model->get_sku($id)) {
            $is_unique = '|is_unique[products.sku]';
        } else {
            $is_unique = '';
        }
        
        $this->form_validation->set_rules('sku', 'lang:ed_sku','required|trim|alpha_dash|max_length[30]|strip_tags|xss_clean'.$is_unique);
        $this->form_validation->set_rules('price', 'lang:ed_price','required|trim|is_numeric|max_length[20]|strip_tags|xss_clean');
        $this->form_validation->set_rules('special_price', 'lang:ed_special_price','required|trim|is_numeric|less_than['.$_POST['price'].']|max_length[20]|strip_tags|xss_clean');
        $this->form_validation->set_rules('slug', 'lang:ed_slug','trim|alpha_dash|max_length[100]|strip_tags|xss_clean');
        $this->form_validation->set_rules('status', 'lang:ed_status','required|trim|is_numeric|max_length[1]');
        
        //validate options
        if(isset($this->input->post('options')['rows']))
        {
            $options = $this->input->post('options')['rows'];
            foreach($options as $c => $o)
            {
                $this->form_validation->set_rules("options[rows][{$c}][name]", 'lang:ed_option_name', 'required|trim|max_length[255]|strip_tags|xss_clean');   
                $this->form_validation->set_rules("options[rows][{$c}][values]", 'lang:ed_option_values', 'required|trim|max_length[255]|strip_tags|xss_clean|alpha_numeric_extra');   
            }
        }

        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }
        
        // make sure we only pass in the fields we want
		$data = array();
        if($type != "insert") $data['product_id'] = $id;         
		$data['sku']            = $this->input->post('sku');
        $data['price']          = $this->input->post('price');
        $data['slug']           = $this->products_model->validate_slug($this->input->post('slug'), $id);;
        $data['qty']            = $this->input->post('qty');
        $data['status']         = $this->input->post('status')=="1"?1:0;
        
        if($this->input->post('special_price'))
        {
            $data['special_price']  = $this->input->post('special_price');
        }
        
        if($this->input->post('start_date'))
        {
            $data['start_date']     = $this->input->post('start_date');
        }
        
        if($this->input->post('end_date'))
        {
            $data['end_date']       = $this->input->post('end_date');
        }
        
        if($this->input->post('manufacturer'))
        {
            $data['manufacturer_id']= $this->input->post('manufacturer');
        }
        
        $langs  = $this->input->post('info');
        $options= $this->input->post('options');
        $images = $this->input->post('images');
        $samples = $this->input->post('samples');
        $related = $this->input->post('related');
        $categories = $this->input->post('categories');

        $this->products_model->controller_delegate($this);
		return $this->products_model->save($data, $langs, $options, $images, $categories, $samples, $related);
	}
    
    private function redirect_to_index()
    {
        $redirect_url = $this->session->userdata('product_index')?$this->session->userdata('product_index'):site_url(SITE_AREA .'/catalog/products');
        Template::redirect($redirect_url);
    }
	//--------------------------------------------------------------------
    
    //--------------------------------------------------------------------
    // !AJAX METHODS
    //--------------------------------------------------------------------
    public function upload_product_image() {
        $result = $this->upload_image('upload_image');
        echo json_encode($result);
        exit;          
    }
    
    public function upload_product_sample() {
        $result = $this->upload_image('upload_image_sample');
        echo json_encode($result);
        exit;          
    }

    public function related_products_form($offset=0) {
        $key = $this->input->post('key');
        $id  = $this->products_model->get_id_by_key($key);
        $related_product_ids = $this->products_model->get_related_product_ids($id);
        
        //build order
        $params['order']    = isset($_POST['order'])?$_POST['order']:'';
        $params['orderby']  = isset($_POST['orderby'])?$_POST['orderby']:'';
        
        $orders  = array();
        switch($params['orderby']) {
            case "id":
                $orders["products.product_id"]  = $params['order'];
                break;
            case "sku":
            case "price":
            case "slug":
                $orders[$params['orderby']]     = $params['order'];
                break;
            case "name":
                $orders["name"]                 = $params['order'];
                break;
            case "quantity": 
                $orders["qty"]                  = $params['order'];
                break;
            default:
                $orders["products.product_id"]  = "desc";
                break;
        }
        
        //build where sql
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('product_form_search', $search);
        } else {
            $search = $this->session->userdata('product_form_search')?$this->session->userdata('product_form_search'):array();
        }
        $visible = isset($search['visible'])?$search['visible']:0;
        if(!empty($search['from_id']))  $where['products.product_id >= '] = $search['from_id'];
        if(!empty($search['to_id']))    $where['products.product_id <= '] = $search['to_id'];
        if(!empty($search['name']))     $where['name LIKE '] = "%" . $search['name'] . "%";
        if(!empty($search['sku']))      $where['sku LIKE '] = "%" . $search['sku'] . "%";
        if(!empty($search['from_price']))  $where['products.price >= '] = $search['from_price'];
        if(!empty($search['to_price']))    $where['products.price <= '] = $search['to_price'];
        if(!empty($search['quantity']))    $where['quantity']           = $search['quantity'];
        if(!empty($search['status']) && $search['status'] != -1)    $where['products.status'] = $search['status'];
         
        //get total
        $this->_init_before_related_products($visible, $related_product_ids);
        $total_records = $this->products_model
                ->exclude_products($id)
                ->where($where)
                ->count_all();
                
        //get related products
        $this->_init_before_related_products($visible, $related_product_ids);
        $records = $this->products_model
                ->exclude_products($id)
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all(1);
        
        //setup pagination
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/catalog/products/related_products_form');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);
        
        $data['records'] = $records;
        $data['related'] = $related_product_ids;
        $data['search']  = isset($search)?$search:'';
        $data['params']  = $params;
        $data['offset']  = $offset;
        Template::block('related_form', 'products/catalog/related', $data);
        
        exit;
    }
    
    private function _init_before_related_products($visible, $related_product_ids) {
        switch($visible)
        {
            case '-1': break;
            case '1': 
                $this->products_model->exclude_products($related_product_ids);
                break;
            case '0':
            default: 
                $this->products_model->where_in('products.product_id', $related_product_ids);
                break;
        }
    }
}