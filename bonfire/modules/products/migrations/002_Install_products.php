<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_products extends Migration {

	public function up()
	{
		$prefix = $this->db->dbprefix;

		$fields = array(
			'product_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			'sku' => array(
				'type' => 'VARCHAR',
				'constraint' => 30,
				
			),
			'start_date' => array(
				'type' => 'DATE',
				'default' => '0000-00-00',
				
			),
			'end_date' => array(
				'type' => 'DATE',
				'default' => '0000-00-00',
				
			),
			'created_on' => array(
				'type' => 'datetime',
				'default' => '0000-00-00 00:00:00',
			),
			'modified_on' => array(
				'type' => 'datetime',
				'default' => '0000-00-00 00:00:00',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('product_id', true);
		$this->dbforge->create_table('products');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('products');

	}

	//--------------------------------------------------------------------

}