<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['products_manage']			= 'Products';
$lang['products_edit']              = 'Edit Product';
$lang['products_create']            = 'New Product';
$lang['products_list']              = 'Products';
$lang['products_create_success']			= 'Products successfully created.';
$lang['products_create_failure']			= 'There was a problem creating the products: ';
$lang['products_create_new_button']			= 'Create New Products';
$lang['products_invalid_id']			= 'Invalid Products ID.';
$lang['products_edit_success']			= 'Products successfully saved.';
$lang['products_edit_failure']			= 'There was a problem saving the products: ';
$lang['products_delete_success']			= 'record(s) successfully deleted.';
$lang['products_delete_failure']			= 'We could not delete the record: ';
$lang['products_delete_error']			    = 'You have not selected any records to delete.';
$lang['products_delete_confirm']			= 'Are you sure you want to delete this products?';
$lang['product_delete_confirm']             = 'Are you sure you want to delete this product?';

$lang['option_value_note']  = 'Values are sperated by comma';
$lang['product_new']        = 'New Product';