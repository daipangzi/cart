<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends BF_Model {

	protected $table		= "products";
	protected $key			= "product_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field  = "created_on";
	protected $modified_field = "modified_on";
    
    private $orderby    = array();
    private $is_admin   = false;
    private $controller_delegate = null;
    
    var $CI;
    
    function __construct() 
    {
        $this->CI =& get_instance();
        $this->CI->load->library('product');
        
        $this->is_admin = is_backend();
    }
    
    function controller_delegate($controller)
    {
        $this->controller_delegate = $controller;
    }
    
    function init_before_find_all()
    {
        //set orderby
        if(empty($this->orderby))
        {
            $this->order_by('products.product_id');
        }
        else
        {
            $this->order_by($this->orderby);
        }
        
        //set filter
        if(!$this->is_admin)
        {
            $this->where('status', 1);
        }
        
        return $this;    
    }
    
    /**
    * save product and language info
    * 
    * @param mixed $data
    * @param mixed $langs
    */
    function save($data, $langs, $options, $images, $categories, $samples, $related) {
        $id = FALSE;
        if(!isset($data[$this->key]))
        {
            $id = $this->insert($data);
        }
        else  
        {
            $id = $data[$this->key];
            
            $this->update($id, $data);
        }
        
        if($id !== FALSE)
        {                             
            $this->save_lang_info($id, $langs);     
            $this->save_options($id, $options);     
            $this->save_images($id, $images);     
            $this->save_samples($id, $samples);     
            $this->save_related_products($id, $related);     
            $this->save_categories($id, $categories);    
            
            //remove temp image folder          
            $this->load->helper('file');
            $temp_path = MEDIAPATH . '/tmp/product';
            delete_files($temp_path);    
        }
        
        return $id;
    }
    
    
    //--------------------------------------------------------------------
    // !Inherite Parent Methods
    //--------------------------------------------------------------------
    
    /**
    * delete manufacturer and language info
    * 
    * @param mixed $manufacturer_id
    * @return bool
    */
    function delete($id) {
        // delete accessories
        $this->db->where('product_id', $id)->delete('products_info');
        $this->db->where('product_id', $id)->delete('products_options');
        $this->db->where('product_id', $id)->delete('products_images');
        $this->db->where('product_id', $id)->delete('products_samples');
        $this->db->where('product_id', $id)->delete('products_related');
        $this->db->where('product_id', $id)->delete('categories_products');
        
        return parent::delete($id);        
    }
    
    /**
    * count all records 
    * 
    */
    function count_all()
    {
        $language_id = global_language_id();
        
        $this->init_before_find_all()
            ->join('products_info pi', "pi.product_id=products.product_id AND pi.language_id={$language_id}", 'left');
        
        return parent::count_all();
    }
    
    /**
    * find all records with current language
    * 
    * @param mixed $return_type: 0:object array, 1:general array, 2:product object array
    */
    function find_all($return_type = 0)
    {
        $temp_return = $return_type>=2?0:$return_type;
        $language_id = global_language_id();
        
        $this->init_before_find_all()
            ->join('products_info pi', "pi.product_id=products.product_id AND pi.language_id={$language_id}", 'left');
        $records = parent::find_all($temp_return);
                              
        if(empty($records))
        {
            return FALSE;
        }
        
        $result = array();
        foreach($records as $r)
        {
            if($return_type == 0)
            {                        
                $pid = $r->product_id;
                $r->options    = $this->get_options($pid, $return_type);
                $r->image      = $this->get_default_image($pid);
                $r->thumbnail  = $this->get_thumbnail_image($pid);
                
                $result[] = $r;
            }
            else if($return_type == 1)
            {
                $pid = $r['product_id'];
                $r['options']   = $this->get_options($pid, $return_type);
                $r['image']     = $this->get_default_image($pid);
                $r['thumbnail'] = $this->get_thumbnail_image($pid);
                
                $result[] = $r;
            }
            else
            {
                $pid = $r->product_id;
                $r->options    = $this->get_options($pid);
                $r->image      = $this->get_default_image($pid);
                $r->thumbnail  = $this->get_thumbnail_image($pid);
                
                $result[] = new Product($r);
            }    
        }        

        return $result;
    }
    
    /**
    * find product with related info
    * 
    * @param mixed $id
    * @param mixed $return_type: 0:object array, 1:general array, 2:product object array
    */
    function find($id, $return_type=0)
    {
        $temp_return = $return_type>=2?0:$return_type;
        
        $record = parent::find($id, $temp_return);
        //if cann't find record
        if(empty($record)) 
        {
            return FALSE;
        }
        
        if($this->is_admin)
        {
            if($return_type == 0)
            {
                $record->info = $this->get_all_language_info($id);
            }
            else
            {
                $record['info'] = $this->get_all_language_info($id);
            }
        }
        else
        {
            //if status disabled
            $temp = (array)$record;
            if($temp['status'] == 0)
            {
                return FALSE;
            }
            
            if($return_type == 0)
            {            
                $record  = (object)array_merge((array)$record, $this->get_language_info($id, global_language_id()));
                $record->options    = $this->get_options($id, $return_type);
                $record->image      = $this->get_default_image($id);
                $record->thumbnail  = $this->get_thumbnail_image($id);
            }
            else if($return_type == 1)
            {
                $record  = array_merge($record, $this->get_language_info($id, global_language_id()));
                $record['options']    = $this->get_options($id, $return_type);
                $record['image']      = $this->get_default_image($id);
                $record['thumbnail']  = $this->get_thumbnail_image($id);
            }
            else
            {
                //$record->info = $this->get_all_language_info($id);
                $record->options    = $this->get_options($id);
                $record->image      = $this->get_default_image($id);
                $record->thumbnail  = $this->get_thumbnail_image($id);
                $record = (object)array_merge((array)$record, $this->get_language_info($id, global_language_id()));
                
                $record = new Product($record);
            }   
        }
          
        return $record;
    }
    
    /**
    * find manufacturer by slug
    * 
    * @param mixed $slug
    */
    function find_by_slug($slug, $return_type=0)
    {
        $record = parent::find_by('slug', $slug);
        
        if(empty($record)) 
        {
            return FALSE;
        }
        
        return $this->find($record->product_id, $return_type);
    }
    
    //--------------------------------------------------------------------
    // !Related Products Methods
    //--------------------------------------------------------------------
    private function save_related_products($id, $related)
    {
        if(empty($related)) return;
        
        $ids_in = $related['ids'];
        $checked = isset($related['check'])?$related['check']:array();
        
        //delete items first
        $this->db->where_in('related_product_id', $ids_in)
                ->where('product_id', $id)
                ->delete('products_related');
            
        foreach($checked as $rid)
        {
            if(in_array($rid, $ids_in))
            {
                $data = array();
                $data['product_id'] = $id;    
                $data['related_product_id'] = $rid;
                $this->db->insert('products_related', $data);
            }                                       
        }
    }
    
    function get_related_products($id, $return_type=0)
    {
        $related_ids = $this->get_related_product_ids($id);
        return $this->where_in('products.product_id', $related_ids)->find_all($return_type);
    }
    
    function get_related_product_ids($id)
    {
        $records = $this->db
                    ->where('product_id', $id)
                    ->get('products_related')
                    ->result_array();
        
        if(empty($records))
        {
            return array('');
        }
        
        return array_column($records, 'related_product_id');
    }
    
    //--------------------------------------------------------------------
    // !Common Methods
    //--------------------------------------------------------------------
    
    /**
    * check if the slug is exist
    * 
    * @param mixed $slug
    * @param mixed $id
    * @return mixed
    */
    private function check_slug($slug, $id=false)
    {
        if($id)
        {
            $this->where('product_id !=', $id);
        }
        $this->where('slug', $slug);
        
        return (bool) parent::count_all();
    }
    
    /**
    * check and valid slug for product
    * 
    * @param mixed $slug
    * @param mixed $id
    * @param mixed $count
    */
    function validate_slug($slug, $id=false, $count=false)
    {
        if($slug == '') return '';
        
        if($this->check_slug($slug.$count, $id))
        {
            if(!$count)
            {
                $count = 1;
            }
            else
            {
                $count++;
            }
            return $this->validate_slug($slug, $id, $count);
        }
        else
        {
            return $slug.$count;
        }
    }
    
    function get_sku($id)
    {
        if($id == 0) return '';
        
        $record = parent::find($id, 1);
        if(empty($record)) return '';
        
        return $record['sku'];
    }
    
    function is_exist($id)
    {
        $count = $this->init_before_find_all()->where('products.product_id', $id)->count_all();

        if($count > 0) return TRUE;
        else return FALSE;
    }
    
    function get_name($id)
    {
        $record = $this->get_language_info($id, global_language_id());
    }
    
    function get_names($id)
    {
        $records = $this->get_all_language_info($id);
        return $records;
    }
    
    function get_id_by_key($key)
    {
        $record = $this->find_by('MD5(product_id)', $key);
        if(empty($record))
        {
            return 0;
        }
        
        return $record->product_id;
    }
    
    //--------------------------------------------------------------------
    // !Lanuage Methods
    //--------------------------------------------------------------------
    
    /**
    * check if row is exist with category and lanuage id
    * 
    * @param mixed $id
    * @param mixed $language_id
    * @return mixed
    */
    private function check_language_row($id, $language_id)
    {
        $where = array();
        $where['product_id']   = $id;
        $where['language_id']  = $language_id;
        
        return (bool)$this->db->where($where)->count_all_results('products_info');
    }  
    
    /**
    * get all lanuage info
    * 
    * @param mixed $category_id
    */
    private function get_all_language_info($id)
    {
        $languages    = $this->CI->languages_model->find_all(1);
        
        $result = array();
        foreach($languages as $lang)
        {
            $l = $lang['language_id'];
            
            $record = $this->get_language_info($id, $l);
            
            $result[$l] = $record;
        }
        
        return $result;
    } 
    
    /**
    * get one lanuage info
    * 
    * @param mixed $category_id
    * @param mixed $language_id
    */
    private function get_language_info($id, $language_id)
    {
        $default_lang = get_language_id_by_code(settings_item('site.default_language'));   
        
        $record = $this->db
                    ->where(array('product_id' => $id, 'language_id' => $language_id))
                    ->get('products_info')
                    ->row_array();
         
        if(empty($record))
        {
            $record = $this->db
                    ->where(array('product_id' => $id, 'language_id' => $default_lang))
                    ->get('products_info')
                    ->row_array();
        } 
        
        return $record;
    }
    
    /**
    * save language info for product
    * 
    * @param mixed $id
    */
    private function save_lang_info($id, $langs)
    {
        foreach($langs as $code => $row)
        {
            //reset tags
            if(isset($row['tags']))
            {
                $temp = explode(',', $row['tags']);
                $tags = array();
                if(empty($tags))
                {
                    foreach($temp as $tag)
                    {
                        if(trim($tag) != '') {
                            $tags[] = trim($tag);
                        }
                    }
                }
            }
            
            $info = array();
            $info['product_id']     = $id;
            $info['language_id']    = $code;
            $info['name']           = $row['name'];
            //$info['tags']           = implode(',', $tags);
            $info['keywords']       = $row['keywords'];
            $info['description']    = $row['description'];
            $info['page_title']     = $row['page_title'];
            $info['meta_keywords']  = $row['meta_keywords'];
            $info['short_description']= $row['short_description'];
            $info['meta_description'] = $row['meta_description'];
            
            if(!$this->check_language_row($id, $code))
            {
                $this->db->insert('products_info', $info);
            }
            else
            {
                $where = array();
                $where['product_id']    = $id;
                $where['language_id']   = $code;
                
                $this->db->where($where)->update('products_info', $info);
            }
        }     
    }
    
    //--------------------------------------------------------------------
    // !Option Methods
    //--------------------------------------------------------------------
    
    /**
    * save options
    * 
    * @param mixed $id
    * @param mixed $options
    */
    private function save_options($id, $options)
    {
        if(empty($options['rows'])) return;
        
        $removed = explode(',', $options['removed']);
        $rows    = $options['rows'];
        
        foreach($removed as $r)
        {
            $this->db->where('option_id', $r)->delete('products_options');
        }
        
        foreach($rows as $code => $row)
        {
            //reset options values
            $temp_values = explode(',', $row['values']);
            $values = array();
            foreach($temp_values as $v)
            {
                if(trim($v) == '') continue;
                $values[] = trim($v);
            }
            
            $info = array();
            $info['product_id']  = $id;
            $info['name']        = $row['name'];
            $info['values']      = implode(',', $values);
            $info['sort_order']  = is_numeric($row['sort_order'])?$row['sort_order']:1;
            $info['required']    = isset($row['required'])?1:0;
            
            if(!is_numeric($code)) {
                if($code = $this->option_exist($row['name']))
                {
                    $where = array();
                    $where['option_id'] = $code;
                
                    $this->db->where($where)->update('products_options', $info);    
                }
                else
                {
                    $this->db->insert('products_options', $info);
                }
            } else {
                $where = array();
                $where['option_id'] = $code;
                
                $this->db->where($where)->update('products_options', $info);
            }
        }     
    }
    
    /**
    * get options of product
    * 
    * @param mixed $id
    * @param mixed $images
    */
    function get_options($pid, $return_type=0)
    {
        $record = array();
        if($return_type == 0)
        {
            $record = $this->db
                ->where(array('product_id' => $pid))
                ->order_by('sort_order')
                ->get('products_options')
                ->result();
        }
        else
        {
            $record = $this->db
                ->where(array('product_id' => $pid))
                ->order_by('sort_order')
                ->get('products_options')
                ->result_array();    
        }
        
        if($record)
        {
            return array_column($record, array('option_id', 'product_id', 'name', 'values', 'sort_order', 'required'), 'option_id');
        }
        
        return FALSE;
    }
    
    function option_exist($name)
    {
        $record = $this->db
            ->where('LOWER(name) =', strtolower($name))
            ->get('products_options')
            ->row_array();
          
        if(!empty($record))
        {
            return $record['option_id'];
        }
        
        return FALSE;
    }
    
    //--------------------------------------------------------------------
    // !Image Methods
    //--------------------------------------------------------------------
    
    /**
    * save images for product
    * 
    * @param mixed $id
    * @param mixed $images
    */
    private function save_images($id, $images)
    {
        if(empty($images['rows'])) return;
        
        $is_default   = $images['is_default'];
        $is_thumbnail = $images['is_thumbnail'];
        $rows         = $images['rows'];
          
        foreach($rows as $code => $row)
        {
            if($row['image'] == '') continue;
            if(isset($row['removed']) && !is_numeric($code)) continue;

            $info = array();
            $info['product_id'] = $id;            
            $info['label']      = $row['label'];
            $info['sort_order'] = $row['sort_order'];
            $info['thumbnail']  = ($row['image']==$is_thumbnail)?1:0;
            $info['default']    = ($row['image']==$is_default)?1:0;
            $info['exclude']    = isset($row['exclude'])?1:0;
            
            if(isset($row['remove']))
            {
                $this->db->where('image_id', $code)->delete('products_images');
            }
            else
            {
                if(!is_numeric($code)) {
                    //move images before save
                    $moved_file_name = $this->controller_delegate->get_moved_image_name($row['image'], 'product', false);                                
                    $info['image']      = $moved_file_name;
                    
                    $this->db->insert('products_images', $info);
                } else {
                    $where = array();
                    $where['image_id'] = $code;
                    
                    $this->db->where($where)->update('products_images', $info);
                }
            }
        }     
    }
    
    /** 
    * get images of product
    * 
    * @param mixed $id
    */
    function get_images($pid, $return_type=0)
    {
        $where = array();
        $where['product_id']= $pid;
        $where['exclude']   = 0;
        
        if($return_type == 0)
        {
            return $this->db
                ->where($where)
                ->order_by('sort_order')
                ->get('products_images')
                ->result();  
        }
        else
        {
            return $this->db
                ->where($where)
                ->order_by('sort_order')
                ->get('products_images')
                ->result_array();     
        }
    }
    
    function get_default_image($pid)
    {
        $record = $this->db
            ->where(array('product_id' => $pid, 'default' => 1))
            ->get('products_images')
            ->row_array();
        
        if(empty($record))
        {
            return '';
        }
        
        return $record['image'];
    }
    
    function get_thumbnail_image($pid)
    {
        $record = $this->db
            ->where(array('product_id' => $pid, 'thumbnail' => 1))
            ->get('products_images')
            ->row_array();
        
        if(empty($record))
        {
            return '';
        }
        
        return $record['image'];
    }
    
    //--------------------------------------------------------------------
    // !Sample Methods
    //--------------------------------------------------------------------
    
    /**
    * save samples for product
    * 
    * @param mixed $id
    * @param mixed $sample images
    */
    private function save_samples($id, $images)
    {
        if(!is_array($images)) return;

        foreach($images as $code => $row)
        {
            if($row['image'] == '') continue;
            if(isset($row['removed']) && !is_numeric($code)) continue;

            $info = array();
            $info['product_id'] = $id;            
            $info['label']      = $row['label'];
            $info['sort_order'] = $row['sort_order'];
            $info['exclude']    = isset($row['exclude'])?1:0;
            
            if(isset($row['remove']))
            {
                $this->db->where('sample_id', $code)->delete('products_samples');
            }
            else
            {
                if(!is_numeric($code)) {
                    //move images before save
                    $moved_file_name = $this->controller_delegate->get_moved_image_name($row['image'], 'product', false);                                
                    $info['image']      = $moved_file_name;
                    
                    $this->db->insert('products_samples', $info);
                } else {
                    $where = array();
                    $where['sample_id'] = $code;
                    
                    $this->db->where($where)->update('products_samples', $info);
                }
            }
        }   
    }
    
    /** 
    * get images of product
    * 
    * @param mixed $id
    */
    function get_samples($pid, $return_type=0)
    {
        if($return_type == 0)
        {
            return $this->db
                ->where(array('product_id' => $pid))
                ->order_by('sort_order')
                ->get('products_samples')
                ->result();  
        }
        else
        {
            return $this->db
                ->where(array('product_id' => $pid))
                ->order_by('sort_order')
                ->get('products_samples')
                ->result_array();     
        }
    }
    
    //--------------------------------------------------------------------
    // !Category Methods
    //--------------------------------------------------------------------
    
    /**
    * get product's categories
    * 
    * @param mixed $id
    */
    function get_categories($id)
    {
        $records = $this->db
                ->where(array('product_id' => $id))
                ->get('categories_products')
                ->result_array();
        return array_column($records, 'category_id');
    }
    
    /**
    * save product categories
    * 
    * @param mixed $id
    * @param mixed $options
    */
    private function save_categories($id, $categories)
    {
        $categories = explode(',', $categories);
                                    
        if(empty($categories)) return;
        
        //delete previous categories before save
        $this->db->where('product_id', $id)->delete('categories_products');
        
        //resave categories
        foreach($categories as $cid)
        {
            if(trim($cid) == '') continue;
            
            $info = array();
            $info['product_id'] = $id;            
            $info['category_id']= $cid;
            
            $this->db->insert('categories_products', $info);    
        }
    }
    
    //--------------------------------------------------------------------
    // !ORDER METHODS
    //--------------------------------------------------------------------
    function order_by_name($order='asc')
    {
        $this->orderby['name'] = $order;
        return $this;
    }
    
    function order_by_price($order='asc')
    {
        $this->orderby['sales_price'] = $order;
        return $this;
    }
    
    function order_by_position($order='asc')
    {
        $this->orderby['products.product_id'] = $order;
        return $this;
    }
    
    function order_by_saled($order='desc')
    {
        $this->orderby['products.ordered'] = $order;
        return $this;
    }
    
    function order_by_start_date($order='asc')
    {
        $this->orderby['start_date'] = $order;
        return $this;
    }
    
    function order_by_end_date($order='asc')
    {
        $this->orderby['end_date'] = $order;
        return $this;
    }
    
    function order_by_rand($order='asc')
    {
        $this->orderby['RAND()'] = $order;
        return $this;
    }
    
    //--------------------------------------------------------------------
    // !FILTER METHODS
    //--------------------------------------------------------------------
    
    /**
    * set flag if backend or frontend
    * 
    */
    function is_admin()
    {
        $this->is_admin = TRUE;   
        return $this; 
    }
    
    function filter_status($status=1)
    {
        $this->where('status', $status);
        return $this;
    }
    
    function filter_category($category_id=FALSE)
    {
        if($category_id != FALSE)
        {
            $this->join('categories_products cp', 'cp.product_id=products.product_id', 'left')
                ->where('cp.category_id', $category_id);
        }
        
        return $this;
    }
    
    function filter_manufacturer($manufacturer=FALSE)
    {
        if($manufacturer != FALSE)
        {
            $this->where('products.manufacturer_id', $manufacturer);
        }
        return $this;
    }
    
    function filter_date($start_date=FALSE, $end_date=FALSE)
    {
        if($start_date != FALSE)
        {
            $this->where('start_date >=', $start_date);
        }
        
        if($end_date != FALSE)
        {
            $this->where('end_date <=', $end_date);
        }
        
        return $this;
    }
    
    /**
    * exclude products in id array when get product list 
    * 
    * @param mixed $product_ids
    */
    function exclude_products($product_ids)
    {
        if(!is_array($product_ids))
        {
            $temp = $product_ids;
            
            $product_ids = array();
            $product_ids[] = $temp;
        }
        
        foreach($product_ids as $id)
        {
            $this->where('products.product_id !=', $id);
        }
        
        return $this;
    }
}
