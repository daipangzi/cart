//////////////////////////////////////////////////////////////////////////
//setup search box change action on index page
//////////////////////////////////////////////////////////////////////////
applySearchRow('list-form', admin_path + "/catalog/products");

//////////////////////////////////////////////////////////////////////////
//select tab again after submit
////////////////////////////////////////////////////////////////////////// 
selected_tab = $('#selected_tab').val();
if(selected_tab != '')
{
    $('a[href="'+selected_tab+'"]').trigger('click');
    if(selected_tab == '#related-tab')
    {
        $('#related-tab').attr('loaded', 'true');
        
        //get related product list by ajax
        last_offset = $('#last_offset').val();
        get_related_product_list_form(admin_path+'/catalog/products/related_products_form/'+last_offset);        
    }
}

$('a[data-toggle="tab"]').on('show', function (e) {
    href = $(this).attr('href');
    $('#selected_tab').val(href);
    
    if(href == '#related-tab' && $('#related-tab').attr('loaded') == 'false')
    {
        $('#related-tab').attr('loaded', 'true');
        
        //get related product list by ajax
        last_offset = $('#last_offset').val();
        get_related_product_list_form(admin_path+'/catalog/products/related_products_form/'+last_offset);        
    }
});
   
//////////////////////////////////////////////////////////////////////////
//setup upload product images
//////////////////////////////////////////////////////////////////////////
event = $.browser.msie?'click':'change';
form = $("#product_form");

$('#upload_image').live(event, function() { 
    $("#errors").html('');
    $("#loading").html('<img src="'+theme_path+'/admin/images/loaders/circular/054.gif" alt="Uploading...."/>');
    
    form.ajaxForm({
        url: admin_path+'/catalog/products/upload_product_image',
        success:  function(responseText) {
            $("#loading").html("");
    
            json = $.parseJSON(responseText);
            if(json.image) {    
                $("#errors").html("");
                
                temp_image_id= generate_temp_id(10);
                
                newRow = $("#image_template_row").clone().removeAttr("id").appendTo("#image_table");
                $("input", newRow).uniform();        
                $("img", newRow).attr("src", base_path+"images/"+json.image+"?type=tmp/product&width=120&height=50&force=yes");
                
                $(".image_name", newRow).val(json.image).attr('name', 'images[rows]['+temp_image_id+'][image]');
                $(".image_label", newRow).attr('name', 'images[rows]['+temp_image_id+'][label]');
                $(".sort_order", newRow).val(getMaxSortOrder('image_table')).attr('name', 'images[rows]['+temp_image_id+'][sort_order]');
                $(".is_thumbnail", newRow).val(json.image);
                $(".is_default", newRow).val(json.image);
                $(".exclude", newRow).val(json.image).attr('name', 'iamges[rows]['+temp_image_id+'][exclude]');
                $(".remove", newRow).val(json.image).attr('name', 'images[rows]['+temp_image_id+'][remove]');
            } else {
                $("#errors").html(json.errors);
            }
            $('#product_form').unbind('submit'); 
        },
    }).submit();
}); 
    
//////////////////////////////////////////////////////////////////////////
//setup upload product sample images
//////////////////////////////////////////////////////////////////////////
$('#upload_image_sample').live(event, function() { 
    $("#errors_sample").html('');
    $("#loading_sample").html('<img src="'+theme_path+'/admin/images/loaders/circular/054.gif" alt="Uploading...."/>');
    
    form.ajaxForm({
        url: admin_path+'/catalog/products/upload_product_sample',
        success:  function(responseText) {
            $("#loading_sample").html("");
    
            json = $.parseJSON(responseText);
            if(json.image) {    
                $('#empty-row-sample').remove();
                
                $("#errors_sample").html("");
                
                temp_image_id= generate_temp_id(10);
                
                newRow = $("#sample_template_row").clone().removeAttr("id").appendTo("#sample_table");
                $("input", newRow).uniform();        
                $("img", newRow).attr("src", base_path+"images/"+json.image+"?type=tmp/product&width=200&height=100&force=yes");
                
                $(".image_name", newRow).val(json.image).attr('name', 'samples['+temp_image_id+'][image]');
                $(".image_label", newRow).attr('name', 'samples['+temp_image_id+'][label]');
                $(".sort_order", newRow).val(getMaxSortOrder('sample_table')).attr('name', 'samples['+temp_image_id+'][sort_order]');
                $(".exclude", newRow).val(json.image).attr('name', 'samples['+temp_image_id+'][exclude]');
                $(".remove", newRow).val(json.image).attr('name', 'samples['+temp_image_id+'][remove]');
            } else {
                $("#errors_sample").html(json.errors);
            }
            
            form.unbind('submit');     
        },
    }).submit();
}); 
   
//////////////////////////////////////////////////////////////////////////
//setup adding option row on options tab
////////////////////////////////////////////////////////////////////////// 
$('#add_new_option').click(function() {
    temp_option_id= generate_temp_id(10);
    newRow = $('#option_template_row').clone().removeAttr('id');
    $(".option_name", newRow).attr('name', 'options[rows]['+temp_option_id+'][name]');;
    $(".option_values", newRow).attr('name', 'options[rows]['+temp_option_id+'][values]');;
    $(".required", newRow).val(temp_option_id).attr('name', 'options[rows]['+temp_option_id+'][required]');
    $(".sort_order", newRow).val(getMaxSortOrder('option_table')).attr('name', 'options[rows]['+temp_option_id+'][sort_order]');
    $(".remove", newRow).click(function() {
        $(this).parent().parent().remove();
    });
    
    lastRow = $(this).parent().parent();
    lastRow.before(newRow);
    $("input[type=checkbox]", newRow).uniform();        
});
    
$('#option_table .remove').click(function() {
    option_id = $(this).val();
    v = $('#removed_options').val();
    $('#removed_options').val(v+option_id+',');
    
    $(this).parent().parent().remove();
});
           
//////////////////////////////////////////////////////////////////////////
//setup all links by ajax on ajax list
//////////////////////////////////////////////////////////////////////////    
$('#related-products .pagination a').live("click", function(e) {
    e.preventDefault();
    get_related_product_list_form($(this).attr('href')); 
});

$('#search_entries').live('click', function() {   
    get_related_product_list_form(admin_path+'/catalog/products/related_products_form');            
});

$('#sort_row a').live('click', function() {
    td = $(this).parent();
    
    orderby = td.attr("data-orderby");
    order = 'asc';
    if(td.hasClass('sorted') && td.hasClass('asc')) order = 'desc';
    
    $('#param_orderby').val(orderby);
    $('#param_order').val(order);
    
    $('#search_entries').trigger('click');
});

//////////////////////////////////////////////////////////////////////////
//accessory functions
////////////////////////////////////////////////////////////////////////// 
function getMaxSortOrder(tableID) {
    var max = 0;
    if($(".sort_order", $("#"+tableID)).length > 0) {
        $(".sort_order", $("#"+tableID)).each(function() {
            v = $(this).val();
            
            if(v == '' || isNaN(v)) v = 0;
            if(max <= parseInt(v)) max = parseInt(v);
        });
    }
    return max+1;
}

function get_related_product_list_form(action_url)
{        
    showLoading();
    
    $("#product_form").ajaxForm({
        url: action_url,
        success: function(responseText) {
            $('#related-products').html(responseText);
            $('#related-products input[type=checkbox], #related-products input[type=text]').uniform();      
            
            $('.check-all', $('#related-products')).on('click', function () {
                $(this).closest('.checkAll').find('input:checkbox').prop('checked', this.checked).closest('.checker>span').toggleClass('checked')
            });
  
            form.unbind('submit'); 
            hideLoading();
        },
    }).submit();
}