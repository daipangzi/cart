<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Posts_model extends BF_Model {

	protected $table		= "posts";
	protected $key			= "post_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";
    
    var $CI;  
    
    function __construct() 
    {
        $this->CI =& get_instance();
                  
        $this->is_admin = is_backend();
    }
    
    function find($id, $return_type=0)
    {
        $record = parent::find($id, $return_type);
        
        //if cann't find record
        if(empty($record)) 
        {
            return FALSE;
        }

        if($this->is_admin)
        {
            if($return_type == 0)
            {
                $record->info = $this->get_all_language_info($id);    
            }
            else
            {
                $record['info'] = $this->get_all_language_info($id);
            }
        }
        else
        {
            //if status disabled
            $temp = (array)$record;
            if($temp['status'] == 0)
            {
                return FALSE;
            }
            
            $record = array_merge($record, $this->get_language_info($id, global_language_id()));    
        }
        return $record;
    }
    
    /**
    * find page by slug
    * 
    * @param mixed $slug
    */
    function find_by_slug($slug)
    {
        $record = $this->find_by('slug', $slug);
        
        if(empty($record)) 
        {
            return FALSE;
        }
        
        return $this->find($record->post_id);
    }
    
    /**
    * count all enabled records 
    * 
    */
    function count_all()
    {
        $language_id = global_language_id();
        
        $this->join('posts_info mi', "mi.post_id=posts.post_id AND mi.language_id={$language_id}", 'left');
        
        return parent::count_all();
    }
    
    /**
    * find all records with current language
    * 
    * @param mixed $mode : 0: normal, 1: dropdown
    */
    function find_all($return_type=0)
    {
        $language_id = global_language_id();
        
        $this->join('posts_info mi', "mi.post_id=posts.post_id AND mi.language_id={$language_id}", 'left');
        
        return parent::find_all($return_type);
    }
    
    /**
    * save page and language info
    * 
    * @param mixed $data
    * @param mixed $langs
    */
    function save($data, $langs, $categories) {
        $id = FALSE;
        if(!isset($data[$this->key]))
        {
            $id = $this->insert($data);
        }
        else  
        {
            $id = $data[$this->key];
            
            $this->update($id, $data);
        }
        
        if($id !== FALSE)
        {
            $this->save_lang_info($id, $langs);     
            $this->save_categories($id, $categories);     
        }
        
        return $id;
    }
    
    /**
    * delete page and language info
    * 
    * @param mixed $id
    * @return bool
    */
    function delete($id) {
        // delete accessories
        $this->db->where('post_id', $id)->delete('posts_info');
        
        return parent::delete($id);        
    }
    
    //--------------------------------------------------------------------
    // !COMMON METHODS
    //--------------------------------------------------------------------
    
    /**
    * check if the slug is exist
    * 
    * @param mixed $slug
    * @param mixed $id
    * @return mixed
    */
    private function check_slug($slug, $id=false)
    {
        if($id)
        {
            $this->where('post_id !=', $id);
        }
        $this->where('slug', $slug);
        
        return (bool) parent::count_all();
    }
    
    /**
    * check and valid slug for page
    * 
    * @param mixed $slug
    * @param mixed $id
    * @param mixed $count
    */
    function validate_slug($slug, $id=false, $count=false)
    {
        if($slug == '') return '';
        
        if($this->check_slug($slug.$count, $id))
        {
            if(!$count)
            {
                $count = 1;
            }
            else
            {
                $count++;
            }
            return $this->validate_slug($slug, $id, $count);
        }
        else
        {
            return $slug.$count;
        }
    }
    
    //--------------------------------------------------------------------
    // !LANGAGE METHODS
    //--------------------------------------------------------------------
    /**
    * check if row is exist with category and lanuage id
    * 
    * @param mixed post_id
    * @param mixed $language_id
    * @return mixed
    */
    private function check_language_row($id, $language_id)
    {
        $where = array();
        $where['post_id']       = $id;
        $where['language_id']   = $language_id;
        
        return (bool)$this->db->where($where)->count_all_results('posts_info');
    }  
    
    /**
    * get all lanuage info
    * 
    * @param mixed post_id
    */
    private function get_all_language_info($id)
    {
        $languages    = $this->CI->languages_model->find_all(1);
        
        $result = array();
        foreach($languages as $lang)
        {
            $l = $lang['language_id'];
            
            $record = $this->get_language_info($id, $l);
            
            $result[$l] = $record;
        }
        
        return $result;
    } 
    
    /**
    * get one lanuage info
    * 
    * @param mixed $category_id
    * @param mixed $language_id
    */
    private function get_language_info($id, $language_id)
    {
        $default_lang = get_language_id_by_code(settings_item('site.default_language'));   
        
        $record = $this->db
                    ->where(array('post_id' => $id, 'language_id' => $language_id))
                    ->get('posts_info')
                    ->row_array();
         
        if(empty($record))
        {
            $record = $this->db
                    ->where(array('post_id' => $id, 'language_id' => $default_lang))
                    ->get('posts_info')
                    ->row_array();
        } 
        
        return $record;
    }
    
    /**
    * save language info for post
    * 
    * @param mixed $id
    */
    private function save_lang_info($id, $langs)
    {
        foreach($langs as $c => $d)
        {
            $info = array();
            $info['post_id']        = $id;
            $info['language_id']    = $c;
            $info['title']          = $d['title'];
            $info['content']        = $d['content'];
            $info['page_title']     = $d['page_title'];
            $info['meta_keywords']  = $d['meta_keywords'];
            $info['meta_description'] = $d['meta_description'];
            
            if(!$this->check_language_row($id, $c))
            {
                $this->db->insert('posts_info', $info);
            }
            else
            {
                $where = array();
                $where['post_id']       = $id;
                $where['language_id']   = $c;
                
                $this->db->where($where)->update('posts_info', $info);
            }
        }      
    }
    
    //--------------------------------------------------------------------
    // !Category Methods
    //--------------------------------------------------------------------
    
    /**
    * get post's categories
    * 
    * @param mixed $id
    */
    function get_categories($id)
    {
        $records = $this->db
                ->where(array('post_id' => $id))
                ->get('post_categories_links')
                ->result_array();
         
        return array_column($records, 'category_id');
    }
    
    /**
    * save post categories
    * 
    * @param mixed $id
    * @param mixed $options
    */
    private function save_categories($id, $categories)
    {
        $categories = explode(',', $categories);
                                    
        if(empty($categories)) return;
        
        //delete previous categories before save
        $this->db->where('post_id', $id)->delete('post_categories_links');
        
        //resave categories
        foreach($categories as $cid)
        {
            if(trim($cid) == '') continue;
            
            $info = array();
            $info['post_id']    = $id;            
            $info['category_id']= $cid;
            
            $this->db->insert('post_categories_links', $info);    
        }
    }
}
