<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class content extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Posts.Content.View');
		
        $this->load->model('posts_model', null, true);
        $this->load->model('post_categories/post_categories_model', null, true);
        
		$this->lang->load('posts');
        
        $temp = config_item('page_templates');
        $templates = array();
        foreach($temp as $t)
        {
            $templates[$t] = lang($t);
        }
        Template::set('templates', $templates);
        
        // for tree
        Assets::add_css( array(
            plugin_path().'misc/tree2/ui.dynatree.css',
        )); 
         
        Assets::add_js( array(
            'jquery.form.js',
            plugin_path().'misc/tree2/jquery.dynatree.js',
        )); 
        
        if(!$this->session->userdata('posts_index'))
        {
            $this->session->set_userdata('posts_index', site_url(SITE_AREA .'/content/posts'));
        }
	}
	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->posts_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('posts_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('posts_delete_failure') . $this->posts_model->error, 'error');
				}
			}
		}
        
        //get order fields
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        //get where fields
        $where = array();  
        
        $orders = array();
        switch($params['orderby']) {
            case "title":
            case "slug":
            case "status":
            case "template":
                $orders[$params['orderby']] = $params['order'];
                break;
                
            case "created":
                $orders["created_on"]  = $params['order'];
                break;

            default:
                $orders["title"]  = "asc";
                break;
        }

		//get total counts
        $this->posts_model->where($where);
        $total_records = $this->posts_model->count_all();
                       
        //get total records
        $records = $this->posts_model
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all();
        Template::set('records', $records);
        
        //build pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/content/post/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/content/post') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);
        
        // set request page  
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('posts_index', $url);                                   
        
        Template::set('toolbar_title', lang('posts_list'));
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: create()

		Creates a Pages object.
	*/
	public function create()
	{
		$this->auth->restrict('Pages.Content.Create');

		if ($this->input->post('save') || $this->input->post('save_continue'))    
		{
			if ($insert_id = $this->save_post())
			{
				Template::set_message(lang('posts_create_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
                else Template::redirect(SITE_AREA .'/content/posts/edit/'.$insert_id);
			}
			else
			{
				Template::set_message(lang('posts_create_failure') . $this->posts_model->error, 'error');
			}
		}
		Assets::add_module_js('posts', 'posts.js');
        Assets::add_js($this->load->view('content/tree_js', null, true), 'inline');
        
        $category_tree = $this->post_categories_model->get_tree_root(2);
        Template::set('category_tree', $category_tree);
        
        Template::set('prev_page', $this->session->userdata('posts_index'));
		Template::set('toolbar_title', lang('posts_create'));
        Template::set_view('posts/content/form');
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: edit()

		Allows editing of Pages data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('posts_invalid_id'), 'error');
			redirect(SITE_AREA .'/content/posts');
		}

		if (isset($_POST['save']) || isset($_POST['save_continue']))
		{
			$this->auth->restrict('Pages.Content.Edit');

			if ($this->save_post('update', $id))
			{
				Template::set_message(lang('posts_edit_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
			}
			else
			{
				Template::set_message(lang('posts_edit_failure') . $this->posts_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Pages.Content.Delete');

			if ($this->posts_model->delete($id))
			{
				Template::set_message(lang('post_delete_success'), 'success');

				$this->redirect_to_index();
			} else
			{
				Template::set_message(lang('posts_delete_failure') . $this->posts_model->error, 'error');
			}
		}
        Assets::add_module_js('posts', 'posts.js');
        Assets::add_js($this->load->view('content/tree_js', null, true), 'inline');
        
        $record = $this->posts_model->find($id, 1);
		Template::set('record', $record);
        
        //build category tree
        $categories = $this->posts_model->get_categories($id);                     
        if(empty($categories))
        {
            $category_tree = $this->post_categories_model->get_tree_root(2);
        }
        else
        {
            $category_tree = $this->post_categories_model->get_tree_from_ids($categories);            
        }        
        Template::set('categories', $categories);
        Template::set('category_tree', $category_tree);
        
        Template::set('prev_page', $this->session->userdata('posts_index'));
		Template::set('toolbar_title', lang('posts_edit'));
        Template::set_view('posts/content/form');
		Template::render();
	}

    public function delete()
    {
        $id = $this->uri->segment(5);

        if (empty($id))
        {
            Template::set_message(lang('posts_invalid_id'), 'error');
            $this->redirect_to_index();
        }
        
        $this->auth->restrict('Pages.Content.Delete');
        
        if ($this->posts_model->delete($id))
        {
            Template::set_message(lang('post_delete_success'), 'success');

            $this->redirect_to_index();
        }
    }
	//--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_posts()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_post($type='insert', $id=0)
	{
        $_POST['slug']   = url_title(convert_accented_characters($this->input->post('slug')), 'dash', TRUE);
        
        //set rules for manufacturer attributes
        $languages = $this->languages_model->find_all(1);
        foreach($languages as $l)
        {
            $c = $l['language_id'];
            
            $this->form_validation->set_rules("info[{$c}][title]", 'lang:ed_title', 'required|trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][content]", 'lang:ed_content', 'trim');
            $this->form_validation->set_rules("info[{$c}][page_title]", 'lang:ed_page_title', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_keywords]", 'lang:ed_meta_keywords', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_description]", 'lang:ed_meta_description', 'trim|max_length[255]|strip_tags|xss_clean');
        }
        
        $this->form_validation->set_rules('slug', 'lang:ed_slug','trim|alpha_dash|max_length[100]|strip_tags|xss_clean');
        $this->form_validation->set_rules('status', 'lang:ed_status','trim');
		$this->form_validation->set_rules('template','lang:ed_template','trim');

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
		$data = array();
        if($id != 0) 
        {
            $data['post_id']= $id;
        }
        $data['slug']       = $this->posts_model->validate_slug($this->input->post('slug'), $id);
		$data['template']   = $this->input->post('template')?$this->input->post('template'):'default';
		$data['status']     = $this->input->post('status')?'1':'0';

		$langs = $this->input->post('info');
        $categories = $this->input->post('categories');
        return $this->posts_model->save($data, $langs, $categories);
	}

	//--------------------------------------------------------------------
    private function redirect_to_index()
    {
        $redirect_url = $this->session->userdata('posts_index')?$this->session->userdata('posts_index'):SITE_AREA .'/content/posts';
        Template::redirect($redirect_url);
    }


}