<?php // Change the css classes to suit your needs
$title = lang('posts_create');
$back_url = isset($prev_page)?$prev_page:SITE_AREA .'/content/posts';
if(isset($record)) {
    $info = $record['info'];    
    $title = $record['info'][global_language_id()]['title'];
}

$category_ids = (isset($categories) && is_array($categories) && count($categories)>0)?(implode(',', $categories)):'';
?>

<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" autocomplete="off"'); ?>
    <input type="hidden" id="categories" name="categories" value="<?php echo $category_ids; ?>"/>
    
    <fieldset>
        <legend><?php echo $title; ?></legend>
        
        <div class="form-buttons">
            <?php echo anchor($back_url, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>                    
            <button type="reset" name="reset" class="btn" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
            
            <?php if(isset($record)) :?>
            <?php if ($this->auth->has_permission('Pages.Content.Delete')) : ?>
            <button type="submit" name="delete" class="btn" id="delete-me" onclick="return confirm('<?php echo lang('post_delete_confirm'); ?>')">
                <i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete'); ?>
            </button>
            <?php endif; ?>
            <?php endif; ?>
            
            <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
            <button type="submit" name="save_continue" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save_continue'); ?></button>&nbsp;
        </div>
        
        <ul id="myTab1" class="nav nav-tabs">
            <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('ed_general'); ?></a></li>
            <li><a href="#description" data-toggle="tab"><?php echo lang('ed_description'); ?></a></li>
            <li><a href="#meta_info" data-toggle="tab"><?php echo lang('ed_meta_info'); ?></a></li>
        </ul>
        
        <div class="tab-content">
            <div class="tab-pane fade in active" id="general">
                
                <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; ?>
                <div class="control-group <?php echo form_error("info[{$c}][title]") ? 'error' : ''; ?>">
                    <?php 
                        if($i == 0)
                            echo form_label(lang('ed_title').lang('bf_form_label_required'), "info_{$c}_title", array('class' => "control-label") ); 
                        else
                            echo form_label('', "info_{$c}_title", array('class' => "control-label") ); 
                    ?>
                    <div class='controls'>
                        <input id="info_<?php echo $c; ?>_title" type="text" name="info[<?php echo $c; ?>][title]" class="span4" maxlength="255" value="<?php echo set_value("info[{$c}][title]", isset($info[$c]['title']) ? $info[$c]['title'] : ''); ?>" <?php echo form_error("info[{$c}][title]") ? 'title="'.form_error("info[{$c}][title]").'"' : ''; ?>/>
                        <span class="inline-help gray"><?php echo strtoupper($l['locale']); ?></span>
                    </div>
                </div>
                <?php $i++; endforeach; ?>
                
                <div class="control-group <?php echo form_error('slug') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_slug'), 'slug', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="slug" type="text" name="slug" class="span4" maxlength="100" value="<?php echo set_value('slug', isset($record['slug']) ? $record['slug'] : ''); ?>" <?php echo form_error('slug') ? 'title="'.form_error('slug').'"' : ''; ?>/>
                    </div>
                </div>
                
                <div class="control-group <?php echo form_error('slug') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_category'), 'slug', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <div class="span4 noLeftMargin">
                            <div id="tree" style="height:150px;">
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php echo form_dropdown('template', $templates, set_value('template', isset($record['template']) ? $record['template'] : 'default'), lang('ed_template'), 'id="template" class="nostyle"'); ?>
                    
                <?php echo form_dropdown('status', array("1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value('status', isset($record['status']) ? $record['status'] : '1'), lang('ed_status'), 'id="status" class="nostyle"'); ?>
            </div>
            
            <div class="tab-pane fade subtab" id="description">
                <ul id="myTab1" class="nav nav-tabs">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <li <?php if($i==0){echo 'class="active"';} ?>><a href="#<?php echo $cd; ?>" data-toggle="tab"><?php echo $l['name']; ?></a></li>
                    <?php $i++; endforeach; ?>
                </ul>   
                
                <div class="tab-content">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <div class="tab-pane fade in <?php if($i==0){echo 'active';} ?>" id="<?php echo $cd; ?>">
                        <div class="control-group <?php echo form_error("info[{$c}][content]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_content'), "info_{$c}_content", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <div class="span8 noLeftMargin">
                                    <textarea id="info_<?php echo $c; ?>_content" name="info[<?php echo $c; ?>][content]" class="span8 tinymce" rows="15" <?php echo form_error("info[{$c}][content]") ? 'title="'.form_error("info[{$c}][content]").'"' : ''; ?>><?php echo set_value("info[{$c}][content]", isset($info[$c]['content']) ? $info[$c]['content'] : ''); ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; endforeach; ?>
                </div>
            </div>
            
            <div class="tab-pane fade subtab" id="meta_info">
                <ul id="myTab1" class="nav nav-tabs">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <li <?php if($i==0){echo 'class="active"';} ?>><a href="#meta_info_<?php echo $cd; ?>" data-toggle="tab"><?php echo $l['name']; ?></a></li>
                    <?php $i++; endforeach; ?>
                </ul>   
                
                <div class="tab-content">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <div class="tab-pane fade in <?php if($i==0){echo 'active';} ?>" id="meta_info_<?php echo $cd; ?>">
                        <div class="control-group <?php echo form_error("info[{$c}][page_title]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_page_title'), "info_{$c}_page_title", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <input id="info_<?php echo $c; ?>_page_title" type="text" name="info[<?php echo $c; ?>][page_title]" class="span8" maxlength="255" value="<?php echo set_value("info[{$c}][page_title]", isset($info[$c]['page_title']) ? $info[$c]['page_title'] : ''); ?>" <?php echo form_error("info[{$c}][page_title]") ? 'title="'.form_error("info[{$c}][page_title]").'"' : ''; ?>/>
                            </div>
                        </div>
                        
                        <div class="control-group <?php echo form_error("info[{$c}][meta_keywords]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_meta_keywords'), "info_{$c}_meta_keywords", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <textarea id="info_<?php echo $c; ?>_meta_keywords" name="info[<?php echo $c; ?>][meta_keywords]" class="span8 limit" rows="4" <?php echo form_error("info[{$c}][meta_keywords]") ? 'title="'.form_error("info[{$c}][meta_keywords]").'"' : ''; ?>><?php echo set_value("info[{$c}][meta_keywords]", isset($info[$c]['meta_keywords']) ? $info[$c]['meta_keywords'] : ''); ?></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group <?php echo form_error("info[{$c}][meta_description]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_meta_description'), "info{$c}_meta_description", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <textarea id="info_<?php echo $c; ?>_meta_description" name="info[<?php echo $c; ?>][meta_description]" class="span8" rows="6" <?php echo form_error("info[{$c}][meta_description]") ? 'title="'.form_error("info[{$c}][meta_description]").'"' : ''; ?>><?php echo set_value("info[{$c}][meta_description]", isset($info[$c]['meta_description']) ? $info[$c]['meta_description'] : ''); ?></textarea>
                            </div>
                        </div> 
                    </div> 
                    <?php $i++; endforeach; ?>
                </div>
            </div>
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>

<script>
var treeData = <?php echo json_encode($category_tree); ?>;
</script>