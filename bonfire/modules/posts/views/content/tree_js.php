$("#tree").dynatree({
    clickFolderMode: 1,
    checkbox: true,
    selectMode: 2,
    children: treeData,
    onLazyRead: function (node) {
        // Mockup a slow reqeuest ...
        node.appendAjax({
            url: "<?php echo site_url(SITE_AREA .'/catalog/categories/get_childrens'); ?>",
            data: {
                'parent_id': node.data.key
            },
            //debugLazyDelay: 750 // don't do this in production code
        });
    },
    onBeforeLazyRead: function() {
        show_loading();
    },
    onAfterLazyRead: function() {
        hide_loading();
    },
    onSelect: function(select, node) {
        // Display list of selected nodes
        var selNodes = node.tree.getSelectedNodes();
        // convert to title/key array
        var selKeys = $.map(selNodes, function(node){
             return node.data.key;
        });
        $("#categories").val(selKeys.join(","));
    },
});

function show_loading() {
    get_tree().disable();
    $('#loading-mask').show();
}

function hide_loading() {
    get_tree().enable();
    $('#loading-mask').hide();
}

function get_active_node() {
    return $("#tree").dynatree("getActiveNode");
}

function get_tree() {
    return $("#tree").dynatree("getTree");
}