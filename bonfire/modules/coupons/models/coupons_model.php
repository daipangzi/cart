<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Coupons_model extends BF_Model {

	protected $table		= "coupons";
	protected $key			= "coupon_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";
    
    function save($data) {
        if(!isset($data[$this->key]))
            return $this->insert($data);
        else  {
            if($this->update($data[$this->key], $data))
                return $data[$this->key];
            else 
                return FALSE;
        }
    }  
    
    function find($id, $return_type=0)
    {
        $this->select('coupon_id, name, code, type, discount, total_amount, start_date, end_date, uses_total, uses_customer, status');
        return parent::find($id, $return_type);
    }
    
    // get coupon details, by code
    function find_by_code($code, $return_type=0)
    {
        $record = $this->find_by('code', $code);
        
        if(empty($record)) return FALSE;
                               
        return $this->find($record->coupon_id, $return_type);
    }
    
    /**
    * checks coupon dates and usage numbers
    * 
    * @param mixed $coupon
    */
    function validate($id)
    {         
        $record = $this->find($id);
         
        if($record->uses_total != '' && $record->uses_total != '0') {
            $uses = $this->count_coupon_used($id);
                 
            if($uses >= $record->uses_total) return FALSE;
        }
                                    
        if($record->start_date != "")
        {
            $s_date = explode("-", $record->start_date);
            $start  = mktime(0,0,0, $s_date[1], $s_date[2], $s_date[0]);
        
            $current = time();
        
            if($current < $start) return FALSE;
        }
        
        if($record->end_date != "")
        {
            $e_date = explode("-", $record->end_date);
            $end = mktime(0,0,0, $e_date[1], (int) $e_date[2] +1 , $e_date[0]); // add a day to account for the end date as the last viable day
        
            $current = time();
        
            if($current > $end) return FALSE;
        }
              
        return TRUE;
    }   
    
    function validate_by_code($code)
    {
        $id = $this->get_id($code);
        
        return $this->validate($id);
    }
    
    /**
    * count coupon used
    * 
    * @param mixed $id
    */
    function count_coupon_used($id)
    {
        return $this->db
                ->from('coupon_history')
                ->where(array('coupon_id' => $id))
                ->count_all_results();
    }    
    
    /**
    * get id from code
    * 
    * @param mixed $code
    */
    function get_id($code)
    {
        $record = $this->find_by('code', $code);
        
        if(!empty($record)) return FALSE;
        
        return $record->coupon_id;    
    }
    
    /**
    * get code from id
    * 
    * @param mixed $id
    */
    function get_code($id)
    {
        $record = $this->find($id);
           
        if(empty($record)) return FALSE;
        
        return $record->code;    
    }
}
