<div class="admin-box">
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset><legend><?php echo lang('coupons_list'); ?><small><?php echo $this->pagination->get_page_status();?></small></legend></fieldset>
    
    <div class="form-buttons">
        <?php if ($this->auth->has_permission('Coupons.Sales.Delete') && isset($records) && is_array($records) && count($records)) : ?>
        <button type="submit" name="delete" id="delete-me" class="btn" value="<?php echo lang('bf_action_delete') ?>" onclick="return confirm('<?php echo lang('coupons_delete_confirm'); ?>')"><span class="icon-remove-sign"></span>&nbsp;<?php echo lang('bf_action_delete') ?></button>            
        <?php endif;?>
        
        <?php if ($this->auth->has_permission('Coupons.Sales.Create')) : ?>
        <?php echo anchor(SITE_AREA .'/sales/coupons/create', '<span class="icon-plus-sign"></span>&nbsp;'.lang('bf_action_insert'), 'class="btn"'); ?>                    
        <?php endif;?>
    </div>
    
    <div class="responsive">
		<table class="table table-striped lrborder checkAll" style="min-width:1000px;">
        <colgroup>
            <?php if ($this->auth->has_permission('Coupons.Sales.Delete') && isset($records) && is_array($records) && count($records)) : ?>
            <col width="50px"/>
            <?php endif;?>
            <col width=""/>
            <col width="160"/>
            <col width="160"/>
            <col width="160"/>
            <col width="160"/>
            <col width="150px"/>
            <col width="120"/>
        </colgroup>
		<thead>
			<tr>
				<?php if ($this->auth->has_permission('Coupons.Sales.Delete') && isset($records) && is_array($records) && count($records)) : ?>
				<th class="column-check"><input class="check-all" type="checkbox" /></th>
				<?php endif;?>
				
                <th class="<?php echo sort_classes($params['orderby'], "name", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/coupons?orderby=name&amp;order='.sort_direction($params['orderby'], "name", $params['order'])); ?>"><span><?php echo lang('ed_coupon_name'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "code", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/coupons?orderby=code&amp;order='.sort_direction($params['orderby'], "code", $params['order'])); ?>"><span><?php echo lang('ed_code'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "discount", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/coupons?orderby=discount&amp;order='.sort_direction($params['orderby'], "discount", $params['order'])); ?>"><span><?php echo lang('ed_discount'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "start", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/coupons?orderby=start&amp;order='.sort_direction($params['orderby'], "start", $params['order'])); ?>"><span><?php echo lang('ed_start_date'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "end", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/coupons?orderby=end&amp;order='.sort_direction($params['orderby'], "end", $params['order'])); ?>"><span><?php echo lang('ed_end_date'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/coupons?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ed_status'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th><?php echo lang('ed_actions'); ?></th>
			</tr>
		</thead>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<?php if ($this->auth->has_permission('Coupons.Sales.Delete')) : ?>
				<td><input type="checkbox" name="checked[]" value="<?php echo $record->coupon_id ?>" /></td>
				<?php endif;?>
				
			    <?php if ($this->auth->has_permission('Coupons.Sales.Edit')) : ?>
			    <td><?php echo anchor(SITE_AREA .'/sales/coupons/edit/'. $record->coupon_id, $record->name) ?></td>
			    <?php else: ?>
			    <td><?php echo $record->name ?></td>
			    <?php endif; ?>
		    
			    <td><?php echo $record->code?></td>
			    <td><?php echo $record->discount; echo $record->type=='P'?'%':'&yen'; ?></td>
			    <td><?php echo $record->start_date?></td>
			    <td><?php echo $record->end_date?></td>
			    <td><?php echo $record->status='1'?lang('ed_enable'):lang('ed_disable'); ?></td>
                <td>
                        <?php if ($this->auth->has_permission('Coupons.Sales.Edit')) : ?>
                        <a href="<?php echo site_url(SITE_AREA .'/sales/coupons/edit/'. $record->coupon_id) ?>" title="<?php echo lang('bf_action_edit'); ?>" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                        <?php endif; ?>
                        
                        <?php if ($this->auth->has_permission('Coupons.Sales.Delete')) : ?>
                        <a href="<?php echo site_url(SITE_AREA .'/sales/coupons/delete/'. $record->coupon_id) ?>" title="<?php echo lang('bf_action_delete'); ?>" class="tip"><span class="icon12 icomoon-icon-remove"></span></a>
                        <?php endif; ?>
                    </td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="13"><?php echo lang('no_record'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
    </div>
	<?php echo form_close(); ?>
    <?php echo $this->pagination->create_links(); ?>
</div>