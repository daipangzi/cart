<?php
$back_url = isset($prev_page)?$prev_page:SITE_AREA .'/catalog/customers';
?>
<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>
        <legend><?php echo lang('coupon_detail'); ?></legend>
        
        <div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_coupon_name'). lang('bf_form_label_required'), 'name', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="name" type="text" class="span4" name="name" maxlength="255" value="<?php echo set_value('name', isset($coupons['name']) ? $coupons['name'] : ''); ?>" autofocus="autofocus"/>
                <span class="help-inline"><?php echo form_error('name'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('code') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_code'). lang('bf_form_label_required'), 'code', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="code" type="text" class="span4" name="code" maxlength="30" value="<?php echo set_value('code', isset($coupons['code']) ? $coupons['code'] : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('code'); ?></span>
                <br/><span class="help-block"><?php echo lang('ed_coupon_code_note'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('type') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_type'), 'type', array('class' => "control-label") ); ?>
            <div class='controls'>
                <?php $options = array('P'=>lang('ed_percentage'), 'F'=>lang('ed_fix_amount')); ?>
                <?php echo form_dropdown2('type', $options, set_value('type', isset($coupons['type']) ? $coupons['type'] : 'P'), 'class="nostyle"')?>                
                <span class="help-inline"><?php echo form_error('type'); ?></span>
                <br/><span class="help-block"><?php echo lang('ed_coupon_type_note'); ?></span>
            </div>
        </div>           
        
        <div class="control-group <?php echo form_error('discount') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_discount'), 'discount', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="discount" type="text" class="span4" name="discount" value="<?php echo set_value('discount', isset($coupons['discount']) ? $coupons['discount'] : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('discount'); ?></span>
            </div>
        </div>
                   
        <div class="control-group <?php echo form_error('total_amount') ? 'error' : ''; ?>">   
            <?php echo form_label(lang('ed_total_amount'), 'total_amount', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="total_amount" type="text" class="span4" name="total_amount" value="<?php echo set_value('total_amount', isset($coupons['total_amount']) ? $coupons['total_amount'] : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('total_amount'); ?></span>
                <br/><span class="help-block"><?php echo lang('ed_coupon_total_amount_note'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('start_date') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_start_date'), 'start_date', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="start_date" type="text" class="span4 date" name="start_date" value="<?php echo set_value('start_date', isset($coupons['start_date']) ? $coupons['start_date'] : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('start_date'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('end_date') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_end_date'), 'end_date', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="end_date" type="text" name="end_date" class="span4 date" value="<?php echo set_value('end_date', isset($coupons['end_date']) ? $coupons['end_date'] : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('end_date'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('uses_total') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_uses_per_coupon'), 'uses_total', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="uses_total" type="text" name="uses_total" class="span4" value="<?php echo set_value('uses_total', isset($coupons['uses_total']) ? $coupons['uses_total'] : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('uses_total'); ?></span>
                <br/><span class="help-block"><?php echo lang('ed_coupon_per_coupon_note'); ?></span>
            </div>
        </div>
        
        <!--<div class="control-group <?php echo form_error('uses_per_customer') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_uses_per_customer'), 'uses_per_customer', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="uses_customer" type="text" name="uses_customer" value="<?php echo set_value('uses_customer', isset($coupons['uses_customer']) ? $coupons['uses_customer'] : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('uses_per_customer'); ?></span>
                <br/><span class="help-block"><?php echo lang('ed_coupon_per_customer_note'); ?></span>
            </div>
        </div>-->
        
        <?php echo form_dropdown('status', array("1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value('status', isset($coupons['status']) ? $coupons['status'] : '1'), lang('ed_status'), 'id="status" class="nostyle"'); ?>

        <div class="form-buttons">
            <?php echo anchor($back_url, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>
            <button type="reset" name="reset" class="btn" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
            
            <?php if(isset($coupons)) :?>
            <?php if ($this->auth->has_permission('Coupons.Sales.Delete')) : ?>
            <button type="submit" name="delete" class="btn" id="delete-me" onclick="return confirm('<?php echo lang('coupon_delete_confirm'); ?>')">
                <i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete'); ?>
            </button>
            <?php endif; ?>
            <?php endif; ?>
            
            <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
            <button type="submit" name="save_continue" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save_continue'); ?></button>&nbsp;
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>
