<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Coupons.Sales.View');
		
        $this->load->model('coupons_model', null, true);
		$this->lang->load('coupons');
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index()
	{

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->coupons_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('coupons_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('coupons_delete_failure') . $this->coupons_model->error, 'error');
				}
			}
		}
        
        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders  = array();
        switch($params['orderby']) {
            case "name":
                $orders["name"]     = $params['order'];
                break;
            case "code":
                $orders["code"]     = $params['order'];
                break;
            case "discount":
                $orders["discount"] = $params['order'];
                break;
            case "start":
                $orders["start_date"]   = $params['order'];
                break;
            case "end":
                $orders["end_date"]     = $params['order'];
                break;
            case "status": 
                $orders["status"]       = $params['order'];
                break;
            default:
                $orders["created_on"]   = "desc";
                break;
        }
		$records = $this->coupons_model->order_by($orders)->find_all();
        Template::set('records', $records);
        
        //setup pagination
        $total_records = $this->coupons_model->count_all();
        
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/sales/coupons/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/sales/coupons') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);

		// set request page                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('coupon_index', $url);
        
        Template::set('toolbar_title', lang('coupons_manage'));
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: create()

		Creates a Coupons object.
	*/
	public function create()
	{
		$this->auth->restrict('Coupons.Sales.Create');

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			if ($insert_id = $this->save_coupons())
			{
				Template::set_message(lang('coupons_create_success'), 'success');
                
                if($this->input->post('save')) $this->redirect_to_index();
                else Template::redirect(SITE_AREA .'/sales/coupons/edit/'.$id);
			}
			else
			{
				Template::set_message(lang('coupons_create_failure') . $this->coupons_model->error, 'error');
			}
		}
		Assets::add_module_js('coupons', 'coupons.js');

        Template::set('prev_page', $this->session->userdata('coupon_index'));
        Template::set('toolbar_title', lang('coupons_create'));
        Template::set_view('coupons/sales/form');
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: edit()

		Allows editing of Coupons data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('coupons_invalid_id'), 'error');
			$this->redirect_to_index();
		}

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			$this->auth->restrict('Coupons.Sales.Edit');

			if ($this->save_coupons('update', $id))
			{
				Template::set_message(lang('coupons_edit_success'), 'success');
                
                if($this->input->post('save')) $this->redirect_to_index();
			}
			else
			{
				Template::set_message(lang('coupons_edit_failure') . $this->coupons_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Coupons.Sales.Delete');

			if ($this->coupons_model->delete($id))
			{
				Template::set_message(lang('coupons_delete_success'), 'success');

				$this->redirect_to_index();
			} else
			{
				Template::set_message(lang('coupons_delete_failure') . $this->coupons_model->error, 'error');
			}
		}
        Assets::add_module_js('coupons', 'coupons.js');
        
        $record = $this->coupons_model->find($id, 1);
		Template::set('coupons', $record);
		
        Template::set('prev_page', $this->session->userdata('coupon_index'));
		Template::set('toolbar_title', lang('coupons_edit'));
        Template::set_view('coupons/sales/form');
		Template::render();
	}

	//--------------------------------------------------------------------
    public function delete()
    {
        $id = $this->uri->segment(5);
        
        if(empty($id))
        {
            Template::set_message(lang('customer_invalid_id'), 'error');
            $this->redirect_to_index();
        }
        
        $this->auth->restrict('Coupons.Sales.Delete');
        
        if ($this->coupons_model->delete($id))
        {
            Template::set_message(lang('coupons_delete_success'), 'success');

            $this->redirect_to_index();
        }
    }

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_coupons()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_coupons($type='insert', $id=0)
	{
        if($this->input->post('code') != $this->coupons_model->get_code($id)) {
            $is_unique = '|is_unique[coupons.code]';
        } else {
            $is_unique = '';
        }
		$this->form_validation->set_rules('name',lang('ed_coupon_name'),'required|trim|max_length[255]|strip_tags|xss_clean');
		$this->form_validation->set_rules('code',lang('ed_code'),'required|trim|alpha_numeric|max_length[30]|strip_tags|xss_clean'.$is_unique);
		$this->form_validation->set_rules('discount',lang('ed_discount'),'trim|is_numeric');
		$this->form_validation->set_rules('total_amount',lang('ed_total_amount'),'trim|is_numeric');
		$this->form_validation->set_rules('start_date',lang('ed_start_date'),'trim|strip_tags|xss_clean');
		$this->form_validation->set_rules('end_date',lang('ed_end_date'),'trim|strip_tags|xss_clean');
		$this->form_validation->set_rules('uses_total',lang('ed_uses_per_coupon'),'trim|is_numeric');
		//$this->form_validation->set_rules('uses_customer',lang('ed_uses_per_customer'),'trim|is_numeric');
		$this->form_validation->set_rules('status',lang('ed_status'),'required|trim|is_numeric|max_length[1]');

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
        if($id != "insert")  $data['coupon_id'] = $id;         
		$data['name']        = $this->input->post('name');
		$data['code']        = $this->input->post('code');
		$data['type']        = $this->input->post('type')=='P'?'P':'F';
		$data['discount']    = $this->input->post('discount')!=''?$this->input->post('discount'):'0';
		$data['total_amount']= $this->input->post('total_amount')!=''?$this->input->post('total_amount'):'0';
		$data['start_date']  = $this->input->post('start_date') ? $this->input->post('start_date') : date('Y-m-d', time());
		$data['end_date']    = $this->input->post('end_date') ? $this->input->post('end_date') : date('Y-m-d', time());
		$data['uses_total']     = $this->input->post('uses_total')?$this->input->post('uses_total'):'0';
		//$data['uses_customer']  = $this->input->post('uses_customer')?$this->input->post('uses_customer'):'0';
		$data['status']         = $this->input->post('status')=='1'?1:0;

		$id = $this->coupons_model->save($data);

		if (is_numeric($id))
			$return = $id;
		else
			$return = FALSE;
            
		return $return;
	}

	//--------------------------------------------------------------------
    private function redirect_to_index()
    {
        $redirect_url = $this->session->userdata('coupon_index')?$this->session->userdata('coupon_index'):SITE_AREA .'/sales/coupons';
        Template::redirect($redirect_url);
    }


}