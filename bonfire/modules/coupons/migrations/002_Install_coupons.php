<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_coupons extends Migration {

	public function up()
	{
		$prefix = $this->db->dbprefix;

		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				
			),
			'code' => array(
				'type' => 'VARCHAR',
				'constraint' => 30,
				
			),
			'type' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				
			),
			'discount' => array(
				'type' => 'DECIMAL',
				'constraint' => 15,2,
				
			),
			'total_amount' => array(
				'type' => 'VARCHAR',
				'constraint' => 15,2,
				
			),
			'start_date' => array(
				'type' => 'DATE',
				'default' => '0000-00-00',
				
			),
			'end_date' => array(
				'type' => 'DATE',
				'default' => '0000-00-00',
				
			),
			'uses_per_coupon' => array(
				'type' => 'INT',
				'constraint' => 20,
				
			),
			'uses_per_customer' => array(
				'type' => 'INT',
				'constraint' => 20,
				
			),
			'status' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				
			),
			'created_on' => array(
				'type' => 'datetime',
				'default' => '0000-00-00 00:00:00',
			),
			'modified_on' => array(
				'type' => 'datetime',
				'default' => '0000-00-00 00:00:00',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('coupons');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('coupons');

	}

	//--------------------------------------------------------------------

}