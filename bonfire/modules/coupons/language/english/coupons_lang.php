<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['coupons_manage']			= 'Coupons';
$lang['coupons_edit']			= 'Edit Coupon';
$lang['coupons_create']			= 'New Coupon';
$lang['coupons_list']           = 'Coupons';
$lang['coupons_create_success']			= 'Coupon successfully created.';
$lang['coupons_create_failure']			= 'There was a problem creating the coupon: ';
$lang['coupons_invalid_id']			    = 'Invalid Coupon ID.';
$lang['coupons_edit_success']			= 'Coupon successfully saved.';
$lang['coupons_edit_failure']			= 'There was a problem saving the coupon: ';
$lang['coupons_delete_success']			= 'record(s) successfully deleted.';
$lang['coupons_delete_failure']			= 'We could not delete the record: ';
$lang['coupons_delete_confirm']			= 'Are you sure you want to delete this coupons?';
$lang['coupon_delete_confirm']          = 'Are you sure you want to delete this coupon?';

$lang['coupon_detail'] = 'Coupon Detail';

$lang['ed_coupon_code_note'] = 'The code the customer enters to get the discount';
$lang['ed_coupon_type_note'] = 'Percentage or Fixed Amount';
$lang['ed_coupon_total_amount_note'] = 'The total amount that must reached before the coupon is valid.';
$lang['ed_coupon_per_coupon_note'] = 'The maximum number of times the coupon can be used by any customer. Leave blank for unlimited';
$lang['ed_coupon_per_customer_note'] = 'The maximum number of times the coupon can be used by a single customer. Leave blank for unlimited';