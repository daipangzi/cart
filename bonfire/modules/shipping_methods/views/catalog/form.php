<?php // Change the css classes to suit your needs
$title = lang('shipping_method_new');
$back_url = isset($prev_page)?$prev_page:SITE_AREA .'/catalog/customers';
if(isset($record)) {
    $info = $record['info'];
    
    $title = $record['info'][global_language_id()]['name'];
}
?>

<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" autocomplete="off"'); ?>
    <fieldset>
        <legend><?php echo $title; ?></legend>
        
        <div class="control-group <?php echo form_error('price') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_price'). lang('bf_form_label_required'), 'price', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="price" type="text" name="price" class="span4" value="<?php echo set_value('price', isset($record['price']) ? $record['price'] : ''); ?>" <?php echo form_error('price')?'title="'.form_error('price').'"':''; ?>/>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('sort_order') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_sort_order'), 'sort_order', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="sort_order" type="text" name="sort_order" maxlength="3" class="span4" value="<?php echo set_value('sort_order', isset($record['sort_order']) ? $record['sort_order'] : '1'); ?>"  />
                <span class="help-inline"><?php echo form_error('sort_order'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('url') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_url'), 'url', array('class' => "control-label") ); ?>
            <div class='controls'>                                                                                
                <input id="url" type="text" name="url" maxlength="255" class="span4" value="<?php echo set_value('url', isset($record['url']) ? $record['url'] : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('url'); ?></span>
            </div>
        </div>

        <?php echo form_dropdown('status', array("1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value('status', isset($manufacturer['status']) ? $manufacturer['status'] : '1'), lang('ed_status'), 'id="status" class="nostyle"'); ?>
        
        <div class="subtab">
            <ul id="myTab1" class="nav nav-tabs">
                <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                <li <?php if($i==0){echo 'class="active"';} ?>><a href="#<?php echo $cd; ?>" data-toggle="tab"><?php echo $l['name']; ?></a></li>
                <?php $i++; endforeach; ?>
            </ul>   
            
            <div class="tab-content">
                <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                <div class="tab-pane fade in <?php if($i==0){echo 'active';} ?>" id="<?php echo $cd; ?>">
                    
                    <div class="control-group <?php echo form_error("info[{$c}][name]") ? 'error' : ''; ?>">
                        <?php echo form_label(lang('ed_name').lang('bf_form_label_required'), "info_{$c}_name", array('class' => "control-label") ); ?>
                        <div class='controls'>
                            <input id="info_<?php echo $c; ?>_name" type="text" name="info[<?php echo $c; ?>][name]" class="span4" maxlength="255" value="<?php echo set_value("info[{$c}][name]", isset($info[$c]['name']) ? $info[$c]['name'] : ''); ?>" <?php echo form_error("info[{$c}][name]") ? 'title="'.form_error("info[{$c}][name]").'"' : ''; ?>/>
                            <span class="inline-help gray"><?php echo strtoupper($l['locale']) ?></span>
                        </div>
                    </div>
                    
                    <div class="control-group <?php echo form_error("info[{$c}][description]") ? 'error' : ''; ?>">
                        <?php echo form_label(lang('ed_description'), "info_{$c}_description", array('class' => "control-label") ); ?>
                        <div class='controls'>
                            <textarea id="info_<?php echo $c; ?>_description" name="info[<?php echo $c; ?>][description]" class="span4" rows="3" <?php echo form_error("info[{$c}][description]") ? 'title="'.form_error("info[{$c}][description]").'"' : ''; ?>><?php echo set_value("info[{$c}][description]", isset($info[$c]['description']) ? $info[$c]['description'] : ''); ?></textarea>
                            <span class="inline-help gray"><?php echo strtoupper($l['locale']); ?></span>
                        </div>
                    </div>
                
                </div>
                <?php $i++; endforeach; ?>
            </div>
        </div>
        
        <div class="form-buttons">
            <?php echo anchor($back_url, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>                    
            <button type="reset" name="reset" class="btn" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
            
            <?php if(isset($record)) :?>
            <?php if ($this->auth->has_permission('Shipping_Methods.Catalog.Delete')) : ?>
            <button type="submit" name="delete" class="btn" id="delete-me" onclick="return confirm('<?php echo lang('shipping_methods_delete_confirm'); ?>')">
                <i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete'); ?>
            </button>
            <?php endif; ?>
            <?php endif; ?>
            
            <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
            <button type="submit" name="save_continue" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save_continue'); ?></button>&nbsp;
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>
