<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class catalog extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Shipping_Methods.Catalog.View');
		$this->load->model('shipping_methods_model', null, true);
		$this->lang->load('shipping_methods');
        
        if(!$this->session->userdata('shipping_index'))
        {
            $this->session->set_userdata('shipping_index', site_url(SITE_AREA .'/catalog/shipping_methods'));
        }
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index()
	{
		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->shipping_methods_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('shipping_methods_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('shipping_methods_delete_failure') . $this->shipping_methods_model->error, 'error');
				}
			}
		}
        
        //get order fields
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        $orders = array();
        switch($params['orderby']) {
            case "name":
            case "price":
            case "sort_order":
            case "url":
            case "status":
                $orders[$params['orderby']]   = $params['order'];
                break;

            default:
                $orders["shipping_methods.method_id"]     = "asc";
                break;
        }

        $total_records = $this->shipping_methods_model
            ->count_all();
            
		$records = $this->shipping_methods_model
            ->order_by($orders)
            ->find_all(1);
		Template::set('records', $records);
        
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/catalog/shipping_methods/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/catalog/shipping_methods') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);
        
        // set request page                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('shipping_index', $url);
        
		Template::set('toolbar_title', lang('shipping_methods_manage'));
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: create()

		Creates a Shipping Methods object.
	*/
	public function create()
	{
		$this->auth->restrict('Shipping_Methods.Catalog.Create');

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			if ($insert_id = $this->save_method())
			{
				Template::set_message(lang('shipping_methods_create_success'), 'success');
				
                if ($this->input->post('save'))
                {
                    $this->redirect_to_index();
                }
                else
                {
                    Template::redirect(SITE_AREA .'/catalog/shipping_methods/edit/'.$insert_id);
                }
			}
			else
			{
				Template::set_message(lang('shipping_methods_create_failure') . $this->shipping_methods_model->error, 'error');
			}
		}
		Assets::add_module_js('shipping_methods', 'shipping_methods.js');

        Template::set('prev_page', $this->session->userdata('shipping_index'));
		Template::set('toolbar_title', lang('shipping_methods_create'));
        Template::set_view('shipping_methods/catalog/form');
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Shipping Methods data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('shipping_methods_invalid_id'), 'error');
			$this->redirect_to_index();
		}

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			$this->auth->restrict('Shipping_Methods.Catalog.Edit');

			if ($this->save_method('update', $id))
			{
				Template::set_message(lang('shipping_methods_edit_success'), 'success');
                
                if($this->input->post('save'))
                {
                    $this->redirect_to_index();
                }
			}
			else
			{
				Template::set_message(lang('shipping_methods_edit_failure') . $this->shipping_methods_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Shipping_Methods.Catalog.Delete');

			if ($this->shipping_methods_model->delete($id))
			{
				Template::set_message(lang('shipping_methods_delete_success'), 'success');

				$this->redirect_to_index();
			} 
            else
			{
				Template::set_message(lang('shipping_methods_delete_failure') . $this->shipping_methods_model->error, 'error');
			}
		}
        Assets::add_module_js('shipping_methods', 'shipping_methods.js');
        
        $record = $this->shipping_methods_model->find($id, 1);
        if (empty($record))
        {
            Template::set_message(lang('shipping_methods_invalid_id'), 'error');
            $this->redirect_to_index();
        }
		Template::set('record', $record);
		
        Template::set('prev_page', $this->session->userdata('shipping_index'));
		Template::set('toolbar_title', lang('shipping_methods_edit'));
        Template::set_view('shipping_methods/catalog/form');
		Template::render();
	}
    
    public function delete()
    {
        $id = $this->uri->segment(5);

        if (empty($id))
        {
            Template::set_message(lang('shipping_methods_invalid_id'), 'error');
            
            $this->redirect_to_index();
        }
        
        $this->auth->restrict('Shipping_Methods.Catalog.Delete');
        
        if ($this->products_model->delete($id))
        {
            Template::set_message(lang('shipping_methods_delete_success'), 'success');
            
            $this->redirect_to_index();
        }
    }

	//--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_method()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_method($type='insert', $id=0)
	{
        $_POST['sort_order']= is_numeric($this->input->post('sort_order'))?$this->input->post('sort_order')==0?'1':$this->input->post('sort_order'):'1';
        $_POST['url']       = prep_url($this->input->post('url'));
        
        //set rules for manufacturer attributes
        $languages = $this->languages_model->find_all(1);
        foreach($languages as $l)
        {
            $c = $l['language_id'];
            
            $this->form_validation->set_rules("info[{$c}][name]", 'lang:ed_name', 'required|trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][description]", 'lang:ed_description', 'trim|max_length[255]|strip_tags|xss_clean');
        }
         
		$this->form_validation->set_rules('price',lang('ed_price'),'required|trim|is_numeric');
		$this->form_validation->set_rules('sort_order',lang('ed_sort_order'),'trim|is_numeric|max_length[3]');
		$this->form_validation->set_rules('url',lang('ed_url'),'trim|max_length[255]');
		$this->form_validation->set_rules('status',lang('ed_status'),'trim|is_numeric|max_length[1]');

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
        if($id != 0) 
        {
            $data['method_id']= $id;
        }
        
		$data['price']          = $this->input->post('price');
		$data['sort_order']     = $this->input->post('sort_order');
		$data['url']            = prep_url($this->input->post('url'));
		$data['status']         = $this->input->post('status')==1?1:0;

		$langs = $this->input->post('info');
        return $this->shipping_methods_model->save($data, $langs);
	}

    private function redirect_to_index()
    {
        $redirect_url = $this->session->userdata('shipping_index')?$this->session->userdata('shipping_index'):SITE_AREA .'/catalog/shipping_methods';
        Template::redirect($redirect_url);
    }
	//--------------------------------------------------------------------



}