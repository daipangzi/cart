<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['shipping_methods_manage']            = 'Shipping Methods';
$lang['shipping_methods_edit']				= 'Edit Method';
$lang['shipping_methods_create']			= 'New Method';
$lang['shipping_methods_list']				= 'Shipping Methods';
$lang['shipping_methods_create_success']			= 'Shipping Method successfully created.';
$lang['shipping_methods_create_failure']			= 'There was a problem creating the Shipping Method: ';
$lang['shipping_methods_invalid_id']			= 'Invalid Shipping Method ID.';
$lang['shipping_methods_edit_success']			= 'Shipping Method successfully saved.';
$lang['shipping_methods_edit_failure']			= 'There was a problem saving the Shipping Method: ';
$lang['shipping_methods_delete_success']			= 'record(s) successfully deleted.';
$lang['shipping_methods_delete_failure']			= 'We could not delete the record: ';
$lang['shipping_methods_delete_error']			= 'You have not selected any records to delete.';
$lang['shipping_methods_delete_confirm']			= 'Are you sure you want to delete this Shipping Methods?';

$lang['shipping_method_detail'] = 'Shipping Method Detail'; 
$lang['shipping_method_new']    = 'New Shipping Method'; 