<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_shipping_methods extends Migration {

	public function up()
	{
		$prefix = $this->db->dbprefix;

		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				
			),
			'price' => array(
				'type' => 'VARCHAR',
				'constraint' => 15,2,
				
			),
			'sort_order' => array(
				'type' => 'INT',
				'constraint' => 3,
				
			),
			'description' => array(
				'type' => 'TEXT',
				
			),
			'url' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				
			),
			'status' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('shipping_methods');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('shipping_methods');

	}

	//--------------------------------------------------------------------

}