<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shipping_methods_model extends BF_Model {

	protected $table		= "shipping_methods";
	protected $key			= "method_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= false;
	protected $set_modified = false;
    
    private $is_admin   = false;
    
    var $CI;  
    
    function __construct() 
    {
        $this->CI =& get_instance();
                  
        $this->is_admin = is_backend();
    }
    
    function is_admin()
    {
        $this->is_admin = true;   
        return $this; 
    }
    
    function init_before_find_all()
    {
        //set filter
        if(!$this->is_admin)
        {
            parent::where('status', 1);            
        }
        
        return $this;    
    }
    
    function get_name($id)
    {
        if(!$this->is_valid($id)) return '';
        
        $lang_info = $this->get_language_info($id, global_language_id());
        return $lang_info['name'];
    }
    
    /**
    * find shipping method
    * 
    * @param mixed $id
    */
    function find($id, $return_type=0)
    {
        $record = parent::find($id, $return_type);
        
        //if cann't find record
        if(empty($record)) 
        {
            return FALSE;
        }

        if($this->is_admin)
        {
            if($return_type == 0)
            {
                $record->info   = $this->get_all_language_info($id);
            }
            else
            {
                $record['info'] = $this->get_all_language_info($id);
            }
        }
        else
        {
            //if status disabled
            $temp = (array)$record;
            if($temp['status'] == 0)
            {
                return FALSE;
            }
            
            if($return_type == 0)
            {       
                $record  = (object)array_merge((array)$record, $this->get_language_info($id, global_language_id()));
            }
            else
            {
                $record  = array_merge($r, $this->get_language_info($id, global_language_id()));
            }  
        }
        
        return $record;
    }
    
    function find_all($return_type=0)
    {
        $language_id = global_language_id();
        
        $this->init_before_find_all()
            ->join('shipping_methods_info si', "si.method_id=shipping_methods.method_id AND si.language_id={$language_id}", 'left');
        
        return parent::find_all($return_type);
    }
    
    /**
    * delete method and language info
    * 
    * @param mixed $manufacturer_id
    * @return bool
    */
    function delete($id) {
        // delete accessories
        $this->db->where('method_id', $id)->delete('shipping_methods_info');
        
        return parent::delete($id);        
    }
    
    /**
    * save shipping methods and language info
    * 
    * @param mixed $data
    * @param mixed $langs
    */
    function save($data, $langs) {
        $id = FALSE;
        if(!isset($data[$this->key]))
        {
            $id = $this->insert($data);
        }
        else  
        {
            $id = $data[$this->key];
            
            $this->update($id, $data);
        }
        
        if($id !== FALSE)
        {
            foreach($langs as $c => $d)
            {
                $info = array();
                $info['method_id']   = $id;
                $info['language_id'] = $c;
                $info['name']        = $d['name'];
                $info['description'] = $d['description'];
                
                if(!$this->check_language_row($id, $c))
                {
                    $this->db->insert('shipping_methods_info', $info);
                }
                else
                {
                    $where = array();
                    $where['method_id']     = $id;
                    $where['language_id']   = $c;
                    
                    $this->db->where($where)->update('shipping_methods_info', $info);
                }
            }   
        }
        
        return $id;
    }
             
    /**
    * check method is valid
    * 
    * @param mixed $id
    */
    function is_valid($id)
    {
        $record = $this->find($id);
        
        if(empty($record))
        {
            return FALSE;
        }
        
        if($record->status == 0)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    //--------------------------------------------------------------------
    // !LANGAGE METHODS
    //--------------------------------------------------------------------
    /**
    * check if row is exist with category and lanuage id
    * 
    * @param mixed $manufacturer_id
    * @param mixed $language_id
    * @return mixed
    */
    private function check_language_row($id, $language_id)
    {
        $where = array();
        $where['method_id']   = $id;
        $where['language_id'] = $language_id;
        
        return (bool)$this->db->where($where)->count_all_results('shipping_methods_info');
    }  
    
    /**
    * get all lanuage info
    * 
    * @param mixed $category_id
    */
    private function get_all_language_info($id)
    {
        $languages    = $this->CI->languages_model->find_all(1);
        
        $result = array();
        foreach($languages as $lang)
        {
            $l = $lang['language_id'];
            
            $record = $this->get_language_info($id, $l);
            
            $result[$l] = $record;
        }
        
        return $result;
    } 
    
    /**
    * get one lanuage info
    * 
    * @param mixed $category_id
    * @param mixed $language_id
    */
    private function get_language_info($id, $language_id)
    {
        $default_lang = get_language_id_by_code(settings_item('site.default_language'));   
        
        $record = $this->db
                    ->where(array('method_id' => $id, 'language_id' => $language_id))
                    ->get('shipping_methods_info')
                    ->row_array();
         
        if(empty($record))
        {
            $record = $this->db
                    ->where(array('method_id' => $id, 'language_id' => $default_lang))
                    ->get('shipping_methods_info')
                    ->row_array();
        } 
        
        return $record;
    }
}
