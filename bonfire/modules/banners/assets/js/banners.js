//--------------------------------------------------------------------   
$(document).ready(function() {    
    event = $.browser.msie?'click':'change'; 
    $('.banner_image').live(event, function() { 
        index = $(this).attr('index');
        
        $("#errors"+index).html('');
        $("#loading"+index).html('<img src="'+theme_path+'/admin/images/loaders/circular/054.gif" alt="Uploading...."/>');
        $('#image_index').val(index);
        $("#banner_form").ajaxForm({
            url: admin_path+'/content/banners/upload_banner_image',
            success: function(responseText)  { 
                index = $('#image_index').val();
                
                json = $.parseJSON(responseText);
                if(json.image) {
                    $("#bannerImage"+index).attr("src", base_path+"images/"+json.image+"?type=tmp/banner&width=300&height=150&force=yes");
                    $("#errors"+index).html("");
                    $("#loading"+index).html('');
                    $('#remover'+index).show();
                    $("#image_name"+index).val(json.image);
                    $("#image_action"+index).val("changed"); 
                } else {
                    $("#loading"+index).html("");
                    $("#errors"+index).html(json.errors);
                }
                //$('#manufacturer_form').unbind('submit').find('input:submit,input:image,button:submit').unbind('click'); 
                $('#banner_form').unbind('submit'); 
            },
        }).submit();
    }); 
    
    $('.remover').live('click', function() {
        index = $(this).attr('index');
        $("#bannerImage"+index).attr("src", media_path+"placeholder/300x150.gif");  
        $("#image_name"+index).val(''); 
        $("#image_action"+index).val("deleted"); 
        $(this).hide();
    });
});