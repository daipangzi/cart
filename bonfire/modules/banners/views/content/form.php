<?php // Change the css classes to suit your needs
$title = lang('banners_create');
$back_url = isset($prev_page)?$prev_page:SITE_AREA .'/content/banners';
if(isset($record)) {
    $info = $record['info'];    
    $title = $record['info'][global_language_id()]['title'] != ''?$record['info'][global_language_id()]['title']:lang('banner_detail');
}
?>

<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'id="banner_form" class="form-horizontal" autocomplete="off"'); ?>
    <input type="hidden" id="image_index" name="image_index" value=""/>
    
    <fieldset>
        <legend><?php echo $title; ?></legend>
        
        <div class="form-buttons">
            <?php echo anchor($back_url, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>                    
            <button type="reset" name="reset" class="btn" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
            
            <?php if(isset($record)) :?>
            <?php if ($this->auth->has_permission('Pages.Content.Delete')) : ?>
            <button type="submit" name="delete" class="btn" id="delete-me" onclick="return confirm('<?php echo lang('page_delete_confirm'); ?>')">
                <i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete'); ?>
            </button>
            <?php endif; ?>
            <?php endif; ?>
            
            <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
            <button type="submit" name="save_continue" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save_continue'); ?></button>&nbsp;
        </div>
        
        <div class="control-group <?php echo form_error('sort_order') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_sort_order'), 'sort_order', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="sort_order" type="text" name="sort_order" maxlength="3" class="span2" value="<?php echo set_value('sort_order', isset($record['sort_order']) ? $record['sort_order'] : '1'); ?>"  />
                <span class="help-inline"><?php echo form_error('sort_order'); ?></span>
            </div>
        </div>
        
        <?php echo form_dropdown('status', array("1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value('status', isset($record['status']) ? $record['status'] : '1'), lang('ed_status'), 'id="status" class="nostyle"'); ?>
        
        <div class="subtab">
            <ul id="myTab1" class="nav nav-tabs">
                <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                <li <?php if($i==0){echo 'class="active"';} ?>><a href="#<?php echo $cd; ?>" data-toggle="tab"><?php echo $l['name']; ?></a></li>
                <?php $i++; endforeach; ?>
            </ul>   
            
            <div class="tab-content">
                <?php 
                $i=0; 
                foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; 
                    $image_name = '';
                    $img = isset($info)?$info[$c]['image']:'';
                    $img_path = isset($info)?'banner':'tmp/banner';      
                ?>
                <div class="tab-pane fade in <?php if($i==0){echo 'active';} ?>" id="<?php echo $cd; ?>">
                    <input type="hidden" name="info[<?php echo $c; ?>][image_name]" id="image_name<?php echo $c; ?>" value="<?php echo set_value('image_name', $image_name); ?>"/>
                    <input type="hidden" name="info[<?php echo $c; ?>][image_action]" id="image_action<?php echo $c; ?>" value=""/>
        
                    <div class="control-group <?php echo form_error("info[{$c}][title]") ? 'error' : ''; ?>">
                        <?php echo form_label(lang('ed_title'), "info_{$c}_title", array('class' => "control-label") ); ?>
                        <div class='controls'>
                            <input id="info_<?php echo $c; ?>_title" type="text" name="info[<?php echo $c; ?>][title]" class="span6" maxlength="255" value="<?php echo set_value("info[{$c}][title]", isset($info[$c]['title']) ? $info[$c]['title'] : ''); ?>" <?php echo form_error("info[{$c}][title]") ? 'title="'.form_error("info[{$c}][title]").'"' : ''; ?>/>
                            <span class="help-inline"><?php echo form_error("info[{$c}][title]"); ?></span>
                        </div>
                    </div>
                    
                    <div class="control-group <?php echo form_error("info[{$c}][url]") ? 'error' : ''; ?>">
                        <?php echo form_label(lang('ed_url'), "info_{$c}_url", array('class' => "control-label") ); ?>
                        <div class='controls'>
                            <input id="info_<?php echo $c; ?>_url" type="text" name="info[<?php echo $c; ?>][url]" class="span6" maxlength="255" value="<?php echo set_value("info[{$c}][url]", isset($info[$c]['url']) ? $info[$c]['url'] : ''); ?>" <?php echo form_error("info[{$c}][url]") ? 'title="'.form_error("info[{$c}][url]").'"' : ''; ?>/>
                            <span class="help-inline"><?php echo form_error("info[{$c}][url]"); ?></span>
                        </div>
                    </div>
                    
                    <div class="control-group <?php echo form_error("info[{$c}][description]") ? 'error' : ''; ?>">
                        <?php echo form_label(lang('ed_description'), "info_{$c}_description", array('class' => "control-label") ); ?>
                        <div class='controls'>
                            <div class="span8 noLeftMargin">
                                <textarea id="info_<?php echo $c; ?>_description" name="info[<?php echo $c; ?>][description]" class="span6" rows="5" <?php echo form_error("info[{$c}][description]") ? 'title="'.form_error("info[{$c}][description]").'"' : ''; ?>><?php echo set_value("info[{$c}][description]", isset($info[$c]['description']) ? $info[$c]['description'] : ''); ?></textarea>               
                                <span class="help-inline"><?php echo form_error("info[{$c}][description]"); ?></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <?php echo form_label(lang('ed_image'), "info_{$c}_banner_image", array('class' => "control-label") ); ?>
                        <div class='controls'>     
                            <input type="file" name="banner_image<?php echo $c; ?>" id="info_<?php echo $c; ?>banner_image" class="banner_image" value="" index="<?php echo $c; ?>"/>
                            <span id="loading<?php echo $c; ?>" class="help-inline"></span>
                            <span id="errors<?php echo $c; ?>" class="help-inline" style="color:#B94A48;"></span><br/><br/>
                            
                            <span class="imgwrapper"><span class="image marginB10">
                                <img src="<?php echo media_file($img, $img_path, 300, 150); ?>" alt="" id="bannerImage<?php echo $c; ?>" class=""/>
                                <span id="remover<?php echo $c; ?>" class="remove remover" <?php if($img == '' || $img == 'deleted') echo 'style="display:none;"';?> index="<?php echo $c; ?>"><i class="icon-remove">&nbsp;</i></span>
                            </span></span>
                        </div>
                    </div>
                    
                </div>
                <?php $i++; endforeach; ?>
            </div>
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>
