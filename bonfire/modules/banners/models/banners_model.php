<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Banners_model extends BF_Model {

	protected $table		= "banners";
	protected $key			= "banner_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= false;
	protected $set_modified = false;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";
    
    var $CI;  
    
    function __construct() 
    {
        $this->CI =& get_instance();
                  
        $this->is_admin = is_backend();
    }
    
    function init_before_find_all()
    {
        //set orderby
        if(empty($this->orderby))
        {
            parent::order_by('sort_order');
        }
        
        //set filter
        if(!$this->is_admin)
        {
            parent::where('status', 1);            
        }
        
        return $this;    
    }
    
    function find($id, $return_type=0)
    {
        $record = parent::find($id, $return_type);
        
        //if cann't find record
        if(empty($record)) 
        {
            return FALSE;
        }

        if($this->is_admin)
        {
            if($return_type == 0)
            {
                $record->info = $this->get_all_language_info($id);    
            }
            else
            {
                $record['info'] = $this->get_all_language_info($id);
            }
        }
        else
        {
            //if status disabled
            $temp = (array)$record;
            if($temp['status'] == 0)
            {
                return FALSE;
            }
            
            $record = array_merge($record, $this->get_language_info($id, global_language_id()));    
        }
        return $record;
    }

    
    /**
    * count all enabled records 
    * 
    */
    function count_all()
    {
        $language_id = global_language_id();
        
        $this->init_before_find_all()
            ->join('banners_info mi', "mi.banner_id=banners.banner_id AND mi.language_id={$language_id}", 'left');
        
        return parent::count_all();
    }
    
    /**
    * find all records with current language
    * 
    * @param mixed $mode : 0: normal, 1: dropdown
    */
    function find_all($return_type=0)
    {
        $language_id = global_language_id();
        
        $this->init_before_find_all()
            ->join('banners_info mi', "mi.banner_id=banners.banner_id AND mi.language_id={$language_id}", 'left');
        
        return parent::find_all($return_type);
    }
    
    /**
    * save page and language info
    * 
    * @param mixed $data
    * @param mixed $langs
    */
    function save($data, $langs) {
        $id = FALSE;
        if(!isset($data[$this->key]))
        {
            $id = $this->insert($data);
        }
        else  
        {
            $id = $data[$this->key];
            
            $this->update($id, $data);
        }
        
        if($id !== FALSE)
        {
            foreach($langs as $c => $d)
            {
                $info = array();
                $info['banner_id']      = $id;
                $info['language_id']    = $c;
                $info['title']          = $d['title'];
                $info['description']    = $d['description'];
                $info['url']            = $d['url'];
                if(isset($d['image']))
                {
                    $info['image']      = $d['image'];
                }
                
                if(!$this->check_language_row($id, $c))
                {
                    $this->db->insert('banners_info', $info);
                }
                else
                {
                    $where = array();
                    $where['banner_id']       = $id;
                    $where['language_id']   = $c;
                    
                    $this->db->where($where)->update('banners_info', $info);
                }
            }   
        }
        
        return $id;
    }
    
    /**
    * delete page and language info
    * 
    * @param mixed $id
    * @return bool
    */
    function delete($id) {
        // delete accessories
        $this->db->where('banner_id', $id)->delete('banners_info');
        
        return parent::delete($id);        
    }
    
    //--------------------------------------------------------------------
    // !LANGAGE METHODS
    //--------------------------------------------------------------------
    /**
    * check if row is exist with category and lanuage id
    * 
    * @param mixed page_id
    * @param mixed $language_id
    * @return mixed
    */
    private function check_language_row($id, $language_id)
    {
        $where = array();
        $where['banner_id']       = $id;
        $where['language_id']   = $language_id;
        
        return (bool)$this->db->where($where)->count_all_results('banners_info');
    }  
    
    /**
    * get all lanuage info
    * 
    * @param mixed page_id
    */
    private function get_all_language_info($id)
    {
        $languages    = $this->CI->languages_model->find_all(1);
        
        $result = array();
        foreach($languages as $lang)
        {
            $l = $lang['language_id'];
            
            $record = $this->get_language_info($id, $l);
            
            $result[$l] = $record;
        }
        
        return $result;
    } 
    
    /**
    * get one lanuage info
    * 
    * @param mixed $category_id
    * @param mixed $language_id
    */
    private function get_language_info($id, $language_id)
    {
        $default_lang = get_language_id_by_code(settings_item('site.default_language'));   
        
        $record = $this->db
                    ->where(array('banner_id' => $id, 'language_id' => $language_id))
                    ->get('banners_info')
                    ->row_array();
         
        if(empty($record))
        {
            $record = $this->db
                    ->where(array('banner_id' => $id, 'language_id' => $default_lang))
                    ->get('banners_info')
                    ->row_array();
        } 
        
        return $record;
    }
}
