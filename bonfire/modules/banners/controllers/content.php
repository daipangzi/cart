<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class content extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Banners.Content.View');
		$this->load->model('banners_model', null, true);
		$this->lang->load('banners');   
        
        Assets::add_js('jquery.form.js');

        $config = init_upload_config('banner');   
        $this->upload->initialize($config);     
        
        if(!$this->session->userdata('banners_index'))
        {
            $this->session->set_userdata('banners_index', site_url(SITE_AREA .'/content/banners'));
        }  
	}
	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->banners_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('banners_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('banners_delete_failure') . $this->banners_model->error, 'error');
				}
			}
		}
        
        //get order fields
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        //get where fields
        $where = array();  
        
        $orders = array();
        switch($params['orderby']) {
            case "title":
            case "sort_order":
            case "url":
            case "status":
                $orders[$params['orderby']] = $params['order'];
                break;

            default:
                $orders["banners.banner_id"]  = "desc";
                break;
        }

		//get total counts
        $this->banners_model->where($where);
        $total_records = $this->banners_model->count_all();
                       
        //get total records
        $records = $this->banners_model
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all();
        Template::set('records', $records);
        
        //build pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/content/banners/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/content/banners') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);
        
        // set request page  
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('banners_index', $url);                                   
        
        Template::set('toolbar_title', lang('banners_list'));
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: create()

		Creates a Pages object.
	*/
	public function create()
	{
		$this->auth->restrict('Banners.Content.Create');

		if ($this->input->post('save') || $this->input->post('save_continue'))    
		{
			if ($insert_id = $this->save_banner())
			{
				Template::set_message(lang('banners_create_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
                else Template::redirect(SITE_AREA .'/content/banners/edit/'.$insert_id);
			}
			else
			{
				Template::set_message(lang('banners_create_failure') . $this->banners_model->error, 'error');
			}
		}
		Assets::add_module_js('banners', 'banners.js');
        
        Template::set('prev_page', $this->session->userdata('banners_index'));
		Template::set('toolbar_title', lang('banners_create'));
        Template::set_view('banners/content/form');
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: edit()

		Allows editing of Pages data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('banners_invalid_id'), 'error');
			redirect(SITE_AREA .'/content/banners');
		}

		if (isset($_POST['save']) || isset($_POST['save_continue']))
		{
			$this->auth->restrict('Banners.Content.Edit');

			if ($this->save_banner('update', $id))
			{
				Template::set_message(lang('banners_edit_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
			}
			else
			{
				Template::set_message(lang('banners_edit_failure') . $this->banners_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Banners.Content.Delete');

			if ($this->banners_model->delete($id))
			{
				Template::set_message(lang('banner_delete_success'), 'success');

				$this->redirect_to_index();
			} else
			{
				Template::set_message(lang('banners_delete_failure') . $this->banners_model->error, 'error');
			}
		}
        Assets::add_module_js('banners', 'banners.js');
        
        $record = $this->banners_model->find($id, 1);
		Template::set('record', $record);
        
        Template::set('prev_page', $this->session->userdata('banners_index'));
		Template::set('toolbar_title', lang('banners_edit'));
        Template::set_view('banners/content/form');
		Template::render();
	}

    public function delete()
    {
        $id = $this->uri->segment(5);

        if (empty($id))
        {
            Template::set_message(lang('banners_invalid_id'), 'error');
            $this->redirect_to_index();
        }
        
        $this->auth->restrict('Banners.Content.Delete');
        
        if ($this->banners_model->delete($id))
        {
            Template::set_message(lang('banners_delete_success'), 'success');

            $this->redirect_to_index();
        }
    }
	//--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_banner()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_banner($type='insert', $id=0)
	{
        //set rules for manufacturer attributes
        $languages = $this->languages_model->find_all(1);
        foreach($languages as $l)
        {
            $c = $l['language_id'];
            
            $this->form_validation->set_rules("info[{$c}][title]", 'lang:ed_title', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][description]", 'lang:ed_description', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][url]", 'lang:ed_url', 'trim|max_length[255]|strip_tags|xss_clean|prep_url');
        }
        $this->form_validation->set_rules('sort_order', 'lang:ed_sort_order','trim|is_numeric|max_length[3]|strip_tags|xss_clean');
        
		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
		$data = array();
        if($id != 0) 
        {
            $data['banner_id']= $id;
        }
        $data['sort_order'] = $this->input->post('sort_order');
		$data['status']     = $this->input->post('status')?'1':'0';

		$langs = $this->input->post('info');
        foreach($langs as $lc => $ld)
        {
            $action = $ld['image_action'];
            $name   = $ld['image_name'];
            if($action == 'changed') { 
                if($name != '') { 
                    $file_name = $this->get_moved_image_name($name, 'banner');
                    if($file_name !== FALSE)
                    {
                        $langs[$lc]['image'] = $file_name;
                    }
                }
            } else if($action == 'deleted') {
                $langs[$lc]['image'] = '';
            }
        }
        
        return $this->banners_model->save($data, $langs);
	}

	//--------------------------------------------------------------------
    private function redirect_to_index()
    {
        $redirect_url = $this->session->userdata('banners_index')?$this->session->userdata('banners_index'):SITE_AREA .'/content/banners';
        Template::redirect($redirect_url);
    }

    //--------------------------------------------------------------------
    // !AJAX METHODS
    //--------------------------------------------------------------------
    public function upload_banner_image() {
        $index = $this->input->post('image_index');
        $result = $this->upload_image("banner_image".$index);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;     
    }
}