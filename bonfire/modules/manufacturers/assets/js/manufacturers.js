//--------------------------------------------------------------------     
event = $.browser.msie?'click':'change'; 
$('#manufacturer_image').live(event, function() { 
    $("#errors").html('');
    $("#loading").html('<img src="'+theme_path+'/admin/images/loaders/circular/054.gif" alt="Uploading...."/>');
    $("#manufacturer_form").ajaxForm({
        url: admin_path+'/catalog/manufacturers/upload_manufacturer_image',
        success: function(responseText) {
            json = $.parseJSON(responseText);
            if(json.image) {
                $("#manufacturerImage").attr("src", base_path+"images/"+json.image+"?type=tmp/manufacturer&width=120&height=60&force=yes");
                $("#errors").html("");
                $("#loading").html('');
                $('#remover').show();
                $("#image_name").val(json.image);
                $("#image_action").val("changed"); 
            } else {
                $("#loading").html("");
                $("#errors").html(json.errors);
            }
            //$('#manufacturer_form').unbind('submit').find('input:submit,input:image,button:submit').unbind('click'); 
            $('#manufacturer_form').unbind('submit'); 
        },
    }).submit();
}); 

$('#remover').live('click', function() {
    $("#manufacturerImage").attr("src", media_path+"placeholder/120x60.gif");  
    $("#image_name").val(''); 
    $("#image_action").val("deleted"); 
    $(this).hide();
});