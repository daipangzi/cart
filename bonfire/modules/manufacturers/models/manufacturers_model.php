<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manufacturers_model extends BF_Model {

	protected $table		= "manufacturers";
	protected $key			= "manufacturer_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= false;
	protected $set_modified = false;
    
    private $orderby    = array();
    private $is_admin   = false;
    
    var $CI;  
    
    function __construct() 
    {
        $this->CI =& get_instance();
                  
        $this->is_admin = is_backend();
    }
           
    function init_before_find_all()
    {
        //set orderby
        if(empty($this->orderby))
        {
            parent::order_by('sort_order');
        }
        else
        {
            $this->order_by($this->orderby);
        }
        
        //set filter
        if(!$this->is_admin)
        {
            parent::where('status', 1);            
        }
        
        return $this;    
    }
    
    /**
    * find row by id
    * 
    * @param mixed $id
    */
    function find($id, $return_type=0)
    {
        $record = parent::find($id, $return_type);
        
        //if cann't find record
        if(empty($record)) 
        {
            return FALSE;
        }

        if($this->is_admin)
        {
            if($return_type == 0)
            {
                $record->info = $this->get_all_language_info($id);    
            }
            else
            {
                $record['info'] = $this->get_all_language_info($id);
            }
        }
        else
        {
            //if status disabled
            $temp = (array)$record;
            if($temp['status'] == 0)
            {
                return FALSE;
            }
            
            $record = array_merge($record, $this->get_language_info($id, global_language_id()));    
        }
        
        return $record;
    }
    
    /**
    * find manufacturer by slug
    * 
    * @param mixed $slug
    */
    function find_by_slug($slug)
    {
        $record = $this->find_by('slug', $slug);
        
        if(empty($record)) 
        {
            return FALSE;
        }
        
        return $this->find($record->manufacturer_id);
    }
    
    /**
    * count all enabled records 
    * 
    */
    function count_all()
    {
        $language_id = global_language_id();
        
        $this->init_before_find_all()
            ->join('manufacturers_info mi', "mi.manufacturer_id=manufacturers.manufacturer_id AND mi.language_id={$language_id}", 'left');
        
        return parent::count_all();
    }
    
    /**
    * find all records with current language
    * 
    * @param mixed $mode : 0: normal, 1: dropdown
    */
    function find_all($return_type=0)
    {
        $language_id = global_language_id();
        
        $this->init_before_find_all()
            ->join('manufacturers_info mi', "mi.manufacturer_id=manufacturers.manufacturer_id AND mi.language_id={$language_id}", 'left');
        
        return parent::find_all($return_type);
    }
    
    /**
    * get result by dropdown mode
    * 
    * @param mixed $empty_row
    * @param mixed $empty_text
    */
    function dropdown_list($empty_row=true, $empty_text='')
    {
        $records     = $this->find_all(1);
        
        $result = array();
        if(!empty($records))
        {
            if($empty_row)
            {
                $result[''] = $empty_text;
            }
            
            foreach($records as $r)
            {
                $id = $r['manufacturer_id'];
                
                $result[$id] = $r['name'];
            }    
        }
        
        return $result;
    }
    
    /**
    * save manufacturer and language info
    * 
    * @param mixed $data
    * @param mixed $langs
    */
    function save($data, $langs) {
        $id = FALSE;
        if(!isset($data[$this->key]))
        {
            $id = $this->insert($data);
        }
        else  
        {
            $id = $data[$this->key];
            
            $this->update($id, $data);
        }
        
        if($id !== FALSE)
        {
            foreach($langs as $c => $d)
            {
                $info = array();
                $info['manufacturer_id']= $id;
                $info['language_id']    = $c;
                $info['name']           = $d['name'];
                $info['page_title']     = $d['page_title'];
                $info['meta_keywords']  = $d['meta_keywords'];
                $info['meta_description'] = $d['meta_description'];
                
                if(!$this->check_language_row($id, $c))
                {
                    $this->db->insert('manufacturers_info', $info);
                }
                else
                {
                    $where = array();
                    $where['manufacturer_id'] = $id;
                    $where['language_id']     = $c;
                    
                    $this->db->where($where)->update('manufacturers_info', $info);
                }
            }   
        }
        
        return $id;
    }
    
    /**
    * delete manufacturer and language info
    * 
    * @param mixed $id
    * @return bool
    */
    function delete($id) {
        // delete accessories
        $this->db->where('manufacturer_id', $id)->delete('manufacturers_info');
        
        return parent::delete($id);        
    }
    
    //--------------------------------------------------------------------
    // !COMMON METHODS
    //--------------------------------------------------------------------
    
    /**
    * check if the slug is exist
    * 
    * @param mixed $slug
    * @param mixed $id
    * @return mixed
    */
    private function check_slug($slug, $id=false)
    {
        if($id)
        {
            $this->where('manufacturer_id !=', $id);
        }
        $this->where('slug', $slug);
        
        return (bool) parent::count_all();
    }
    
    /**
    * check and valid slug for manufacturer
    * 
    * @param mixed $slug
    * @param mixed $id
    * @param mixed $count
    */
    function validate_slug($slug, $id=false, $count=false)
    {
        if($slug == '') return '';
        
        if($this->check_slug($slug.$count, $id))
        {
            if(!$count)
            {
                $count = 1;
            }
            else
            {
                $count++;
            }
            return $this->validate_slug($slug, $id, $count);
        }
        else
        {
            return $slug.$count;
        }
    }
    
    //--------------------------------------------------------------------
    // !LANGAGE METHODS
    //--------------------------------------------------------------------
    /**
    * check if row is exist with category and lanuage id
    * 
    * @param mixed manufacturer_id
    * @param mixed $language_id
    * @return mixed
    */
    private function check_language_row($id, $language_id)
    {
        $where = array();
        $where['manufacturer_id']   = $id;
        $where['language_id']       = $language_id;
        
        return (bool)$this->db->where($where)->count_all_results('manufacturers_info');
    }  
    
    /**
    * get all lanuage info
    * 
    * @param mixed $category_id
    */
    private function get_all_language_info($id)
    {
        $languages    = $this->CI->languages_model->find_all(1);
        
        $result = array();
        foreach($languages as $lang)
        {
            $l = $lang['language_id'];
            
            $record = $this->get_language_info($id, $l);
            
            $result[$l] = $record;
        }
        
        return $result;
    } 
    
    /**
    * get one lanuage info
    * 
    * @param mixed $category_id
    * @param mixed $language_id
    */
    private function get_language_info($id, $language_id)
    {
        $default_lang = get_language_id_by_code(settings_item('site.default_language'));   
        
        $record = $this->db
                    ->where(array('manufacturer_id' => $id, 'language_id' => $language_id))
                    ->get('manufacturers_info')
                    ->row_array();
         
        if(empty($record))
        {
            $record = $this->db
                    ->where(array('manufacturer_id' => $id, 'language_id' => $default_lang))
                    ->get('manufacturers_info')
                    ->row_array();
        } 
        
        return $record;
    }
    
    //--------------------------------------------------------------------
    // !ORDER METHODS
    //--------------------------------------------------------------------
    function order_by_name($order='asc')
    {
        $this->order_by['name'] = $order;
        return $this;
    }
    
    function order_by_id($order='asc')
    {
        $this->order_by['manufacturers.manufacturer_id'] = $order;
        return $this;
    }
    
    function order_by_url($order='asc')
    {
        $this->order_by['url'] = $order;
        return $this;
    }
    
    function order_by_slug($order='asc')
    {
        $this->order_by['slug'] = $order;
        return $this;
    }
    
    function order_by_sort($order='asc')
    {
        $this->order_by['sort_order'] = $order;
        return $this;
    }
    
    function order_by_status($order='asc')
    {
        $this->order_by['status'] = $order;
        return $this;
    }
    
    //--------------------------------------------------------------------
    // !FILTER METHODS
    //--------------------------------------------------------------------
    
    /**
    * set flag if backend or frontend
    * 
    */
    function is_admin()
    {
        $this->is_admin = true;   
        return $this; 
    }
    
    function filter_status($status=1)
    {
        $this->where('status', $status);
        return $this;
    }
}
