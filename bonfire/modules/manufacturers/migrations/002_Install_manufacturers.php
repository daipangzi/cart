<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_manufacturers extends Migration {

	public function up()
	{
		$prefix = $this->db->dbprefix;

		$fields = array(
			'manufacturer_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			'url_key' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				
			),
			'sort_order' => array(
				'type' => 'VARCHAR',
				'constraint' => 3,
				
			),
			'deleted' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => '0',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('manufacturer_id', true);
		$this->dbforge->create_table('manufacturers');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('manufacturers');

	}

	//--------------------------------------------------------------------

}