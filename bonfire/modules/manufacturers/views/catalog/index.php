<div class="admin-box">
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset><legend><?php echo lang('manufacturer_list'); ?><small><?php echo $this->pagination->get_page_status();?></small></legend></fieldset>
    
    <div class="form-buttons">
        <?php if ($this->auth->has_permission('Manufacturers.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
        <button type="submit" name="delete" id="delete-me" class="btn" value="<?php echo lang('bf_action_delete') ?>" onclick="return confirm('<?php echo lang('manufacturers_delete_confirm'); ?>')"><span class="icon-remove-sign"></span>&nbsp;<?php echo lang('bf_action_delete') ?></button>            
        <?php endif;?>
        
        <?php if ($this->auth->has_permission('Manufacturers.Catalog.Create')) : ?>
        <?php echo anchor(SITE_AREA .'/catalog/manufacturers/create', '<span class="icon-plus-sign"></span>&nbsp;'.lang('bf_action_insert'), 'class="btn"'); ?>                    
        <?php endif;?>
    </div>
    
    <div class="responsive">
		<table class="table table-striped lrborder checkAll" id="flex_table" style="min-width:1000px;">
        <colgroup>
            <?php if ($this->auth->has_permission('Manufacturers.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
            <col width="40px"/>
            <?php endif;?>
            <col width="50"/>
            <col width=""/>
            <col width="150"/>
            <col width="150"/>
            <col width="200"/>
            <col width="150"/>
            <col width="120"/>
        </colgroup>
		<thead>
			<tr>
				<?php if ($this->auth->has_permission('Manufacturers.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
				<th class="column-check"><input class="check-all" type="checkbox" /></th>
				<?php endif;?>
                <th class="<?php echo sort_classes($params['orderby'], "id", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/catalog/manufacturers?orderby=id&amp;order='.sort_direction($params['orderby'], "id", $params['order'])); ?>"><span><?php echo lang('ed_id'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "name", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/catalog/manufacturers?orderby=name&amp;order='.sort_direction($params['orderby'], "name", $params['order'])); ?>"><span><?php echo lang('ed_name'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "sort_order", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/catalog/manufacturers?orderby=sort_order&amp;order='.sort_direction($params['orderby'], "sort_order", $params['order'])); ?>"><span><?php echo lang('ed_sort_order'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "slug", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/catalog/manufacturers?orderby=slug&amp;order='.sort_direction($params['orderby'], "slug", $params['order'])); ?>"><span><?php echo lang('ed_slug'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "url", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/catalog/manufacturers?orderby=url&amp;order='.sort_direction($params['orderby'], "url", $params['order'])); ?>"><span><?php echo lang('ed_url'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/catalog/manufacturers?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ed_status'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th><?php echo lang('ed_actions'); ?></th>
			</tr>
		</thead>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<?php if ($this->auth->has_permission('Manufacturers.Catalog.Delete')) : ?>
				<td>
                    <input type="checkbox" name="checked[]" value="<?php echo $record['manufacturer_id'] ?>" />
                </td>
				<?php endif;?>
			
                <td><?php echo $record['manufacturer_id'] ?></td>	
                
                <?php if ($this->auth->has_permission('Languages.Settings.Edit')) : ?>
                <td><a href="<?php echo site_url(SITE_AREA .'/catalog/manufacturers/edit/'.$record['manufacturer_id']).'/'.md5($record['manufacturer_id']) ?>"><?php echo $record['name'] ?></a></td>
                <?php else: ?>
			    <td><?php echo $record['name'] ?></td>
                <?php endif; ?>
                
			    <td><?php echo $record['sort_order'] ?></td>
                <td><?php echo $record['slug'] ?></td>
                <td><?php echo $record['url'] ?></td>
                <td><?php echo $record['status']==1?lang("ed_enable"):lang("ed_disable"); ?></td>
                <td>
                    <?php if ($this->auth->has_permission('Languages.Settings.Edit')) : ?>
                    <a href="<?php echo site_url(SITE_AREA .'/catalog/manufacturers/edit/'.$record['manufacturer_id'].'/'.md5($record['manufacturer_id'])) ?>" title="<?php echo lang('bf_action_edit'); ?>" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                    <?php endif; ?>
                    
                    <?php if ($this->auth->has_permission('Languages.Settings.Delete')) : ?>
                    <a href="<?php echo site_url(SITE_AREA .'/catalog/manufacturers/delete/'. $record['manufacturer_id'].'/'.md5($record['manufacturer_id'])) ?>" title="<?php echo lang('bf_action_delete'); ?>" class="tip"><span class="icon12 icomoon-icon-remove"></span></a>
                    <?php endif; ?>
                </td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="10"><?php echo lang('no_record'); ?></td>
			</tr>
		<?php endif; ?>
	    </tbody>
	    </table>
    </div>
	<?php echo form_close(); ?>
    <?php echo $this->pagination->create_links(); ?>
</div>