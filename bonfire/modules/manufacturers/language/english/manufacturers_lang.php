<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['manufacturers_manage']			    = 'Manufacturer Management';
$lang['manufacturers_edit']				    = 'Edit Manufacturer';
$lang['manufacturers_create']			    = 'New Manufacturer';
$lang['manufacturers_list']                 = 'Manufacturers';
$lang['manufacturers_create_success']		= 'Manufacturer successfully created.';
$lang['manufacturers_create_failure']		= 'There was a problem creating the manufacturer: ';
$lang['manufacturers_invalid_id']			= 'Invalid Manufacturer ID.';
$lang['manufacturers_edit_success']			= 'Manufacturer successfully saved.';
$lang['manufacturers_edit_failure']			= 'There was a problem saving the manufacturer: ';
$lang['manufacturers_delete_success']		= 'record(s) successfully deleted.';
$lang['manufacturers_delete_failure']		= 'We could not delete the record: ';
$lang['manufacturers_delete_error']			= 'You have not selected any records to delete.';
$lang['manufacturers_delete_confirm']       = 'Are you sure you want to delete this manufacturers?';
$lang['manufacturer_delete_confirm']		= 'Are you sure you want to delete this manufacturer?';

$lang['manufacturer_list']  = 'Manufacturers';
$lang['manufacturer_new']   = 'New Manufacturer';
