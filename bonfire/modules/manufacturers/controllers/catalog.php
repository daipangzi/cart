<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class catalog extends Admin_Controller {

	//--------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Manufacturers.Catalog.View');
        
		$this->load->model('manufacturers_model', null, true);           
		$this->lang->load('manufacturers');
        
        Assets::add_js('jquery.form.js');

        $config = init_upload_config('manufacturer');   
        $this->upload->initialize($config);
        
        if(!$this->session->userdata('manufacturer_index'))
        {
            $this->session->set_userdata('manufacturer_index', site_url(SITE_AREA .'/catalog/manufacturers'));
        }  
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
                
                $router_ids = $this->input->post('routes');
				foreach ($checked as $pid)
				{
					$result = $this->manufacturers_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('manufacturers_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('manufacturers_delete_failure') . $this->manufacturers_model->error, 'error');
				}
			}
		}
        
        //get where fields
        $where = array();        
        
        //get order fields
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);

        $orders = array();
        switch($params['orderby']) {
            case "sort_order":
            case "slug":
            case "status":
            case "name":
                $orders[$params['orderby']] = $params['order'];
                break;
            
            case "id":    
            default:
                $orders["manufacturers.manufacturer_id"]  = "asc";
                break;
        }
        
        //get total counts
        $this->manufacturers_model->where($where);
        $total_records = $this->manufacturers_model->count_all();
                       
        //get total records
		$records = $this->manufacturers_model
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all(1);
		Template::set('records', $records);
        
        //build pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/catalog/manufacturers/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/catalog/manufacturers') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);
        
        // set request page  
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('manufacturer_index', $url);                                   
        
		Template::set('toolbar_title', lang('manufacturers_list'));
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: create()

		Creates a Manufacturers object.
	*/
	public function create()
	{
		$this->auth->restrict('Manufacturers.Catalog.Create');

		if ($this->input->post('save') || $this->input->post('save_continue'))    
		{
			if ($insert_id = $this->save_manufacturer())
			{
				Template::set_message(lang('manufacturers_create_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
				else Template::redirect(SITE_AREA .'/catalog/manufacturers/edit/'.$insert_id);
			}
			else
			{
				Template::set_message(lang('manufacturers_create_failure') . $this->manufacturers_model->error, 'error');
			}
		}
		Assets::add_module_js('manufacturers', 'manufacturers.js');
        
        //for avatar image
        if($this->input->post('image_name') && $this->input->post('image_name') != "deleted")
        {
            Template::set("img", $this->input->post('image_name')?$this->input->post('image_name'):'');    
            Template::set("img_path", 'tmp/avatar');    
        }
        else
        {
            Template::set("img", '');    
            Template::set("img_path", 'tmp/avatar');    
        }

        Template::set('prev_page', $this->session->userdata('manufacturer_index'));
		Template::set('toolbar_title', lang('manufacturers_create'));
        Template::set_view('manufacturers/catalog/form');
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: edit()

		Allows editing of Manufacturers data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('manufacturers_invalid_id'), 'error');
			$this->redirect_to_index();
		}

		if (isset($_POST['save']) || isset($_POST['save_continue']))
		{
			$this->auth->restrict('Manufacturers.Catalog.Edit');

            //check id and key
            $key = $this->input->post('key');
            if ($key != md5($id))
            {
                Template::set_message(lang('invalid_action'), 'error');
                $this->redirect_to_index();
            } 
            
			if ($this->save_manufacturer($id, "update"))
			{
				Template::set_message(lang('manufacturers_edit_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
			}
			else
			{
                Template::set_message(lang('manufacturers_edit_failure') . $this->manufacturers_model->error, 'error');  
                              
                Template::set("image_name", $this->input->post('image_name')?$this->input->post('image_name'):'');    
			}      
		}
		else if (isset($_POST['delete']))
		{
            $key = $this->input->post('key');
            if ($key != md5($id))
            {
                Template::set_message(lang('invalid_action'), 'error');
                $this->redirect_to_index();
            } 
            
			$this->auth->restrict('Manufacturers.Catalog.Delete');

			if ($this->manufacturers_model->delete($id))
			{
				Template::set_message(lang('manufacturers_delete_success'), 'success');

				$this->redirect_to_index();
			} else
			{
				Template::set_message(lang('manufacturers_delete_failure') . $this->manufacturers_model->error, 'error');
			}
		}
        Assets::add_module_js('manufacturers', 'manufacturers.js');
                  
        $record = $this->manufacturers_model->find($id, 1);    
        Template::set('record', $record);
        
        //if save failed, them save avatar image
        if($this->input->post('image_name') && $this->input->post('image_name') != "deleted")
        {
            Template::set("img", $this->input->post('image_name'));    
            Template::set("img_path", 'tmp/manufacturer');    
        }
        else
        {
            Template::set("img", trim($record['image']));    
            Template::set("img_path", 'manufacturer');    
        }      
                
        Template::set('prev_page', $this->session->userdata('manufacturer_index'));
		Template::set('toolbar_title', lang('manufacturers_edit'));
        Template::set_view('manufacturers/catalog/form');
		Template::render();
	}
	//--------------------------------------------------------------------
    
    public function delete()
    {
        $id = $this->uri->segment(5);
        $key = $this->uri->segment(6);

        if (empty($id))
        {
            Template::set_message(lang('manufacturers_invalid_id'), 'error');
            $this->redirect_to_index();
        }
        
        if(md5($id) != $key)
        {
            Template::set_message(lang('invalid_action'), 'error');
            $this->redirect_to_index();
        }
        
        $this->auth->restrict('Manufacturers.Catalog.Delete');
        
        if ($this->manufacturers_model->delete($id))
        {
            Template::set_message(lang('manufacturers_delete_success'), 'success');

            $this->redirect_to_index();
        }
    }
    //--------------------------------------------------------------------
    
    //--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_manufacturer()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_manufacturer($id=0, $type="insert")
	{
        $_POST['slug']   = url_title(convert_accented_characters($this->input->post('slug')), 'dash', TRUE);
        $_POST['sort_order']= is_numeric($this->input->post('sort_order'))?$this->input->post('sort_order')==0?'1':$this->input->post('sort_order'):'1';
        $_POST['url']       = prep_url($this->input->post('url'));
        
        //set rules for manufacturer attributes
        $languages = $this->languages_model->find_all(1);
        foreach($languages as $l)
        {
            $c = $l['language_id'];
            
            $this->form_validation->set_rules("info[{$c}][name]", 'lang:ed_name', 'required|trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][page_title]", 'lang:ed_page_title', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_keywords]", 'lang:ed_meta_keywords', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_description]", 'lang:ed_meta_description', 'trim|max_length[255]|strip_tags|xss_clean');
        }
        
        $this->form_validation->set_rules('slug', 'lang:ed_slug','trim|alpha_dash|max_length[100]|strip_tags|xss_clean');
		$this->form_validation->set_rules('sort_order', 'lang:ed_sort_order','trim|is_numeric|max_length[3]');
        $this->form_validation->set_rules('url', 'lang:ed_url','trim|max_length[100]|strip_tags|xss_clean');
        $this->form_validation->set_rules('status', 'lang:ed_status','required|trim|is_numeric|max_length[1]');

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}
              
        //upload manufacturer image
        $data = array();
        
        $action = $this->input->post('image_action');
        if($action == 'changed') { 
            if($this->input->post('image_name')) { 
                $file_name = $this->get_moved_image_name($this->input->post('image_name'), 'manufacturer');
                if($file_name !== FALSE)
                {
                    $data['image'] = $file_name;
                }
            }
        } else if($action == 'deleted') {
            $data['image'] = '';
        }
        //end
             
        // make sure we only pass in the fields we want
        // save manufacturer
        if($id != 0) 
        {
            $data['manufacturer_id']= $id;
        }
        
        $data['sort_order']   = $this->input->post('sort_order');
        $data['slug']         = $this->manufacturers_model->validate_slug($this->input->post('slug'), $id);
        $data['url']          = $this->input->post('url');
        $data['status']       = $this->input->post('status')=='1'?'1':'0';
        
        $langs = $this->input->post('info');
        return $this->manufacturers_model->save($data, $langs);
	}
    
    private function redirect_to_index()
    {
        $redirect_url = $this->session->userdata('manufacturer_index')?$this->session->userdata('manufacturer_index'):SITE_AREA .'/catalog/manufacturers';
        Template::redirect($redirect_url);
    }
    
    //--------------------------------------------------------------------
    // !AJAX METHODS
    //--------------------------------------------------------------------
    public function upload_manufacturer_image() {
        $result = $this->upload_image('manufacturer_image');
        echo json_encode($result);
        exit;     
    }
    
	//--------------------------------------------------------------------
}