<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_categories extends Migration {

	public function up()
	{
		$prefix = $this->db->dbprefix;

		$fields = array(
			'category_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			'categories_sort_order' => array(
				'type' => 'VARCHAR',
				'constraint' => 3,
				
			),
			'categories_thumbnail' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				
			),
			'categories_image' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				
			),
			'categories_meta_keyword' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				
			),
			'categories_meta_description' => array(
				'type' => 'VARCHAR',
				'constraint' => 1024,
				
			),
			'categories_in_navigation' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('category_id', true);
		$this->dbforge->create_table('post_categories');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('post_categories');

	}

	//--------------------------------------------------------------------

}