language_id = '<?php echo global_language_id(); ?>';

// --- Initialize first Dynatree -------------------------------------------
var sel_key = '1';
var action = '';

$("#tree").dynatree({
    clickFolderMode: 1,
    selectMode: 1,
    minExpandLevel: 2,
    generateIds: true,
    initAjax: {
        url: 'post_categories/get_childrens',
        data: {'parent_id': 0},
    },
    onClick: function (node) {
        action = 'update';
        if (node.data.key == sel_key) return;

        sel_key = node.data.key;

        show_loading();

        $.ajax({
            url: "post_categories/category_form",
            type: "post",
            data: {
                'category_id': node.data.key,
                "ci_csrf_token": ci_csrf_token()
            },
            success: function (res) {
                $('#tree_form').html(res);
                $("#tree_form input").uniform();

                if (node.data.key == 0) {
                    $("#tree_form input, #tree_form select, #tree_form textarea").prop("disabled", true);
                    $('#delete-me, #save, #reset').prop("disabled", true);
                }

                hide_loading();
            }
        });
    },
    onLazyRead: function (node) {
        // Mockup a slow reqeuest ...
        node.appendAjax({
            url: "<?php echo site_url(SITE_AREA .'/content/post_categories/get_childrens'); ?>",
            data: {
                'parent_id': node.data.key
            },
            //debugLazyDelay: 750 // don't do this in production code
        });
    },
    onBeforeLazyRead: function() {
        show_loading();
    },
    onAfterLazyRead: function() {
        node = get_tree().getNodeByKey(sel_key);
        node.activate();
                                
        hide_loading();
    },
    
    onExpand: function(flag, dtnode) {
        
    },
    dnd: {
        onDragStart: function (node) {
            /** This function MUST be defined to enable dragging for the tree.
             *  Return false to cancel dragging of node.
             */
            logMsg("tree.onDragStart(%o)", node);
            return true;
        },
        onDragStop: function (node) {
            // This function is optional.
            logMsg("tree.onDragStop(%o)", node);
        },
        autoExpandMS: 100,
        preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
        onDragEnter: function (node, sourceNode) {
            /** sourceNode may be null for non-dynatree droppables.
             *  Return false to disallow dropping on node. In this case
             *  onDragOver and onDragLeave are not called.
             *  Return 'over', 'before, or 'after' to force a hitMode.
             *  Return ['before', 'after'] to restrict available hitModes.
             *  Any other return value will calc the hitMode from the cursor position.
             */
            logMsg("tree.onDragEnter(%o, %o)", node, sourceNode);

            if(node.isLazy() && !node.isExpanded()){
                return false;
            }  
            
            return true;
        },
        onDragOver: function (node, sourceNode, hitMode) {
            /** Return false to disallow dropping this node.
             *
             */
            logMsg("tree.onDragOver(%o, %o, %o)", node, sourceNode, hitMode);
            // Prevent dropping a parent below it's own child
            if (node.isDescendantOf(sourceNode)) {
                return false;
            }
            
            // Prohibit creating childs in non-folders (only sorting allowed)
            if (!node.data.isFolder && hitMode === "over") {
                return "after";
            }            
        },
        onDrop: function (node, sourceNode, hitMode, ui, draggable) {
            /** This function MUST be defined to enable dropping of items on
             * the tree.
             */
            logMsg("tree.onDrop(%o, %o, %s)", node, sourceNode, hitMode);
            sourceNode.move(node, hitMode);
            // expand the drop target
            // sourceNode.expand(true);
        },
        onDragLeave: function (node, sourceNode) {
            /** Always called if onDragEnter was called.
             */
            logMsg("tree.onDragLeave(%o, %o)", node, sourceNode);
        },
        onDragStop: function (sourceNode) {

            show_loading();

            parentNode = sourceNode.getParent();
            if (parentNode.isLazy() && !parentNode.isExpanded()) {
                parentNode.reloadChildren();
                parentNode.expand(true);
            } 
            else if(!parentNode.isExpanded())
            {
                parentNode.expand(true);
            }
            
            setTimeout( function(){
                childrens = parentNode.getChildren();
                child_keys = '';
                for (i = 0; i < childrens.length; i++) {
                    child = childrens[i];
                    child_keys += child.data.key + ',';
                }

                $.ajax({
                    url: "post_categories/update_positions",
                    type: "post",
                    data: {
                        'moved_id': sourceNode.data.key,
                        'parent_id': parentNode.data.key,
                        'child_keys': child_keys,
                        "ci_csrf_token": ci_csrf_token()
                    },
                    success: function (res) {
                        hide_loading();
                    }
                });   
             }, 0);
        }
    }
});

$('#insert_new').click(function () {
    show_loading();

    $.ajax({
        url: "post_categories/category_form",
        type: "post",
        data: {
            'category_id': "new",
            "ci_csrf_token": ci_csrf_token()
        },
        success: function (res) {
            $('#tree_form').html(res);
            $("#tree_form input").uniform();

            $('#action').val('insert');
            action = 'insert';

            hide_loading();
        }
    });
});

$('#save').live('click', function (event) {
    event.preventDefault();

    if (sel_key == '') {
        alert('<?php echo lang('category_select_parent '); ?>');
        $(this).trigger('blur');
        return;
    }

    show_loading();

    $('#parent_id').val(sel_key);
    $.ajax({
        url: "post_categories/validate_form",
        type: "post",
        data: $('#category_form').serialize(),
        success: function(responseText) {
            json = $.parseJSON(responseText);

            //if validation is failed
            if (json.status == 'error') {
                for (var error in json.messages) {
                    $('#' + json.messages[error].field).parent().parent().addClass('error');
                    $('#' + json.messages[error].field).attr('title', json.messages[error].message);

                    $('#' + json.messages[error].field).qtip({
                        content: false,
                        position: {
                            my: 'left center',
                            at: 'right center',
                            viewport: $(window)
                        },
                        style: {
                            classes: 'qtip-red'
                        }
                    });
                }

                hide_loading();
            } else {
                $('.control-group input').qtip('destroy');
                $('.control-group').removeClass('error');

                //$('#category_form').submit(); 
                $.ajax({
                    url: "post_categories/save",
                    type: "post",
                    data: $('#category_form').serialize(),
                    success: function (res) {
                        $('#tree_form').html(res);
                        $("#tree_form input").uniform();

                        //add new node if action is insert
                        node = get_active_node();
                        if (action == 'insert') {
                            node.data.isLazy = true;
                            node.reloadChildren();
                            node.expand(true);

                            sel_key = $('#category_id').val();
                        } else {
                            node.setTitle($('#info_' + language_id + '_name').val());
                        }

                        hide_loading();
                    },
                });
            }
        },
    });
});

$('#delete-me').live('click', function (event) {
    event.preventDefault();

    if (sel_key == '') {
        alert('<?php echo lang('category_select_parent '); ?>');
        $(this).trigger('blur');
        return;
    } else {
        show_loading();

        $.ajax({
            url: "post_categories/delete/" + sel_key,
            type: "post",
            data: {
                "ci_csrf_token": ci_csrf_token(),
            },
            success: function (res) {
                if (res != '') {
                    $('#tree_form').html(res);
                    $("#tree_form input").uniform();

                    selected_node = get_active_node();
                    selected_node.removeChildren();
                    selected_node.remove();

                    sel_key = '';

                    hide_loading();
                }
            }
        });
    }
});

function show_loading() {
    get_tree().disable();
    $('#loading-mask').show();
}

function hide_loading() {
    get_tree().enable();
    $('#loading-mask').hide();
}

function get_active_node() {
    return $("#tree").dynatree("getActiveNode");
}

function get_tree() {
    return $("#tree").dynatree("getTree");
}