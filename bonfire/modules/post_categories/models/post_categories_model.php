<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Post_Categories_model extends BF_Model {

	protected $table		= "post_categories";
	protected $key			= "category_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = true;
    
    private $orderby    = array();
    private $is_admin   = false;
    private $all_in_navigation = false;
    
    var $CI;
    
    function __construct() 
    {
        $this->CI =& get_instance();
        $this->CI->load->library('post_category');
        
        $this->is_admin = is_backend();
        $this->init();
    }
    
    function init()
    {
        // check root category and insert
        $rows = $this->count_by('category_id', 1);
        if($rows == 0)
        {
            $root = array();
            $root['category_id'] = 1;
            parent::insert($root);
        }        
        
        $languages = $this->CI->languages_model->find_all(1);  

        foreach($languages as $lang)
        {
            $l = $lang['language_id'];
            
            if(!$this->check_language_row(1, $l))
            {
                $row = array();
                $row['category_id'] = 1;
                $row['language_id'] = $l;
                $row['name']        = 'Default Category';
                $row['page_title']  = '';
                $row['meta_keywords']    = '';
                $row['meta_description'] = '';
                
                $this->db->insert('post_categories_info', $row);
            }
        }
    }
    
    function init_before_find_all()
    {
        //set orderby
        if(empty($this->orderby))
        {
            $this->order_by('sort_order');
        }
        else
        {
            $this->order_by($this->orderby);
        }
        
        //set filter
        if(!$this->is_admin)
        {
            $this->where('status', 1);
        }
        
        return $this;    
    }
    
    /**
    * find category by id
    * 
    * @param mixed $id
    * @param mixed $return_type
    */
    function find($id, $return_type=0)
    {
        $temp_return = $return_type>=2?0:$return_type;
        
        $record = parent::find($id, $temp_return);
        //if cann't find record
        if(empty($record)) 
        {
            return FALSE;
        }
        
        if($this->is_admin)
        {
            if($return_type == 0)
            {
                $record->info = $this->get_all_language_info($id);
            }
            else
            {
                $record['info'] = $this->get_all_language_info($id);
            }
        }
        else
        {
            //if status disabled
            $temp = (array)$record;
            if($temp['status'] == 0)
            {
                return FALSE;
            }
            
            if($return_type == 0)
            {       
                $record  = (object)array_merge((array)$r, $this->get_language_info($id, global_language_id()));
            }
            else if($return_type == 1)
            {
                $record  = array_merge($r, $this->get_language_info($id, global_language_id()));
            }
            else
            {   
                $record = (object)array_merge((array)$record, $this->get_language_info($id, global_language_id()));
                //$record->info = $this->get_all_language_info($id);            
                $record->hasChildrens = $this->has_childrens($id);
                  
                $record = new Post_Category($record);
            }
        }
          
        return $record;
    }
     
    /**
    * find category by slug
    * 
    * @param mixed $slug
    */
    function find_by_slug($slug)
    {
        $record = parent::find_by('slug', $slug);
        
        if(empty($record)) 
        {
            return FALSE;
        }
        
        return $this->find($record->category_id);
    }
    
    function find_by_path($path)
    {
        $record = parent::find_by('path', $path);
        
        if(empty($record)) 
        {
            return FALSE;
        }
        
        return $this->find($record->category_id);
    }
    
    /**
    * count all records 
    * 
    */
    function count_all()
    {
        $language_id = global_language_id();
        
        $this->init_before_find_all()
            ->join('post_categories_info ci', "ci.category_id=post_categories.category_id AND ci.language_id={$language_id}", 'left');
        
        return parent::count_all();
    }
    
    /**
    * find all records with current language
    * 
    * @param mixed $return_type: 0: , 1: , 2:
    */
    function find_all($return_type=0)
    {
        $temp_return = $return_type>=2?0:$return_type;
        $language_id = global_language_id();
        
        $this->init_before_find_all()
            ->join('post_categories_info ci', "ci.category_id=post_categories.category_id AND ci.language_id={$language_id}", 'left');
        $records = parent::find_all($temp_return);
        
        if($return_type != 0 && $return_type != 1 && !empty($records))
        {
            $result = array();
            foreach($records as $r)
            {
                $category = new Category($r);
                $category->hasChildrens = $this->has_childrens($r->category_id);
                $result[] = $category;
            }    
            
            return $result;                   
        }
        
        return $records;
    }
    
    /**
    * get all endabled records by tree structure
    * 
    * @param mixed $parent
    */
    function find_all_tierd($parent=0)
    {
        $this->init_before_find_all();
        
        $records = $this->get_childrens($parent, 1);
        
        $result  = array();
        if(!empty($records))
        {
            foreach($records as $r)
            {
                $id = $r['category_id'];
                $temp  = array_merge($r, $this->get_language_info($id, global_language_id()));
                
                $result[$id]['category'] = $temp;
                $result[$id]['children'] = $this->find_all_tierd($id);
            }
        }
        
        return $result;
    }
    
    function delete($id)
    {
        // delete category itself
        $this->db->where('category_id', $id)->delete('post_categories_info');
        $this->db->where('category_id', $id)->delete('post_categories_links');
        
        return parent::delete($id); 
    }
    
    function delete_node($id)
    {
        $path = $this->get_path($id);
        
        $this->join('post_categories_info ci', 'ci.category_id=post_categories.category_id', 'left')
            ->where('path LIKE', $path.'%');
        
        $childrens = parent::find_all(1);
        if(empty($childrens)) 
        {
            return FALSE;
        }
        
        foreach($childrens as $child)
        {
            $child_id = $child['category_id'];
            $this->delete($child_id);
        }
        return TRUE;
    }
    
    /**
    * update positions when drag/drop
    * 
    * @param mixed $moved_id
    * @param mixed $parent_id
    * @param mixed $keys
    */
    function update_positions($moved_id, $parent_id, $keys)
    {
        if($parent_id == '_1')
        {
            $parent_id = 0;
        }
        
        $parent_path = $this->get_path($parent_id);
        
        //update target's childrens include me
        $sort_order = 1;
        $children_ids = explode(',', $keys);
        
        foreach($children_ids as $id)
        {
            if($id == '') continue; 
            
            $new_path = $parent_path!=''?($parent_path.'/'.$id):$id;
            
            $data = array();
            $data['sort_order'] = $sort_order;
            $data['parent_id']  = $parent_id;
            $data['path']       = $new_path;
            $data['level']      = count(explode('/', $new_path));
         
            $this->update($id, $data);
            
            $sort_order++;
        }
        
        //update moved node's childrens's path
        if(!$this->has_childrens($moved_id)) return;
        
        $childrens = $this->where('parent_id', $parent_id)->find_all(1);
        foreach($childrens as $child)
        {
            $data = array();
            $data['path'] = $parent_path . '/' . $child['category_id'];
            
            $this->update($child['category_id'], $data);
        }
    }
    
    // save category with language data
    function save($data, $langs) {
        $id = FALSE;
        if(!isset($data[$this->key]))
        {
            $id = $this->insert($data);
            
            // update path info and level
            $parent_path = $this->get_path($data['parent_id']);
            
            $patch_data = array();
            $patch_data['path'] = $parent_path . '/' . $id;
            $patch_data['level'] = count(explode('/', $patch_data['path']));
            $this->update($id, $patch_data);
        }
        else  
        {         
            $id = $data[$this->key];
            
            $this->update($id, $data);
        }
        
        // if saved successfully
        if($id !== FALSE)
        {
            // save lang info
            foreach($langs as $c => $d)
            {
                $info = array();
                $info['category_id']    = $id;
                $info['language_id']    = $c;
                $info['name']           = $d['name'];
                $info['page_title']     = $d['page_title'];
                $info['meta_keywords']  = $d['meta_keywords'];
                $info['meta_description'] = $d['meta_description'];
                
                if(!$this->check_language_row($id, $c))
                {
                    $this->db->insert('post_categories_info', $info);
                }
                else
                {
                    $where = array();
                    $where['category_id'] = $id;
                    $where['language_id'] = $c;
                    
                    $this->db->where($where)->update('post_categories_info', $info);
                }
            }   
        }
        
        return $id;
    }
    
    /**
    * check if category has childrens
    * 
    * @param mixed $parent_id
    * @return mixed
    */
    function has_childrens($parent_id=0)
    {
        $count = $this->where('parent_id', $parent_id)->count_all();
        return (bool)$count;
    }  
    
    /**
    * count childrens
    * 
    * @param mixed $parent_id
    */
    function count_childrens($parent_id=0)
    {
        $count = $this->where('parent_id', $parent_id)->count_all();
        return $count;
    }
    
    /**
    * get childrens of category
    * 
    * @param mixed $parent_id
    * @param mixed $tree_mode
    */
    function get_childrens($parent_id=0, $return_type=0)
    {
        $temp_return = $return_type>=2?0:$return_type;
        
        $records = $this
            ->init_before_find_all()
            ->where('parent_id', $parent_id)
            ->find_all($temp_return);

        $result = array();
        if(!empty($records))
        {
            foreach($records as $r)
            {
                $temp   = (array)$r;
                $id     = $temp['category_id'];
                
                $item = array();  
                if($return_type == 0)
                {       
                    $item  = (object)array_merge((array)$r, $this->get_language_info($id, global_language_id()));
                }
                else if($return_type == 1)
                {
                    $item  = array_merge($r, $this->get_language_info($id, global_language_id()));
                }
                else
                {
                    $id    = $r->category_id;
                    $item  = (object)array_merge((array)$r, $this->get_language_info($id, global_language_id()));
                    $item->hasChildrens = $this->has_childrens($id);    
                    
                    $item = new Category($item);
                }
                
                $result[] = $item;    
            }
        }
        
        return $result;   
    }
    
    function get_top_categories($return_type=0)
    {
        return $this->get_childrens(1, $return_type);
    }
    
    //get childrens for building tree dynamic(admin)
    function get_tree_root($level=1, $parent_id=0)
    {
        $records = $this->get_childrens($parent_id, 1);
         
        $result  = array();
        if(!empty($records))
        {
            foreach($records as $r)
            {            
                $cid = $r['category_id'];
                
                $item = array();
                $item['key']    = $cid;
                $item['title']  = $r['name'];
                $item['isFolder']   = true;
                $item['disable']    = $r['status']==1?false:true;
                
                //activate root category
                if($parent_id == 0)
                {
                    $item['activate'] = true;
                    $item['hideCheckbox'] = true;
                }
                
                if($this->has_childrens($cid))
                {
                    if($level >= $r['level'])
                    {
                        if($level > $r['level'])
                        {
                            $item['expand'] = true;
                            $item['children'] = $this->get_tree_root($level, $cid);
                        } 
                        else
                        {
                            $item['isLazy'] = true;
                        }
                    }
                    else
                    {
                        $item['isLazy'] = true;
                    }
                }
                
                $result[] = $item;
            }
        }
        
        return $result;  
    }
    
    function get_tree_childrens($parent_id=0)
    {
        $records = $this
            ->init_before_find_all()
            ->where('parent_id', $parent_id)
            ->find_all(1);

        $result = array();
        if(!empty($records))
        {
            foreach($records as $r)
            {
                $id     = $r['category_id'];
                
                $item = array();
                $item['key']    = $r['category_id'];
                $item['title']  = $r['name'];
                $item['isFolder'] = true;
                $item['disable'] = $r['status']==1?false:true;
                
                if($this->has_childrens($r['category_id']))
                {
                    $item['isLazy'] = true;
                }
                
                $result[] = $item;    
            }
        }
        
        return $result;   
    }
    
    private $required_ids;
    function get_tree_from_ids($category_ids=false, $parent_id=0)
    {
        //save original required category ids tempererily
        $this->required_ids = $category_ids;        
        $path_ids = $this->get_path_ids($category_ids);
        return $this->get_tree_with_selected_nodes($path_ids);
    }
    
    private function get_tree_with_selected_nodes($path_ids, $parent_id=0)
    {
        $this->init_before_find_all();
        
        $records = $this->get_childrens($parent_id, 1);
         
        $result  = array();
        if(!empty($records))
        {
            foreach($records as $r)
            {            
                $cid = $r['category_id'];
                
                $item = array();
                $item['key']    = $cid;
                $item['title']  = $r['name'];
                $item['isFolder'] = true;
                
                if(in_array($cid, $this->required_ids))
                {
                    $item['select'] = true;
                }
                
                if($this->has_childrens($cid))
                {
                    if(in_array($cid, $path_ids))
                    {
                        $item['expand'] = true;
                        
                        $item['children'] = $this->get_tree_with_selected_nodes($path_ids, $cid);
                    }
                    else
                    {
                        $item['isLazy'] = true;
                    }
                }
                
                $result[] = $item;
            }
        }
        
        return $result;         
    }
    
    private function get_path_ids($ids)
    {
        $result = array();
        foreach($ids as $id)
        {
            $path = $this->get_path($id);
            
            $result = array_merge($result, explode('/', $path));
        }
        
        return array_unique($result);
    }
       
    //--------------------------------------------------------------------
    // !LANUAGE METHODS
    //--------------------------------------------------------------------
    
    /**
    * check if row is exist with category and lanuage id
    * 
    * @param mixed $id
    * @param mixed $language_id
    * @return mixed
    */
    private function check_language_row($id, $language_id)
    {
        $where = array();
        $where['category_id']   = $id;
        $where['language_id']   = $language_id;
        
        return (bool)$this->db->where($where)->count_all_results('post_categories_info');
    }  
    
    /**
    * get all lanuage info
    * 
    * @param mixed $id
    */
    private function get_all_language_info($id)
    {
        $languages    = $this->CI->languages_model->find_all(1);
        
        $result = array();
        foreach($languages as $lang)
        {
            $l = $lang['language_id'];
            
            $record = $this->get_language_info($id, $l);
            
            $result[$l] = $record;
        }
        
        return $result;
    } 
    
    /**
    * get one lanuage info
    * 
    * @param mixed $id
    * @param mixed $language_id
    */
    private function get_language_info($id, $language_id)
    {
        $default_lang = get_language_id_by_code(settings_item('site.default_language'));   
        
        $record = $this->db
                    ->where(array('category_id' => $id, 'language_id' => $language_id))
                    ->get('post_categories_info')
                    ->row_array();
         
        if(empty($record))
        {
            $record = $this->db
                    ->where(array('category_id' => $id, 'language_id' => $default_lang))
                    ->get('post_categories_info')
                    ->row_array();
        } 
        
        return $record;
    }
    
    //--------------------------------------------------------------------
    // !COMMON METHODS
    //--------------------------------------------------------------------
    
    /**
    * check if the slug is exist
    * 
    * @param mixed $slug
    * @param mixed $id
    * @return mixed
    */
    private function check_slug($slug, $id=false)
    {
        if($id)
        {
            $this->where('category_id !=', $id);
        }
        $this->where('slug', $slug);
        
        return (bool) parent::count_all();
    }
    
    // check and get valid slug
    function validate_slug($slug, $id=false, $count=false)
    {
        if($slug == '') return '';
        
        if($this->check_slug($slug.$count, $id))
        {
            if(!$count)
            {
                $count = 1;
            }
            else
            {
                $count++;
            }
            return $this->validate_slug($slug, $id, $count);
        }
        else
        {
            return $slug.$count;
        }
    }
    
    /**
    * get category path by id
    * 
    * @param mixed $id
    * @return mixed
    */
    private function get_path($id)
    {
        $row = parent::find($id, 1);
        if(empty($row)) return '';
        
        return $row['path'];
    }
    
    private function get_id_by_path($path)
    {
        $row = parent::find_by('path', $path);
        if(empty($row)) return 0;
        
        return $row->category_id;
    }
    
    private function get_row($id)
    {
        $record = parent::find($id, 1);
         
        //if cann't find record
        if(empty($record)) 
        {
            return FALSE;
        }
        
        //if status disabled
        if($record['status'] == 0)
        {
            return FALSE;
        }
        
        $record = array_merge($record, $this->get_language_info($id, global_language_id()));    
          
        return $record;
    }
    
    //--------------------------------------------------------------------
    // !FILTER METHODS
    //--------------------------------------------------------------------
    
    /**
    * set flag if backend or frontend
    * 
    */
    function is_admin()
    {
        $this->is_admin = true;   
        return $this; 
    }
    
    function all_in_navigation()
    {
        $this->all_in_navigation = true;
        return $this;
    }
    
    function filter_status($status)
    {
        $this->where('status', $status);
        return $this;
    }
    
    //--------------------------------------------------------------------
    // !ORDER METHODS
    //--------------------------------------------------------------------
    function order_by_name($order='asc')
    {
        $this->order_by['name'] = $order;
        return $this;
    }
    
    function order_by_sort_order($order='asc')
    {
        $this->order_by['sort_order'] = $order;
        return $this;
    }
    
    //--------------------------------------------------------------------
    // !POSTS METHODS
    //--------------------------------------------------------------------
    function count_posts($id)
    {
        //TODO
        return 0;
    }
}
