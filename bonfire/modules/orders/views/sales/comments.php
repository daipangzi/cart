<ul class="note-list">
    <?php if(!empty($comments)) : ?>
    <?php foreach($comments as $c) : ?>
    <li>
        <strong><?php echo format_date($c->created_on); ?></strong>
        <?php echo format_time($c->created_on); ?>
        <span class="separator">|</span>
        <strong><?php echo lang($c->order_status); ?></strong>
        <br/>
        
        <small><?php echo lang('ed_customer');?> 
            <?php if($c->notified == 1): ?>
            <strong class="subdue"><?php echo lang('ord_notified');?> <span class="icon12 minia-icon-checkmark green"></span></strong>
            <?php else : ?>
            <strong class="subdue"><?php echo lang('ord_no_notified');?></strong>
            <?php endif; ?>
        </small>
        <br/>
        
        <?php echo $c->message; ?>
    </li>
    <?php endforeach; ?>
    <?php endif; ?>
</ul>