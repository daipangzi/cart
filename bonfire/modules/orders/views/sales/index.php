<div class="admin-box">
	<?php echo form_open($this->uri->uri_string(), 'id="list-form" class="form-horizontal" autocomplete="off"'); ?>
    <input type="hidden" id="param_orderby" name="orderby" value="<?php echo $params['orderby']; ?>"/>
    <input type="hidden" id="param_order" name="order" value="<?php echo $params['order']; ?>"/>
    
    <fieldset><legend><?php echo lang('ed_order_list'); ?><small><?php echo $this->pagination->get_page_status();?></small></legend></fieldset>
    
    <div class="form-buttons">
        <?php if(!empty($search)) :?>
        <button type="submit" name="reset_search" id="reset-search" class="btn" value="reset"><?php echo lang('bf_action_reset_filter') ?></button>
        <?php endif; ?>
        
        <button type="submit" name="search_entries" id="search-entries" class="btn" value="search"><span class="icon-zoom-in"></span>&nbsp;<?php echo lang('bf_action_filter') ?></button>
    </div>
    
    <div class="responsive">
		<table class="table table-striped lrborder checkAll" style="min-width:1000px;">
            <colgroup>
                <?php if ($this->auth->has_permission('Orders.Sales.Edit') && isset($records) && is_array($records) && count($records)) : ?>
                <col width="20"/>
                <?php endif;?>
                
                <col width="150"/>
                <col width="200"/>
                <col width=""/>
                <col width=""/>
                <col width="120"/>
                <col width="100"/>
                <col width="100"/>
            </colgroup>
			<thead>
                <tr>
                    <td colspan="8" style="text-align:right;">
                        <?php echo lang('bf_actions'); ?> &nbsp;
                        <?php echo form_dropdown2('mass_actions', get_mass_order_actions(), set_value('mass_actions', ''), 'id="mass_actions" class="nostyle"'); ?>
                        <button type="submit" name="mass_action" id="mass-action" class="btn" value="mass"><?php echo lang('bf_action_submit') ?></button>
                    </td>
                </tr>
				<tr>
					<?php if ($this->auth->has_permission('Orders.Sales.Edit') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
                    <th class="<?php echo sort_classes($params['orderby'], "order", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=order&amp;order='.sort_direction($params['orderby'], "order", $params['order'])); ?>"><span><?php echo lang('ed_order'); ?> #</span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "date", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=date&amp;order='.sort_direction($params['orderby'], "date", $params['order'])); ?>"><span><?php echo lang('ed_purchased_on'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "customer", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=customer&amp;order='.sort_direction($params['orderby'], "customer", $params['order'])); ?>"><span><?php echo lang('ed_customer'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "receiver", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=receiver&amp;order='.sort_direction($params['orderby'], "receiver", $params['order'])); ?>"><span><?php echo lang('ed_receiver'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "total", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=total&amp;order='.sort_direction($params['orderby'], "total", $params['order'])); ?>"><span><?php echo lang('ed_total'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ed_status'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th><?php echo lang('bf_action');?></th>
				</tr>
                <tr id="search_row">
                    <?php if ($this->auth->has_permission('Orders.Sales.Edit') && isset($records) && is_array($records) && count($records)) : ?>
                    <td>&nbsp;</td>
                    <?php endif;?>

                    <td><input type="text" name="search[order_no]" id="serach_order_no" value="<?php echo isset($search['order_no'])?$search['order_no']:'';?>" class="wd95"/></td>
                    <td>
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="date right wd100px" name="search[date_from]" id="search_date_from" value="<?php echo isset($search['date_from'])?$search['date_from']:'';?>"/>
                        <div class="clear"></div>
                        <hr/>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="date right wd100px" name="search[date_to]" id="search_date_to" value="<?php echo isset($search['date_to'])?$search['date_to']:'';?>"/>
                    </td>
                    <td><input type="text" name="search[customer]" id="serach_customer" value="<?php echo isset($search['customer'])?$search['customer']:'';?>" class="wd95"/></td>
                    <td><input type="text" name="search[receiver]" id="serach_receiver" value="<?php echo isset($search['receiver'])?$search['receiver']:'';?>" class="wd95"/></td>
                    <td>
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="right wd50px" name="search[price_from]" id="search_price_from" value="<?php echo isset($search['price_from'])?$search['price_from']:'';?>"/>
                        <div class="clear"></div>
                        <hr/>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="right wd50px" name="search[price_to]" id="search_price_to" value="<?php echo isset($search['price_to'])?$search['price_to']:'';?>"/>
                    </td>
                    <td><?php echo form_dropdown2('search[status]', get_order_status(), set_value('status', isset($search['status']) ? $search['status'] : ''), 'id="search_status" class="nostyle autowidth"'); ?></td>
                    <td>&nbsp;</td>
                </tr>
			</thead>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
                    <?php if ($this->auth->has_permission('Orders.Sales.Edit')) : ?>					
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->order_id ?>" /></td>
                    <?php endif;?>
                    
                    <td><a href="<?php echo site_url(SITE_AREA . '/sales/orders/edit/'.$record->order_id.'/'.md5($record->order_id)); ?>"><?php echo $record->order_no ?></a></td>
                    <td><?php echo format_date_time($record->created_on); ?></td>
				    <td><?php echo $record->customer_name ?></td>
				    <td><?php echo $record->receiver_name; ?></td>
				    <td><?php echo format_price($record->total); ?></td>
				    <td><?php echo lang($record->status); ?></td>                                       
                    <td><a href="<?php echo site_url(SITE_AREA . '/sales/orders/edit/'.$record->order_id.'/'.md5($record->order_id)); ?>"><?php echo lang('bf_action_view'); ?></a></td>
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="8"><?php echo lang('no_record'); ?></td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
    </div>
	<?php echo form_close(); ?>
    <?php echo $this->pagination->create_links(); ?>
</div>