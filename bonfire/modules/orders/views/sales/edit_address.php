<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    
    <fieldset>
        <legend><?php echo sprintf(lang('order_edit_address'), $order->order_no); ?></legend>
        
        <div class="form-buttons">
            <?php echo anchor(SITE_AREA .'/sales/orders/edit/'.$order->order_id, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>                    
            <button type="reset" name="reset" class="btn" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
            
            <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;    
        </div>
        
        <div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_name').lang('bf_form_label_required'), 'name', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="name" type="text" name="name" class="span4" value="<?php echo set_value('name', isset($ship_address->name) ? $ship_address->name : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('name'); ?></span>
            </div>
        </div> 
        
        <div class="control-group <?php echo (form_error('province') || form_error('city')) ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_province_city').lang('bf_form_label_required'), 'province', array('class' => "control-label") ); ?>
            <div class='controls'>
                <?php echo form_dropdown2('province', $provinces, set_value('province', isset($ship_address->province) ? $ship_address->province : ''), 'id="province" class="nostyle"'); ?>
                <?php echo form_dropdown2('city', $cities, set_value('city', isset($ship_address->city) ? $ship_address->city : ''), 'id="city" class="nostyle"'); ?>
                <span class="help-inline"><?php echo form_error('province') ?></span>
                <span class="help-inline"><?php echo form_error('city') ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('address') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_address').lang('bf_form_label_required'), 'address', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="address" type="text" name="address" class="span6" value="<?php echo set_value('address', isset($ship_address->address) ? $ship_address->address : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('address'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('email') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_email').lang('bf_form_label_required'), 'email', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="email" type="text" name="email" class="span4" value="<?php echo set_value('email', isset($ship_address->email) ? $ship_address->email : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('email'); ?></span>
            </div>
        </div> 
        
        <div class="control-group <?php echo form_error('phone') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_phone'), 'email', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="phone" type="text" name="phone" class="span4" value="<?php echo set_value('phone', isset($ship_address->phone) ? $ship_address->phone : ''); ?>"  />
                <span class="help-inline"><?php echo form_error('phone'); ?></span>
            </div>
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>
