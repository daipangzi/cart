<input type="hidden" id="offset" name="offset" value="<?php echo $offset; ?>"/>

<div class="responsive">
    <table class="table table-striped lrborder checkAll" style="min-width:1000px;">
        <colgroup>
            <col width="150"/>
            <col width="200"/>
            <col width=""/>
            <col width=""/>
            <col width="120"/>
            <col width="100"/>
            <col width="100"/>
        </colgroup>
        <thead>
            <tr>
                <th class="<?php echo sort_classes($params['orderby'], "order", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=order&amp;order='.sort_direction($params['orderby'], "order", $params['order'])); ?>"><span><?php echo lang('ed_order'); ?> #</span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "date", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=date&amp;order='.sort_direction($params['orderby'], "date", $params['order'])); ?>"><span><?php echo lang('ed_purchased_on'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "customer", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=customer&amp;order='.sort_direction($params['orderby'], "customer", $params['order'])); ?>"><span><?php echo lang('ed_customer'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "receiver", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=receiver&amp;order='.sort_direction($params['orderby'], "receiver", $params['order'])); ?>"><span><?php echo lang('ed_receiver'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "total", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=total&amp;order='.sort_direction($params['orderby'], "total", $params['order'])); ?>"><span><?php echo lang('ed_total'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/sales/orders?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ed_status'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th><?php echo lang('bf_action');?></th>
            </tr>
            <tr id="search_row">
                <td><input type="text" name="search[order_no]" id="serach_order_no" value="<?php echo isset($search['order_no'])?$search['order_no']:'';?>" class="wd95"/></td>
                <td>
                    <span class="lbl"><?php echo lang('ed_from'); ?></span>
                    <input type="text" class="date right wd100px" name="search[date_from]" id="search_date_from" value="<?php echo isset($search['date_from'])?$search['date_from']:'';?>"/>
                    <div class="clear"></div>
                    <hr/>
                    <span class="lbl"><?php echo lang('ed_to'); ?></span>
                    <input type="text" class="date right wd100px" name="search[date_to]" id="search_date_to" value="<?php echo isset($search['date_to'])?$search['date_to']:'';?>"/>
                </td>
                <td><input type="text" name="search[customer]" id="serach_customer" value="<?php echo isset($search['customer'])?$search['customer']:'';?>" class="wd95"/></td>
                <td><input type="text" name="search[receiver]" id="serach_receiver" value="<?php echo isset($search['receiver'])?$search['receiver']:'';?>" class="wd95"/></td>
                <td>
                    <span class="lbl"><?php echo lang('ed_from'); ?></span>
                    <input type="text" class="right wd50px" name="search[price_from]" id="search_price_from" value="<?php echo isset($search['price_from'])?$search['price_from']:'';?>"/>
                    <div class="clear"></div>
                    <hr/>
                    <span class="lbl"><?php echo lang('ed_to'); ?></span>
                    <input type="text" class="right wd50px" name="search[price_to]" id="search_price_to" value="<?php echo isset($search['price_to'])?$search['price_to']:'';?>"/>
                </td>
                <td><?php echo form_dropdown2('search[status]', get_order_status(), set_value('status', isset($search['status']) ? $search['status'] : ''), 'id="search_status" class="nostyle autowidth"'); ?></td>
                <td>&nbsp;</td>
            </tr>
        </thead>
        <tbody>
        <?php if (isset($records) && is_array($records) && count($records)) : ?>
        <?php foreach ($records as $record) : ?>
            <tr>
                <td><a href="<?php echo site_url(SITE_AREA . '/sales/orders/edit/'.$record->order_id.'/'.md5($record->order_id)); ?>"><?php echo $record->order_no ?></a></td>
                <td><?php echo format_date_time($record->created_on); ?></td>
                <td><?php echo $record->customer_name ?></td>
                <td><?php echo $record->receiver_name; ?></td>
                <td><?php echo format_price($record->total); ?></td>
                <td><?php echo lang($record->status); ?></td>                                       
                <td><a href="<?php echo site_url(SITE_AREA . '/sales/orders/edit/'.$record->order_id.'/'.md5($record->order_id)); ?>"><?php echo lang('bf_action_view'); ?></a></td>
            </tr>
        <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="8"><?php echo lang('no_record'); ?></td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
<br/>
<?php echo $this->pagination->create_links(); ?>