<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class sales extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Orders.Sales.View');
		$this->lang->load('orders');
        
        $this->load->helper('orders/order');
        
        $this->load->model('region_model', null, true);
        $this->load->model('orders/orders_model', null, true);
        $this->load->model('customers/customer_model', null, true);
        
        Assets::add_module_js('orders', 'orders.js');
        
        if(!$this->session->userdata('orders_index'))
        {
            $this->session->set_userdata('orders_index', site_url(SITE_AREA .'/sales/orders'));
        } 
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		// Deleting anything?
		if ($this->input->post('mass_action'))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					//$result = $this->orders_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('orders_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('orders_delete_failure') . $this->orders_model->error, 'error');
				}
			}
		}

        //get order fields
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        // build where sql
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('order_search', $search);
        } else if($this->input->post('reset_search')) {
        } else {
            $search = $this->session->userdata('order_search')?$this->session->userdata('order_search'):array();
        }
        if(!empty($search['order_no'])) $where['orders.order_no LIKE'] = "%" . $search['order_no'] . "%";
        if(!empty($search['date_from']))$where['orders.created_on >= '] = $search['date_from'];
        if(!empty($search['date_to']))  $where['orders.created_on <= '] = $search['date_to'];
        if(!empty($search['customer'])) $where['orders.customer_name LIKE'] = "%" . $search['customer'] . "%";
        if(!empty($search['receiver'])) $where['orders_address.name LIKE'] = "%" . $search['receiver'] . "%";
        if(!empty($search['price_from'])) $where['orders.total >='] = $search['price_from'];
        if(!empty($search['price_to']))   $where['orders.total <='] = $search['price_to'];        
        if(!empty($search['status']))     $where['orders.status'] = $search['status'];
        Template::set('search', $search);
        
        // build order sql
        $orders = array();
        switch($params['orderby']) {
            case "order":
                $orders["order_no"]     = $params['order'];
                break;
            
            case "date":
                $orders["created_on"]   = $params['order'];
                break;
                
            case "customer":
                $orders["customer_name"]= $params['order'];
                break;
                
            case "receiver":
                $orders["orders_address.name"]= $params['order'];
                break;
                
            case "total":
                $orders["orders.total"] = $params['order'];
                break;
                
            case "status":
                $orders["orders.status"]= $params['order'];
                break;
                
            default:
                $orders["created_on"]   = "desc";
                break;
        }
        $records = $this->orders_model
            ->where($where)
            ->limit($this->limit, $offset)
            ->order_by($orders)
            ->find_all();
        Template::set('records', $records);
        
        //get total counts
        $total_records = $this->orders_model
            ->where($where)
            ->count_all();
        
        //build pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/sales/orders/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;  
        $this->pager['first_url']   = site_url(SITE_AREA .'/sales/orders') . $url_suffix;
        $this->pager['current_rows']= count($records);
        $this->pagination->initialize($this->pager);
        
        // set request page                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('orders_index', $url);
        
		Template::set('toolbar_title', lang('order_manage'));
		Template::render();
	}

	//--------------------------------------------------------------------
	/*
		Method: edit()

		Allows editing of Orders data.
	*/
	public function edit()
	{
        $this->auth->restrict('Orders.Sales.Edit');
        
		$id = $this->uri->segment(5);
        
        if (empty($id))
		{
			Template::set_message(lang('order_invalid_id'), 'error');
			redirect(SITE_AREA .'/sales/orders');
		}

        // change order status
		if ($this->input->post('action'))
		{
            if(md5($id) == $this->input->post('key')) 
            {
			    $this->save_order($id);
            }
            else
            {
                Template::set_message(lang('orders_invalid_action'), 'error');
            }
		}
        
        // get order info
        $order = $this->orders_model->find($id);
        if(empty($order))
        {
            Template::set_message(lang('orders_invalid_id'), 'error');
            redirect(SITE_AREA .'/sales/orders');
        }
        Template::set('order', $order); 
        
        // get order address          
        $ship_address = $this->orders_model->get_shipping_address($id);     
        Template::set('ship_address', $ship_address); 
        
        // get order items
        $order_items = $this->orders_model->get_items($id);
        Template::set('order_items', $order_items); 
        
        // get comment history
        $comments = $this->orders_model->get_comments($id);
        Template::set('comments', $comments); 
        
        // get customer original address
        if($order->customer_id != '') 
        {
            $customer_address = $this->customer_model->find_address($order->customer_id);
            Template::set('customer_address', $customer_address); 
        }
        
        Template::set('prev_page', $this->session->userdata('orders_index'));
		Template::set('toolbar_title', lang('order_edit'));
		Template::render();
	}
	//--------------------------------------------------------------------
    
    public function edit_address()
    {
        $order_id = $this->uri->segment(5);
        
        if (empty($order_id))
        {
            Template::set_message(lang('order_invalid_id'), 'error');
            redirect(SITE_AREA .'/sales/orders');
        }

        // change order status
        if ($this->input->post('save'))
        {
            if($this->save_shipping_address($order_id))
            {
                Template::set_message(lang('order_address_edit_success'), 'success');
                Template::redirect(SITE_AREA .'/sales/orders/edit/'.$order_id);
            }
            else
            {
                Template::set_message(lang('order_address_edit_failure'), 'error');
            }
        }
        
        // get order info
        $order = $this->orders_model->find($order_id);
        if(empty($order))
        {
            Template::set_message(lang('orders_invalid_id'), 'error');
            redirect(SITE_AREA .'/sales/orders');
        }
        Template::set('order', $order); 
        
        // get order address          
        $ship_address = $this->orders_model->get_shipping_address($order_id);     
        Template::set('ship_address', $ship_address);
        
        // get province list
        $provinces = $this->region_model->province_dropdown_list(TRUE, lang('ed_select'));
        Template::set('provinces', $provinces);
        
        // get province list
        $cities = $this->region_model->city_dropdown_list($ship_address->province, TRUE, lang('ed_select'));
        Template::set('cities', $cities);
        
        Template::set('toolbar_title', lang('order_edit'));
        Template::render();
    }
    //--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	private function save_order($order_id)
	{
		$action = $this->input->post('action');
         
        $data = array();
        switch($action)
        {
            case 'cancel':
                Template::set_message(lang('order_canceled'), 'success');
                
                $data['status']     = 'canceled';
                $data['is_paid']    = 0;
                $data['is_shipped'] = 0;
                break;
            case 'hold':
                Template::set_message(lang('order_holded'), 'success');
                
                $data['status'] = 'holded';
                break;
            case 'unhold':
                Template::set_message(lang('order_unholded'), 'success');
                
                $data['status'] = $this->get_previous_status($order_id);
                break;
            case 'invoice':
                //save invoice
                if($this->orders_model->save_invoice($order_id)) {
                    Template::set_message(lang('order_invoiced'), 'success');

                    $data['is_paid'] = 1;
                    $data['status']  = $this->get_next_status($order_id, 'invoice');
                }
                break;
            case 'ship':
                //save invoice
                if($this->orders_model->save_shipment($order_id)) {
                    Template::set_message(lang('order_shipped'), 'success');
                    
                    $data['is_shipped'] = 1;
                    $data['status']     = $this->get_next_status($order_id, 'ship');
                }
                break;
            case 'refund':
                //save invoice
                if($this->orders_model->save_refundment($order_id)) {
                    Template::set_message(lang('order_refunded'), 'success');
                    
                    $data['is_paid']    = 0;
                    $data['is_shipped'] = 0;
                    $data['status']     = 'refunded';
                }
                break;
        }
        
        if(!empty($data)) 
        {
            $this->orders_model->update($order_id, $data);
        }
	}
    
    private function save_shipping_address($order_id)
    {
        //set rules for manufacturer attributes
        $this->form_validation->set_rules("name", 'lang:ed_name','required|trim|max_length[255]|xss_clean');
        $this->form_validation->set_rules("province", 'lang:ed_province','required|trim|max_length[2]|xss_clean');
        $this->form_validation->set_rules("city", 'lang:ed_city','required|trim|max_length[4]|xss_clean');
        $this->form_validation->set_rules("address", 'lang:ed_address','required|trim|xss_clean');
        $this->form_validation->set_rules("email", 'lang:ed_email','required|trim|xss_clean|valid_email');
        $this->form_validation->set_rules("phone", 'lang:ed_phone','trim|xss_clean');

        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }
        
        $data = array();
        $data['name']   = $this->input->post('name');
        $data['email']  = $this->input->post('email');
        $data['phone']  = $this->input->post('phone');
        $data['province'] = $this->input->post('province');
        $data['city']     = $this->input->post('city');
        $data['address']  = $this->input->post('address');
        
        return $this->orders_model->update_shipping_address($order_id, $data);
    }

    private function get_previous_status($order_id)
    {
        $order = $this->orders_model->find($order_id, 1);
        
        if($order['is_paid'] == '1' || $order['is_shipped'] == '1')
        {
            return 'processing';
        }
        else
        {
            return 'pending';
        }
    }
    
    private function get_next_status($order_id, $action)
    {
        $order = $this->orders_model->find($order_id, 1);
        
        if(($action == 'invoice' && $order['is_shipped'] == '1')
            || ($action == 'ship' && $order['is_paid'] == '1'))
        {
            return 'completed';
        }
        else
        {
            return 'processing';
        }
    }
    
    private function send_email($order, $new_status, $msg)
    {
        $site_name = settings_item('site.title');
        
        $data = array(
            'order'       => $order,
            'new_status'  => $new_status,
            'msg'         => $msg
        );
        $email_mess = $this->load->view('_emails/sales/order_update', $data, true);   
        $subject    = sprintf(lang('ed_ord_update_subject'), $site_name, $order->order_no);
        
        // Now send the email
        $this->load->library('emailer/emailer');
        $data = array(
            'to'         => $order->customer_email,
            'subject'    => $subject,
            'message'    => $email_mess
        );
        //print_r( $email_mess); exit;
        $this->emailer->send($data);
    }
	//--------------------------------------------------------------------
    
    //--------------------------------------------------------------------
    // !AJAX METHODS
    //--------------------------------------------------------------------
    public function send_comment()
    {
        if(!$this->input->post('key')
            || !$this->input->post('status')
            || !$this->input->post('msg'))
        {
            return;
        }
        
        //get order_id from key
        $order = $this->orders_model->find_by_key($this->input->post('key'));
        if(empty($order)) 
        {
            return;
        }
          
        // send comment to customer
        $order_id = $order->order_id;
        $status   = $this->input->post('status');
        $msg      = $this->input->post('msg');
        $notify   = $this->input->post('notify')=='1'?'1':'0';
        $visible  = $this->input->post('visible')=='1'?'1':'0';        
        $this->orders_model->save_comment($order_id, $status, $msg, $notify, $visible);
        
        if($notify == '1')
        {
            $this->send_email($order, $status, $msg);
        }
        
        // get all comments
        $comments = $this->orders_model->get_comments($order_id);
        
        $data = array();
        $data['comments'] = $comments;
        echo Template::block('comments', 'orders/sales/comments', $data);
        exit;
    }
}