<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('order_available_cancel'))
{
    function order_available_cancel($order)
    {
        $status  = $order->status;
        $is_paid = $order->is_paid;
        
        $temp = array();
        $temp[] = 'pending';
        if($status == 'processing' && $is_paid == 0)
        {
            $temp[] = 'processing';
        }
        
        if(in_array($status, $temp))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

if ( ! function_exists('order_available_refund'))
{
    function order_available_refund($order)
    {
        $status  = $order->status;
        $is_paid = $order->is_paid;
        
        $temp = array();
        $temp[] = 'completed';
        if($status == 'processing' && $is_paid == 1)
        {
            $temp[] = 'processing';
        }
        
        if(in_array($status, $temp))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

if ( ! function_exists('order_available_hold'))
{
    function order_available_hold($order)
    {
        $status  = $order->status;
        
        $temp = array();
        $temp[] = 'pending';
        $temp[] = 'processing';
        
        if(in_array($status, $temp))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

if ( ! function_exists('order_available_unhold'))
{
    function order_available_unhold($order)
    {
        $status  = $order->status;
        
        $temp = array();
        $temp[] = 'holded';
        
        if(in_array($status, $temp))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

if ( ! function_exists('order_available_invoice'))
{
    function order_available_invoice($order)
    {
        $status  = $order->status;
        $is_paid = $order->is_paid;
        
        $temp = array();
        $temp[] = 'pending';
        if($status == 'processing' && $is_paid == 0)
        {
            $temp[] = 'processing';
        }
        
        if(in_array($status, $temp))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

if ( ! function_exists('order_available_ship'))
{
    function order_available_ship($order)
    {
        $status  = $order->status;
        $is_shipped = $order->is_shipped;
        
        $temp = array();
        $temp[] = 'pending';
        if($status == 'processing' && $is_shipped == 0)
        {
            $temp[] = 'processing';
        }
        
        if(in_array($status, $temp))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}