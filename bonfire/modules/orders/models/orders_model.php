<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends BF_Model {

	protected $table		= "orders";
	protected $key			= "order_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";
	
    var $CI;
    
    function __construct() 
    {
        $this->CI =& get_instance();
    }
    
    function get_shipping_name($id)
    {
        $default_lang = get_language_id_by_code(settings_item('site.default_language'));   
        $golbal_lang  = global_language_id();
        
        $record = $this->db
                    ->where(array('method_id' => $id, 'language_id' => $golbal_lang))
                    ->get('shipping_methods_info')
                    ->row_array();
         
        if(empty($record))
        {
            $record = $this->db
                    ->where(array('method_id' => $id, 'language_id' => $default_lang))
                    ->get('shipping_methods_info')
                    ->row_array();
        } 
        
        if(empty($record))
        {
            return '';
        }
        
        return $record['name'];
    }
    
    //--------------------------------------------------------------------
    // !ORDER METHODS
    //--------------------------------------------------------------------    
    public function find_short_order($id, $return_type=0)
    {
        $record = parent::find($id);
        if(empty($record)) return FALSE;
        
        $record->ship_address = $this->get_shipping_address($id);
        $record->order_items  = $this->get_items($id); 
        if($this->get_shipping_name($record->shipping_code) != '')
        {
            $record->shipping_method = $this->get_shipping_name($record->shipping_code);
        }
        
        if($return_type == 1) 
        {
            $record = (array)$record;
        }
        
        return $record;
    }
      
    public function find_by_key($key)
    {          
        return $this->find_by('MD5(order_id) =', $key);
    }
    
    public function find($id, $return_type=0)
    {
        $record = parent::find($id);
        if(empty($record)) return FALSE;
        
        $record->ship_address = $this->get_shipping_address($id);
        $record->order_items  = $this->get_items($id);
        $record->comments     = $this->get_comments($id);
        if($this->get_shipping_name($record->shipping_code) != '')
        {
            $record->shipping_method = $this->get_shipping_name($record->shipping_code);
        }
        
        if($return_type == 1) 
        {
            $record = (array)$record;
        }
        
        return $record;
    }
    
    public function count_all()
    {
        $this->select('orders.*, orders_address.name receiver_name')
            ->join('orders_address', 'orders_address.order_id=orders.order_id', 'left');
                    
        return parent::count_all();
    }
    
    public function find_all($return_type=0)
    {
        $this->select('orders.*, orders_address.name receiver_name')
            ->join('orders_address', 'orders_address.order_id=orders.order_id', 'left');
                    
        return parent::find_all($return_type);
    }
    
    public function save($order, $items, $address)
	{
	    $order_id = $this->insert($order);
        if(is_numeric($order_id))
        {
            // save order number again
            $this->_save_order_number($order_id);
            
            // save order items
            $this->save_items($order_id, $items);
            
            // save address 
            $this->save_shipping_address($order_id, $address);
            
            // save order status by comment
            $this->save_comment($order_id, 'pending');
            
            return $order_id;    
        }
        
        return FALSE;
	}
    
    public function update($order_id, $data)
    {
        if(parent::update($order_id, $data))
        {
            $this->save_comment($order_id, $data['status']);
        }
    }
    
    private function _save_order_number($order_id)
    {
        $order_no    = generate_order_number($order_id);
        parent::update($order_id, array('order_no' => $order_no));
    }
    
    //--------------------------------------------------------------------
    // !ITEM METHODS
    //--------------------------------------------------------------------
    public function get_option_name($option_id)
    {
        $record = $this->db->where('option_id', $option_id)->get('products_options')->row();
        
        if(empty($record))
        {
            return 0;
        }
        
        return $record->name;
    }
    
    public function item_option_string($options)
    {
        if(empty($options) || count($options) == 0) return '';
        
        $result = array();
        foreach($options as $opt_id => $opt_value)
        {
            $opt_name = $this->get_option_name($opt_id);
            $result[$opt_name] = $opt_value;
        }
        return json_encode($result);
    }
    
    public function save_items($order_id, $items)
    {
        $languages = $this->CI->languages_model->find_all(1);
        $product_model = $this->CI->products_model;
        
        foreach($items as $item)
        {
            $data = array();
            $data['order_id']   = $order_id;
            $data['product_id'] = $item->product_id;
            $data['sku']        = $item->sku;
            $data['price']      = $item->price;
            $data['qty']        = $item->qty;
            $data['options']    = $this->item_option_string($item->options);
            $this->db->insert('orders_items', $data);
            
            $item_id    = $this->db->insert_id();
            $item_names = $product_model->get_names($item->product_id);
            foreach($languages as $lang)
            {
                $l = $lang['language_id'];
                                                             
                $data_info = array();
                $data_info['item_id']       = $item_id;
                $data_info['product_id']    = $item->product_id;
                $data_info['language_id']   = $l;
                $data_info['product_name']  = $item_names[$l]['name'];    
                $this->db->insert('orders_items_info', $data_info);
            } 
        }
    }
    
    public function get_items($order_id)
    {
        $language_id = global_language_id();
        
        $records = $this->db
            ->join('orders_items_info oii', "oii.item_id=orders_items.item_id AND oii.language_id={$language_id}", 'left')
            ->where('orders_items.order_id', $order_id)
            ->get('orders_items')
            ->result();
            
        return $records;        
    }
    
    //--------------------------------------------------------------------
    // !ADDRESS METHODS
    //--------------------------------------------------------------------
    public function save_shipping_address($order_id, $address)
    {
        $address['order_id'] = $order_id;
        $this->db->insert('orders_address', $address);
        
        return $this->db->insert_id();
    }
    
    public function get_shipping_address($order_id)
    {
        $language = global_language();
        $record = $this->db->select('orders_address.*, rp.name_chinese province_name, rc.name_chinese city_name')
            ->join('region rp', 'rp.region_id=orders_address.province', 'left')
            ->join('region rc', 'rc.region_id=orders_address.city', 'left')
            ->where('order_id', $order_id)
            ->get('orders_address')
            ->row();
        
        if(empty($record))
        {
            return FALSE;
        }
        
        return $record;
    }
    
    public function update_shipping_address($order_id, $data)
    {
        return $this->db->where('order_id', $order_id)->update('orders_address', $data);
    }
    
    //--------------------------------------------------------------------
    // !COMMENTS METHODS
    //--------------------------------------------------------------------
    public function save_comment($order_id, $status='pending', $message='', $notify=FALSE, $visible=FALSE)
    {
        $comment = array();
        $comment['order_id']    = $order_id;
        $comment['order_status']= $status;
        $comment['message']     = $message;
        $comment['notified']    = $notify?1:0;
        $comment['visibled']    = $visible?1:0;
        $comment['created_on']  = date('Y-m-d H:i:s', time());
        $this->db->insert('orders_comments', $comment);
        
        return $this->db->insert_id();
    }
    
    public function get_comments($order_id)
    {
        $records = $this->db
            ->where('order_id', $order_id)
            ->order_by('created_on', 'desc')
            ->get('orders_comments')
            ->result();
            
        return $records;               
    }
    
    //--------------------------------------------------------------------
    // !INVOICE METHODS
    //--------------------------------------------------------------------
    public function save_invoice($order_id)
    {
        $invoice = array();
        $invoice['order_id']    = $order_id;
        $invoice['created_on']  = date('Y-m-d H:i:s', time());
        $this->db->insert('orders_invoices', $invoice);
        
        $inserted_id = $this->db->insert_id();
        $this->_save_invoice_number($inserted_id);
        
        return $inserted_id;
    }
    
    private function _save_invoice_number($invoice_id)
    {
        $data = array();
        $data['invoice_no'] = generate_order_number($invoice_id);
        $this->db->where('invoice_id', $invoice_id)->update('orders_invoices', $data);
    }
    
    //--------------------------------------------------------------------
    // !SHIPMENT METHODS
    //--------------------------------------------------------------------
    public function save_shipment($order_id)
    {
        $shipment = array();
        $shipment['order_id']    = $order_id;
        $shipment['created_on']  = date('Y-m-d H:i:s', time());
        $this->db->insert('orders_shipments', $shipment);
        
        $inserted_id = $this->db->insert_id();
        $this->_save_shipment_number($inserted_id);
        
        return $inserted_id;
    }
    
    private function _save_shipment_number($shipment_id)
    {
        $data = array();
        $data['shipment_no'] = generate_order_number($shipment_id);
        $this->db->where('shipment_id', $shipment_id)->update('orders_shipments', $data);
    }
    
    //--------------------------------------------------------------------
    // !REFUND METHODS
    //--------------------------------------------------------------------
    public function save_refundment($order_id)
    {
        $refund = array();
        $refund['order_id']    = $order_id;
        $refund['created_on']  = date('Y-m-d H:i:s', time());
        $this->db->insert('orders_refunds', $refund);
        
        $inserted_id = $this->db->insert_id();
        $this->_save_refundment_number($inserted_id);
        
        return $inserted_id;
    }
    
    private function _save_refundment_number($refund_id)
    {
        $data = array();
        $data['refund_no'] = generate_order_number($refund_id);
        $this->db->where('refund_id', $refund_id)->update('orders_refunds', $data);
    }
    
    ////
    function get_customer_orders($customer_id)
    {
        $records = parent::where('customer_id', $customer_id)
            ->order_by('created_on', 'desc')->find_all();    
            
        if(empty($records))
        {
            return FALSE;
        }
        
        $result = array();
        foreach($records as $r)
        {
            $result[] = $this->find($r->order_id);
        }
        
        return $result;
    }
}
