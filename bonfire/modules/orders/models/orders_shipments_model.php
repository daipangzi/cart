<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_shipments_model extends BF_Model {

	protected $table		= "orders_shipments";
	protected $key			= "shipment_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = false;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";
    
    function count_all()
    {
        $total = $this
            ->join('orders', 'orders.order_id=orders_shipments.order_id', 'left')
            ->join('orders_address', 'orders.order_id=orders_address.order_id', 'left');
        
        return parent::count_all();
    }
    
    function find_all($return_type=0)
    {
        $records = $this
            ->select('orders_shipments.shipment_id, orders_shipments.shipment_no, orders_shipments.created_on shipment_created_on, orders.*, orders_address.name rec_name, orders_address.email rec_email, orders_address.phone rec_phone, orders_address.province rec_province, orders_address.city rec_city, orders_address.address rec_address')
            ->join('orders', 'orders.order_id=orders_shipments.order_id', 'left')
            ->join('orders_address', 'orders.order_id=orders_address.order_id', 'left');
        
        return parent::find_all($return_type);
    }
}
