<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_invoices_model extends BF_Model {

	protected $table		= "orders_invoices";
	protected $key			= "invoice_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = false;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";
    
    function count_all()
    {
        $this->join('orders', 'orders.order_id=orders_invoices.order_id', 'left')
            ->join('orders_address', 'orders.order_id=orders_address.order_id', 'left');
        
        return parent::count_all();
    }
    
    function find_all($return_type=0)
    {
        $this->select('orders_invoices.invoice_id, orders_invoices.invoice_no, orders_invoices.created_on invoice_created_on, orders.*, orders_address.name rec_name, orders_address.email rec_email, orders_address.phone rec_phone, orders_address.province rec_province, orders_address.city rec_city, orders_address.address rec_address')
            ->join('orders', 'orders.order_id=orders_invoices.order_id', 'left')
            ->join('orders_address', 'orders.order_id=orders_address.order_id', 'left');
        
        return parent::find_all($return_type);
    }
}
