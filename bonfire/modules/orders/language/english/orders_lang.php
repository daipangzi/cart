<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['order_manage']		= 'Orders';
$lang['order_edit']		    = 'Edit Order';
$lang['order_edit_address'] = 'Edit Shipping Address for Order %s';
$lang['order_invalid_id']	= 'Invalid Orders ID.';
$lang['order_canceled']     = 'The order has been canceled.';
$lang['order_holded']		= 'The order has been put on hold.';
$lang['order_unholded']     = 'The order has been released from holding status.';
$lang['order_shipped']      = 'The shipment has been created.';
$lang['order_invoiced']     = 'The invoice has been created.';
$lang['order_refunded']     = 'The order has been refunded.';
$lang['order_invalid_action'] = 'Invalid order action. Please try again.';

$lang['invoice_manage']     = 'Invoices';
$lang['refund_manage']      = 'Refundments';
$lang['shipment_manage']    = 'Shipments';

$lang['ed_order_list']  = 'Orders';
$lang['ed_invoice_list']= 'Invoices';
$lang['ed_shipment_list']= 'Shipments';
$lang['ed_refund_list'] = 'Refunds';

$lang['order_address_edit_success'] = 'Shipping address successfully saved.';
$lang['order_address_edit_failure'] = 'There was a problem saving the address:';

$lang['ord_notified']    = 'Notified';
$lang['ord_no_notified'] = 'Not Notified';

$lang['ed_ord_info']    = 'Order Information';
$lang['ed_ord_date']    = 'Order Date';
$lang['ed_ord_status']  = 'Order Status';
$lang['ed_ord_ip']      = 'Placed from IP';
$lang['ed_ord_shipping']= 'Shipping Method';
$lang['ed_ord_payment'] = 'Payment Method';
$lang['ed_ord_items']   = 'Items Ordered';
$lang['ed_ord_totals']  = 'Order Totals';
$lang['ed_cust_info']   = 'Customer Information';
$lang['ed_cust_name']   = 'Customer Name';
$lang['ed_ship_address'] = 'Shipping Address';
$lang['ed_ord_update_subject'] = '%s : Order # %s update';
$lang['ed_inv_items']   = 'Items Invoiced';
$lang['ed_shp_items']   = 'Items Shipped';

$lang['invoice_invalid_id'] = 'Invalid Invoice ID';
$lang['invoice_invalid']    = 'Invalid Invoice';
$lang['shipment_invalid_id'] = 'Invalid Invoice ID';
$lang['shipment_invalid']    = 'Invalid Invoice';
$lang['refund_invalid_id'] = 'Invalid ID';
$lang['refund_invalid']    = 'Invalid';

$lang['invoice_create_confirm']     = 'Are you sure you want to create Invoice for this order?';
$lang['refund_create_confirm']      = 'Are you sure you want to refund for this order?';
$lang['shipment_create_confirm']    = 'Are you sure you want to create Shipment for this order?';
$lang['ord_cancel_confirm']     = 'Are you sure you want to cancel this order?';
$lang['email_ord_status']       = 'Your order # %s has been <strong>%s</strong>';
$lang['email_ord_you_can_check']= 'You can check the status of your order by <a href="%s" style="color:#1E7EC8;">logging into your account</a>.';
$lang['email_ord_if_you_question']= 'If you have any questions, please feel free to contact us at<a href="mailto:%s" style="color:#1E7EC8;">%s</a>or by phone at %s.';