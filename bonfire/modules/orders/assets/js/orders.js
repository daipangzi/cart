applySearchRow('list-form', admin_path + "/sales/orders");

$('#send_comment_btn').click(function() {
    $(this).prop('disabled', true);
    $('#loader').show();
    
    $.ajax({
      url: admin_path+'/sales/orders/send_comment',
      type: "post",
      data: {
        'key': $('#key').val(), 
        'status': $('#order_status').val(), 
        'msg': $('#order_comment').val(), 
        'notify': $('#order_comment_notify').prop('checked')?1:0, 
        'visible': $('#order_comment_visible').prop('checked')?1:0, 
        'msg': $('#order_comment').val(), 
        "ci_csrf_token": ci_csrf_token()
      },
      success: function(res){ 
          $('#send_comment_btn').removeProp('disabled');        
          $('#loader').hide();
          
          $('#order_comment').val('')
          $('#order_comment_notify').prop('checked', false);
          $('#order_comment_visible').prop('checked', false);
          
          $('#comment_history').html(res);
      }   
    });    
});