<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends BF_Model {

	protected $table		= "customers";
	protected $key			= "customer_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "registered_on";
	protected $modified_field = "updated_on";
    
    /**
     * Helper Method for Generating Password Hints based on Settings library.
     *
     * Call this method in your controller and echo $password_hints in your view.
     *
     * @access public
     *
     * @return void
     */
    public function password_hints()
    {
        $min_length = (string) $this->settings_lib->item('auth.password_min_length');

        $message = sprintf( lang('password_min_length_help'), $min_length );


        if ( $this->settings_lib->item('auth.password_force_numbers') == 1 )
        {
            $message .= '<br />' . lang('password_number_required_help');
        }

        if ( $this->settings_lib->item('auth.password_force_symbols') == 1 )
        {
            $message .= '<br />' . lang('password_symbols_required_help');
        }

        if ( $this->settings_lib->item('auth.password_force_mixed_case') == 1 )
        {
            $message .= '<br />' . lang('password_caps_required_help');
        }

        Template::set('password_hints', $message);

        unset ($min_length, $message);

    }//end password_hints()

    //--------------------------------------------------------------------

    /**
     * Creates a new customer in the database.
     *
     * Required parameters sent in the $data array:
     * * password
     * * A unique email address
     *
     * If no _group_id_ is passed in the $data array, it will assign the default role from <Roles> model.
     *
     * @access public
     *
     * @param array $data An array of customer information.
     *
     * @return bool|int The ID of the new customer.
     */
    public function insert($data=array())
    {
        if (!$this->_function_check(FALSE, $data))
        {
            return FALSE;
        }

        if (!isset($data['password']) || empty($data['password']))
        {
            $this->error = lang('us_no_password');
            return FALSE;
        }
             
        if (!isset($data['email']) || empty($data['email']))
        {
            $this->error = lang('us_no_email');
            return FALSE;
        }

        // Is this a unique email?
        if ($this->is_unique('email', $data['email']) == FALSE)
        {
            $this->error = lang('us_email_taken');
            return FALSE;
        }

        list($password, $salt) = $this->hash_password($data['password']);

        unset($data['password'], $data['pass_confirm'], $data['submit']);

        $data['password_hash'] = $password;
        $data['salt'] = $salt;

        $id = parent::insert($data);

        Events::trigger('after_create_customer', $id);

        return $id;

    }//end insert()

    //--------------------------------------------------------------------

    /**
     * Updates an existing customer. Before saving, it will:
     * * generate a new password/salt combo if both password and pass_confirm are passed in.
     * * store the country code
     *
     * @access public
     *
     * @param int   $id   An INT with the customer's ID.
     * @param array $data An array of key/value pairs to update for the customer.
     *
     * @return bool TRUE/FALSE
     */
    public function update($id=null, $data=array())
    {
        if ($id)
        {
            $trigger_data = array('customer_id'=>$id, 'data'=>$data);
            Events::trigger('before_customer_update', $trigger_data);
        }

        if (empty($data['pass_confirm']) && isset($data['password']))
        {
            unset($data['pass_confirm'], $data['password']);
        }
        else if (!empty($data['password']) && !empty($data['pass_confirm']) && $data['password'] == $data['pass_confirm'])
        {
            list($password, $salt) = $this->hash_password($data['password']);

            unset($data['password'], $data['pass_confirm']);

            $data['password_hash'] = $password;
            $data['salt'] = $salt;
        }

        $return = parent::update($id, $data);

        if ($return)
        {
            $trigger_data = array('customer_id'=>$id, 'data'=>$data);
            Events::trigger('after_customer_update', $trigger_data);
        }

        return $return;

    }//end update()
    
    function save($data, $address) {
        $id = FALSE;
        if(!isset($data[$this->key]))
        {
            $id = $this->insert($data);
        }
        else  
        {
            $id = $data[$this->key];
            
            $this->update($id, $data);
        }
        
        if($id !== FALSE && $address !== FALSE)
        {
            $address['customer_id'] = $id;
            $this->save_address($id, $address);
        }
        
        return $id;
    }
    
    //--------------------------------------------------------------------
    public function save_user_language($customer_id, $lang)
    {
        $this->update($customer_id, array('language' => $lang));
    }

    //--------------------------------------------------------------------

    /**
     * Finds an individual customer record. Also returns role information for the customer.
     *
     * @access public
     *
     * @param int $id An INT with the customer's ID.
     *
     * @return bool|object An object with the customer's information.
     */
    public function find($id=null)
    {
        if (empty($this->selects))
        {
            $this->select($this->table .'.*, group_name');
        }

        $this->db->join('customer_group', 'customer_group.group_id = customers.group_id', 'left');

        return parent::find($id);

    }//end find()     

    //--------------------------------------------------------------------

    /**
     * Returns all customer records, and their associated role information.
     *
     * @access public
     *
     * @param bool $show_deleted If FALSE, will only return non-deleted customers. If TRUE, will return both deleted and non-deleted customers.
     *
     * @return bool An array of objects with each customer's information.
     */
    public function find_all()
    {
        if (empty($this->selects))
        {
            $this->select($this->table .'.*, group_name');
        }

        $this->db->join('customer_group', 'customer_group.group_id = customers.group_id', 'left');

        return parent::find_all();

    }//end find_all()

    //--------------------------------------------------------------------

    /**
     * Locates a single customer based on a field/value match, with their role information.
     * If the $field string is 'both', then it will attempt to find the customer
     *
     * @access public
     *
     * @param string $field A string with the field to match.
     * @param string $value A string with the value to search for.
     *
     * @return bool|object An object with the customer's info, or FALSE on failure.
     */
    public function find_by($field=null, $value=null)
    {
        $this->db->join('customer_group', 'customer_group.group_id = customers.group_id', 'left');

        if (empty($this->selects))
        {
            $this->select($this->table .'.*, group_name');
        }

        return parent::find_by($field, $value);

    }//end find_by()

    //--------------------------------------------------------------------

    /**
     * Performs a standard delete, but also allows for purging of a record.
     *
     * @access public
     *
     * @param int  $id    An INT with the record ID to delete.
     * @param bool $purge If FALSE, will perform a soft-delete. If TRUE, will permanently delete the record.
     *
     * @return bool TRUE/FALSE
     */
    public function delete($id=0)
    {
        //delete address row
        $this->db->where('customer_id', $id)->delete('customer_address');

        return parent::delete($id);

    }//end delete()

    //--------------------------------------------------------------------


    //--------------------------------------------------------------------
    // !AUTH HELPER METHODS
    //--------------------------------------------------------------------

    /**
     * Generates a new salt and password hash for the given password.
     *
     * @access public
     *
     * @param string $old The password to hash.
     *
     * @return array An array with the hashed password and new salt.
     */
    public function hash_password($old='')
    {
        if (!function_exists('do_hash'))
        {
            $this->load->helper('security');
        }

        $salt = $this->generate_salt();
        $pass = do_hash($salt . $old);

        return array($pass, $salt);

    }//end hash_password()

    //--------------------------------------------------------------------

    /**
     * Create a salt to be used for the passwords
     *
     * @access private
     *
     * @return string A random string of 7 characters
     */
    private function generate_salt()
    {
        if (!function_exists('random_string'))
        {
            $this->load->helper('string');
        }

        return random_string('alnum', 7);

    }//end generate_salt()

    //--------------------------------------------------------------------


    //--------------------------------------------------------------------
    // !HMVC METHOD HELPERS
    //--------------------------------------------------------------------

    /**
     * Returns the most recent login attempts and their description.
     *
     * @access public
     *
     * @param int $limit An INT which is the number of results to return.
     *
     * @return bool|array An array of objects with the login information.
     */
    public function get_login_attempts($limit=15)
    {
        $this->db->limit($limit);
        $this->db->order_by('login', 'desc');
        $query = $this->db->get('customer_login_attempts');

        if ($query->num_rows())
        {
            return $query->result();
        }

        return FALSE;

    }//end get_login_attempts()
    //--------------------------------------------------------------------
    
    //--------------------------------------------------------------------
    // !ADDRESS METHODS
    //--------------------------------------------------------------------
    public function find_address($customer_id)
    {
        $record = $this->db->select('customer_address.*, rp.name_chinese province_name, rc.name_chinese city_name')
            ->join('region rp', 'rp.region_id=customer_address.province', 'left')
            ->join('region rc', 'rc.region_id=customer_address.city', 'left')
            ->where('customer_id', $customer_id)
            ->get('customer_address')
            ->row();
                   
        if(empty($record))
        {
            return FALSE;
        }
           
        return $record;
    }
    
    public function has_address($customer_id)
    {
        $count = $this->db
            ->where('customer_id', $customer_id)
            ->count_all_results('customer_address');
        
        return $count>0?TRUE:FALSE;
    }
    
    public function save_address($customer_id, $data)
    {
        if(!$this->has_address($customer_id))
        {
            $this->db->insert('customer_address', $data);
        }
        else
        {
            $this->db->where('customer_id', $customer_id)->update('customer_address', $data);
        }
        
        return TRUE;
    }
    
    //--------------------------------------------------------------------
    // !GET METHODS
    //--------------------------------------------------------------------
    public function get_email($id)
    {
        $record = parent::find($id);
        
        if(empty($record))
        {
            return FALSE;
        }
        
        return $record->email;
    }
    
    public function get_orders($id)
    {
        $ci = & get_instance();
        $ci->load->model('orders/orders_model');
        
        return $ci->orders_model->get_customer_orders($id);
    }
}
