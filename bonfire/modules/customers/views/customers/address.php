<div class="content_wrap"><div class="wrap">
<?php
if(isset($address))
{
    $address = (array)$address;
}
?>
<div class="profile_menu"><!-- Profile Menu -->
    <ul>
        <li><a href="<?php echo site_url('customers/profile');?>" class="main_bordercolor"><?php echo lang('ed_my_account'); ?></a></li>
        <li><a href="<?php echo site_url('customers/edit_address');?>" class="selected main_bordercolor"><?php echo lang('ed_shipping_address'); ?></a></li>
        <li><a href="#this" class="main_bordercolor">Manage Account</a></li>
    </ul>
    <div class="clear"></div>
</div>
<div class="wrapcon"><!-- Content Wrap-->
<?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'autocomplete' => 'off')); ?>
    <div class="profilepage main_bordercolor"><!-- Content -->
        <div class="profile_content"><!-- profile content-->
            <h2><?php echo lang('ed_shipping_address'); ?></h2>
            <div class="addr_wrap">
                <table border="0" cellpadding="0" cellspacing="0">
                <colgroup>
                    <col width="22%" />
                    <col width="3%" />
                    <col width="" />
                </colgroup>
                <tr>
                    <td align="right"><label for="province"><?php echo lang('ed_province_city'); ?></label> :</td>
                    <td>&nbsp;</td>
                    <td>
                        <?php echo form_dropdown2('province', $provinces, set_value('province', isset($address['province']) ? $address['province'] : ''), 'id="province" class="sub_bordercolor"'); ?>
            <?php echo form_dropdown2('city', $cities, set_value('city', isset($address['city']) ? $address['city'] : ''), 'id="city" class="sub_bordercolor"'); ?>
                    </td>
                </tr>
                <tr>
                    <td align="right"><label for="address"><?php echo lang('ed_address'); ?><span><?php echo lang('label_required'); ?></span></label></td>
                    <td>&nbsp;</td>
                    <td><input type="text" id="address" name="address" value="<?php echo set_value('address', isset($address['address']) ? $address['address'] : '') ?>" class="profile_input" size="50" />
                    <?php if( form_error('address') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('address'); ?>
                        </div>
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td>
                        <input type="submit" id="customer_address_submit" name="save" value="<?php echo lang('ed_save'); ?>" tabindex="5" class="confirm_btn main_bordercolor main_color"/>
                    </td>
                </tr>
                </table>
            </div>
            <br /><br />
        </div><!-- profile content-->
        
        <div class="profile_sidebar"><!-- profile sidebar--><div class="profile_sidebarwrap main_bordercolor">
            <div class="sidebar_info">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <colgroup>
                    <col width="25%" />
                    <col width="2%" />
                    <col width="35%" />
                    <col width="" />
                </colgroup>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2"><strong class="main_color">Jiang</strong></td>
                </tr>
                <tr>
                    <td class="customer_img"><img src="http://lc.edayshop.com/images/548924375892375841386320526.jpg?type=product&width=400&height=0&force=yes" /></td>
                    <td>&nbsp;</td>
                    <td colspan="2">
                        欢迎光临易天团购系统网站！
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><a href="#this" class="main_color">[<?php echo lang('ed_logout'); ?>]</a></td>
                </tr>
                </table>
                
                <div class="profile_tip main_bordercolor">
                    <h4>Email的重要性？</h4>
                    <p>Email是初始注册时的必填项目，也是与帐号
                        密码安全相关的项目，可以用来登陆、找回
                        密码、接收服务通知，请务必真实填写。</p>
                </div>
            </div>
        </div></div><!-- profile sidebar-->
        <div class="clear"></div>
    </div>
    <?php echo form_close(); ?>