<div class="content_wrap"><div class="wrap">

<div class="profile_menu"><!-- Profile Menu -->
    <ul>
        <li><a href="<?php echo site_url('customers/profile');?>" class="selected main_bordercolor"><?php echo lang('ed_my_account'); ?></a></li>
        <li><a href="<?php echo site_url('customers/edit_address');?>" class="main_bordercolor"><?php echo lang('ed_shipping_address'); ?></a></li>
        <li><a href="#this" class="main_bordercolor">Manage Account</a></li>
    </ul>
    <div class="clear"></div>
</div>
<div class="wrapcon"><!-- Content Wrap-->
<?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'autocomplete' => 'off')); ?>
    <div class="profilepage main_bordercolor"><!-- Content -->
        <div class="profile_content"><!-- profile content-->
            <h2><?php echo lang('ed_my_account'); ?></h2>
            <div class="addr_wrap">
                <table border="0" cellpadding="0" cellspacing="0">
                <colgroup>
                    <col width="22%" />
                    <col width="3%" />
                    <col width="" />
                </colgroup>
                <tr>
                    <td align="right"><label for="name"><?php echo lang('ed_name'); ?></label> :</td>
                    <td>&nbsp;</td>
                    <td><input type="text" id="name" name="name" class="profile_input" value="<?php echo set_value('name', isset($customer) ? $customer->name : '') ?>" size="30" /></td>
                </tr>
                <tr>
                    <td align="right"><label for="email"><?php echo lang('ed_email'); ?></label> :</td>
                    <td>&nbsp;</td>
                    <td><input type="text" id="email" name="email" class="profile_input" value="<?php echo set_value('email', isset($customer) ? $customer->email : '') ?>" size="30" /></td>
                </tr>
                <tr>
                    <td align="right"><label for="password"><?php echo lang('ed_password'); ?></label> :</td>
                    <td>&nbsp;</td>
                    <td><input type="password" id="password" class="profile_input" name="password" value="" size="30" /></td>
                </tr>
                <tr>
                    <td align="right"><label for="pass_confirm"><?php echo lang('ed_password_confirm'); ?></label> :</td>
                    <td>&nbsp;</td>
                    <td><input type="password" id="pass_confirm" class="profile_input" name="pass_confirm" value="" size="30" /></td>
                </tr>
                <tr>
                    <td align="right"><label for="qq_code"><?php echo lang('ed_qq'); ?></label> :</td>
                    <td>&nbsp;</td>
                    <td><input type="text" id="qq_code" class="profile_input" name="qq_code" value="<?php echo set_value('qq_code', isset($customer) ? $customer->qq_code : '') ?>" size="30" /></td>
                </tr>
                <tr>
                    <td align="right"><label for="phone"><?php echo lang('ed_phone'); ?></label> :</td>
                    <td>&nbsp;</td>
                    <td><input type="text" id="phone" class="profile_input" name="phone" value="<?php echo set_value('phone', isset($customer) ? $customer->phone : '') ?>" size="30" /></td>
                </tr>
                <tr>
                    <td align="right"><label for="newsletter"><?php echo lang('ed_newsletter'); ?></label> :</td>
                    <td>&nbsp;</td>
                    <td><input type="checkbox" id="newsletter" class="profile_input" name="newsletter" value="1" <?php echo (isset($customer) && $customer->newsletter == 1)?"checked='checked'":"";?> /></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td>
                        <input type="submit" id="submit" name="submit" value="<?php echo lang('ed_save'); ?>" tabindex="5" class="confirm_btn main_bordercolor main_color"/>
                    </td>
                </tr>
                </table>
            </div>
            <br /><br />
        </div><!-- profile content-->
        
        <div class="profile_sidebar"><!-- profile sidebar--><div class="profile_sidebarwrap main_bordercolor">
            <div class="sidebar_info">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <colgroup>
                    <col width="25%" />
                    <col width="2%" />
                    <col width="35%" />
                    <col width="" />
                </colgroup>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2"><strong class="main_color">Jiang</strong></td>
                </tr>
                <tr>
                    <td class="customer_img"><img src="http://lc.edayshop.com/images/548924375892375841386320526.jpg?type=product&width=400&height=0&force=yes" /></td>
                    <td>&nbsp;</td>
                    <td colspan="2">
                        欢迎光临易天团购系统网站！
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><a href="#this" class="main_color">[<?php echo lang('ed_logout'); ?>]</a></td>
                </tr>
                </table>
                
                <div class="profile_tip main_bordercolor">
                    <h4>Email的重要性？</h4>
                    <p>Email是初始注册时的必填项目，也是与帐号
                        密码安全相关的项目，可以用来登陆、找回
                        密码、接收服务通知，请务必真实填写。</p>
                </div>
            </div>
        </div></div><!-- profile sidebar-->
        <div class="clear"></div>
    </div>
    <?php echo form_close(); ?>