<div class="content_wrap"><div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
    <div class="loginpage"><!-- Content -->
        <div class="login_wrap main_bordercolor">
        <?php echo form_open('register', array('class' => "form-horizontal", 'autocomplete' => 'off')); ?>
            <div class="login_title main_bordercolor">
                <h2><?php echo lang('ed_register'); ?></h2>
                <a href="<?php echo site_url('/customers/login'); ?>" class="register_btn main_bgcolor"><?php echo lang('ed_login'); ?></a>
                <div class="clear"></div>
            </div>
            <div class="login_content">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <colgroup>
                    <col width="30%" />
                    <col width="2%" />
                    <col width="40%" />
                    <col width="" />
                </colgroup>
                <tr>
                    <td align="right"><label for="email"><strong><?php echo lang('ed_email'); ?>:</strong></label></td>
                    <td>&nbsp;</td>
                    <td><input type="text" class="login_input sub_bordercolor" id="email" name="email" value="<?php echo set_value('email'); ?>" tabindex="1" placeholder="<?php echo lang('ed_email') ?>" />
                    <?php if( form_error('email') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('email'); ?>
                        </div>
                    <?php } ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right"><label for="name"><strong><?php echo lang('ed_full_name'); ?>:</strong></label></td>
                    <td>&nbsp;</td>
                    <td><input type="text" class="login_input sub_bordercolor" id="name" name="name" value="<?php echo set_value('name'); ?>" tabindex="2" placeholder="" />
                    <?php if( form_error('name') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('name'); ?>
                        </div>
                    <?php } ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right"><label for="password"><strong><?php echo lang('ed_password'); ?>:</strong></label></td>
                    <td>&nbsp;</td>
                    <td><input type="password" class="login_input sub_bordercolor" id="password" name="password" value="" tabindex="3" placeholder="<?php echo lang('ed_password'); ?>" />
                    <?php if( form_error('password') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('password'); ?>
                        </div>
                    <?php } ?>
                    </td>
                    <td><span><?php echo lang('us_password_mins'); ?></span></td>
                </tr>
                <tr>
                    <td align="right"><label for="pass_confirm"><strong><?php echo lang('ed_password_confirm'); ?>:</strong></label></td>
                    <td>&nbsp;</td>
                    <td><input type="password" class="login_input sub_bordercolor" id="pass_confirm" name="pass_confirm" value="" tabindex="4" placeholder="<?php echo lang('ed_password_confirm'); ?>" />
                    <?php if( form_error('pass_confirm') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('pass_confirm'); ?>
                        </div>
                    <?php } ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <input type="checkbox" id="newsletter" name="newsletter" value="1" tabindex="5" />&nbsp;
                        <label for="newsletter"><?php echo lang('ed_newsletter'); ?></label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><input type="submit" id="submit" name="submit" value="<?php echo lang('ed_register'); ?>" tabindex="6" class="confirm_btn main_bordercolor main_color"/></td>
                    <td>&nbsp;</td>
                </tr>
                </table>
            </div>
        <?php echo form_close(); ?>
        </div>
    </div>