<div class="content_wrap"><div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
    <div class="loginpage"><!-- Content -->
        <div class="login_wrap main_bordercolor">
        <?php echo form_open('login', array('class' => "form-horizontal", 'autocomplete' => 'off')); ?>
            <div class="login_title main_bordercolor">
                <h2><?php echo lang('ed_login'); ?></h2>
                <a href="<?php echo site_url('/customers/register'); ?>" class="register_btn main_bgcolor"><?php echo lang('ed_register'); ?></a>
                <div class="clear"></div>
            </div>
            <div class="login_content">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <colgroup>
                    <col width="30%" />
                    <col width="2%" />
                    <col width="40%" />
                    <col width="" />
                </colgroup>
                <tr>
                    <td>&nbsp;</td><td>&nbsp;</td>
                    <td>
                        <?php echo Template::message(); ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right"><label for="login_value"><strong><?php echo lang('ed_email'); ?>:</strong></label></td>
                    <td>&nbsp;</td>
                    <td><input type="text" class="login_input sub_bordercolor" id="login_value" name="login" value="<?php echo set_value('login'); ?>" tabindex="1" placeholder="<?php echo lang('ed_email') ?>" /></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right"><label for="password"><strong><?php echo lang('ed_password'); ?>:</strong></label></td>
                    <td>&nbsp;</td>
                    <td><input type="password" class="login_input sub_bordercolor" id="password" name="password" value="" tabindex="2" placeholder="<?php echo lang('ed_password'); ?>" /></td>
                    <td>&nbsp;&nbsp;<a href="<?php echo site_url('/customers/forgot_password'); ?>" class="wangji main_color"><?php echo lang('us_forgot_your_password'); ?></a></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <input type="checkbox" id="remember_me" name="remember_me" value="1" tabindex="3" />&nbsp;
                        <label for="remember_me"><?php 
                            $remember_length = settings_item('auth.remember_length');
                            $prefix = '';
                            if($remember_length == '604800') $prefix = '1 '.lang('ed_week');
                            if($remember_length == '1209600') $prefix = '2 '.lang('ed_week');
                            if($remember_length == '1814400') $prefix = '3 '.lang('ed_week');
                            if($remember_length == '2592000') $prefix = '30 '.lang('ed_days');
                            echo sprintf(lang('us_remember_note'), $prefix); 
                        ?></label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><input type="submit" id="submit" name="submit" value="<?php echo lang('ed_login'); ?>" tabindex="5" class="confirm_btn main_bordercolor main_color"/></td>
                    <td>&nbsp;</td>
                </tr>
                </table>
            </div>
        <?php echo form_close(); ?>
        </div>
    </div>