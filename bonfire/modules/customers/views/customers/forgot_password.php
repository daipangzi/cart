<div class="content_wrap"><div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
    <div class="loginpage"><!-- Content -->
        <div class="login_wrap main_bordercolor">
        <?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal", 'autocomplete' => 'off')); ?>
            <div class="login_title main_bordercolor">
                <h2><?php echo lang('us_forgot_your_password'); ?></h2>
                <a href="<?php echo site_url('/customers/login'); ?>" class="register_btn main_bgcolor"><?php echo "Login"; ?></a>
                <div class="clear"></div>
            </div>
            <div class="login_content">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <colgroup>
                    <col width="30%" />
                    <col width="2%" />
                    <col width="40%" />
                    <col width="" />
                </colgroup>
                <tr>
                    <td>&nbsp;</td><td>&nbsp;</td>
                    <td>
                        <?php echo Template::message(); ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right"><strong><?php echo lang('ed_email'); ?>:</strong></td>
                    <td>&nbsp;</td>
                    <td><input type="text" class="login_input sub_bordercolor" id="email" name="email" value="<?php echo set_value('email') ?>" tabindex="1" /></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><input type="submit" id="submit" name="submit" value="Send Password" tabindex="2" class="confirm_btn main_bordercolor main_color"/></td>
                    <td>&nbsp;</td>
                </tr>
                </table>
            </div>
        <?php echo form_close(); ?>
        </div>
    </div>