<div class="content_wrap"><div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
    <div class="loginpage"><!-- Content -->
        <div class="login_wrap main_bordercolor">
        <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
            <input type="hidden" name="customer_id" value="<?php echo $customer->customer_id ?>" />
            <div class="login_title main_bordercolor">
                <h2><?php echo lang('us_reset_password'); ?></h2>
                <div class="clear"></div>
            </div>
            <div class="login_content">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <colgroup>
                    <col width="30%" />
                    <col width="2%" />
                    <col width="40%" />
                    <col width="" />
                </colgroup>
                <tr>
                    <td>&nbsp;</td><td>&nbsp;</td>
                    <td>
                        <div class="alert alert-info fade in">
                            <a data-dismiss="alert" class="close">&times;</a>
                            <h4 class="alert-heading">Enter your new password below to reset your password.</h4>
                        </div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right"><label for="password"><strong><?php echo lang('bf_password'); ?></strong></label></td>
                    <td>&nbsp;</td>
                    <td><input type="password" class="login_input sub_bordercolor" id="password" name="password" value="" tabindex="1" placeholder="Password..." />
                    <?php if( form_error('password') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('password'); ?>
                        </div>
                    <?php } ?>
                    </td>
                    <td><span><?php echo lang('us_password_mins'); ?></span></td>
                </tr>
                <tr>
                    <td align="right"><label for="pass_confirm"><strong><?php echo lang('bf_password'); ?></strong></label></td>
                    <td>&nbsp;</td>
                    <td><input type="password" class="login_input sub_bordercolor" id="pass_confirm" name="pass_confirm" value="" tabindex="2" placeholder="<?php echo lang('bf_password_confirm'); ?>" />
                    <?php if( form_error('pass_confirm') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('pass_confirm'); ?>
                        </div>
                    <?php } ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><input type="submit" id="submit" name="submit" value="Save New Password" tabindex="5" class="confirm_btn main_bordercolor main_color"/></td>
                    <td>&nbsp;</td>
                </tr>
                </table>
            </div>
        <?php echo form_close(); ?>
        </div>
    </div>