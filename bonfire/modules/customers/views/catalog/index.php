<div class="admin-box">
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" id="list-form"'); ?>
    <fieldset><legend><?php echo lang('customer_list'); ?><small><?php echo $this->pagination->get_page_status();?></small></legend></fieldset>
        
    <div class="form-buttons">
        <?php if(!empty($search)) :?>
        <?php echo anchor(SITE_AREA . '/catalog/products', lang('bf_action_reset_filter'), 'class="btn"'); ?>                    
        <?php endif; ?>
        
        <button type="submit" name="search" id="search-entries" class="btn" value="search"><span class="icon-zoom-in"></span>&nbsp;<?php echo lang('bf_action_filter') ?></button>
        
        <?php if ($this->auth->has_permission('Customers.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
        <button type="submit" name="delete" id="delete-me" class="btn" value="<?php echo lang('bf_action_delete') ?>" onclick="return confirm('<?php echo lang('customer_delete_list_confirm'); ?>')"><span class="icon-remove-sign"></span>&nbsp;<?php echo lang('bf_action_delete') ?></button>            
        <?php endif;?>
        
        <?php if ($this->auth->has_permission('Customers.Catalog.Create')) : ?>
        <?php echo anchor(SITE_AREA .'/catalog/customers/create', '<span class="icon-plus-sign"></span>&nbsp;'.lang('bf_action_insert'), 'class="btn"'); ?>                    
        <?php endif;?>
    </div>
    
    <div class="responsive">
		<table class="table table-striped lrborder checkAll">
            <colgroup>
                <?php if ($this->auth->has_permission('Customers.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                <col width="40"/>
                <?php endif;?>
                <col width="100"/>
                <col width=""/>
                <col width="200"/>
                <col width="120"/>
                <col width="120"/>
                <col width="200"/>
                <col width="120"/>
                <col width="100"/>
            </colgroup>
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Customers.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					
                    <th class="<?php echo sort_classes($params['orderby'], "id", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/customers?orderby=id&amp;order='.sort_direction($params['orderby'], "id", $params['order'])); ?>"><span><?php echo lang('ed_id'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "name", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/customers?orderby=name&amp;order='.sort_direction($params['orderby'], "name", $params['order'])); ?>"><span><?php echo lang('ed_name'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "email", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/customers?orderby=email&amp;order='.sort_direction($params['orderby'], "email", $params['order'])); ?>"><span><?php echo lang('ed_email'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "phone", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/customers?orderby=phone&amp;order='.sort_direction($params['orderby'], "phone", $params['order'])); ?>"><span><?php echo lang('ed_phone'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "qq_code", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/customers?orderby=qq_code&amp;order='.sort_direction($params['orderby'], "qq_code", $params['order'])); ?>"><span><?php echo lang('ed_qq'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "registered", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/customers?orderby=registered&amp;order='.sort_direction($params['orderby'], "registered", $params['order'])); ?>"><span><?php echo lang('ed_customer_since'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/catalog/customers?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ed_status'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
					<th><?php echo lang('ed_actions'); ?></th>
				</tr>
                <tr id="search_row">
                    <?php if ($this->auth->has_permission('Customers.Catalog.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                    <td>&nbsp;</td>
                    <?php endif;?>
                    
                    <td style="text-align:right;">
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="right wd30px noBottomMargin" name="search[from_id]" value="<?php echo isset($search['from_id'])?$search['from_id']:''; ?>" />
                        <div class="clear"></div>
                        <hr>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="right wd30px noBottomMargin" name="search[to_id]" value="<?php echo isset($search['to_id'])?$search['to_id']:''; ?>"/>
                    </td>
                    <td><input type="text" name="search[name]" value="<?php echo isset($search['name'])?$search['name']:''; ?>" style="width:90%;"><br/></td>
                    <td><input type="text" name="search[email]" value="<?php echo isset($search['email'])?$search['email']:''; ?>" style="width:90%;"><br/></td>
                    <td><input type="text" name="search[phone]" value="<?php echo isset($search['phone'])?$search['phone']:''; ?>" style="width:90%;"><br/></td>
                    <td><input type="text" name="search[qq_code]" value="<?php echo isset($search['qq_code'])?$search['qq_code']:''; ?>" style="width:90%;"><br/></td>
                    <td style="text-align:right;">
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="right wd100px noBottomMargin date" name="search[from_date]" value="<?php echo isset($search['from_date'])?$search['from_date']:''; ?>"/>                                   
                        <div class="clear"></div>
                        <hr/>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="right wd100px noBottomMargin date" name="search[to_date]" value="<?php echo isset($search['to_date'])?$search['to_date']:''; ?>"/>
                    </td>
                    <td><?php echo form_dropdown2('search[status]', array('-1' => '',"1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value('search[status]', isset($search['status']) ? $search['status'] : '-1'), 'class="nostyle autowidth"'); ?></td>
                    <td></td>
                </tr>
			</thead>
            <tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Customers.Catalog.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->customer_id ?>" /></td>
					<?php endif;?>
					
                    <td><?php echo $record->customer_id?></td>
				    <?php if ($this->auth->has_permission('Customers.Catalog.Edit')) : ?>
				    <td><?php echo anchor(SITE_AREA ."/catalog/customers/edit/{$record->customer_id}/".md5($record->customer_id), '' .  $record->name) ?></td>
				    <?php else: ?>
				    <td><?php echo $record->name ?></td>
				    <?php endif; ?>
			    
                    <td><?php echo $record->email ?></td>
				    <td><?php echo $record->phone ?></td>
                    <td><?php echo $record->qq_code ?></td>
				    <td><?php echo format_date_time($record->registered_on);?></td>
                    <td><?php echo $record->active==1?lang("ed_enable"):lang("ed_disable"); ?></td>
                    <td>
                        <?php if ($this->auth->has_permission('Customers.Catalog.Edit')) : ?>
                        <a href="<?php echo site_url(SITE_AREA .'/catalog/customers/edit/'.$record->customer_id.'/'.md5($record->customer_id)) ?>" title="<?php echo lang('bf_action_edit'); ?>" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                        <?php endif; ?>
                        
                        <?php if ($this->auth->has_permission('Customers.Catalog.Delete')) : ?>
                        <a href="<?php echo site_url(SITE_AREA .'/catalog/customers/delete/'. $record->customer_id.'/'.md5($record->customer_id)) ?>" title="<?php echo lang('bf_action_delete'); ?>" class="tip"><span class="icon12 icomoon-icon-remove"></span></a>
                        <?php endif; ?>
                    </td>
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="11"><?php echo lang('no_record'); ?></td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
    </div>
	<?php echo form_close(); ?>
    <?php echo $this->pagination->create_links(); ?>
</div>