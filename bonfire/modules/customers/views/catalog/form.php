<?php
$title = lang('customer_create');
$back_url = isset($prev_page)?$prev_page:SITE_AREA .'/catalog/customers';
if(isset($customer)) {
    $customer = (array)$customer;
    $title = $customer['name'];
}
?>
<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'id="customer-form" class="form-horizontal" autocomplete="off"'); ?>
    <input type="hidden" name="image_name" id="image_name" value="<?php echo set_value('image_name', isset($image_name)?$image_name:''); ?>"/>
    <input type="hidden" name="image_action" id="image_action" value=""/>
    <input type="hidden" name="key" value="<?php echo md5(isset($customer['customer_id']) ? $customer['customer_id'] : '0');?>"/>
    <input type="hidden" name="selected_tab" id="selected_tab" value="<?php echo $selected_tab; ?>"/>
    <input type="hidden" id="param_orderby" name="orderby" value="<?php echo $params['orderby']; ?>"/>
    <input type="hidden" id="param_order" name="order" value="<?php echo $params['order']; ?>"/>
    <input type="hidden" id="last_offset" name="last_offset" value="<?php echo $offset; ?>"/>
    
    <fieldset>
        <legend><?php echo $title; ?></legend>
        
        <div class="form-buttons">
            <?php echo anchor($back_url, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>
            <button type="reset" name="reset" class="btn" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
            
            <?php if(isset($customer)) :?>
            <?php if ($this->auth->has_permission('Customers.Catalog.Delete')) : ?>
            <button type="submit" name="delete" class="btn" id="delete-me" onclick="return confirm('<?php echo lang('customer_delete_confirm'); ?>')">
                <i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete'); ?>
            </button>
            <?php endif; ?>
            <?php endif; ?>
            
            <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
            <button type="submit" name="save_continue" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save_continue'); ?></button>&nbsp;
        </div>
        
        <ul id="myTab1" class="nav nav-tabs">
            <li class="active"><a href="#account-tab" data-toggle="tab"><?php echo lang('ed_account'); ?></a></li>
            <li><a href="#address-tab" data-toggle="tab"><?php echo lang('ed_address'); ?></a></li>
            <li><a href="#order-tab" data-toggle="tab"><?php echo lang('ed_orders'); ?></a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade in active" id="account-tab">  
                <div class="control-group <?php echo form_error('email') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_email').lang('bf_form_label_required'), 'email', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="email" type="email" name="email" maxlength="120" class="span4" value="<?php echo set_value('email', isset($customer['email']) ? $customer['email'] : ''); ?>"  />
                        <span class="help-inline"><?php echo form_error('email'); ?></span>
                    </div>
                </div>
                
                <div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_name').lang('bf_form_label_required'), 'name', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="name" type="text" name="name" maxlength="100" class="span4" value="<?php echo set_value('name', isset($customer['name']) ? $customer['name'] : ''); ?>"  />
                        <span class="help-inline"><?php echo form_error('name'); ?></span>
                    </div>
                </div>
                
                <?php if(!isset($customer)) : ?>
                <div class="control-group <?php echo form_error('password') ? 'error' : ''; ?>">
                    <label class="control-label required" for="password"><?php echo lang('ed_password').lang('label_required'); ?></label>
                    <div class="controls">
                        <input class="span4" type="password" name="password" id="password" value=""/>
                        <span class="help-inline"><?php echo form_error('password'); ?></span>
                    </div>
                </div>

                <div class="control-group <?php echo form_error('pass_confirm') ? 'error' : ''; ?>">
                    <label class="control-label required" for="pass_confirm"><?php echo lang('ed_password_confirm').lang('label_required'); ?></label>
                    <div class="controls">
                        <input class="span4" type="password" name="pass_confirm" id="pass_confirm" value="" />
                        <span class="help-inline"><?php echo form_error('pass_confirm'); ?></span>
                    </div>
                </div>
                <?php else: ?>
                <input type="hidden" id="require_change" name="require_change" value=""/>
                <div class="control-group">
                    <div class='controls'>
                        <span id="change_password" class="gray cursor"><?php echo lang('change_password'); ?></span>
                    </div>
                </div> 
                <div id="pass_section" style="<?php echo ($require_change=='1')?'':'display:none;';?>">
                    <div class="control-group <?php echo form_error('password') ? 'error' : ''; ?>">
                        <label class="control-label required" for="password"><?php echo lang('ed_password').lang('label_required'); ?></label>
                        <div class="controls">
                            <input class="span4" type="password" name="password" id="password" value=""/>
                            <span class="help-inline"><?php echo form_error('password'); ?></span>
                        </div>
                    </div>

                    <div class="control-group <?php echo form_error('pass_confirm') ? 'error' : ''; ?>">
                        <label class="control-label required" for="pass_confirm"><?php echo lang('ed_password_confirm').lang('label_required'); ?></label>
                        <div class="controls">
                            <input class="span4" type="password" name="pass_confirm" id="pass_confirm" value="" />
                            <span class="help-inline"><?php echo form_error('pass_confirm'); ?></span>
                        </div>
                    </div>    
                </div>
                <?php endif; ?>
                
                <div class="control-group <?php echo form_error('phone') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_phone'), 'phone', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="phone" type="text" name="phone" maxlength="20" class="span4" value="<?php echo set_value('phone', isset($customer['phone']) ? $customer['phone'] : ''); ?>"  />
                        <span class="help-inline"><?php echo form_error('phone'); ?></span>
                    </div>
                </div>
                
                <div class="control-group <?php echo form_error('qq_code') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_qq'), 'qq_code', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="qq_code" type="text" name="qq_code" maxlength="20" class="span4" value="<?php echo set_value('qq_code', isset($customer['qq_code']) ? $customer['qq_code'] : ''); ?>" />
                        <span class="help-inline"><?php echo form_error('qq_code'); ?></span>
                    </div>
                </div>
                
                <?php 
                $status = array("1" => lang("ed_enable"), "0" => lang("ed_disable"));
                echo form_dropdown('status', $status, set_value('status', isset($customer['active']) ? $customer['active'] : '111'), lang('ed_status'), 'id="status" class="nostyle"');
                ?>
                
                <div class="control-group">
                    <?php echo form_label(lang('ed_newsletter'), 'newsletter', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="newsletter" type="checkbox" name="newsletter" value="1" <?php echo (isset($customer['newsletter']) && $customer['newsletter'] == 1) ? 'checked="checked"' : ''; ?>/>
                    </div>
                </div> 
                
                <div class="control-group">
                    <?php echo form_label(lang('ed_avatar'), 'avatar_image', array('class' => "control-label") ); ?>
                    <div class='controls'>     
                        <span class="imgwrapper"><span class="image marginB10 w100_h80">
                            <img src="<?php echo media_file(isset($img)?$img:'', isset($img_path)?$img_path:'', 100, 80); ?>" alt="" id="avatarImage" class="" width="100" height="80"/>
                            <span id="remover" class="remove" <?php if($img=='') echo 'style="display:none;"';?>><i class="icon-remove">&nbsp;</i></span>
                        </span></span>
                        <span id="errors" class="help-inline" style="color:#B94A48;"><?php echo isset($image_error)?$image_error:''; ?></span>
                        <br/>
                        
                        <input type="file" name="avatar_image" id="avatar_image" class="" value=""/>
                        <span id="loading" class="help-inline"></span>
                    </div>
                </div>
                
            </div>
        
            <div class="tab-pane fade in" id="address-tab"> 
                <div class="control-group">
                    <?php echo form_label(lang('ed_province_city'), 'province', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <?php echo form_dropdown2('province', $provinces, set_value('province', isset($address->province) ? $address->province : ''), 'id="province" class="nostyle"'); ?>
                        <?php echo form_dropdown2('city', $cities, set_value('city', isset($address->city) ? $address->city : ''), 'id="city" class="nostyle"'); ?>   
                    </div>
                </div>
                
                <div class="control-group">
                    <?php echo form_label(lang('ed_address'), 'address', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="address" type="text" name="address" maxlength="20" class="span4" value="<?php echo set_value('address', isset($address->address) ? $address->address : ''); ?>"/>
                    </div>
                </div>
                
                <div class="control-group">
                    <?php echo form_label(lang('ed_zipcode'), 'address', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="zipcode" type="text" name="zipcode" maxlength="20" class="span4" value="<?php echo set_value('zipcode', isset($address->zipcode) ? $address->zipcode : ''); ?>"/>
                    </div>
                </div>
            </div>
            
            <div class="tab-pane fade in" id="order-tab" loaded="false"> 
                <div class="customer-orders">
                </div>    
            </div>
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>