<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Bonfire
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   Bonfire
 * @author    Bonfire Dev Team
 * @copyright Copyright (c) 2011 - 2012, Bonfire Dev Team
 * @license   http://guides.cibonfire.com/license.html
 * @link      http://cibonfire.com
 * @since     Version 1.0
 * @filesource
 */

class Customer_auth
{

	/**
	 * An array of errors generated.
	 *
	 * @access public
	 *
	 * @var array
	 */
	public $errors = array();

	/**
	 * Stores the logged in value after the first test to improve performance.
	 *
	 * @access private
	 *
	 * @var NULL
	 */
	private $logged_in = NULL;

	/**
	 * Stores the ip_address of the current customer for performance reasons.
	 *
	 * @access private
	 *
	 * @var string
	 */
	private $ip_address;

	/**
	 * A pointer to the CodeIgniter instance.
	 *
	 * @access private
	 *
	 * @var object
	 */
	private $ci;

	//--------------------------------------------------------------------

	/**
	 * Grabs a pointer to the CI instance, gets the customer's IP address,
	 * and attempts to automatically log in the customer.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->ci =& get_instance();

		$this->ip_address = $this->ci->input->ip_address();

		// We need the customers language file for this to work
		// from other modules.
		$this->ci->lang->load('customers/customers');
        $this->ci->lang->load('users/users');

		log_message('debug', 'Customer Auth class initialized.');

		if (!class_exists('CI_Session'))
		{
			$this->ci->load->library('session');
		}

		// Try to log the customer in from session/cookie data
		$this->autologin();

	}//end __construct()

	//--------------------------------------------------------------------

	/**
	 * Attempt to log the customer in.
	 *
	 * @access public
	 *
	 * @param string $login    The customer's login credentials (email/username)
	 * @param string $password The customer's password
	 * @param bool   $remember Whether the customer should be remembered in the system.
	 *
	 * @return bool
	 */
	public function login($login=NULL, $password=NULL, $remember=FALSE) 
	{                                                                   
		if (empty($login) || empty($password))
		{
			$error = lang('ed_email');
			Template::set_message(sprintf(lang('us_fields_required'), $error), 'error');
			return FALSE;
		}

		if (!class_exists('Customer_model'))
		{
			$this->ci->load->model('customers/Customer_model', 'customer_model', TRUE);
		}

		// Grab the customer from the db
		$selects = 'customer_id, email, name, salt, password_hash, deleted, active';

		$customer = $this->ci->customer_model->select($selects)->find_by('email', $login);

		// check to see if a value of FALSE came back, meaning that the name or email or password doesn't exist.
		if($customer == FALSE)
		{
			Template::set_message(lang('us_bad_email_pass'), 'error');
			return FALSE;
		}

		if (is_array($customer))
		{
			$customer = $customer[0];
		}

		// check if the account has been activated.
        if ($customer->active == 0)
		{
		    Template::set_message(lang('us_account_not_active'), 'error');
			return FALSE;
		}

		// check if the account has been soft deleted.
		if ($customer->deleted >= 1) // in case we go to a unix timestamp later, this will still work.
		{
			Template::set_message(sprintf(lang('us_account_deleted'), settings_item("site.system_email")), 'error');
			return FALSE;
		}

		if ($customer)
		{
			// Validate the password
			if (!function_exists('do_hash'))
			{
				$this->ci->load->helper('security');
			}

			// Try password
			if (do_hash($customer->salt . $password) == $customer->password_hash)
			{
				$this->clear_login_attempts($login);

				// We've successfully validated the login, so setup the session     
				$this->setup_session($customer->customer_id, $customer->email, $customer->password_hash, $remember,'', $customer->name);

				// Save the login info
				$data = array(
					'last_login'			=> date('Y-m-d H:i:s', time()),
					'last_ip'				=> $this->ip_address,
				);
				$this->ci->customer_model->update($customer->customer_id, $data);

				$trigger_data = array('customer_id' => $customer->customer_id);
								
				Events::trigger('after_customer_login', $trigger_data );

				return TRUE;
			}

			// Bad password
			else
			{
				Template::set_message(lang('us_bad_email_pass'), 'error');
				$this->increase_login_attempts($login);
			}
		}
		else
		{
			Template::set_message(lang('us_bad_email_pass'), 'error');
		}//end if

		return FALSE;

	}//end login()

	//--------------------------------------------------------------------

	/**
	 * Destroys the autologin information and the current session.
	 *
	 * @access public
	 *
	 * @return void
	 */
	public function logout()
	{
		$data = array(
			'customer_id'	=> $this->customer_id()
		);

		Events::trigger('before_customer_logout', $data);

		// Destroy the autologin information
		$this->delete_autologin();

		// Destroy the session
		//$this->ci->session->sess_destroy();
        $data = array('customer_id' => '',
                    'customer_email' => '',
                    'customer_name' => '',
                    'customer_token' => '',
                    'customer_identity' => '',
                    'customer_logged_in' => '');
        $this->ci->session->unset_userdata($data);

	}//end logout()

	//--------------------------------------------------------------------

	/**
	 * Checks the session for the required info, then verifies against the database.
	 *
	 * @access public
	 *
	 * @return bool|NULL
	 */
	public function is_logged_in()
	{
		// If we've already checked this session,
		// return that.
		if (!is_null($this->logged_in))
		{
			return $this->logged_in;
		}

		if (!class_exists('CI_Session'))
		{
			$this->ci->load->library('session');
		}

		// Is there any session data we can use?
		if ($this->ci->session->userdata('customer_identity') && $this->ci->session->userdata('customer_id'))
		{
			// Grab the user account
			$customer = $this->ci->customer_model->select('customer_id, email, salt, password_hash')->find($this->ci->session->userdata('customer_id'));

			if ($customer !== FALSE)
			{
				if (!function_exists('do_hash'))
				{
					$this->ci->load->helper('security');
				}

				// Ensure customer_token is still equivalent to the SHA1 of the customer_id and password_hash
				if (do_hash($this->ci->session->userdata('customer_id') . $customer->password_hash) === $this->ci->session->userdata('customer_token'))
				{
					$this->logged_in = TRUE;
					return TRUE;
				}
			}
		}//end if

		$this->logged_in = FALSE;
		return FALSE;

	}//end is_logged_in()

	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !UTILITY METHODS
	//--------------------------------------------------------------------

	/**
	 * Retrieves the customer_id from the current session.
	 *
	 * @access public
	 *
	 * @return int
	 */
	public function customer_id()
	{
		return (int) $this->ci->session->userdata('customer_id');

	}//end customer_id()

	//--------------------------------------------------------------------

	/**
	 * Retrieves the logged identity from the current session.
	 * Built from the user's submitted login.
	 *
	 * @access public
	 *
	 * @return string The identity used to login.
	 */
	public function identity()
	{
		return $this->ci->session->userdata('customer_identity');

	}//end identity()

	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !LOGIN ATTEMPTS
	//--------------------------------------------------------------------

	/**
	 * Records a login attempt into the database.
	 *
	 * @access protected
	 *
	 * @param string $login The login id used (typically email or username)
	 *
	 * @return void
	 */
	protected function increase_login_attempts($login=NULL)
	{
		if (empty($this->ip_address) || empty($login))
		{
			return;
		}

		$this->ci->db->insert('customer_login_attempts', array('ip_address' => $this->ip_address, 'login' => $login, 'time' => date('Y-m-d H:i:s', time())));

	}//end increase_login_attempts()

	//--------------------------------------------------------------------

	/**
	 * Clears all login attempts for this user, as well as cleans out old logins.
	 *
	 * @access protected
	 *
	 * @param string $login   The login credentials (typically email)
	 * @param int    $expires The time (in seconds) that attempts older than will be deleted
	 *
	 * @return void
	 */
	protected function clear_login_attempts($login=NULL, $expires = 86400)
	{
		if (empty($this->ip_address) || empty($login))
		{
			return;
		}

		$this->ci->db->where(array('ip_address' => $this->ip_address, 'login' => $login));

		// Purge obsolete login attempts
		$this->ci->db->or_where('UNIX_TIMESTAMP(time) <', time() - $expires);

		$this->ci->db->delete('customer_login_attempts');

	}//end clear_login_attempts()

	//--------------------------------------------------------------------

	/**
	 * Get number of attempts to login occurred from given IP-address and/or login
	 *
	 * @param null $login (Optional) The login id to check for (email/username). If no login is passed in, it will only check against the IP Address of the current user.
	 *
	 * @return int An int with the number of attempts.
	 */
	function num_login_attempts($login=NULL)
	{
		$this->ci->db->select('1', FALSE);
		$this->ci->db->where('ip_address', $this->ip_address);
		if (strlen($login) > 0) $this->ci->db->or_where('login', $login);

		$query = $this->ci->db->get('customer_login_attempts');
		return $query->num_rows();

	}//end num_login_attempts()

	//--------------------------------------------------------------------
	// !AUTO-LOGIN
	//--------------------------------------------------------------------

	/**
	 * Attempts to log the user in based on an existing 'autologin' cookie.
	 *
	 * @access private
	 *
	 * @return void
	 */
	private function autologin()
	{
		if ($this->ci->settings_lib->item('auth.allow_remember') == FALSE)
		{
			return;
		}

		$this->ci->load->helper('cookie');

		$cookie = get_cookie('autologin', TRUE);

		if (!$cookie) {	return;	}

		// We have a cookie, so split it into customer_id and token
		list($customer_id, $test_token) = explode('~', $cookie);

		// Try to pull a match from the database
		$this->ci->db->where( array('customer_id' => $customer_id, 'token' => $test_token) );
		$query = $this->ci->db->get('customer_cookies');

		if ($query->num_rows() == 1)
		{
			// Save logged in status to save on db access later.
			$this->logged_in = TRUE;

			// If a session doesn't exist, we need to refresh our autologin token
			// and get the session started.
			if (!$this->ci->session->userdata('customer_id'))
			{
				// Grab the current user info for the session
				$this->ci->load->model('customers/customer_model', 'customer_model', TRUE);
				$customer = $this->ci->customer_model->select('customer_id, email, password_hash, name')->find($customer_id);

				if (!$customer) { return; }

				$this->setup_session($customer->customer_id, $customer->email, $customer->password_hash, TRUE, $test_token, $customer->name);
			}
		}

		unset($query, $customer);

	}//end autologin()

	//--------------------------------------------------------------------


	/**
	 * Create the auto-login entry in the database. This method uses
	 * Charles Miller's thoughts at:
	 * http://fishbowl.pastiche.org/2004/01/19/persistent_login_cookie_best_practice/
	 *
	 * @access private
	 *
	 * @param int    $customer_id    An int representing the customer_id.
	 * @param string $old_token The previous token that was used to login with.
	 *
	 * @return bool Whether the autologin was created or not.
	 */
	private function create_autologin($customer_id=0, $old_token=NULL)
	{       
		if (empty($customer_id) || $this->ci->settings_lib->item('auth.allow_remember') == FALSE)
		{
			return FALSE;
		}
       
		// Generate a random string for our token
		if (!function_exists('random_string')) { $this->load->helper('string'); }

		$token = random_string('alnum', 128);

		// If an old_token is presented, we're refreshing the autologin information
		// otherwise we're creating a new one.
		if (empty($old_token))
		{
			// Create a new token
			$data = array(
				'customer_id'		=> $customer_id,
				'token'			=> $token,
				'created_on'	=> date('Y-m-d H:i:s')
			);
			$this->ci->db->insert('customer_cookies', $data);
		}
		else
		{
			// Refresh the token
			$this->ci->db->where('customer_id', $customer_id);
			$this->ci->db->where('token', $old_token);
			$this->ci->db->set('token', $token);
			$this->ci->db->set('created_on', date('Y-m-d H:i:s'));
			$this->ci->db->update('customer_cookies');
		}
        
        if ($this->ci->db->affected_rows())
		{
			// Create the autologin cookie
			$this->ci->input->set_cookie('autologin', $customer_id .'~'. $token, $this->ci->settings_lib->item('auth.remember_length'));

			return TRUE;
		}
		else
		{
			return FALSE;
		}

	}//end create_autologin()()

	//--------------------------------------------------------------------

	/**
	 * Deletes the autologin cookie for the current user.
	 *
	 * @access private
	 *
	 * @return void
	 */
	private function delete_autologin()
	{
		if ($this->ci->settings_lib->item('auth.allow_remember') == FALSE)
		{
			return;
		}

		// First things first.. grab the cookie so we know what row
		// in the customer_cookies table to delete.
		if (!function_exists('delete_cookie'))
		{
			$this->ci->load->helper('cookie');
		}

		$cookie = get_cookie('autologin');
        if ($cookie)
		{
			list($customer_id, $token) = explode('~', $cookie);

			// Now we can delete the cookie
			delete_cookie('autologin');

			// And clean up the database
			$this->ci->db->where('customer_id', $customer_id);
			$this->ci->db->where('token', $token);
			$this->ci->db->delete('customer_cookies');
		}

		// Also perform a clean up of any autologins older than 2 months
		$this->ci->db->where('created_on', '< DATE_SUB(CURDATE(), INTERVAL 2 MONTH)');
		$this->ci->db->delete('customer_cookies');

	}//end delete_autologin()

	//--------------------------------------------------------------------

	/**
	 * Creates the session information for the current user. Will also create an autologin cookie if required.
	 *
	 * @access private
	 *
	 * @param int $customer_id          An int with the user's id
	 * @param string $customername      The user's username
	 * @param string $password_hash The user's password hash. Used to create a new, unique customer_token.
	 * @param string $email         The user's email address
	 * @param bool   $remember      A boolean (TRUE/FALSE). Whether to keep the user logged in.
	 * @param string $old_token     User's db token to test against
	 * @param string $customer_name     User's made name for displaying options
	 *
	 * @return bool TRUE/FALSE on success/failure.
	 */
	private function setup_session($customer_id=0, $email='', $password_hash=NULL, $remember=FALSE, $old_token=NULL, $customer_name='')
	{
		if (empty($customer_id) || empty($email))
		{
			return FALSE;
		}

		// What are we using as login identity?
		//Should I use _identity_login() and move bellow code?
		$login = $email;

		// Save the user's session info
		if (!class_exists('CI_Session'))
		{
			$this->ci->load->library('session');
		}

		if (!function_exists('do_hash'))
		{
			$this->ci->load->helper('security');
		}

		$data = array(
			'customer_id'		=> $customer_id,
			'customer_email'	=> $email,
            'customer_name'     => $customer_name,
			'customer_token'	=> do_hash($customer_id . $password_hash),
			'customer_identity' => $login,
			'customer_logged_in'=> TRUE,
		);

		$this->ci->session->set_userdata($data);

		// Should we remember the user?   
		if ($remember == TRUE) //no bug? //Justin Jenkins
		{
			return $this->create_autologin($customer_id, $old_token);
		}

		return TRUE;

	}//end setup_session

	//--------------------------------------------------------------------

	/**
	 * Returns the identity to be used upon user registration.
	 *
	 * @access private
	 * @todo Decision to be made with this method.
	 *
	 * @return void
	 */
	private function _identity_login()
	{
		//Should I move indentity conditional code from setup_session() here?
		//Or should conditional code be moved to auth->identity(),
		//  and if Optional TRUE is passed, it would then determine wich identity to store in userdata?

	}//end _identity_login()

	//--------------------------------------------------------------------

}//end Auth

//--------------------------------------------------------------------

if (!function_exists('customer_auth_errors'))
{
	/**
	 * A utility function for showing authentication errors.
	 *
	 * @access public
	 *
	 * @return string A string with a <ul> tag of any auth errors, or an empty string if no errors exist.
	 */
	function customer_auth_errors()
	{
		$ci =& get_instance();

		$errors = $ci->customer_auth->errors;

		if (count($errors))
		{
			$str = '<ul>';
			foreach ($errors as $e)
			{
				$str .= "<li>$e</li>";
			}

			$str .= "</ul>";

			return $str;
		}

		return '';

	}//end auth_errors()
}

//--------------------------------------------------------------------