<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_customers extends Migration {

	public function up()
	{
		$prefix = $this->db->dbprefix;

		$fields = array(
			'customer_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => 30,
				
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => 120,
				
			),
			'display_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				
			),
			'last_login' => array(
				'type' => 'DATETIME',
				'default' => '0000-00-00 00:00:00',
				
			),
			'language' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => 1,
				
			),
			'deleted' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => '0',
			),
			'registered_on' => array(
				'type' => 'datetime',
				'default' => '0000-00-00 00:00:00',
			),
			'updated_on' => array(
				'type' => 'datetime',
				'default' => '0000-00-00 00:00:00',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('customer_id', true);
		$this->dbforge->create_table('customers');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('customers');

	}

	//--------------------------------------------------------------------

}