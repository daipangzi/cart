<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Customers extends Front_Controller
{
    //--------------------------------------------------------------------

    /**
     * Setup the required libraries etc
     *
     * @retun void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('region_model');
        $this->lang->load('customers');

    }//end __construct()

    //--------------------------------------------------------------------

    /**
     * Presents the login function and allows the customer to actually login.
     *
     * @access public
     *
     * @return void
     */
    public function login()
    {
        // if the customer is not logged in continue to show the login page
        if ($this->customer_auth->is_logged_in() === FALSE)
        {
            if ($this->input->post('submit'))
            {
                $remember = $this->input->post('remember_me') == '1' ? TRUE : FALSE;

                // Try to login
                if ($this->customer_auth->login($this->input->post('login'), $this->input->post('password'), $remember) === TRUE)
                {
                    // Log the Activity
                    $this->activity_model->log_activity($this->customer_auth->customer_id(), lang('customer_log_logged').': ' . $this->input->ip_address(), 'customers');
                    
                    //merge cart with db
                    $this->merge_cart($this->customer_auth->customer_id());
                           
                    if (!empty($this->requested_page))
                    {
                        Template::redirect($this->requested_page);
                    }
                    else
                    {
                        Template::redirect('/customers/profile');
                    }
                }//end if
            }//end if

            Template::set_view('customers/customers/login');
            Template::set('page_title', lang('ed_login'));
            Template::render('no_sidebar');
        }
        else
        {
            Template::redirect('/');
        }//end if

    }//end login()

    //--------------------------------------------------------------------

    /**
     * Calls the auth->logout method to destroy the session and cleanup,
     * then redirects to the home page.
     *
     * @access public
     *
     * @return void
     */
    public function logout()
    {
        if(isset($this->current_customer)) {
            // Log the Activity
            $this->activity_model->log_activity($this->current_customer->customer_id, lang('customer_log_logged_out').': ' . $this->input->ip_address(), 'customers');

            $this->customer_auth->logout();
            
            //clear current cart content
            $this->clear_cart();
        }
        
        redirect('/');

    }//end  logout()

    //--------------------------------------------------------------------

    /**
     * Allows a customer to start the process of resetting their password.
     * An email is allowed with a special temporary link that is only valid
     * for 24 hours. This link takes them to reset_password().
     *
     * @access public
     *
     * @return void
     */
    public function forgot_password()
    {
        // if the customer is not logged in continue to show the login page
        if ($this->customer_auth->is_logged_in() === FALSE)
        {
            if (isset($_POST['submit']))
            {
                $this->form_validation->set_rules('email', 'lang:ed_email', 'required|trim|strip_tags|valid_email|xss_clean');

                if ($this->form_validation->run() === FALSE)
                {
                    Template::set_message(lang('customer_invalid_email'), 'error');
                }
                else
                {
                    // We validated. Does the customer actually exist?
                    $customer = $this->customer_model->find_by('email', $_POST['email']);

                    if ($customer !== FALSE)
                    {
                        // User exists, so create a temp password.
                        $this->load->helpers(array('string', 'security'));

                        $pass_code = random_string('alnum', 40);

                        $hash = do_hash($pass_code . $customer->salt . $_POST['email']);

                        // Save the hash to the db so we can confirm it later.
                        $this->customer_model->update_where('email', $_POST['email'], array('reset_hash' => $hash, 'reset_by' => strtotime("+24 hours") ));

                        // Create the link to reset the password
                        $pass_link = site_url('reset_password/'. str_replace('@', ':', $_POST['email']) .'/'. $hash);

                        // Now send the email
                        $this->load->library('emailer/emailer');

                        $data = array(
                                    'to'    => $_POST['email'],
                                    'subject'    => lang('customer_reset_pass_subject'),
                                    'message'    => $this->load->view('_emails/forgot_password', array('link' => $pass_link), TRUE)
                             );
                                      
                        if ($this->emailer->send($data))
                        {
                            Template::set_message(lang('customer_reset_pass_message'), 'success');
                        }
                        else
                        {
                            Template::set_message(lang('customer_reset_pass_error'). $this->emailer->errors, 'error');
                        }
                    }//end if
                    else 
                    {
                        Template::set_message(lang('customer_invalid_email'), 'error');   
                    }
                }//end if
            }//end if

            Template::set_view('customers/customers/forgot_password');
            Template::set('page_title', lang('customer_password_reset'));
            Template::render('no_sidebar');
        }
        else
        {

            Template::redirect('/');
        }//end if

    }//end forgot_password()

    //--------------------------------------------------------------------

    /**
     * Allows a customer to edit their own profile information.
     *
     * @access public
     *
     * @return void
     */
    public function profile()
    {
        Assets::add_js('customs/profile.js');
        
        if ($this->customer_auth->is_logged_in() === FALSE)
        {
            $this->customer_auth->logout();
            redirect('login');
        }

        if ($this->input->post('submit'))
        {
            $customer_id = $this->current_customer->customer_id;
            if ($this->save_customer($customer_id))
            {
                // Log the Activity
                $customer = $this->customer_model->find($customer_id);
                $log_name = (isset($customer->name) && !empty($customer->name)) ? $customer->name : $customer->email;
                $this->activity_model->log_activity($this->current_customer->customer_id, lang('customer_log_edit_profile') .': '.$log_name, 'customers');

                Template::set_message(lang('customer_profile_updated_success'), 'success');

                // redirect to make sure any language changes are picked up
                Template::redirect('/customers/profile');
                exit;
            }
            else
            {
                Template::set_message(lang('customer_profile_updated_error'), 'error');
            }//end if
        }//end if

        // get the current customer information
        $customer = $this->customer_model->find_by('customer_id', $this->current_customer->customer_id);

        // Generate password hint messages.
        $this->customer_model->password_hints();

        Template::set('customer', $customer);

        Template::set_view('customers/customers/profile');
        Template::render('no_popup');

    }//end profile()

    //--------------------------------------------------------------------
        
    /**
     * Allows a customer to edit their own address information.
     *
     * @access public
     *
     * @return void
     */
    public function edit_address()
    {
        Assets::add_js('customs/profile.js');
        
        if ($this->customer_auth->is_logged_in() === FALSE)
        {
            $this->customer_auth->logout();
            redirect('login');
        }

        $customer_id = $this->current_customer->customer_id;
        if ($this->input->post('save'))
        {
            if ($this->save_address($customer_id))
            {
                Template::set_message(lang('customer_address_updated_success'), 'success');
                Template::redirect('/customers/edit_address');
            }
            else
            {
                Template::set_message(lang('customer_address_updated_error'), 'error');
            }
        }
        Assets::add_module_js('customers', 'customers.js');
        
        // get customer address info
        $address = $this->customer_model->find_address($customer_id);
        Template::set('address', $address);
        
        $provinces = $this->region_model->province_dropdown_list(true, lang('ed_select'));
        Template::set('provinces', $provinces);
        
        $customer_province = '00';
        if($address != FALSE)
        {
            $customer_province = $address->province;
        }
        $cities = $this->region_model->city_dropdown_list($customer_province, true, lang('ed_select'));
        Template::set('cities', $cities);        
        
        Template::set_view('customers/customers/address');
        Template::render('no_popup');

    }//end address()

    //--------------------------------------------------------------------

    /**
     * Allows the customer to create a new password for their account. At the moment,
     * the only way to get here is to go through the forgot_password() process,
     * which creates a unique code that is only valid for 24 hours.
     *
     * @access public
     *
     * @param string $email The email address to check against.
     * @param string $code  A randomly generated alphanumeric code. (Generated by forgot_password() ).
     *
     * @return void
     */
    public function reset_password($email='', $code='')
    {
        // if the customer is not logged in continue to show the login page
        if ($this->customer_auth->is_logged_in() === FALSE)
        {
            // If there is no code, then it's not a valid request.
            if (empty($code) || empty($email))
            {
                Template::set_message(lang('customer_reset_invalid_email'), 'error');
                Template::redirect('/login');
            }

            // Handle the form
            if ($this->input->post('submit'))
            {
                $this->form_validation->set_rules('password', 'lang:bf_password', 'required|trim|strip_tags|min_length[8]|max_length[120]|valid_password');
                $this->form_validation->set_rules('pass_confirm', 'lang:bf_password_confirm', 'required|trim|strip_tags|matches[password]');

                if ($this->form_validation->run() !== FALSE)
                {
                    // The customer model will create the password hash for us.
                    $data = array('password' => $this->input->post('password'),
                                  'pass_confirm'    => $this->input->post('pass_confirm'),
                                  'reset_by'        => 0,
                                  'reset_hash'    => '');

                    if ($this->customer_model->update($this->input->post('customer_id'), $data))
                    {
                        // Log the Activity

                        $this->activity_model->log_activity($this->input->post('customer_id'), lang('customer_log_reset') , 'customers');
                        Template::set_message(lang('customer_reset_password_success'), 'success');
                        Template::redirect('/customers/login');
                    }
                    else
                    {
                        Template::set_message(lang('customer_reset_password_error'). $this->customer_model->error, 'error');
                    }
                }
            }//end if

            // Check the code against the database
            $email = str_replace(':', '@', $email);
            $customer = $this->customer_model->find_by(array(
                                        'email' => $email,
                                        'reset_hash' => $code,
                                        'reset_by >=' => time()
                                   ));

            // It will be an Object if a single result was returned.
            if (!is_object($customer))
            {
                Template::set_message( lang('customer_reset_invalid_email'), 'error');
                Template::redirect('/customers/login');
            }

            // If we're here, then it is a valid request....
            Template::set('customer', $customer);

            Template::set_view('customers/customers/reset_password');
            Template::render('no_sidebar');
        }
        else
        {

            Template::redirect('/');
        }//end if

    }//end reset_password()

    //--------------------------------------------------------------------

    /**
     * Display the registration form for the customer and manage the registration process
     *
     * @access public
     *
     * @return void
     */
    public function register()
    {
        if ($this->customer_auth->is_logged_in() == TRUE) 
        {
            redirect('/');
        }
        
        if ($this->input->post('submit'))
        {
            // Validate input
            $this->form_validation->set_rules('email', 'lang:ed_email', 'required|trim|strip_tags|valid_email|max_length[120]|unique[customers.email]|xss_clean');
            $this->form_validation->set_rules('password', 'lang:ed_password', 'required|trim|strip_tags|min_length[8]|max_length[120]|valid_password');
            $this->form_validation->set_rules('pass_confirm', 'lang:ed_password_confirm', 'required|trim|strip_tags|matches[password]');
            $this->form_validation->set_rules('name', 'lang:ed_full_name', 'required|trim|strip_tags|max_length[255]|xss_clean');

            if ($this->form_validation->run($this) !== FALSE)
            {
                // Time to save the customer...
                $data = array(
                    'email'       => $_POST['email'],
                    'password'    => $_POST['password'],
                    'name'=> $this->input->post('name'),
                );

                if ($customer_id = $this->customer_model->insert($data))
                {
                    // Prepare customer messaging vars
                    $subject    = '';
                    $email_mess = '';
                    $message    = lang('customer_email_thank_you');
                    $type       = 'success';
                    $site_title = $this->settings_lib->item('site.title');
                    $error      = false;

                    // No activation required. Activate the customer and send confirmation email
                    $subject         =  str_replace('[SITE_TITLE]',$this->settings_lib->item('site.title'),lang('customer_account_reg_complete'));
                    $email_mess      = $this->load->view('_emails/activated', array('title'=>$site_title,'link' => site_url()), true);
                    $message        .= lang('customer_account_active_login');

                    // Now send the email
                    $this->load->library('emailer/emailer');
                    $data = array(
                        'to'        => $_POST['email'],
                        'subject'    => $subject,
                        'message'    => $email_mess
                    );

                    if (!$this->emailer->send($data))
                    {
                        $message .= lang('customer_err_no_email'). $this->emailer->errors;
                        $error    = true;
                    }

                    if ($error)
                    {
                        $type = 'error';
                    }
                    else
                    {
                        $type = 'success';
                    }

                    Template::set_message($message, $type);

                    // Log the Activity

                    $this->activity_model->log_activity($customer_id, lang('customer_log_register') , 'customers');
                    Template::redirect('login');
                }
                else
                {
                    Template::set_message(lang('customer_registration_fail'), 'error');
                    redirect('customers/register');
                }//end if
            }//end if
        }//end if

        // Generate password hint messages.
        $this->customer_model->password_hints();

        Template::set_view('customers/customers/register');
        Template::set('page_title', lang('ed_register'));
        Template::render('no_sidebar');

    }//end register()

    //--------------------------------------------------------------------

    /**
     * Save the customer
     *
     * @access private
     *
     * @param int   $id          The id of the customer in the case of an edit operation
     * @param array $meta_fields Array of meta fields fur the customer
     *
     * @return bool
     */
    private function save_customer($id=0)
    {
        if($id == 0)
        {
            return FALSE;
        }
        
        if($this->input->post('email') != $this->customer_model->get_email($id)) {
            $is_unique = '|is_unique[customers.email]';
        } else {
            $is_unique = '';
        }
        
        $pass_min_len = $this->settings_lib->item('auth.password_min_length');
        $this->form_validation->set_rules('email', 'lang:ed_email', 'required|trim|valid_email|max_length[120]|xss_clean'.$is_unique);
        $this->form_validation->set_rules('password', 'lang:ed_password', "trim|strip_tags|min_length[{$pass_min_len}]|max_length[120]|valid_password");

        // check if a value has been entered for the password - if so then the pass_confirm is required
        // if you don't set it as "required" the pass_confirm field could be left blank and the form validation would still pass
        $extra_rules = !empty($_POST['password']) ? 'required|' : '';
        $this->form_validation->set_rules('pass_confirm', 'lang:ed_password_confirm', 'trim|strip_tags|'.$extra_rules.'matches[password]');
        $this->form_validation->set_rules('name', 'lang:ed_full_name', 'trim|strip_tags|max_length[255]|xss_clean');
        $this->form_validation->set_rules('qq_code', 'lang:ed_qq', 'trim|strip_tags|max_length[255]|xss_clean');
        $this->form_validation->set_rules('phone', 'lang:ed_phone', 'trim|strip_tags|max_length[255]|xss_clean');

        // Added Event "before_customer_validation" to run before the form validation
        $payload = array ('customer_id' => $id, 'data' => $this->input->post());
        Events::trigger('before_customer_validation', $payload);

        if ($this->form_validation->run($this) === FALSE)
        {
            return FALSE;
        }

        // Compile our core customer elements to save.
        $data = array(
            'email'         => $this->input->post('email'),
            'name'  => $this->input->post('name'),
            'qq_code'       => $this->input->post('qq_code'),
            'newsletter'    => $this->input->post('newsletter')==1?1:0,
            'phone'         => $this->input->post('phone'),
        );

        if ($this->input->post('password'))
        {
            $data['password'] = $this->input->post('password');
        }

        if ($this->input->post('pass_confirm'))
        {
            $data['pass_confirm'] = $this->input->post('pass_confirm');
        }

        // Any modules needing to save data?
        // Event to run after saving a customer
        Events::trigger('save_customer', $payload );

        return $this->customer_model->update($id, $data);

    }//end save_customer()
    
    //------------------------------------------------------
    
    /**
     * Save the customer address
     *
     * @access private
     *
     * @param string $type        The type is for insert or update
     * @param int    $id          The id of the customer address in the case of an edit operation
     * @param array $meta_fields Array of meta fields fur the customer
     *
     * @return bool
     */
    private function save_address($customer_id)
    {
        $this->form_validation->set_rules('province', 'lang:ed_province', 'required|trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('city', 'lang:ed_city', 'required|trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('address', 'lang:ed_address', 'required|trim|strip_tags|min_length[5]|max_length[255]|xss_clean');
        $this->form_validation->set_rules('address', 'lang:ed_address', 'required|trim|strip_tags|min_length[5]|max_length[255]|xss_clean');
            
        if ($this->form_validation->run($this) === FALSE)
        {
            return FALSE;
        }
            
        // Compile our core customer elements to save.
        $data = array();
        $data['province']   = $this->input->post('province');
        $data['city']       = $this->input->post('city');
        $data['customer_id']= $customer_id;
        $data['address']    = $this->input->post('address');

        return $this->customer_model->save_address($customer_id, $data);

    }//end save_customer()
    
    //----------------------------------------------------
    
    ///////////////////////////////////////////////////////////////////////
    //!MANAGE CART CONTENT WHEN LOGIN AND LOGOUT
    ///////////////////////////////////////////////////////////////////////
    function clear_cart() {
        $this->cart->destroy(false, false);
    }
    
    function merge_cart($customer_id) { 
        $this->cart->merge_with_db($customer_id);
    }
    
    
    ///////////////////////////////////////////////////////////////////////
    //!AJAX METHODS
    ///////////////////////////////////////////////////////////////////////
    public function get_city_list() {
        $province = $this->input->post('province');        
        $cities = $this->region_model->city_dropdown_list($province, TRUE, lang('ed_select'));
        echo form_dropdown2('city', $cities, set_value('city', ''));
        exit;
    }//end get_city_list()

    //--------------------------------------------------------------------

}//end Users

/* Front-end Customers Controller */
/* End of file customers.php */
/* Location: ./application/core_modules/users/controllers/users.php */