<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class catalog extends Admin_Controller {

	//--------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Customers.Catalog.View');
		
        $this->load->model('region_model', null, true);
        $this->load->model('customer_model', null, true);
		$this->lang->load('customers');	
        
        Assets::add_js('jquery.form.js');
        Assets::add_module_js('customers', 'customers.js');

        $config = init_upload_config('avatar');   
        $this->upload->initialize($config); 	
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->customer_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('customer_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('customer_delete_failure') . $this->customer_model->error, 'error');
				}
			}
		}

        //setup where conditions
        $where = array();
        $search = $this->input->post('search');
         if(!empty($search['from_id']))  $where['customers.customer_id >= '] = $search['from_id'];
        if(!empty($search['to_id']))    $where['customers.customer_id <= '] = $search['to_id'];
        if(!empty($search['name']))     $where['name LIKE '] = "%" . $search['name'] . "%";
        if(!empty($search['email']))    $where['email LIKE '] = "%" . $search['email'] . "%";
        if(!empty($search['phone']))    $where['phone LIKE '] = "%" . $search['phone'] . "%";
        if(!empty($search['qq_code']))    $where['qq_code LIKE '] = "%" . $search['qq_code'] . "%";
        if(!empty($search['from_date']))  $where['registered_on >= '] = $search['from_date'];
        if(!empty($search['to_date']))    $where['registered_on <= '] = $search['to_date'];
        if(isset($search['status']) && $search['status'] != -1)    $where['active'] = $search['status'];
        Template::set('search', $search);
           
        //setup ordder array
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
               
        $orders  = array();
        switch($params['orderby']) {
            case "id":
                $orders["customers.customer_id"]= $params['order'];
                break;
            case "name":
            case "email":
            case "phone":
            case "qq_code":
                $orders[$params['orderby']]     = $params['order'];
                break;
            case "registered": 
                $orders["registered_on"]        = $params['order'];
                break;
            default:
                $orders["customers.customer_id"]  = "desc";
                break;
        }
        
        //get total counts
        $total_records = $this->customer_model
                ->where($where)
                ->count_all();

        //get all products
        $records = $this->customer_model
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all();  
        Template::set('records', $records);
       
        //setup pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/catalog/customers/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/catalog/customers') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);
        
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('customer_index', $url);

		Template::set('toolbar_title', lang('customer_list'));
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: create()

		Creates a Customers object.
	*/
	public function create()
	{
		$this->auth->restrict('Customers.Catalog.Create');

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			if ($insert_id = $this->save_customer())
			{
				Template::set_message(lang('customer_create_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
                else Template::redirect(SITE_AREA .'/catalog/customers/edit/'.$insert_id);
			}
			else
			{
				Template::set_message(lang('customer_create_failure') . $this->customer_model->error, 'error');
			}
		}
        
        //get region info
        $provinces = $this->region_model->province_dropdown_list(true, lang('ed_select'));
        Template::set('provinces', $provinces);
        
        $cities = $this->region_model->city_dropdown_list('00', true, lang('ed_select'));
        Template::set('cities', $cities);        
        
        //for avatar image
        if($this->input->post('image_name') && $this->input->post('image_name') != "deleted")
        {
            Template::set("img", $this->input->post('image_name')?$this->input->post('image_name'):'');    
            Template::set("img_path", 'tmp/avatar');    
        }
        else
        {
            Template::set("img", '');    
            Template::set("img_path", 'tmp/avatar');    
        }
        
        Template::set('prev_page', $this->session->userdata('customer_index'));
		Template::set('toolbar_title', lang('customer_create'));
        Template::set_view('customers/catalog/form.php');
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Customers data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);
          
		if (empty($id))
		{
			Template::set_message(lang('customer_invalid_id'), 'error');
			$this->redirect_to_index();
		} 
            
		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			$this->auth->restrict('Customers.Catalog.Edit');
            
            //check id and key
            $key = $this->input->post('key');
            if ($key != md5($id))
            {
                Template::set_message(lang('invalid_action'), 'error');
                $this->redirect_to_index();
            } 
            
			if ($this->save_customer('update', $id))
			{
				Template::set_message(lang('customer_edit_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
			}
			else
			{
				Template::set_message(lang('customer_edit_failure') . $this->customer_model->error, 'error');
                Template::set("image_name", $this->input->post('image_name')?$this->input->post('image_name'):'');    
			}
		}
		else if (isset($_POST['delete']))
		{
            $key = $this->input->post('key');
            if ($key != md5($id))
            {
                Template::set_message(lang('invalid_action'), 'error');
                $this->redirect_to_index();
            } 
            
			$this->auth->restrict('Customers.Catalog.Delete');

			if ($this->customer_model->delete($id))
			{
				Template::set_message(lang('customer_delete_success'), 'success');

				$this->redirect_to_index();
			} else
			{
				Template::set_message(lang('customer_delete_failure') . $this->customer_model->error, 'error');
			}
		}
        
        $customer   = $this->customer_model->find($id);
        $address    = $this->customer_model->find_address($id);
        
        //if save failed, them save avatar image
        if($this->input->post('image_name') && $this->input->post('image_name') != "deleted")
        {
            Template::set("img", $this->input->post('image_name'));    
            Template::set("img_path", 'tmp/avatar');    
        }
        else
        {
            Template::set("img", trim($customer->avatar));    
            Template::set("img_path", 'avatar');    
        }      
        
        if($this->input->post('require_change') == '1')
        {
            Template::set("require_change", '1');        
        }
        else
        {
            Template::set("require_change", '0');        
        }
		Template::set('customer', $customer);
        Template::set('address', $address);
        
        //get region info
        $provinces = $this->region_model->province_dropdown_list(true, lang('ed_select'));
        Template::set('provinces', $provinces);
        
        $cities = $this->region_model->city_dropdown_list(!empty($address)?$address->province:'00', true, lang('ed_select'));
        Template::set('cities', $cities);  
        
        Template::set('prev_page', $this->session->userdata('customer_index'));
		Template::set('toolbar_title', lang('customer_edit'));
        Template::set_view('customers/catalog/form.php');
		Template::render();
	}
    
    public function delete()
    {
        $id = $this->uri->segment(5);
        $key = $this->uri->segment(6);
        
        if(empty($id))
        {
            Template::set_message(lang('customer_invalid_id'), 'error');
            $this->redirect_to_index();
        }
        
        if(md5($id) != $key)
        {
            Template::set_message(lang('invalid_action'), 'error');
            $this->redirect_to_index();
        }
        
        $this->auth->restrict('Customers.Catalog.Delete');
        
        if ($this->customer_model->delete($id))
        {
            Template::set_message(lang('customer_delete_success'), 'success');

            $this->redirect_to_index();
        }
    }
	//--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_customers()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_customer($type='insert', $id=0)
	{        
        if(($type=='insert') 
            || ($type=='update' && $this->input->post('email') != $this->customer_model->get_email($id))) {
            $unique_rule = '|unique[customers.email]';
        } else {
            $unique_rule = '';
        }              
        
		$this->form_validation->set_rules('email','lang:ed_email','required|trim|valid_email|max_length[120]strip_tags|xss_clean'.$unique_rule);
		$this->form_validation->set_rules('name','lang:ed_name','required|trim|max_length[255]|strip_tags|xss_clean');
        $this->form_validation->set_rules('phone','lang:ed_phone','trim|max_length[20]|strip_tags|xss_clean');
        $this->form_validation->set_rules('qq_code','lang:ed_qq','trim|max_length[20]|strip_tags|xss_clean');
        $this->form_validation->set_rules('newsletter','lang:ed_newsletter','trim');
        $this->form_validation->set_rules('status','lang:ed_status','trim');
        
        if($type=='insert' || $this->input->post('require_change') == '1')
        {
            $pass_min_len = $this->settings_lib->item('auth.password_min_length');
            $this->form_validation->set_rules('password', 'lang:ed_password', 'required|trim|strip_tags|min_length['.$pass_min_len.']|max_length[120]|valid_password');
            $this->form_validation->set_rules('pass_confirm', 'lang:ed_password_confirm', 'required|trim|strip_tags|matches[password]');
        }

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
		$data = array();
        if($id != 0) 
        {
            $data['customer_id']= $id;
        }
		$data['email']          = $this->input->post('email');
		$data['name']           = $this->input->post('name');
		$data['phone']          = $this->input->post('phone');
		$data['qq_code']        = $this->input->post('qq_code');
        $data['newsletter']     = $this->input->post('newsletter')?'1':'0';
        $data['active']         = $this->input->post('status')?'1':'0';
        $data['password']       = $this->input->post('password');
        $data['pass_confirm']   = $this->input->post('pass_confirm');
        
        //upload avatar image
        $action = $this->input->post('image_action');
        if($action == 'changed') { 
            if($this->input->post('image_name')) { 
                $file_name = $this->get_moved_image_name($this->input->post('image_name'), 'avatar');
                if($file_name !== FALSE)
                {
                    $data['avatar'] = $file_name;
                }
            }
        } else if($action == 'deleted') {
            $data['avatar'] = '';
        }
        
        //address info
        $address = FALSE;
        if($this->input->post('province')
            || $this->input->post('city')
            || $this->input->post('address')
            || $this->input->post('zipcode'))
        {
            $address = array();
            $address['province']= $this->input->post('province');
            $address['city']    = $this->input->post('city');
            $address['address'] = $this->input->post('address');
            $address['zipcode'] = $this->input->post('zipcode');
        }
            
		return $this->customer_model->save($data, $address);
	}

    private function redirect_to_index()
    {
        $redirect_url = $this->session->userdata('customer_index')?$this->session->userdata('customer_index'):site_url(SITE_AREA .'/catalog/customers');
        Template::redirect($redirect_url);
    }
	//--------------------------------------------------------------------

    //--------------------------------------------------------------------
    // !AJAX METHODS
    //--------------------------------------------------------------------
    public function upload_avatar() {
        $result = $this->upload_image('avatar_image');
        echo json_encode($result);
        exit;     
    }

}