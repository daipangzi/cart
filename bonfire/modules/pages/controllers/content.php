<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class content extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Pages.Content.View');
		$this->load->model('pages_model', null, true);
		$this->lang->load('pages');
        
        $temp = config_item('page_templates');
        $templates = array();
        foreach($temp as $t)
        {
            $templates[$t] = lang($t);
        }
        Template::set('templates', $templates);
        
        if(!$this->session->userdata('pages_index'))
        {
            $this->session->set_userdata('pages_index', site_url(SITE_AREA .'/content/pages'));
        }
	}
	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->pages_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('pages_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('pages_delete_failure') . $this->pages_model->error, 'error');
				}
			}
		}
        
        //get order fields
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        //get where fields
        $where = array();  
        
        $orders = array();
        switch($params['orderby']) {
            case "title":
            case "slug":
            case "status":
            case "template":
                $orders[$params['orderby']] = $params['order'];
                break;
                
            case "created":
                $orders["created_on"]  = $params['order'];
                break;

            default:
                $orders["title"]  = "asc";
                break;
        }

		//get total counts
        $this->pages_model->where($where);
        $total_records = $this->pages_model->count_all();
                       
        //get total records
        $records = $this->pages_model
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all();
        Template::set('records', $records);
        
        //build pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/content/pages/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/content/pages') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);
        
        // set request page  
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('pages_index', $url);                                   
        
        Template::set('toolbar_title', lang('pages_list'));
		Template::render();
	}

	//--------------------------------------------------------------------



	/*
		Method: create()

		Creates a Pages object.
	*/
	public function create()
	{
		$this->auth->restrict('Pages.Content.Create');

		if ($this->input->post('save') || $this->input->post('save_continue'))    
		{
			if ($insert_id = $this->save_page())
			{
				Template::set_message(lang('pages_create_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
                else Template::redirect(SITE_AREA .'/content/pages/edit/'.$insert_id);
			}
			else
			{
				Template::set_message(lang('pages_create_failure') . $this->pages_model->error, 'error');
			}
		}
		Assets::add_module_js('pages', 'pages.js');
        
        Template::set('prev_page', $this->session->userdata('pages_index'));
		Template::set('toolbar_title', lang('pages_create'));
        Template::set_view('pages/content/form');
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Pages data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('pages_invalid_id'), 'error');
			redirect(SITE_AREA .'/content/pages');
		}

		if (isset($_POST['save']) || isset($_POST['save_continue']))
		{
			$this->auth->restrict('Pages.Content.Edit');

			if ($this->save_page('update', $id))
			{
				Template::set_message(lang('pages_edit_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) $this->redirect_to_index();
			}
			else
			{
				Template::set_message(lang('pages_edit_failure') . $this->pages_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Pages.Content.Delete');

			if ($this->pages_model->delete($id))
			{
				Template::set_message(lang('page_delete_success'), 'success');

				$this->redirect_to_index();
			} else
			{
				Template::set_message(lang('pages_delete_failure') . $this->pages_model->error, 'error');
			}
		}
        Assets::add_module_js('pages', 'pages.js');
        
        $record = $this->pages_model->find($id, 1);
		Template::set('record', $record);
        
        Template::set('prev_page', $this->session->userdata('pages_index'));
		Template::set('toolbar_title', lang('pages_edit'));
        Template::set_view('pages/content/form');
		Template::render();
	}

    public function delete()
    {
        $id = $this->uri->segment(5);

        if (empty($id))
        {
            Template::set_message(lang('pages_invalid_id'), 'error');
            $this->redirect_to_index();
        }
        
        $this->auth->restrict('Pages.Content.Delete');
        
        if ($this->pages_model->delete($id))
        {
            Template::set_message(lang('page_delete_success'), 'success');

            $this->redirect_to_index();
        }
    }
	//--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_pages()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_page($type='insert', $id=0)
	{
        $_POST['slug']   = url_title(convert_accented_characters($this->input->post('slug')), 'dash', TRUE);
        
        //set rules for manufacturer attributes
        $languages = $this->languages_model->find_all(1);
        foreach($languages as $l)
        {
            $c = $l['language_id'];
            
            $this->form_validation->set_rules("info[{$c}][title]", 'lang:ed_title', 'required|trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][heading]", 'lang:ed_content_heading', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][content]", 'lang:ed_content', 'trim');
            $this->form_validation->set_rules("info[{$c}][page_title]", 'lang:ed_page_title', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_keywords]", 'lang:ed_meta_keywords', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_description]", 'lang:ed_meta_description', 'trim|max_length[255]|strip_tags|xss_clean');
        }
        
        $this->form_validation->set_rules('slug', 'lang:ed_slug','trim|alpha_dash|max_length[100]|strip_tags|xss_clean');
        $this->form_validation->set_rules('status', 'lang:ed_status','trim');
		$this->form_validation->set_rules('template','lang:ed_template','trim');

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
		$data = array();
        if($id != 0) 
        {
            $data['page_id']= $id;
        }
        $data['slug']       = $this->pages_model->validate_slug($this->input->post('slug'), $id);
		$data['template']   = $this->input->post('template')?$this->input->post('template'):'default';
		$data['status']     = $this->input->post('status')?'1':'0';

		$langs = $this->input->post('info');
        $id = $this->pages_model->save($data, $langs);
        if(is_numeric($id))
        {
            if($this->input->post('as_homepage'))
            {
                $this->settings_lib->set('site.homepage', $id);
            }
            
            return $id;
        }
        
        return FALSE;
	}

	//--------------------------------------------------------------------
    private function redirect_to_index()
    {
        $redirect_url = $this->session->userdata('pages_index')?$this->session->userdata('pages_index'):SITE_AREA .'/content/pages';
        Template::redirect($redirect_url);
    }


}