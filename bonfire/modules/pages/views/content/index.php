<?php
    $default = settings_item('site.homepage');
?>
<div class="admin-box">
	<?php echo form_open($this->uri->uri_string()); ?>
        <fieldset><legend><?php echo lang('pages_list'); ?><small><?php echo $this->pagination->get_page_status();?></small></legend></fieldset>
    
        <div class="form-buttons">
            <?php if ($this->auth->has_permission('Pages.Content.Delete') && isset($records) && is_array($records) && count($records)) : ?>
            <button type="submit" name="delete" id="delete-me" class="btn" value="<?php echo lang('bf_action_delete') ?>" onclick="return confirm('<?php echo lang('pages_delete_confirm'); ?>')"><span class="icon-remove-sign"></span>&nbsp;<?php echo lang('bf_action_delete') ?></button>            
            <?php endif;?>
            
            <?php if ($this->auth->has_permission('Pages.Content.Create')) : ?>
            <?php echo anchor(SITE_AREA .'/content/pages/create', '<span class="icon-plus-sign"></span>&nbsp;'.lang('bf_action_insert'), 'class="btn"'); ?>                    
            <?php endif;?>
        </div>
        
		<table class="table table-striped lrborder checkAll" style="min-width:1000px;">
        <colgroup>
            <?php if ($this->auth->has_permission('Pages.Content.Delete') && isset($records) && is_array($records) && count($records)) : ?>
            <col width="40px"/>
            <?php endif;?>
            <col width=""/>
            <col width="150"/>
            <col width="150"/>
            <col width="200"/>
            <col width="120"/>
            <col width="100"/>
        </colgroup>
		<thead>
			<tr>
				<?php if ($this->auth->has_permission('Pages.Content.Delete') && isset($records) && is_array($records) && count($records)) : ?>
				<th class="column-check"><input class="check-all" type="checkbox" /></th>
				<?php endif;?>
				
				<th class="<?php echo sort_classes($params['orderby'], "title", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/content/pages?orderby=title&amp;order='.sort_direction($params['orderby'], "title", $params['order'])); ?>"><span><?php echo lang('ed_title'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "slug", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/content/pages?orderby=slug&amp;order='.sort_direction($params['orderby'], "slug", $params['order'])); ?>"><span><?php echo lang('ed_slug'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "template", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/content/pages?orderby=template&amp;order='.sort_direction($params['orderby'], "template", $params['order'])); ?>"><span><?php echo lang('ed_template'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "created", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/content/pages?orderby=created&amp;order='.sort_direction($params['orderby'], "created", $params['order'])); ?>"><span><?php echo lang('ed_created'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "status", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/catalog/pages?orderby=status&amp;order='.sort_direction($params['orderby'], "status", $params['order'])); ?>"><span><?php echo lang('ed_status'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th><?php echo lang('ed_actions'); ?></th>
			</tr>
		</thead>
		<tbody>
		<?php if (isset($records) && is_array($records) && count($records)) : ?>
		<?php foreach ($records as $record) : ?>
			<tr>
				<?php if ($this->auth->has_permission('Pages.Content.Delete')) : ?>
				<td><input type="checkbox" name="checked[]" value="<?php echo $record->page_id ?>" /></td>
				<?php endif;?>
				
                <td>
			        <?php if ($this->auth->has_permission('Pages.Content.Edit')) : ?>
			        <?php echo anchor(SITE_AREA .'/content/pages/edit/'. $record->page_id, $record->title) ?>
			        <?php else: ?>
			        <?php echo $record->title ?>
			        <?php endif; ?>
                    <?php if($record->page_id == $default) echo '<span class="gray">('.lang('ed_homepage').')</span>'; ?>
                </td>
		        
                <td><?php echo $record->slug ?></td>
                <td><?php echo $templates[$record->template] ?></td>
                <td><?php echo format_date_time($record->created_on); ?></td>
			    <td><?php echo $record->status==1?lang("ed_enable"):lang("ed_disable"); ?></td>
                <td>
                    <?php if ($this->auth->has_permission('Pages.Content.Edit')) : ?>
                    <a href="<?php echo site_url(SITE_AREA .'/content/pages/edit/'.$record->page_id) ?>" title="<?php echo lang('bf_action_edit'); ?>" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                    <?php endif; ?>
                    
                    <?php if ($this->auth->has_permission('Pages.Content.Delete')) : ?>
                    <a href="<?php echo site_url(SITE_AREA .'/content/pages/delete/'. $record->page_id) ?>" title="<?php echo lang('bf_action_delete'); ?>" class="tip"><span class="icon12 icomoon-icon-remove"></span></a>
                    <?php endif; ?>
                </td>
			</tr>
		<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="15"><?php echo lang('no_record'); ?></td>
			</tr>
		<?php endif; ?>
		</tbody>
		</table>
	<?php echo form_close(); ?>
</div>