<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['pages_manage']			= 'Pages';
$lang['pages_edit']				= 'Edit Page';
$lang['pages_create']			= 'New Page';
$lang['pages_list']				= 'Pages';
$lang['pages_create_success']			= 'Page successfully created.';
$lang['pages_create_failure']			= 'There was a problem creating the page: ';
$lang['pages_invalid_id']			= 'Invalid Page ID.';
$lang['pages_edit_success']			= 'Page successfully saved.';
$lang['pages_edit_failure']			= 'There was a problem saving the page: ';
$lang['pages_delete_success']			= 'record(s) successfully deleted.';
$lang['pages_delete_failure']			= 'We could not delete the record: ';
$lang['pages_delete_confirm']			= 'Are you sure you want to delete this pages?';
$lang['page_delete_confirm']            = 'Are you sure you want to delete this page?';
