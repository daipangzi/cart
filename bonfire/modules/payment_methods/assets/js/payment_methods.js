$('.accordion-heading').click(function() {
    expand = $('.expand', $(this).parent());
    
    if(expand.val() == '1')
    {
        expand.val('0');
    }
    else
    {
        expand.val('1');
    }
});

$('.disable_method').change(function() {
    if($(this).val() != '') 
    {
        $('#'+$(this).attr('target')).prop('disabled', false);
    }
    else
    {
        $('#'+$(this).attr('target')).val('0');
        $('#'+$(this).attr('target')).prop('disabled', true);
    }
});