<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_methods_model extends BF_Model {

	protected $table		= "payment_methods";
	protected $key			= "code";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= false;
	protected $set_modified = true;
	protected $modified_field = "modified_on";
    
    protected $lang_table   = "payment_methods_info";
    private $is_admin       = false;
    
    var $CI;  
    
    function __construct() 
    {
        $this->CI =& get_instance();
                  
        $this->is_admin = is_backend();
    }
    
    function is_admin()
    {
        $this->is_admin = true;   
        return $this; 
    }
    
    function init_before_find_all()
    {
        //set filter
        if(!$this->is_admin)
        {
            parent::where('status', 1);            
        }
        
        return $this;    
    }
    
    /**
    * find payment method
    * 
    * @param mixed $id
    */
    function find($code, $return_type=0)
    {
        $record = parent::find_by('code', $code);
        if($return_type == 1) $record = (array)$record;

        //if cann't find record
        if(empty($record)) 
        {
            return FALSE;
        }

        if($this->is_admin)
        {
            if($return_type == 0)
            {
                $record->info    = $this->get_all_language_info($code);
                $record->options = $this->find_meta_for($code);
            }
            else
            {
                $record['info']     = $this->get_all_language_info($code);
                $record['options']  = (array)$this->find_meta_for($code);
            }
        }
        else
        {    
            //if status disabled
            $temp = (array)$record;
            if($temp['status'] == 0)
            {
                return FALSE;
            }
            
            if($return_type == 0)
            {       
                $record  = (object)array_merge((array)$record, $this->get_language_info($code, global_language_id()), (array)$this->find_meta_for($code));
            }
            else
            {
                $record  = array_merge($record, $this->get_language_info($code, global_language_id()), (array)$this->find_meta_for($code));
            }
        }
        
        return $record;
    }
    
    function find_meta_for($code)
    {
        $this->db->where('code', $code);
        $query = $this->db->get('payment_methods_meta');

        if ($query->num_rows())
        {
            $rows = $query->result();

            $result = new stdClass;
            foreach ($rows as $row)
            {
                $key = $row->meta_key;
                $result->$key = $row->meta_value;
            }
        }
        else
        {
            $result = FALSE;
        }

        return $result;
    }
    
    function find_all($return_type=0)
    {
        $this->init_before_find_all();
        $records = parent::find_all($return_type);
        
        if(empty($records))
        {
            return FALSE;
        }
        
        $result = array();
        foreach($records as $record)
        {
            $temp = (array)$record;
            $code = $temp['code'];
            
            if($this->is_admin)
            {
                if($return_type == 0)
                {
                    $record->info       = $this->get_all_language_info($code);
                    $record->options    = $this->find_meta_for($code);
                }
                else
                {
                    $record['info']     = $this->get_all_language_info($code);
                    $record['options']  = (array)$this->find_meta_for($code);
                }
            }
            else
            {
                //if status disabled
                $temp = (array)$record;
                if($temp['status'] == 0)
                {
                    return FALSE;
                }
                
                if($return_type == 0)
                {       
                    $record  = (object)array_merge($temp, $this->get_language_info($code, global_language_id()), (array)$this->find_meta_for($code));
                }
                else
                {
                    $record  = array_merge($record, $this->get_language_info($code, global_language_id()), (array)$this->find_meta_for($code));
                }
            }
            
            $result[] = $record;
        }
        
        return $result;
    }
    
    /**
    * check method is valid
    * 
    * @param mixed $id
    */
    function is_valid($code)
    {
        $record = $this->find($code);
            
        if(empty($record))
        {
            return FALSE;
        }
        
        if($record->status == 0)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
    * save payment methods and language info
    * 
    * @param mixed $data
    * @param mixed $langs
    */
    function save($data, $langs, $options) {
        
        $code = $data['code'];
        $this->update_where('code', $code, $data);
             
        foreach($langs as $c => $d)
        {
            $info = array();
            $info['code']    = $code;
            $info['language_id'] = $c;
            $info['description'] = $d['description'];
                        
            if(!$this->check_language_row($code, $c))
            {
                $this->db->insert($this->lang_table, $info);
            }
            else
            {
                $where = array();
                $where['code']    = $code;
                $where['language_id'] = $c;
                
                $this->db->where($where)->update($this->lang_table, $info);
            }
        }
        
        if(is_array($options) && !empty($options))
        {
            foreach($options as $key => $value)
            {
                if($value == '' || $value == '*****') continue;
                
                $data = array();
                $data['code']       = $code;
                $data['meta_key']   = $key;
                $data['meta_value'] = $value;
                
                if(!$this->check_meta_row($code, $key))
                {
                    $this->db->insert('payment_methods_meta', $data);
                }
                else
                {
                    $where = array();
                    $where['code']     = $code;
                    $where['meta_key'] = $key;
                    $this->db->where($where)->update('payment_methods_meta', $data);
                }
            }    
        }
    }
    
    function get_settings($code)
    {
        return (array)$this->find_meta_for($code);
    }
    
    //--------------------------------------------------------------------
    // !LANGAGE METHODS
    //--------------------------------------------------------------------
    /**
    * check if row is exist with category and lanuage id
    * 
    * @param mixed $manufacturer_id
    * @param mixed $language_id
    * @return mixed
    */
    private function check_language_row($code, $language_id)
    {
        $where = array();
        $where['code']   = $code;
        $where['language_id'] = $language_id;
        
        return (bool)$this->db->where($where)->count_all_results($this->lang_table);
    }  
    
    /**
    * get all lanuage info
    * 
    * @param mixed $category_id
    */
    private function get_all_language_info($code)
    {
        $languages    = $this->CI->languages_model->find_all(1);
        
        $result = array();
        foreach($languages as $lang)
        {
            $l = $lang['language_id'];
            
            $record = $this->get_language_info($code, $l);
            
            $result[$l] = $record;
        }
        
        return $result;
    } 
    
    /**
    * get one lanuage info
    * 
    * @param mixed $category_id
    * @param mixed $language_id
    */
    private function get_language_info($code, $language_id)
    {
        $default_lang = get_language_id_by_code(settings_item('site.default_language'));   
        
        $record = $this->db
                    ->where(array('code' => $code, 'language_id' => $language_id))
                    ->get($this->lang_table)
                    ->row_array();
         
        if(empty($record))
        {
            $record = $this->db
                    ->where(array('code' => $code, 'language_id' => $default_lang))
                    ->get($this->lang_table)
                    ->row_array();
        } 
        
        return $record;
    }
    
    //--------------------------------------------------------------------
    // !META METHODS
    //--------------------------------------------------------------------
    private function check_meta_row($code, $key)
    {
        $where = array();
        $where['code']    = $code;
        $where['meta_key'] = $key;
        
        return (BOOL)$this->db->where($where)->count_all_results('payment_methods_meta');
    }
}
