<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_payment_methods extends Migration {

	public function up()
	{
		$prefix = $this->db->dbprefix;

		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				
			),
			'sort_order' => array(
				'type' => 'VARCHAR',
				'constraint' => 3,
				
			),
			'options' => array(
				'type' => 'TEXT',
				
			),
			'status' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				
			),
			'modified_on' => array(
				'type' => 'datetime',
				'default' => '0000-00-00 00:00:00',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('payment_methods');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('payment_methods');

	}

	//--------------------------------------------------------------------

}