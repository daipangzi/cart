<div class="admin-box">
<?php
    echo validation_errors();
?>
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" autocomplete="off"'); ?>
    <fieldset>
        <legend><?php echo lang('payment_methods_list'); ?></legend>
        
        <div class="form-buttons">
            <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
        </div>
         
        <div class="accordion" id="accordion1">
            <?php foreach($records as $record) : $key = $record['code']; ?>
                <?php Template::block($key, 'payment_methods/catalog/payments/'.$key, array('record'=>$record)); ?>          
            <?php endforeach; ?>
        </div><!-- End .accordion1 -->
        
    </fieldset>
    <?php echo form_close(); ?>

</div>
