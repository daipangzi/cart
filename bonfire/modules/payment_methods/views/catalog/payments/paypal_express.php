<?php 
    $expand = $record['expand'];
    $code   = $record['code']; 
    $info   = $record['info'];

    $options = $record['options'];
?>

<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle <?php echo $expand?'':'collapsed';?>" data-toggle="collapse" href="#<?php echo $code; ?>_content" class="accordion-toggle collapsed"><?php echo lang($code); ?></a>
    </div>
    <div id="<?php echo $code; ?>_content" class="accordion-body collapse <?php echo $expand?'in':'';?>" style="<?php echo $expand?'height:auto':'height:0px';?>">
        <div class="accordion-inner morePadding">
            <input type="hidden" name="payment[<?php echo $code; ?>][expand]" class="expand" value="<?php echo $expand; ?>"/>

            <?php
                $extra = '';
                $status = isset($record['status'])?$record['status'] : '0';
                if(!isset($options['merchant_email']) || $options['merchant_email'] == '') 
                { 
                    $extra = 'disabled="disabled"';
                    $status = 0;
                }
            ?>            
            <div class="control-group">
                <?php echo form_label(lang('ed_status'), "{$code}_status", array('class' => "control-label") ); ?>
                <div class='controls'>
                    <?php echo form_dropdown2("payment[{$code}][status]", array("1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value("payment[{$code}][status]", $status), "id='{$code}_status' class='nostyle wd100px;' ".$extra); ?>
                </div>
            </div>
            
            <div class="control-group <?php echo form_error("payment[{$code}][options][merchant_email]") ? 'error' : ''; ?>">
                <?php echo form_label(lang('paypal_merchant_email'), "{$code}_options_merchant_email", array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="<?php echo $code; ?>_options_merchant_email" type="text" name="payment[<?php echo $code; ?>][options][merchant_email]" class="span4 disable_method" maxlength="255" value="<?php echo set_value("payment[{$code}][options][merchant_email]", isset($options['merchant_email']) ? $options['merchant_email'] : ''); ?>"  target="<?php echo $code; ?>_status"/>
                    <span class="help-inline"><?php echo form_error("payment[{$code}][options][merchant_email]"); ?></span>
                </div>
            </div>
            
            <div class="control-group">
                <?php echo form_label(lang('paypal_username'), "{$code}_options_username", array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="<?php echo $code; ?>_options_username" type="text" name="payment[<?php echo $code; ?>][options][username]" class="span4 disable_method" maxlength="255" value="*****"/>
                </div>
            </div>
            
            <div class="control-group">
                <?php echo form_label(lang('paypal_password'), "{$code}_options_password", array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="<?php echo $code; ?>_options_password" type="text" name="payment[<?php echo $code; ?>][options][password]" class="span4 disable_method" maxlength="255" value="*****"/>
                </div>
            </div>
            
            <div class="control-group">
                <?php echo form_label(lang('paypal_signature'), "{$code}_options_signature", array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="<?php echo $code; ?>_options_signature" type="text" name="payment[<?php echo $code; ?>][options][signature]" class="span4 disable_method" maxlength="255" value="*****"/>
                </div>
            </div>
            
            <div class="control-group">
                <?php echo form_label(lang('paypal_sandbox_mode'), "{$code}_options_testmode", array('class' => "control-label") ); ?>
                <div class='controls'>
                    <?php echo form_dropdown2("payment[{$code}][options][testmode]", array("0"=>lang("bf_no"), "1"=>lang("bf_yes")), set_value("payment[{$code}][options][status]", isset($options['test_mode']) ? $options['test_mode'] : '0'), "id='{$code}_options_testmode' class='nostyle wd100px;'"); ?>
                </div>
            </div>            
            
            <div class="control-group <?php echo form_error("payment[{$code}][sort_order]") ? 'error' : ''; ?>">
                <?php echo form_label(lang('ed_sort_order'), "{$code}_sort_order", array('class' => "control-label") ); ?>
                <div class='controls'>
                    <input id="<?php echo $code; ?>_sort_order" type="text" name="payment[<?php echo $code; ?>][sort_order]" class="span4" maxlength="3" value="<?php echo set_value("payment[{$code}][sort_order]", isset($record['sort_order']) ? $record['sort_order'] : '1'); ?>"  />
                    <span class="help-inline"><?php echo form_error("payment[{$code}][sort_order]"); ?></span>
                </div>
            </div>
            
            <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; ?>
            <div class="control-group">
                <?php 
                    if($i == 0)
                        echo form_label(lang('ed_description'), "{$code}_info_{$c}_description", array('class' => "control-label") ); 
                    else
                        echo form_label('', "{$code}_info_{$c}_description", array('class' => "control-label") ); 
                ?>
                <div class='controls'>
                    <textarea id="<?php echo $code; ?>_info_<?php echo $c; ?>_description" name="payment[<?php echo $code; ?>][info][<?php echo $c; ?>][description]" class="span4" rows="2" ><?php echo set_value("payment[$code}][info][{$c}][description]", isset($info[$c]['description']) ? $info[$c]['description'] : ''); ?></textarea>
                    <span class="help-inline gray"><?php echo strtoupper($l['locale']); ?></span>
                </div>
            </div>
            <?php $i++; endforeach; ?>
                        
        </div>
    </div>
</div> 