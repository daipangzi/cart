<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['payment_methods_manage']			= 'Payment Methods';
$lang['payment_methods_list']           = 'Payment Methods';
$lang['payment_methods_edit_success']	= 'Payment Methods successfully saved.';
$lang['payment_methods_edit_failure']	= 'There was a problem saving the payment_methods: ';

$lang['paypal_merchant_email']  = 'Merchant Email';
$lang['paypal_username']        = 'API Username';
$lang['paypal_password']        = 'API Pasword';
$lang['paypal_signature']       = 'API Signature';
$lang['paypal_sandbox_mode']    = 'Sandbox Mode';

