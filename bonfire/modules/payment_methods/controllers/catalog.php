<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class catalog extends Admin_Controller {

	//--------------------------------------------------------------------
    private $payment_options = array();

	public function __construct()
	{
		parent::__construct();

        $this->auth->restrict('Payment_Methods.Catalog.Edit');
		$this->load->model('payment_methods_model', null, true);
		$this->lang->load('payment_methods');
	}

	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Payment Methods data.
	*/
	public function index()
	{
        if ($this->input->post('save'))    
        {
            if ($this->save_method())
            {
                Template::set_message(lang('payment_methods_edit_success'), 'success');
            }
            else
            {
                Template::set_message(lang('payment_methods_edit_failure') . $this->payment_methods_model->error, 'error');
            }
        }
        Assets::add_module_js('payment_methods', 'payment_methods.js');
        
        $records = $this->payment_methods_model->find_all(1);    
        Template::set('records', $records);
        
		Template::set('toolbar_title', lang('payment_methods_manage'));
		Template::render();
	}

	//--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_payment_methods()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_method()
	{
        //check validation
        $languages  = $this->languages_model->find_all(1);
        $methods    = $this->input->post('payment');
        
        foreach($methods as $code => $method)
        {
            //set rules for manufacturer attributes            
            foreach($languages as $l)
            {
                $c = $l['language_id'];                
                $this->form_validation->set_rules("{$code}[info][{$c}][description]", 'lang:ed_description', 'trim|max_length[255]|strip_tags|xss_clean');
            }
            
            //set extra fields validation
            $payment_options = config_item('payment_options');
            if(isset($method['status']) && $method['status'] == '1') 
            {
                $options = $payment_options[$code];
                if(is_array($options) && count($options) > 0)
                {
                    foreach($options as $opt)
                    {
                        $this->form_validation->set_rules("payment[{$code}][options][{$opt['field']}]", $opt['lang'], $opt['rule']);
                    }
                }
            }
            
            $_POST['payment'][$code]['sort_order'] = is_numeric($method['sort_order'])?$method['sort_order']:'1';
        }
        
        if ($this->form_validation->run() === FALSE)
        {
            return FALSE;
        }
        
        //save data
        $methods = $this->input->post('payment');
        foreach($methods as $code => $method)
        {
            $data = array();
            $data['code']       = $code;
            $data['sort_order'] = $method['sort_order'];
            $data['status']     = (isset($method['status']) && $method['status']=='1')?'1':'0';
            $data['expand']     = $method['expand']=='1'?'1':'0';
            
            $langs  = $method['info'];   
            $options = isset($method['options'])?$method['options']:FALSE;
            $this->payment_methods_model->save($data, $langs, $options);
        }
        
	    return TRUE;	
	}

	//--------------------------------------------------------------------
    


}