<?php
    $default = settings_item('site.default_currency');
?>

<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>
        <legend><?php echo lang('currencies_list'); ?><small><?php echo $this->pagination->get_page_status();?></small></legend>
    </fieldset>
    
    <div class="form-buttons">
        <?php if ($this->auth->has_permission('Currencies.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
        <button type="submit" name="delete" id="delete-me" class="btn" value="<?php echo lang('bf_action_delete') ?>" onclick="return confirm('<?php echo lang('currencies_delete_confirm'); ?>')"><span class="icon-remove-sign"></span>&nbsp;<?php echo lang('bf_action_delete') ?></button>            
        <?php endif;?>
        
        <?php if ($this->auth->has_permission('Currencies.Settings.Create')) : ?>
        <?php echo anchor(SITE_AREA .'/settings/currencies/create', '<span class="icon-plus-sign"></span>&nbsp;'.lang('bf_action_insert'), 'class="btn"'); ?>                    
        <?php endif;?>
    </div>
    
    <div class="responsive">
        <table class="table table-striped lrborder checkAll" id="flex_table" style="min-width:900px;">
        <colgroup>
            <?php if ($this->auth->has_permission('Currencies.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
            <col width="40"/>
            <?php endif;?>
            <col width="50"/>
            <col width=""/>
            <col width="100"/>
            <col width="200"/>
            <col width="200"/>
            <col width="120"/>
        </colgroup>
        <thead>
            <tr>
                <?php if ($this->auth->has_permission('Currencies.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                <th class="column-check"><input class="check-all" type="checkbox" /></th>
                <?php endif;?>
                <th class="<?php echo sort_classes($params['orderby'], "id", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/settings/currencies?orderby=id&amp;order='.sort_direction($params['orderby'], "id", $params['order'])); ?>"><span><?php echo lang('ed_id'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "title", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/settings/currencies?orderby=title&amp;order='.sort_direction($params['orderby'], "title", $params['order'])); ?>"><span><?php echo lang('ed_title'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "code", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/settings/currencies?orderby=code&amp;order='.sort_direction($params['orderby'], "code", $params['order'])); ?>"><span><?php echo lang('ed_code'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th class="<?php echo sort_classes($params['orderby'], "rate", $params['order'])?>">
                    <a href="<?php echo site_url(SITE_AREA.'/settings/currencies?orderby=rate&amp;order='.sort_direction($params['orderby'], "rate", $params['order'])); ?>"><span><?php echo lang('ed_rate'); ?></span><span class="sorting-indicator"</span></a>
                </th>
                <th><?php echo lang('ed_example'); ?></th>
                <th><?php echo lang('ed_actions'); ?></th>
            </tr>
        </thead>
        <tbody>
        <?php if (isset($records) && is_array($records) && count($records)) : ?>
        <?php foreach ($records as $record) : ?>
            <tr>
                <?php if ($this->auth->has_permission('Currencies.Settings.Delete')) : ?>
                <td>
                    <input type="checkbox" name="checked[]" value="<?php echo $record['currency_id'] ?>" />
                </td>
                <?php endif;?>
                
                <td><?php echo $record['currency_id'] ?></td>
                <td>
                    <?php if ($this->auth->has_permission('Currencies.Settings.Edit')) : ?>
                    <a href="<?php echo site_url(SITE_AREA .'/settings/currencies/edit/'. $record['currency_id']) ?>" title="<?php echo lang('bf_action_edit'); ?>"><?php echo $record['title'] ?></a>
                    <?php else: ?>
                    <?php echo $record['title'] ?>
                    <?php endif; ?>
                    
                    <?php if($record['code'] == $default) echo '<span class="gray">('.lang('ed_default').')</span>'; ?>
                </td>
                <td><?php echo $record['code'] ?></td>
                <td><?php echo $record['rate'] ?></td>
                <td><?php echo $record['symbol_left'].number_format('1499.99', 2, '.', ',').$record['symbol_right']; ?></td>
                <td>
                    <?php if ($this->auth->has_permission('Currencies.Settings.Edit')) : ?>
                    <a href="<?php echo site_url(SITE_AREA .'/settings/currencies/edit/'. $record['currency_id']) ?>" title="<?php echo lang('bf_action_edit'); ?>" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                    <?php endif; ?>
                    
                    <?php if ($this->auth->has_permission('Currencies.Settings.Delete')) : ?>
                    <a href="<?php echo site_url(SITE_AREA .'/settings/currencies/delete/'. $record['currency_id']) ?>" title="<?php echo lang('bf_action_delete'); ?>" class="tip"><span class="icon12 icomoon-icon-remove"></span></a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="10"><?php echo lang('no_record'); ?></td>
            </tr>
        <?php endif; ?>
        </tbody>
        </table>
    </div>
    <?php echo form_close(); ?>
    <?php echo $this->pagination->create_links(); ?>
</div>