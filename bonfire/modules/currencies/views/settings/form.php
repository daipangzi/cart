<?php
    $default = settings_item('site.default_currency');
?>
<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset>
        <legend><?php echo lang('currency_detail'); ?></legend>
        
        <div class="control-group <?php echo form_error('title') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_title'). lang('bf_form_label_required'), 'title', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="title" type="text" class="span4" name="title" maxlength="32" value="<?php echo set_value('title', isset($record['title']) ? $record['title'] : ''); ?>" autofocus="autofocus"/>
                <span class="help-inline"><?php echo form_error('title'); ?></span>
            </div>
        </div>
        
        <?php if(!isset($record)) :?>
        <div class="control-group <?php echo form_error('code') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_code'). lang('bf_form_label_required'), 'code', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="code" type="text" class="span4" name="code" maxlength="3" value="<?php echo set_value('code', isset($record['code']) ? $record['code'] : ''); ?>"/>
                <span class="help-inline"><?php echo form_error('code'); ?></span>
            </div>
        </div>
        <?php endif; ?>
        
        <div class="control-group <?php echo form_error('symbol_left') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_symbol_left'), 'symbol_left', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="symbol_left" type="text" class="span4" name="symbol_left" maxlength="12" value="<?php echo set_value('symbol_left', isset($record['symbol_left']) ? $record['symbol_left'] : ''); ?>"/>
                <span class="help-inline"><?php echo form_error('symbol_left'); ?></span>
            </div>
        </div>
        
         <div class="control-group <?php echo form_error('symbol_right') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_symbol_right'), 'symbol_right', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="symbol_right" type="text" class="span4" name="symbol_right" maxlength="12" value="<?php echo set_value('symbol_right', isset($record['symbol_right']) ? $record['symbol_right'] : ''); ?>"/>
                <span class="help-inline"><?php echo form_error('symbol_right'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('rate') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_rate'). lang('bf_form_label_required'), 'rate', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="rate" type="text" class="span4" name="rate" maxlength="21" value="<?php echo set_value('rate', isset($record['rate']) ? $record['rate'] : ''); ?>"/>
                <span class="help-inline"><?php echo form_error('rate'); ?></span>
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label(lang('ed_as_default'), 'as_default', array('class' => "control-label") ); ?>
            <div class='controls'> 
                <input type="checkbox" id="as_default" name="as_default" <?php if(isset($record['code']) && $default==$record['code']){ echo 'checked="checked" disabled="disabled"';} ?>/>
            </div>
        </div>

        <div class="form-buttons">
            <?php echo anchor($prev_page, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>
            <button type="reset" name="reset" class="btn" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
            
            <?php if(isset($record)) :?>
            <?php if ($default!=$record['code'] && $this->auth->has_permission('Currencies.Settings.Delete')) : ?>
            <button type="submit" name="delete" class="btn" id="delete-me" onclick="return confirm('<?php echo lang('currencies_delete_record_confirm'); ?>')">
                <i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete'); ?>
            </button>
            <?php endif; ?>
            <?php endif; ?>
            
            <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
            <button type="submit" name="save_continue" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save_continue'); ?></button>&nbsp;
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>