<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_currencies extends Migration {

	public function up()
	{
		$prefix = $this->db->dbprefix;

		$fields = array(
			'currencies_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			'modified_on' => array(
				'type' => 'datetime',
				'default' => '0000-00-00 00:00:00',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('currencies_id', true);
		$this->dbforge->create_table('currencies');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('currencies');

	}

	//--------------------------------------------------------------------

}