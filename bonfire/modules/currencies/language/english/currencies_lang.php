<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['currencies_manage']			= 'Currencies';
$lang['currencies_edit']			= 'Edit Currency';
$lang['currencies_create']			= 'New Currency';
$lang['currencies_list']			= 'Currencies';
$lang['currencies_create_success']	= 'Currency successfully created.';
$lang['currencies_create_failure']	= 'There was a problem creating the currency: ';
$lang['currencies_invalid_id']		= 'Invalid Currencies ID.';
$lang['currencies_edit_success']	= 'Currencies successfully saved.';
$lang['currencies_edit_failure']	= 'There was a problem saving the currency: ';
$lang['currencies_delete_success']	= 'record(s) successfully deleted.';
$lang['currencies_delete_failure']	= 'We could not delete the record: ';
$lang['currencies_delete_record_confirm']= 'Are you sure you want to delete this currency?';
$lang['currencies_delete_confirm']	     = 'Are you sure you want to delete these currencies?';
$lang['currencies_failed_default']       = 'The default currency cannot be deleted. Please set another currency as the default currency and try again.';

$lang['currency_detail'] = 'Currency Detail';