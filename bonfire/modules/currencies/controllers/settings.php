<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Currencies.Settings.View');
        
		$this->lang->load('currencies');
        
        if(!$this->session->userdata('currency_index'))
        {
            $this->session->set_userdata('currency_index', site_url(SITE_AREA .'/settings/currencies'));
        }
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
                $default_id =  $this->currencies_model->get_id_by_code(settings_item('site.default_currency'));
				foreach ($checked as $pid)
				{
                    if ($pid == $default_id)
                    {
                        Template::set_message(lang('currencies_failed_default'), 'warning');
                        $result = 'default';
                    }
                    else
                    {
					    $result = $this->currencies_model->delete($pid);
                    }
				}
                
                if($result != 'default')
                {
				    if ($result)
				    {
					    Template::set_message(count($checked) .' '. lang('currencies_delete_success'), 'success');
				    }
				    else
				    {
					    Template::set_message(lang('currencies_delete_failure') . $this->currencies_model->error, 'error');
				    }
                }
			}
		}

		//get where fields
        $where = array();        
        
        //get order fields
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);

        $orders = array();
        switch($params['orderby']) {
            case "title":
            case "code":
            case "rate":
                $orders[$params['orderby']] = $params['order'];
                break;

            case "id":
            default:
                $orders["currency_id"]     = "asc";
                break;
        }
        
        //get total counts
        $total_records = $this->currencies_model
            ->where($where)
            ->count_all();
        
        //get total records
        $records = $this->currencies_model
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all(1);
		Template::set('records', $records);
        
        //build pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/settings/currencies/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/settings/currencies') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);
        
        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('currency_index', $url);

		Template::set('toolbar_title', lang('currencies_manage'));
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: create()

		Creates a Currencies object.
	*/
	public function create()
	{
		$this->auth->restrict('Currencies.Settings.Create');

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			if ($insert_id = $this->save_currencies())
			{
				Template::set_message(lang('currencies_create_success'), 'success');
				
                //redirect by buttons clicked
                if($this->input->post('save')) Template::redirect(SITE_AREA .'/settings/currencies');
                else Template::redirect(SITE_AREA .'/settings/currencies/edit/'.$insert_id);
			}
			else
			{
				Template::set_message(lang('currencies_create_failure') . $this->currencies_model->error, 'error');
			}
		}
		Assets::add_module_js('currencies', 'currencies.js');

        Template::set('prev_page', $this->session->userdata('currency_index'));
		Template::set('toolbar_title', lang('currencies_create'));
        Template::set_view('currencies/settings/form');
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Currencies data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('currencies_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/currencies');
		}

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			$this->auth->restrict('Currencies.Settings.Edit');

			if ($this->save_currencies('update', $id))
			{
				Template::set_message(lang('currencies_edit_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) Template::redirect(SITE_AREA .'/settings/currencies');
			}
			else
			{
				Template::set_message(lang('currencies_edit_failure') . $this->currencies_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Currencies.Settings.Delete');
            
            $default_id =  $this->currencies_model->get_id_by_code(settings_item('site.default_currency'));
            if ($id == $default_id)
            {
                Template::set_message(lang('currencies_failed_default'), 'warning');
            }
            else
            {
			    if ($this->currencies_model->delete($id))
			    {
				    Template::set_message(lang('currencies_delete_success'), 'success');

				    redirect(SITE_AREA .'/settings/currencies');
			    } else
			    {
				    Template::set_message(lang('currencies_delete_failure') . $this->currencies_model->error, 'error');
			    }
            }
		}
        Assets::add_module_js('currencies', 'currencies.js');
		
        $record = $this->currencies_model->find($id, 1);
        if (empty($record))
        {
            Template::set_message(lang('currencies_invalid_id'), 'error');
            redirect(SITE_AREA .'/settings/currencies');
        }        
        Template::set('record', $record);
		
        Template::set('prev_page', $this->session->userdata('currency_index'));
		Template::set('toolbar_title', lang('currencies_edit'));
        Template::set_view('currencies/settings/form');
		Template::render();
	}    

	//--------------------------------------------------------------------

    public function delete()
    {
        $id = $this->uri->segment(5);

        if (empty($id))
        {
            Template::set_message(lang('currencies_invalid_id'), 'error');
            redirect(SITE_AREA .'/settings/currencies');
        }
        
        $this->auth->restrict('Currencies.Settings.Delete');
        
        $default_id =  $this->currencies_model->get_id_by_code(settings_item('site.default_currency'));
        if ($id == $default_id)
        {
            Template::set_message(lang('currencies_failed_default'), 'warning');

            redirect(SITE_AREA .'/settings/currencies');   
        }
        
        if ($this->currencies_model->delete($id))
        {
            Template::set_message(lang('currencies_delete_success'), 'success');

            redirect(SITE_AREA .'/settings/currencies');
        }
    }
    //--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_currencies()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_currencies($type='insert', $id=0)
	{
        $_POST['sort_order']= (!is_numeric($this->input->post('sort_order')) || $this->input->post('sort_order')==0)?'1':$this->input->post('sort_order');
        
		$this->form_validation->set_rules('title','lang:ed_title','required|trim|max_length[32]|strip_tags|xss_clean');
        $this->form_validation->set_rules('symbol_left','lang:ed_symbol','trim|max_length[12]|strip_tags|xss_clean');
        $this->form_validation->set_rules('symbol_right','lang:ed_symbol','trim|max_length[12]|strip_tags|xss_clean');
        $this->form_validation->set_rules('rate','lang:rate','required|trim|max_length[20]|strip_tags|xss_clean|is_numberic');
        
        if($type == 'insert')
        {
            $this->form_validation->set_rules('code','lang:ed_code','required|trim|max_length[3]|strip_tags|xss_clean');
        }
                
		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
        if($id != 0)
        {
            $data['currency_id']= $id;
        }
        else
        {
            $data['code']    = $this->input->post('code');
        }
        
        $data['title']       = $this->input->post('title');
        $data['symbol_left'] = $this->input->post('symbol_left');
        $data['symbol_right']= $this->input->post('symbol_right');
        $data['rate']        = $this->input->post('rate');
        
        $id = $this->currencies_model->save($data);
        if(is_numeric($id))
        {
            if($this->input->post('as_default'))
            {
                $this->settings_lib->set('site.default_currency', $this->currencies_model->get_code_by_id($id));
            }
            
            return $id;
        }
        
        return FALSE;
	}

	//--------------------------------------------------------------------



}