<div class="responsive"><div class="admin-box hasSearchBox" style="min-width:1000px;">
    <div class="row-fluid">
        <div id="tree_content">
            <div class="form-horizontal row-fluid">
                <fieldset><legend><?php echo lang('categories_list'); ?></legend></fieldset>
                <button type="button" name="insert" id="insert_new" class="btn right" value="insert" style="margin-top:-55px;"><span class="icon-plus-sign"></span>&nbsp;<?php echo lang('bf_action_new') ?></button>            
            </div>
            
            <div id="tree" data-source="ajax">
            </div>
        </div>
        
        <div id="tree_form">
            <?php Template::block('category_form', 'categories/catalog/form', $language_id); ?>          
        </div>
    </div>
</div></div>
