<?php // Change the css classes to suit your needs
$img = '';
$img_path = ''; 
if(isset($record)) {
    $info = $record['info'];
    
    $img = isset($image_name)?$image_name:$record['image'];
    $img_path = isset($image_name)?'tmp/category':'category';
}
$title = isset($info[$language_id]['name'])?$info[$language_id]['name']."(ID:{$record['category_id']})":lang('category_new');  
?>

<div class="row-fluid messages-area"><div class="span6">
    <?php echo Template::message(); ?>
</div></div>

<div class="admin-box">
    <?php echo form_open_multipart(SITE_AREA.'/catalog/categories/save', 'id="category_form" class="form-horizontal" autocomplete="off"'); ?>
    <input type="hidden" name="image_name" id="image_name" value=""/>
    <input type="hidden" name="image_action" id="image_action" value=""/>
    <input type="hidden" name="action" id="action" value="update"/>
    <input type="hidden" name="parent_id" id="parent_id" value="1"/>
    <input type="hidden" name="category_id" id="category_id" value="<?php echo isset($record['category_id'])?$record['category_id']:1?>"/>
    
    <fieldset>
        <legend><?php echo $title; ?></legend>
        
        <ul id="myTab1" class="nav nav-tabs">
            <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('ed_general'); ?></a></li>
            <li><a href="#meta_info" data-toggle="tab"><?php echo lang('ed_meta_info'); ?></a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade in active" id="general">
                <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; ?>
                <div class="control-group <?php echo form_error("info[{$c}][name]") ? 'error' : ''; ?>">
                    <?php 
                        if($i == 0)
                            echo form_label(lang('ed_name').lang('bf_form_label_required'), "info_{$c}_name", array('class' => "control-label") );
                        else
                            echo form_label('', "info_{$c}_name", array('class' => "control-label") );
                    ?>
                    <div class='controls'>
                        <input id="info_<?php echo $c; ?>_name" type="text" name="info[<?php echo $c; ?>][name]" class="span4" maxlength="255" value="<?php echo set_value("info[{$c}][name]", isset($info[$c]['name']) ? $info[$c]['name'] : ''); ?>" <?php echo form_error("info[{$c}][name]") ? 'title="'.form_error("info[{$c}][name]").'"' : ''; ?>/>
                        <span class="inline-help gray"><?php echo strtoupper($l['locale']); ?></span>
                    </div>
                </div>
                <?php $i++; endforeach; ?>
                
                <div class="control-group <?php echo form_error('slug') ? 'error' : ''; ?>">
                    <?php echo form_label(lang('ed_slug'), 'slug', array('class' => "control-label") ); ?>
                    <div class='controls'>
                        <input id="slug" type="text" name="slug" class="span4" maxlength="100" value="<?php echo set_value('slug', isset($record['slug']) ? $record['slug'] : ''); ?>" <?php echo form_error('slug') ? 'title="'.form_error('slug').'"' : ''; ?>/>
                    </div>
                </div>
                
                <?php $options = array('1'=>lang('bf_yes'), '0'=>lang('bf_no')); ?>
                <?php echo form_dropdown('in_navigation', $options, set_value('in_navigation', isset($record['in_navigation']) ? $record['in_navigation'] : '1'), lang('category_in_navigation'), 'id="in_navigation" class="nostyle"')?>

                <?php echo form_dropdown('status', array("1"=>lang("ed_enable"), "0"=>lang("ed_disable")), set_value('status', isset($record['status']) ? $record['status'] : '1'), lang('ed_status'), 'id="status" class="nostyle"'); ?>
                
                <div class="control-group">
                    <?php echo form_label(lang('ed_image'), 'category_image', array('class' => "control-label") ); ?>
                    <div class='controls'>   
                        <span class="imgwrapper"><span class="image marginB10 w120_h60">
                            <img src="<?php echo media_file($img, $img_path, 120, 60); ?>" alt="" id="categoryImage" class="" width="120" height="60"/>
                            <span id="remover" class="remove" <?php if($img == '' || $img == 'deleted') echo 'style="display:none;"';?>><i class="icon-remove">&nbsp;</i></span>
                        </span></span>
                        <span id="errors" class="help-inline" style="color:#B94A48;"><?php echo isset($image_error)?$image_error:''; ?></span>
                        <br/>
                        
                        <input type="file" name="category_image" id="category_image" class="" value=""/>
                        <span id="loading" class="help-inline"></span>
                    </div>
                </div>
            </div>
        
            <div class="tab-pane fade subtab" id="meta_info">
                <ul id="myTab1" class="nav nav-tabs">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <li <?php if($i==0){echo 'class="active"';} ?>><a href="#<?php echo $cd; ?>" data-toggle="tab"><?php echo $l['name']; ?></a></li>
                    <?php $i++; endforeach; ?>
                </ul>   
                
                <div class="tab-content">
                    <?php $i=0; foreach($languages as $l) : $c = $l['language_id']; $cd = $l['code']; ?>
                    <div class="tab-pane fade in <?php if($i==0){echo 'active';} ?>" id="<?php echo $cd; ?>">
                        <div class="control-group <?php echo form_error("info[{$c}][page_title]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_page_title'), "info_{$c}_page_title", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <input id="info_<?php echo $c; ?>_page_title" type="text" name="info[<?php echo $c; ?>][page_title]" class="span6" maxlength="255" value="<?php echo set_value("info[{$c}][page_title]", isset($info[$c]['page_title']) ? $info[$c]['page_title'] : ''); ?>" <?php echo form_error("info[{$c}][page_title]") ? 'title="'.form_error("info[{$c}][page_title]").'"' : ''; ?>/>
                            </div>
                        </div>
                        
                        <div class="control-group <?php echo form_error("info[{$c}][meta_keywords]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_meta_keywords'), "info{$c}_meta_keywords", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <textarea id="info_<?php echo $c; ?>_meta_keywords" name="info[<?php echo $c; ?>][meta_keywords]" class="span6 limit" rows="4" <?php echo form_error("info[{$c}][meta_keywords]") ? 'title="'.form_error("info[{$c}][meta_keywords]").'"' : ''; ?>><?php echo set_value("info[{$c}][meta_keywords]", isset($info[$c]['meta_keywords']) ? $info[$c]['meta_keywords'] : ''); ?></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group <?php echo form_error("info[{$c}][meta_description]") ? 'error' : ''; ?>">
                            <?php echo form_label(lang('ed_meta_description'), "info_{$c}_meta_description", array('class' => "control-label") ); ?>
                            <div class='controls'>
                                <textarea id="info_<?php echo $c; ?>_meta_description" name="info[<?php echo $c; ?>][meta_description]" class="span6" rows="5" <?php echo form_error("info[{$c}][meta_description]") ? 'title="'.form_error("info[{$c}][meta_description]").'"' : ''; ?>><?php echo set_value("info[{$c}][meta_description]", isset($info[$c]['meta_description']) ? $info[$c]['meta_description'] : ''); ?></textarea>
                            </div>
                        </div> 
                    </div> 
                    <?php $i++; endforeach; ?>
                </div>
            </div>
        </div>
        
        <div class="form-buttons">
            <button type="reset" name="reset" class="btn" id="reset" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
            
            <?php if(isset($record) && $record['category_id'] != 1 && !empty($record)) :?>
            <?php if ($this->auth->has_permission('Categories.Catalog.Delete')) : ?>
            <button type="submit" name="delete" class="btn" id="delete-me" onclick="return confirm('<?php echo lang('category_delete_confirm'); ?>')"><i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete'); ?></button>
            <?php endif; ?>
            <?php endif; ?>
            
            <button type="submit" name="save" class="btn" id="save" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>