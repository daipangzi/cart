<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class catalog extends Admin_Controller {

	//--------------------------------------------------------------------
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Categories.Catalog.View');
        
		$this->load->model('categories_model', null, true);
		$this->lang->load('categories');
        
        Assets::add_css( array(
            plugin_path().'misc/tree2/ui.dynatree.css',
        )); 
         
        Assets::add_js( array(
            'jquery.form.js',
            plugin_path().'misc/tree2/jquery.dynatree.js',
        ));   
        
        Assets::add_js($this->load->view('catalog/tree_js', null, true), 'inline');
        
        $config = init_upload_config('category');   
        $this->upload->initialize($config); 
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{
        Assets::add_module_js('categories', 'categories.js');
                              
        $record = $this->categories_model->find(1, 1);
        Template::set('record', $record);
        
        Template::set('language_id', global_language_id());
		Template::set('toolbar_title', lang('categories_manage'));
		Template::render('left-sidebar');
	}
    
    public function save()
    {
        // if action wrong action
        if((!$this->input->post('action')) 
            || ($this->input->post('action') == 'insert' && !$this->input->post('parent_id'))
            || ($this->input->post('action') == 'update' && !$this->input->post('category_id'))) 
        {
            Template::set_message(lang('category_select_parent'), 'error');
            
            $this->get_form($this->input->post('category_id'));
            return;
        }
        
        $action = $this->input->post('action');
        $id = $this->input->post('category_id');
        
        if($id = $this->save_category($id, $action))
        {
            Template::set_message(lang('categories_edit_success'), 'success');
        }
        else
        {
            Template::set_message(lang('categories_edit_failure') . $this->categories_model->error, 'error');
        }
        $this->get_form($id);
        exit;
    }
    
    public function delete()
    {
        $this->auth->restrict('Categories.Catalog.Delete');
        
        $id = $this->uri->segment(5);
        if ($this->categories_model->delete_node($id))
        {
            Template::set_message(lang('categories_delete_success'), 'success');
            
            $this->get_form('new');
        } 
        else
        {
            Template::set_message(lang('categories_delete_failure') . $this->categories_model->error, 'error');
            
            $this->get_form($id);
        }   
        exit;    
    }

	//--------------------------------------------------------------------


    //--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_category()

		Does the actual validation and saving of form data.

		Parameters:
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_category($id=0, $type="insert")
	{
        $_POST['slug']   = url_title(convert_accented_characters($this->input->post('slug')), 'dash', TRUE);
        
        //set rules for manufacturer attributes
        $languages = $this->languages_model->find_all(1);
        foreach($languages as $l)
        {
            $c = $l['language_id'];
            
            $this->form_validation->set_rules("info[{$c}][name]", 'lang:ed_name', 'required|trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][page_title]", 'lang:ed_page_title', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_keywords]", 'lang:ed_keyword', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_description]", 'lang:ed_meta_description', 'trim|max_length[255]|strip_tags|xss_clean');
        }
        
        $this->form_validation->set_rules('slug', 'lang:ed_slug','trim|alpha_dash|max_length[100]|strip_tags|xss_clean');
        $this->form_validation->set_rules('in_navigation', 'lang:ed_in_navigation','required|trim|is_numeric|max_length[1]');
        $this->form_validation->set_rules('status', 'lang:ed_status','required|trim|is_numeric|max_length[1]');

		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}
        
        // make sure we only pass in the fields we want
        $data = array();
        if($type == "update") 
        {
            $data['category_id'] = $id;            
        }
        else if($type == "insert") 
        {
            $data['parent_id'] = $this->input->post('parent_id');            
        }
        
        $data['in_navigation'] = $this->input->post('in_navigation')?$this->input->post('in_navigation'):1;
        $data['status']        = $this->input->post('status');
        $data['slug']          = $this->categories_model->validate_slug($this->input->post('slug'), $id);;
        
        //upload images  
        $action = $this->input->post('image_action');
        if($action == 'changed') {       
            if($this->input->post('image_name')) {  
                $file_name = $this->get_moved_image_name($this->input->post('image_name'), 'category');
                if($file_name !== FALSE) $data['image'] = $file_name;
            }
        } else if($action == 'deleted') {
            $data['image'] = '';
        }
        //end
        
        $langs = $this->input->post('info');
        return $this->categories_model->save($data, $langs);
	}
    
    private function get_form($category_id)
    {
        if($category_id == '') return;
        
        $record = array();
        
        //check category is root or not
        $record = $this->categories_model->find($category_id, 1);
        $languages = $this->languages_model->find_all(1);               
        
        $data['record']     = $record;
        $data['languages']  = $languages;
        $data['language_id']= global_language_id();
        
        Template::block('category_form', 'categories/catalog/form', $data);
    }
    
    //--------------------------------------------------------------------
    
    //--------------------------------------------------------------------
    // !AJAX METHODS
    //--------------------------------------------------------------------
    public function upload_category_image() {
        $result = $this->upload_image('category_image');
        echo json_encode($result);
        exit;   
    }
    
    public function get_childrens()
    {
        if($this->input->get('parent_id') == '') return;
        
        $parent_id = $this->input->get('parent_id');
        if($parent_id == 0)
        {
            $records = $this->categories_model->get_tree_root(2);
        }
        else
        {
            $records = $this->categories_model->get_tree_childrens($parent_id, true);
        }
        
        echo json_encode($records);
        exit;
    }
    
    public function category_form()
    {
        $this->get_form($this->input->post('category_id'));
        exit;
    }
    
    public function update_positions()
    {
        if($this->input->post('moved_id') == ''
            ||$this->input->post('parent_id') == '' 
            || $this->input->post('child_keys') == '') 
        {
            return;
        }
        
        $this->categories_model
            ->update_positions($this->input->post('moved_id'), $this->input->post('parent_id'), $this->input->post('child_keys'));
        exit;
    }
    
    public function validate_form()
    {
        //set rules for manufacturer attributes
        $languages = $this->languages_model->find_all(1);
        foreach($languages as $l)
        {
            $c = $l['language_id'];
            
            $this->form_validation->set_rules("info[{$c}][name]", 'lang:ed_name', 'required|trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][page_title]", 'lang:ed_page_title', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_keywords]", 'lang:ed_keyword', 'trim|max_length[255]|strip_tags|xss_clean');
            $this->form_validation->set_rules("info[{$c}][meta_description]", 'lang:ed_meta_description', 'trim|max_length[255]|strip_tags|xss_clean');
        }
        
        //$this->form_validation->set_rules('slug', 'lang:ed_slug','trim|alpha_dash|max_length[100]|strip_tags|xss_clean');
        $this->form_validation->set_rules('in_navigation', 'lang:ed_in_navigation','required|trim|is_numeric|max_length[1]');
        $this->form_validation->set_rules('status', 'lang:ed_status','required|trim|is_numeric|max_length[1]');
        
        $result = array();
        $errors = array();
        if ($this->form_validation->run() === FALSE)
        {
            foreach($languages as $l)
            {
                $c = $l['language_id'];
                
                if(form_error("info[{$c}][name]"))
                {
                    $error = array();
                    $error['field'] = "info_{$c}_name";
                    $error['message'] = form_error("info[{$c}][name]");
                    
                    $errors[] = $error;
                }
                
                if(form_error("info[{$c}][page_title]"))
                {
                    $error = array();
                    $error['field'] = "info_{$c}_page_title";
                    $error['message'] = form_error("info[{$c}][page_title]");
                    
                    $errors[] = $error;
                }
                
                if(form_error("info[{$c}][meta_keywords]"))
                {
                    $error = array();
                    $error['field'] = "info_{$c}_meta_keywords";
                    $error['message'] = form_error("info[{$c}][meta_keywords]");
                    
                    $errors[] = $error;
                }
                
                if(form_error("info[{$c}][meta_description]"))
                {
                    $error = array();
                    $error['field'] = "info_{$c}_meta_description";
                    $error['message'] = form_error("info[{$c}][meta_description]");
                    
                    $errors[] = $error;
                }
            }
            
            if(form_error("slug"))
            {
                $error = array();
                $error['field'] = "slug";
                $error['message'] = form_error("slug");
                
                $errors[] = $error;
            }
            
            $result['status'] = 'error';
            $result['messages'] = $errors;
        }
        else
        {
            $result['status'] = 'success';
        }
        
        echo json_encode($result);
        exit;
    }
}