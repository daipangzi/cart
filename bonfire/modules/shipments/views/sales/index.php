<div class="admin-box">
	<?php echo form_open($this->uri->uri_string(), 'id="list-form" class="form-horizontal" autocomplete="off"'); ?>
    <fieldset><legend><?php echo lang('ed_shipment_list'); ?><small><?php echo $this->pagination->get_page_status();?></small></legend></fieldset>
    
    <div class="form-buttons">
        <?php if(!empty($search)) :?>
        <button type="submit" name="reset_search" id="reset-search" class="btn" value="reset"><?php echo lang('bf_action_reset_filter') ?></button>
        <?php endif; ?>
        
        <button type="submit" name="search_entries" id="search-entries" class="btn" value="search"><span class="icon-zoom-in"></span>&nbsp;<?php echo lang('bf_action_filter') ?></button>
        <!--<button type="submit" name="action" class="btn" value="print"><span class="icomoon-icon-print"></span>&nbsp;<?php echo lang('ed_print'); ?>&nbsp;</button>&nbsp;-->
    </div>
    
    <div class="responsive">
		<table class="table table-striped lrborder checkAll" style="min-width:1000px;">
            <colgroup>
                <?php if ($this->auth->has_permission('Orders.Sales.Edit') && isset($records) && is_array($records) && count($records)) : ?>
                <col width="20"/>
                <?php endif;?>
                
                <col width="150"/>
                <col width="200"/>
                <col width="150"/>
                <col width="200"/>
                <col width=""/>
                <col width="120"/>
                <col width="100"/>
            </colgroup>
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Orders.Sales.Edit') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
                    <th class="<?php echo sort_classes($params['orderby'], "shipment", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/shipments?orderby=shipment&amp;order='.sort_direction($params['orderby'], "shipment", $params['order'])); ?>"><span><?php echo lang('ed_shipment'); ?> #</span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "date_shipmented", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/shipments?orderby=date_shipmentd&amp;order='.sort_direction($params['orderby'], "date_shipmentd", $params['order'])); ?>"><span><?php echo lang('ed_date_shipped'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "order", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/shipments?orderby=order&amp;order='.sort_direction($params['orderby'], "order", $params['order'])); ?>"><span><?php echo lang('ed_order'); ?> #</span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "date", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/shipments?orderby=date&amp;order='.sort_direction($params['orderby'], "date", $params['order'])); ?>"><span><?php echo lang('ed_date_ordered'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "customer", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/shipments?orderby=customer&amp;order='.sort_direction($params['orderby'], "customer", $params['order'])); ?>"><span><?php echo lang('ed_customer'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th class="<?php echo sort_classes($params['orderby'], "total_qty", $params['order'])?>">
                        <a href="<?php echo site_url(SITE_AREA.'/sales/shipments?orderby=total_qty&amp;order='.sort_direction($params['orderby'], "total_qty", $params['order'])); ?>"><span><?php echo lang('ed_total_qty'); ?></span><span class="sorting-indicator"</span></a>
                    </th>
                    <th><?php echo lang('bf_action');?></th>
				</tr>
                <tr class="search_row">
                    <?php if ($this->auth->has_permission('Orders.Sales.Edit') && isset($records) && is_array($records) && count($records)) : ?>
                    <td>&nbsp;</td>
                    <?php endif;?>
                    <td><input type="text" name="search[order_no]" id="serach_order_no" value="<?php echo isset($search['order_no'])?$search['order_no']:'';?>" class="wd95"/></td>
                    <td>
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="date right wd100px" name="search[date_from_shipment]" id="search_date_from_shipment" value="<?php echo isset($search['date_from_shipment'])?$search['date_from_shipment']:'';?>"/>
                        <div class="clear"></div>
                        <hr/>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="date right wd100px" name="search[date_to_shipment]" id="search_date_to_shipment" value="<?php echo isset($search['date_to_shipment'])?$search['date_to_shipment']:'';?>"/>
                    </td>
                    <td><input type="text" name="search[order_no]" id="serach_order_no" value="<?php echo isset($search['order_no'])?$search['order_no']:'';?>" class="wd95"/></td>
                    <td>
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="date right wd100px" name="search[date_from]" id="search_date_from" value="<?php echo isset($search['date_from'])?$search['date_from']:'';?>"/>
                        <div class="clear"></div>
                        <hr/>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="date right wd100px" name="search[date_to]" id="search_date_to" value="<?php echo isset($search['date_to'])?$search['date_to']:'';?>"/>
                    </td>
                    <td><input type="text" name="search[customer]" id="serach_customer" value="<?php echo isset($search['customer'])?$search['customer']:'';?>" class="wd95"/></td>
                    <td>
                        <span class="lbl"><?php echo lang('ed_from'); ?></span>
                        <input type="text" class="right wd50px" name="search[qty_from]" id="search_qty_from" value="<?php echo isset($search['qty_from'])?$search['qty_from']:'';?>"/>
                        <div class="clear"></div>
                        <hr/>
                        <span class="lbl"><?php echo lang('ed_to'); ?></span>
                        <input type="text" class="right wd50px" name="search[qty_to]" id="search_qty_to" value="<?php echo isset($search['qty_to'])?$search['qty_to']:'';?>"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
			</thead>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
                    <?php if ($this->auth->has_permission('Orders.Sales.Edit')) : ?>					
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->order_id ?>" /></td>
                    <?php endif;?>
                    <td><a href="<?php echo site_url(SITE_AREA . '/sales/shipments/view/'.$record->shipment_id); ?>"><?php echo $record->shipment_no ?></a></td>
                    <td><?php echo format_date_time($record->shipment_created_on); ?></td>
                    <td><?php echo $record->order_no ?></td>
                    <td><?php echo format_date_time($record->created_on); ?></td>
				    <td><?php echo $record->customer_name ?></td>
				    <td><?php echo $record->total_qty; ?></td>
                    <td><a href="<?php echo site_url(SITE_AREA . '/sales/shipments/view/'.$record->shipment_id); ?>"><?php echo lang('bf_action_view'); ?></a></td>
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="8"><?php echo lang('no_record'); ?></td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
    </div>
	<?php echo form_close(); ?>
    <?php echo $this->pagination->create_links(); ?>
</div>