<div style="page-break-after: always;">
    <h1><?php echo lang('ed_invoice'); ?>#<?php echo $invoice['invoice_no']; ?></h1>
    
    <table class="table">
    <colgroup>
        <col width=""/>
        <col width="150"/>
        <col width="150"/>
        <col width="100"/>
        <col width="150"/>
    </colgroup>
    <thead>
        <tr>
            <th><?php echo lang('ed_product'); ?></th>
            <th><?php echo lang('ed_sku'); ?></th>
            <th><?php echo lang('ed_price'); ?></th>
            <th><?php echo lang('ed_qty'); ?></th>
            <th><?php echo lang('ed_subtotal'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($order_items as $item) : ?>
        <tr>
            <td>
                <?php 
                echo $item['product_name']; 
                
                if(isset($item['options'])) {
                    echo "(";
                    $options = $item['options'];
                    
                    $i      = 0;
                    $prefix = '';
                    foreach($options as $k => $v) {
                        if($i != 0) $prefix = ',&nbsp;';
                        echo $prefix . $k . ":" . $v;
                        
                        $i++;
                    }
                    echo ")";
                }
                ?>
            </td>
            <td><?php echo $item['sku']; ?></td>
            <td><?php echo format_price($item['price']); ?></td>
            <td><?php echo $item['qty']; ?></td>
            <td><?php echo format_price($item['price']*$item['qty']); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    </table>
</div>