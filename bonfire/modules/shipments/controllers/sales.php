<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class sales extends Admin_Controller {

    //--------------------------------------------------------------------


    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict('shipments.Sales.View');
        
        $this->lang->load('orders/orders');
        
        //$this->load->helper('orders/order');
        
        $this->load->model('orders/orders_model', null, true);
        $this->load->model('orders/orders_shipments_model', null, true);
        $this->load->model('customers/customer_model', null, true);
        
        Assets::add_module_js('shipments', 'shipments.js');
        
        if(!$this->session->userdata('shipments_index'))
        {
            $this->session->set_userdata('shipments_index', site_url(SITE_AREA .'/sales/shipments'));
        } 
    }

    //--------------------------------------------------------------------



    /*
        Method: index()

        Displays a list of form data.
    */
    public function index($offset=0)
    {
        // Deleting anything?
        if ($this->input->post('mass_action'))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    //$result = $this->orders_model->delete($pid);
                }
             }
        }

        //get order fields
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);
        
        // build where sql
        $where = array();
        $search = FALSE;
        if($this->input->post('search_entries')) {
            $search = $this->input->post('search');    
            $this->session->set_userdata('shipment_search', $search);
        } else if($this->input->post('reset_search')) {
        } else {
            $search = $this->session->userdata('shipment_search')?$this->session->userdata('shipment_search'):array();
        }
        if(!empty($search['shipment_no'])) $where['orders_shipments.shipment_no LIKE'] = "%" . $search['shipment_no'] . "%";
        if(!empty($search['date_from_shipment']))$where['orders_shipments.created_on >= '] = $search['date_from_shipment'];
        if(!empty($search['date_toshipment_no']))  $where['orders_shipments.created_on <= '] = $search['date_to_shipment'];
        if(!empty($search['order_no'])) $where['orders.order_no LIKE'] = "%" . $search['order_no'] . "%";
        if(!empty($search['date_from']))$where['orders.created_on >= '] = $search['date_from'];
        if(!empty($search['date_to']))  $where['orders.created_on <= '] = $search['date_to'];
        if(!empty($search['customer'])) $where['orders.customer_name LIKE'] = "%" . $search['customer'] . "%";
        if(!empty($search['qty_from'])) $where['orders.total_qty >='] = $search['qty_from'];
        if(!empty($search['qty_to']))   $where['orders.total_qty <='] = $search['qty_to'];        
        if(!empty($search['status']))   $where['orders.status'] = $search['status'];
        Template::set('search', $search);
        
        // build order sql
        $orders = array();
        switch($params['orderby']) {
            case "shipment":
                $orders["shipment_no"]   = $params['order'];
                break;
            
            case "date_shipmentd":
                $orders["orders_shipments.created_on"]   = $params['order'];
                break;

            case "order":
                $orders["order_no"]     = $params['order'];
                break;
            
            case "date":
                $orders["created_on"]   = $params['order'];
                break;
                
            case "customer":
                $orders["customer_name"]= $params['order'];
                break;
            
            case "total_qty":
                $orders["orders.total_qty"] = $params['order'];
                break;
                
            case "status":
                $orders["orders.status"] = $params['order'];
                break;
                
            default:
                $orders["orders_shipments.created_on"]   = "desc";
                break;
        }
        $records = $this->orders_shipments_model
            ->where($where)
            ->limit($this->limit, $offset)
            ->order_by($orders)
            ->find_all();
        Template::set('records', $records);
        
        //get total counts
        $total_records = $this->orders_shipments_model
            ->where($where)
            ->count_all();
        
        //build pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/sales/shipments/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/sales/shipments') . $url_suffix;
        $this->pager['current_rows']= count($records);
        $this->pagination->initialize($this->pager);
        
        // set request page                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('shipments_index', $url);
        
        Template::set('toolbar_title', lang('shipment_manage'));
        Template::render();
    }

    //--------------------------------------------------------------------
    /*
        Method: edit()

        Allows editing of Orders data.
    */
    public function view()
    {
        $shipment_id = $this->uri->segment(5);
        
        if (empty($shipment_id))
        {
            Template::set_message(lang('shipment_invalid_id'), 'error');
            redirect(SITE_AREA .'/sales/shipments');
        }
        
        // get shipment info
        $shipment = $this->orders_shipments_model->find($shipment_id, 1);
        if(empty($shipment))
        {
            Template::set_message(lang('shipment_invalid_id'), 'error');
            redirect(SITE_AREA .'/sales/shipments');
        }
        Template::set('shipment', $shipment); 
        
        $id = $shipment['order_id'];
        
        // get order info
        $order = $this->orders_model->find($id, 1);
        if(empty($order))
        {
            Template::set_message(lang('shipment_invalid'), 'error');
            redirect(SITE_AREA .'/sales/shipments');
        }
        Template::set('order', $order); 
        
        // get order address          
        $ship_address = $this->orders_model->get_shipping_address($id);     
        Template::set('ship_address', $ship_address); 
        
        // get order items
        $order_items = $this->orders_model->get_items($id);
        Template::set('order_items', $order_items); 
        
        // get customer original address
        if($order['customer_id'] != '') 
        {
            $customer_address = $this->customer_model->find_address($order['customer_id']);
            Template::set('customer_address', $customer_address); 
        }
        
        $page_title = lang('ed_shipment').'#'.$shipment['shipment_no'];
        Template::set('toolbar_title', $page_title);
        
        if($this->input->post('action') == 'print')
        {
            exit;
        }
        
        Template::set('prev_page', $this->session->userdata('shipments_index'));
        Template::render();
    }
    //--------------------------------------------------------------------
    
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------    
    private function send_email($order, $new_status, $msg)
    {
        $site_name = settings_item('site.title');
        
        $data = array(
            'order'       => $order,
            'new_status'  => $new_status,
            'msg'         => $msg
        );
        $email_mess = $this->load->view('_emails/sales/order_update', $data, true);   
        $subject    = sprintf(lang('ed_ord_update_subject'), $site_name, $order['order_no']);
        
        // Now send the email
        $this->load->library('emailer/emailer');
        $data = array(
            'to'         => $order['customer_email'],
            'subject'    => $subject,
            'message'    => $email_mess
        );
        //print_r( $email_mess); exit;
        $this->emailer->send($data);
    }
    //--------------------------------------------------------------------
}