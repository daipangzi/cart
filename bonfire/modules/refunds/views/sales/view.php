<?php
$back_url = isset($prev_page)?$prev_page:(SITE_AREA .'/sales/refunds');
?>

<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <input type="hidden" name="key" id="key" value="<?php echo MD5($refund['refund_id']); ?>"/>    
    <fieldset><legend><?php echo lang('ed_refundment'); ?> <?php echo $refund['refund_no']; ?> | <?php echo format_date_time($refund['created_on']); ?></legend></fieldset>
    
    <div class="form-buttons">
        <?php echo anchor($back_url, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>
        
        <!--<button type="submit" name="action" class="btn" value="print"><span class="icomoon-icon-print"></span>&nbsp;<?php echo lang('ed_print'); ?>&nbsp;</button>&nbsp;-->
    </div>
    <!--end form-buttons-->
    <?php echo form_close(); ?>
    
    <div class="row-fluid"><div class="span12"><div class="box gradient">
        <div class="title"><h4><span><?php echo lang('ed_ord_info'); ?></span></h4></div>
        <div class="content responsive">
            <table class="classic2" style="min-width:300px;">
            <colgroup>
                <col width="150"/>
                <col/>
            </colgroup>
            <tr>
                <td><?php echo lang('ed_order'); ?></td>
                <td><strong><?php echo $order['order_no']; ?></strong></td>
            </tr>
            <tr>
                <td><?php echo lang('ed_ord_date'); ?></td>
                <td><strong><?php echo format_date_time($order['created_on']); ?></strong></td>
            </tr>
            <tr>
                <td><?php echo lang('ed_ord_status'); ?></td>
                <td class="color-orange"><strong><?php echo lang($order['status']); ?></strong></td>
            </tr>
            <tr>
                <td><?php echo lang('ed_ord_ip'); ?></td>
                <td><strong><?php echo $order['remote_ip']; ?></strong></td>
            </tr>
            <tr>
                <td><?php echo lang('ed_ord_shipping'); ?></td>
                <td><strong><?php echo $order['shipping_method']; ?> - <?php echo format_price($order['shipping']); ?></strong></td>
            </tr>
            <tr>
                <td><?php echo lang('ed_ord_payment'); ?></td>
                <td><strong><?php echo lang($order['payment_method']); ?></strong></td>
            </tr>
            <tr>
                <td><?php echo lang('ed_note'); ?></td>
                <td><?php echo $order['note']; ?></td>
            </tr>
            </table>
        </div>
    </div></div></div>
    <!--end order-information-->
    
    <div class="row-fluid"><div class="span6"><div class="box gradient">
        <div class="title"><h4><span><?php echo lang('ed_cust_info'); ?></span></h4></div>
        <div class="content responsive">
            <table class="classic2" style="min-width:300px;">
            <colgroup>
                <col width="150"/>
                <col/>
            </colgroup>
            <tr>
                <td><?php echo lang('ed_cust_name'); ?></td>
                <td>
                    <?php if($order['customer_id'] != '') : ?>
                    <a href="<?php echo site_url(SITE_AREA.'/catalog/customers/edit/'.$order['customer_id']); ?>"><?php echo $order['customer_name']; ?></a>
                    <?php else: ?>
                    <?php echo $order['customer_name']; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td><?php echo lang('ed_email'); ?></td>
                <td><a href="mailto:<?php echo $order['customer_email']; ?>"><?php echo $order['customer_email']; ?></a></td>
            </tr>
            <?php if(isset($customer_address)) :?>
            <tr>
                <td><?php echo lang('ed_address'); ?></td>
                <td><?php echo $customer_address->province_name ?>&nbsp;<?php echo $customer_address->city_name ?>&nbsp;<?php echo $customer_address->address ?></td>
            </tr>
            <?php else: ?>
            <tr>
                <td></td>
                <td><?php echo lang('ed_guest'); ?></td>
            </tr>
            <?php endif; ?>
            </table>
        </div>
    </div></div>
    
    <div class="span6"><div class="box gradient">
        <div class="title"><h4><span><?php echo lang('ed_ship_address'); ?></span></h4></div>
        <div class="content responsive">
            <table class="classic2" style="min-width:300px;">
            <colgroup>
                <col width="150"/>
                <col/>
            </colgroup>
            <tr>
                <td><?php echo lang('ed_name'); ?></td>
                <td><?php echo $ship_address->name; ?></td>
            </tr>
            <tr>
                <td><?php echo lang('ed_email'); ?></td>
                <td><a href="mailto:<?php echo $ship_address->email; ?>"><?php echo $ship_address->email; ?></a></td>
            </tr>
            <tr>
                <td><?php echo lang('ed_address'); ?></td>
                <td>
                    <?php echo $ship_address->province_name ?>&nbsp;<?php echo $ship_address->city_name ?>&nbsp;<?php echo $ship_address->address ?>
                </td>
            </tr>
            </table>
        </div>
    </div></div></div>
    <!--end shipping-address-->
    
    <div class="row-fluid"><div class="span12"><div class="box gradient">
        <div class="title"><h4><span><?php echo lang('ed_inv_items'); ?></span></h4></div>
        <div class="content noPad"><div class="responsive">
            <table class="table">
            <colgroup>
                <col width=""/>
                <col width="150"/>
                <col width="150"/>
                <col width="100"/>
                <col width="150"/>
            </colgroup>
            <thead>
                <tr>
                    <th><?php echo lang('ed_product'); ?></th>
                    <th><?php echo lang('ed_sku'); ?></th>
                    <th><?php echo lang('ed_price'); ?></th>
                    <th><?php echo lang('ed_qty'); ?></th>
                    <th><?php echo lang('ed_subtotal'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($order_items as $item) : ?>
                <tr>
                    <td>
                        <?php 
                        echo '<strong>'.$item->product_name.'</strong>'; 
                        
                        $options = json_decode($item->options);          
                        if(isset($options) && !empty($options)) {
                            $i      = 0;
                            $prefix = '';
                            foreach($options as $k => $v) {
                                $prefix = '<br/>';
                                echo $prefix . $k . ":" . $v;
                                
                                $i++;
                            }
                        }
                        ?>
                    </td>
                    <td><?php echo $item->sku; ?></td>
                    <td><?php echo format_price($item->price); ?></td>
                    <td><?php echo $item->qty; ?></td>
                    <td><?php echo format_price($item->price*$item->qty); ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            </table>
        </div></div>
    </div></div></div>
    <!--end order-items-->
    
    <div class="row-fluid">
        <div class="span6" style="min-height:1px;"></div>
        <!--end comment-history-->
        
        <div class="span6"><div class="box gradient">
            <div class="title"><h4><span><?php echo lang('ed_ord_totals'); ?></span></h4></div>
            <div class="content noPad order-total">
                <table class="classic">
                <colgroup>
                    <col width=""/>
                    <col width="150"/>
                </colgroup>
                <tbody>
                    <tr>
                        <td class="text-right"><?php echo lang('ed_subtotal'); ?></td>
                        <td><?php echo format_price($order['subtotal']); ?></td>
                    </tr>
                    <?php if(!empty($order['coupon_code'])) : ?>
                    <tr>
                        <td class="text-right"><?php echo sprintf(lang('counpon_summary'), $order['coupon_code']); ?></td>
                        <td><?php echo format_price($order['coupon_discount']); ?></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td class="text-right"><?php echo lang('ed_shipping'); ?></td>
                        <td><?php echo format_price($order['shipping']); ?></td>
                    </tr>
                    <tr class="color-orange">
                        <td class="text-right"><strong><?php echo lang('ed_total'); ?></strong></td>
                        <td><strong><?php echo format_price($order['total']); ?></strong></td>
                    </tr>
                    
                    <?php if($order['status'] == 'refunded') : ?>
                    <tr class="color-orange">
                        <td class="text-right"><strong><?php echo lang('ed_total_refunded'); ?></strong></td>
                        <td><strong><?php echo format_price($order['total']); ?></strong></td>
                    </tr>
                    <?php endif; ?>
                </tbody>
                </table>
            </div>
        </div></div>
        <!--end order-total-->
    </div>
</div>