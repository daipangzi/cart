<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Admin_Controller {

	//--------------------------------------------------------------------


	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Languages.Settings.View');
		
        $this->lang->load('languages');
        
        if(!$this->session->userdata('language_index'))
        {
            $this->session->set_userdata('language_index', site_url(SITE_AREA .'/settings/languages'));
        }
	}

	//--------------------------------------------------------------------

	/*
		Method: index()

		Displays a list of form data.
	*/
	public function index($offset=0)
	{

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
                $default_id = $this->languages_model->get_id_by_code(settings_item('site.default_language'));
				foreach ($checked as $pid)
				{
                    if($pid == $default_id)
                    {
                        Template::set_message(lang('languages_failed_default'), 'warning');    
                        $result = 'default';
                    }
                    else
                    {
					    $result = $this->languages_model->delete($pid);
                    }
                    
                    if($result != 'default')
                    {
                        if ($result)
                        {
                            Template::set_message(count($checked) .' '. lang('languages_delete_success'), 'success');
                        }
                        else
                        {
                            Template::set_message(lang('languages_delete_failure') . $this->languages_model->error, 'error');
                        }
                    }
				}
			}
		}

		//get where fields
        $where = array();        
        
        //get order fields
        $params['order']    = isset($_GET['order'])?$_GET['order']:'';
        $params['orderby']  = isset($_GET['orderby'])?$_GET['orderby']:'';
        Template::set('params', $params);

        $orders = array();
        switch($params['orderby']) {
            case "name":
            case "code":
                $orders[$params['orderby']] = $params['order'];
                break;
            
            case "id":
            default:
                $orders["language_id"]      = "asc";
                break;
        }
        
        //get total counts
        $total_records = $this->languages_model
            ->where($where)
            ->count_all();
        
        //get total records
        $records = $this->languages_model
                ->where($where)
                ->limit($this->limit, $offset)
                ->order_by($orders)
                ->find_all(1);
		Template::set('records', $records);
        
        //build pagination
        $url_suffix = (!empty($params['orderby']) && !empty($params['order']))?"?orderby={$params['orderby']}&order={$params['order']}":'';
        $this->load->library('pagination');
        $this->pager['base_url']    = site_url(SITE_AREA .'/settings/languages/index');
        $this->pager['total_rows']  = $total_records;
        $this->pager['per_page']    = $this->limit;
        $this->pager['uri_segment'] = 5;
        $this->pager['suffix']      = $url_suffix;
        $this->pager['first_url']   = site_url(SITE_AREA .'/settings/languages') . $url_suffix;
        $this->pager['current_rows'] = count($records);
        $this->pagination->initialize($this->pager);

        // set request page                                                                    
        $url = $this->uri->uri_string();
        $url .= $url_suffix;
        $this->session->set_userdata('language_index', $url);
        
		Template::set('toolbar_title', lang('languages_manage'));
		Template::render();
	}

	//--------------------------------------------------------------------

	/*
		Method: edit()

		Allows editing of Languages data.
	*/
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('languages_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/languages');
		}

		if ($this->input->post('save') || $this->input->post('save_continue'))
		{
			$this->auth->restrict('Languages.Settings.Edit');

			if ($this->save_languages('update', $id))
			{
				Template::set_message(lang('languages_edit_success'), 'success');
                
                //redirect by buttons clicked
                if($this->input->post('save')) Template::redirect(SITE_AREA .'/settings/languages');
			}
			else
			{
				Template::set_message(lang('languages_edit_failure') . $this->languages_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Languages.Settings.Delete');
            
            $default_id = $this->languages_model->get_id_by_code(settings_item('site.default_language'));
            if($id == $default_id)
            {
			    Template::set_message(lang('languages_failed_default'), 'warning');    
            }
            else
            {
                if ($this->languages_model->delete($id))
                {
                    Template::set_message(lang('languages_delete_success'), 'success');

                    redirect(SITE_AREA .'/settings/languages');
                } 
                else
                {
                    Template::set_message(lang('languages_delete_failure') . $this->languages_model->error, 'error');
                }
            }
		}
        Assets::add_module_js('languages', 'languages.js');
		
        $record = $this->languages_model->find($id, 1);
        if (empty($record))
        {
            Template::set_message(lang('languages_invalid_id'), 'error');
            redirect(SITE_AREA .'/settings/languages');
        }        
        Template::set('record', $record);
		
        Template::set('prev_page', $this->session->userdata('language_index'));
		Template::set('toolbar_title', lang('languages_edit'));
        Template::set_view('languages/settings/form');
		Template::render();
	}    

	//--------------------------------------------------------------------

    public function delete()
    {
        $id = $this->uri->segment(5);

        if (empty($id))
        {
            Template::set_message(lang('languages_invalid_id'), 'error');
            redirect(SITE_AREA .'/settings/languages');
        }
        
        $this->auth->restrict('Languages.Settings.Delete');
        
        $default_id = $this->languages_model->get_id_by_code(settings_item('site.default_language'));
        if ($id == $default_id)
        {
            Template::set_message(lang('languages_failed_default'), 'warning');

            redirect(SITE_AREA .'/settings/languages');   
        }
        
        if ($this->languages_model->delete($id))
        {
            Template::set_message(lang('languages_delete_success'), 'success');

            redirect(SITE_AREA .'/settings/languages');
        }
    }
    //--------------------------------------------------------------------

	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/*
		Method: save_languages()

		Does the actual validation and saving of form data.

		Parameters:
			$type	- Either "insert" or "update"
			$id		- The ID of the record to update. Not needed for inserts.

		Returns:
			An INT id for successful inserts. If updating, returns TRUE on success.
			Otherwise, returns FALSE.
	*/
	private function save_languages($type='insert', $id=0)
	{
        $_POST['sort_order']= (!is_numeric($this->input->post('sort_order')) || $this->input->post('sort_order')==0)?'1':$this->input->post('sort_order');
        
		$this->form_validation->set_rules('name','lang:ed_name','required|trim|max_length[32]|strip_tags|xss_clean');
        $this->form_validation->set_rules('date_format','lang:ed_date_format','required|trim|max_length[32]|strip_tags|xss_clean');        
        $this->form_validation->set_rules('time_format','lang:ed_time_format','required|trim|max_length[32]|strip_tags|xss_clean');        
        $this->form_validation->set_rules('sort_order','lang:ed_sort_order','trim|max_length[3]|strip_tags|xss_clean|is_numeric');
        
		if ($this->form_validation->run() === FALSE)
		{
			return FALSE;
		}

		// make sure we only pass in the fields we want
        if($id != 0)
        {
            $data['language_id']= $id;
        }
        
        $data['name']           = $this->input->post('name');
        $data['date_format']    = $this->input->post('date_format');
        $data['time_format']    = $this->input->post('time_format');
        $data['sort_order']     = $this->input->post('sort_order');
        
        if($this->input->post('as_default'))
        {
            $data['sort_order'] = 1;   
        }
        
        
        $id = $this->languages_model->save($data);
        if(is_numeric($id))
        {
            if($this->input->post('as_default'))
            {
                $this->settings_lib->set('site.default_language', $this->languages_model->get_code_by_id($id));
            }
            
            return $id;
        }
        
        return FALSE;
	}

	//--------------------------------------------------------------------

    function get_time_string()
    {
        $format = $this->input->post('format_string');
        echo date($format, time());
        exit;
    }

}