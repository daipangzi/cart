<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_languages extends Migration {

	public function up()
	{
		$prefix = $this->db->dbprefix;

		$fields = array(
			'languages_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
			),
			'modified_on' => array(
				'type' => 'datetime',
				'default' => '0000-00-00 00:00:00',
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('languages_id', true);
		$this->dbforge->create_table('languages');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$prefix = $this->db->dbprefix;

		$this->dbforge->drop_table('languages');

	}

	//--------------------------------------------------------------------

}