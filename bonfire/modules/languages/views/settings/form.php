<?php
    $default = settings_item('site.default_language');
    echo validation_errors();
?>
<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" autocomplete="off"'); ?>
    
    <fieldset>
        <legend><?php echo lang('language_detail'); ?></legend>
        
        <div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_title'). lang('bf_form_label_required'), 'name', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="name" type="text" class="span4" name="name" maxlength="32" value="<?php echo set_value('name', isset($record['name']) ? $record['name'] : ''); ?>" autofocus="autofocus"/>
                <span class="help-inline"><?php echo form_error('name'); ?></span>
            </div>
        </div>
        
         <div class="control-group <?php echo form_error('ed_date_format') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_date_format'). lang('bf_form_label_required'), 'date_format', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="date_format" type="text" class="span4 changetime" name="date_format" maxlength="32" value="<?php echo set_value('date_format', isset($record['date_format']) ? $record['date_format'] : ''); ?>"/>
                <span class="example gray"><?php echo date(set_value('date_format', isset($record['date_format']) ? $record['date_format'] : ''), time()); ?></span>
                <span class="help-inline"><?php echo form_error('date_format'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('time_format') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_time_format'). lang('bf_form_label_required'), 'time_format', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="time_format" type="text" class="span4 changetime" name="time_format" maxlength="32" value="<?php echo set_value('time_format', isset($record['time_format']) ? $record['time_format'] : ''); ?>"/>
                <span class="example gray"><?php echo date(set_value('time_format', isset($record['time_format']) ? $record['time_format'] : ''), time()); ?></span>
                <span class="help-inline"><?php echo form_error('time_format'); ?></span>
            </div>
        </div>
        
        <div class="control-group <?php echo form_error('sort_order') ? 'error' : ''; ?>">
            <?php echo form_label(lang('ed_sort_order'), 'sort_order', array('class' => "control-label") ); ?>
            <div class='controls'>
                <input id="sort_order" type="text" name="sort_order" class="span4" maxlength="3" value="<?php echo set_value('sort_order', isset($manufacturer['sort_order']) ? $manufacturer['sort_order'] : '1'); ?>"  />
                <span class="help-inline"><?php echo form_error('sort_order'); ?></span>
            </div>
        </div>
        
        <div class="control-group">
            <?php echo form_label(lang('ed_as_default'), 'as_default', array('class' => "control-label") ); ?>
            <div class='controls'> 
                <input type="checkbox" id="as_default" name="as_default" <?php if(isset($record['code']) && $default==$record['code']){ echo 'checked="checked" disabled="disabled"';} ?>/>
            </div>
        </div>

        <div class="form-buttons">
            <?php echo anchor($prev_page, '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>
            <button type="reset" name="reset" class="btn" value="Reset">&nbsp;<?php echo lang('bf_action_reset'); ?>&nbsp;</button>&nbsp;
            
            <?php if(isset($record)) :?>
            <?php if ($default!=$record['code'] && $this->auth->has_permission('Languages.Settings.Delete')) : ?>
            <button type="submit" name="delete" class="btn" id="delete-me" onclick="return confirm('<?php echo lang('languages_delete_record_confirm'); ?>')">
                <i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete'); ?>
            </button>
            <?php endif; ?>
            <?php endif; ?>
            
            <button type="submit" name="save" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save'); ?></button>&nbsp;
            <button type="submit" name="save_continue" class="btn" value="Save"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save_continue'); ?></button>&nbsp;
        </div>
    </fieldset>
    <?php echo form_close(); ?>
</div>