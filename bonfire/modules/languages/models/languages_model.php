<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Languages_model extends BF_Model {

	protected $table		= "languages";
	protected $key			= "language_id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";
	protected $set_created	= false;
	protected $set_modified = false;
	protected $modified_field = "modified_on";
    
    private $is_admin = false;
    
    /**
    * set flag if backend or frontend
    * 
    */
    function is_admin()
    {
        $this->is_admin = true;   
        return $this; 
    }
    
    function save($data) {
        if(!isset($data[$this->key]))
            return $this->insert($data);
        else  {
            if($this->update($data[$this->key], $data))
                return $data[$this->key];
            else 
                return FALSE;
        }
    }
    
    /**
    * get result by dropdown mode
    * 
    * @param mixed $empty_row
    * @param mixed $empty_text
    */
    function dropdown_list($empty_row=true, $empty_text='')
    {
        $records     = $this->find_all(1);
        
        $result = array();
        if(!empty($records))
        {
            if($empty_row)
            {
                $result[''] = $empty_text;
            }
            
            foreach($records as $r)
            {
                $code = $r['code'];
                
                $result[$code] = $r['name'];
            }    
        }
        
        return $result;
    }
    
    function find_by_code($code)
    {
        return $this->find_by('code', $code);
    }
    
    //--------------------------------------------------------------------
    // !COMMON METHODS
    //--------------------------------------------------------------------
    
    function get_id_by_code($code)
    {
        $record = $this->find_by('code', $code);
        if(empty($record)) return FALSE;
        
        return $record->language_id;
    }
    
    function get_code_by_id($id)
    {
        $record = parent::find($id, 1);
        return $record['code'];
    }
    
    function is_valid($code)
    {
        if($this->get_id_by_code($code) == FALSE)
        {
            return FALSE;
        }
        
        return TRUE;
    }
}
