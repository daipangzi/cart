<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['languages_manage']		= 'Languages';
$lang['languages_edit']			= 'Edit Language';
$lang['languages_list']			= 'Languages';
$lang['languages_create_success']	= 'Language successfully created.';
$lang['languages_create_failure']	= 'There was a problem creating the language: ';
$lang['languages_invalid_id']		= 'Invalid language ID.';
$lang['languages_edit_success']	= 'Language successfully saved.';
$lang['languages_edit_failure']	= 'There was a problem saving the language: ';
$lang['languages_delete_success']	= 'record(s) successfully deleted.';
$lang['languages_delete_failure']	= 'We could not delete the record: ';
$lang['languages_delete_record_confirm']= 'Are you sure you want to delete this language?';
$lang['languages_delete_confirm']	     = 'Are you sure you want to delete these languages?';
$lang['languages_failed_default']       = 'The default language cannot be deleted. Please set another language as the default language and try again.';

$lang['language_detail'] = 'Language Detail';