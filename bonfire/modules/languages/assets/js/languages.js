$(document).ready(function() {
    $('.changetime').change(function() {
        format_object = $(this);
        $.ajax({
            url: admin_path+"/settings/languages/get_time_string",
            type: "post",
            data: {
                'format_string': $(this).val(),
                "ci_csrf_token": ci_csrf_token()
            },
            success: function (res) {
                $('.example', format_object.parent()).html(res);
            }
        });   
    });
});