<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Cart {

    private $CI;    
    private $_reset_required = FALSE;
    public $_cart_contents;
    
    public $message = FALSE;
    
    //--------------------------------------------------------------------
    // !INIT METHODS
    //--------------------------------------------------------------------
    function __construct() 
    {
        $this->CI =& get_instance();
        $this->CI->load->model(array('carts_model', 
            'coupons/coupons_model', 
            'orders/orders_model', 
            'payment_methods/payment_methods_model'));
        
        // Load the saved session
        if ($this->CI->session->userdata('cart_contents') !== FALSE)
        {
            $this->_cart_contents = $this->CI->session->userdata('cart_contents');
        }
        else
        {
            // Or init a new session
            $this->_init_properties();            
        }         
    }
    
    private function _init_properties($totals_only=FALSE, $preserve_customer=FALSE)
    {        
        // This is the discount amount to subtract from the cart total when the order is finalized
        $this->_cart_contents['coupon_discount']         = 0;
        
        // totals
        $this->_cart_contents['subtotal']                = 0;
        $this->_cart_contents['discounted_subtotal']     = 0;
        $this->_cart_contents['cart_total']              = 0;
        $this->_cart_contents['total_items']             = 0;
        $this->_cart_contents['total_qty']               = 0;
        
        // We want to preserve the cart items and properties, but reset total values when recalculating
        if( ! $totals_only) 
        {
            // product items will live in here
            $this->_cart_contents['items'] = array();
            
            if(!$preserve_customer)
            {
                // customer data container
                $this->_cart_contents['customer'] = FALSE;
            }
           
            // coupon code
            $this->_cart_contents['coupon_code']    = FALSE;
            
            // Container for payment details
            $this->_cart_contents['payment'] = array();  
            
            // shipping details container
            $this->_cart_contents['shipping'] = array();
            $this->_cart_contents['shipping']['method'] = FALSE;  // defaults
            $this->_cart_contents['shipping']['price']  = FALSE;
            $this->_cart_contents['shipping']['code']   = FALSE;      
        }
    }
       
    /**
    * load cart data from db and merge with current items
    * 
    * @param mixed $customer_id
    */
    function merge_with_db()
    {
        $customer_id = $this->CI->customer_auth->customer_id();
        if($old_cart = $this->CI->carts_model->find_cart($customer_id))
        {
            //we will ignore old coupon
            $new_items = $this->_cart_contents['items'];
                      
            $this->_cart_contents['items'] = $old_cart->items;    
                         
            if(is_array($new_items) && count($new_items) > 0)
            {   
                //set reset flag so that check items before display them 
                $this->_reset_required = TRUE;
                
                foreach($new_items as $key => $item)
                {
                    if(isset($this->_cart_contents['items'][$key]))    
                    {
                        $this->_cart_contents['items'][$key]->qty = intval($this->_cart_contents['items'][$key]->qty) + intval($item->qty);
                    } 
                    else
                    {
                        $this->_cart_contents['items'][$key] = $item;                        
                    }
                }
                $this->_save_cart(TRUE);
            }
            else
            {
                $this->_save_cart();
            }
        }                
    }
    
    //--------------------------------------------------------------------
    // !PUBLIC METHODS
    //--------------------------------------------------------------------
    /**
     * Insert items into the cart and save it to the session table
     *
     * @access    public
     * @param    array
     * @return    bool
     */
    function insert($items = array())
    {
        // Was any cart data passed? No? Bah...
        if ( ! is_array($items) OR count($items) == 0)
        {
            return FALSE;
        }
        
        // You can either insert a single product using a one-dimensional array, 
        // or multiple products using a multi-dimensional one. The way we
        // determine the array type is by looking for a required array key named "id"
        // at the top level. If it's not found, we will assume it's a multi-dimensional array.
    
        $save_cart = FALSE;        
        if (isset($items['product_id']))
        {            
            if ($this->_insert($items) == TRUE)
            {
                $save_cart = TRUE;
            }
        }
        else
        {
            foreach ($items as $val)
            {
                if (is_array($val) AND isset($val['product_id']))
                {
                    if ($this->_insert($val) == TRUE)
                    {
                        $save_cart = TRUE;
                    }
                }            
            }
        }

        // Save the cart data if the insert was successful
        if ($save_cart == TRUE)
        {
            $this->_save_cart();
            
            return TRUE;
        }

        return FALSE;
    }
    
    /**\
    * upate cart items
    * 
    * @param mixed $qty_list
    * @param mixed $coupon_code
    */
    function update_cart($qty_list=false, $coupon_code=false)
    {
        // We might not need to save the cart, if nothing changes
        $save_cart = FALSE;
        
        //on update clear the payments & shipping
        $this->clear_payment();
        $this->clear_shipping();
        
        // insert any coupons that might be sent
        if($coupon_code) 
        {
            if($this->_insert_coupon($coupon_code))
            { 
                $save_cart = TRUE;
            }
        }
                
        if($qty_list)
        {
            foreach($qty_list as $item_key => $qty)
            {
                if(!is_numeric($qty)) continue; // ignore if there's garble in it
                                            
                //have to run an isset here due to an error in IE (presumeably due to self signed certificate error double loading)
                if(isset($this->_cart_contents['items'][$item_key]) && $this->_cart_contents['items'][$item_key]->qty != $qty)
                {    
                    if((int)$qty>0)
                    {
                        // don't update the quantity of no_quantity items 
                        // TODO
                        if(!isset($this->_cart_contents['items'][$item_key]->no_quantity)) 
                        {
                            $this->_update($item_key, $qty);
                        }
                    } else {
                        $this->_remove($item_key);
                    }
                    $save_cart = TRUE;
                }

            }
        }
        
        if($save_cart)
        {
            $this->_save_cart();
        }

        return $response;
    }
    
    /**
    * remove shipping details
    * 
    */
    function clear_shipping()
    {
        $this->_cart_contents['shipping']['method']  = FALSE;
        $this->_cart_contents['shipping']['price']   = FALSE;
        $this->_cart_contents['shipping']['code']    = FALSE;
        
        $this->_save_cart();
    }
    
    /**
    * remve payment detail
    * 
    */
    function clear_payment()
    {
        $this->_cart_contents['payment'] = FALSE;
        
        // save cart - no recalculation necessary
        $this->_save_cart(FALSE);
    }
    
    /**
     * Destroy the cart
     *
     * Empties the cart
     * 
     *
     * @access    public
     * @return    null
     */
    function destroy($keep_customer_data=TRUE, $save_db=TRUE)
    {    
        // reset the cart values
        $this->_init_properties(FALSE, $keep_customer_data);        
        
        // save the updated cart to our session
        $this->_save_cart(FALSE, $save_db);
    }
    
    /**
    * check if cart is empty
    * 
    */
    function is_empty()
    {
        $this->_reset_required = TRUE;
        return $this->items()?FALSE:TRUE;
    }   
    
    /**
    * check if cart has error
    * 
    */
    function has_errors()
    {
        if($this->get_additional_detail('errors'))
        {
            return TRUE;
        }
        return FALSE;
    } 
    
    /**
    * get cart summart
    * 
    */                    
    function summary()
    {
        $sum = array();
        
        $subtotal = $this->_cart_contents['subtotal'];
        $sum[lang('ed_subtotal')] = $subtotal;
        
        if($this->_cart_contents['coupon_discount'] > 0) {
            $coupon_code = $this->_cart_contents['coupon_code'];
            
            $lang = sprintf(lang('counpon_summary'), $coupon_code);        
            
            $sum[$lang] = -$this->_cart_contents['coupon_discount'];
        }
        
        return $sum;
    }
    
    /**
    * check if cart has prepared_order
    * 
    */
    function has_prepared_order()
    {
        if(!isset($this->_cart_contents['order_prepared']))
        {
            return FALSE;
        }
                             
        return TRUE;
    }
    
    /**
    * check if cart has been completed
    * 
    */
    function has_completed_order()
    {
        if(!isset($this->_cart_contents['order_id']))
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
    * get short order information for success page
    * 
    */
    function get_short_order()
    {
        $order_id = $this->get_additional_detail('order_id');
        $order = $this->CI->orders_model->find_short_order($order_id);

        $this->remove_additional_detail('order_prepared');
        $this->remove_additional_detail('order_id');
        
        return $order;
    }
    
    /**
    * save order
    * 
    */
    function save_order() {
        $customer    = $this->_cart_contents['customer'];
        $ship        = $customer['ship_address'];
        
        //prepare our data for being inserted into the database
        $order = array();
        
        //if the id exists, then add it to the array $save array and remove it from the customer
        if(isset($customer['customer_id']) && $customer['customer_id'] != '')
        {
            $order['customer_id']    = $customer['customer_id'];
        }
        
        $order['customer_name']     = $customer['customer_name']; 
        $order['customer_email']    = $customer['customer_email'];
        
        //shipping information
        $order['shipping_code']     = $this->_cart_contents['shipping']['code'];
        $order['shipping_method']   = $this->_cart_contents['shipping']['method'];
        $order['shipping']          = $this->_cart_contents['shipping']['price'];
        
        //store the payment info
        $order['payment_code']      = $this->_cart_contents['payment']['module'];
        $order['payment_method']    = $this->_cart_contents['payment']['module'];
        
        //discounts
        if($this->_cart_contents['coupon_discount'] > 0)
        {
            $order['coupon_code']        = $this->_cart_contents['coupon_code'];
            $order['coupon_discount']    = $this->_cart_contents['coupon_discount'];
        }
        
        $order['subtotal']      = $this->_cart_contents['subtotal'];
        $order['discounted_subtotal'] = $this->_cart_contents['discounted_subtotal'];
        $order['total']         = $this->_cart_contents['discounted_subtotal'] + $order['shipping'];
        $order['total_items']   = $this->_cart_contents['total_items'];
        $order['total_qty']     = $this->_cart_contents['total_qty'];
        $order['remote_ip']     = $this->CI->input->ip_address();
        $order['store_currency']= settings_item('site.default_currency');
        
        //save additional details
        $order['note']                = $this->get_additional_detail('note')?$this->get_additional_detail('note'):'';
        
        // shipping address
        $address = array();
        $address['name']    = $ship['name'];
        $address['email']   = $ship['email'];
        $address['phone']   = $ship['phone'];
        $address['province']= $ship['province'];
        $address['city']    = $ship['city'];
        $address['address'] = $ship['address'];
        
        // order items
        $items = $this->_cart_contents['items'];
        
        //get settings and parameter arrays for payments
        $paycode  = $this->_cart_contents['payment']['module'];
        $settings = $this->CI->payment_methods_model->get_settings($paycode);
        $params   = $this->_build_request_params($paycode);
        
        //load payment driver
        $this->CI->load->library('merchant');
        $this->CI->merchant->load($paycode);
        $this->CI->merchant->initialize($settings);
        $response = $this->CI->merchant->purchase_return($params);
         
        //if request is rejected
        if($response->status() == Merchant_response::COMPLETE)
        {
            //sotre payment information                       
            $order['transaction_id']= $response->reference();
            $order['status']        = strtolower($response->order_status());
            $order['pending_reason']= $response->order_pending_reason();
            if(isset($_GET['PayerID']))
            {
                $order['payer_id'] = $_GET['PayerID'];
            }
            
            // save the order content
            $order_id = $this->CI->orders_model->save($order, $items, $address);
               
            // dont do anything else if the order failed to save
            if(is_numeric($order_id))
            {
                $this->set_additional_detail('order_id', $order_id);
                
                $this->_remove_cart();
                return $order_id;    
            }
            else
            {
                $this->message = lang('order_has_problem_on_saving');
            }
        }
        else
        {
            $this->message = lang($response->message());    
        }
        
        return FALSE;                                         
    }
    
    function place_order()
    {
        $this->set_additional_detail('order_prepared', '1');
        $this->_save_cart(FALSE, FALSE);
        
        //get settings and parameter arrays for payments
        $paycode  = $this->_cart_contents['payment']['module'];
        $settings = $this->CI->payment_methods_model->get_settings($paycode);
        $params   = $this->_build_request_params($paycode);
        
        //load payment driver
        $this->CI->load->library('merchant');
        $this->CI->merchant->load($paycode);
        $this->CI->merchant->initialize($settings);
        $response = $this->CI->merchant->purchase($params);
          
        //if request is rejected
        if($response->status() == Merchant_response::FAILED)
        {
            Template::set_message(lang($response->message()), 'error');
            Template::redirect('cart');
        }
    }
    
    //--------------------------------------------------------------------
    // !PROPERTY METHODS
    //--------------------------------------------------------------------
    function total()
    {
        return $this->_cart_contents['cart_total'];
    }
    
    function subtotal()
    {
        return $this->_cart_contents['subtotal'];
    }
    
    function coupon_discount()
    {
        return $this->_cart_contents['coupon_discount'];
    }
    
    function discounted_subtotal()
    {
        return $this->_cart_contents['discounted_subtotal'];
    }
    
    function total_items()
    {
        return $this->_cart_contents['total_items'];
    }
    
    function total_qty()
    {
        return $this->_cart_contents['total_qty'];
    }
    
    //for additional details
    function set_additional_detail($key, $data)
    {
        $this->_cart_contents[$key]    = $data;
        $this->_save_cart(FALSE);
    }
    
    function get_additional_detail($key)
    {
        if(isset($this->_cart_contents[$key]))
        {
            return $this->_cart_contents[$key];
        }
        else
        {
            return FALSE;
        }
    }
    
    function remove_additional_detail($key)
    {
        if(isset($this->_cart_contents[$key])) {
            unset($this->_cart_contents[$key]);
            $this->_save_cart(false);
        }
    }
    
    function customer()
    {
        if(!$this->_cart_contents['customer'])
        {
            return FALSE;
        }
        else
        {
            return $this->_cart_contents['customer'];
        }
    }

    function save_customer($data)
    {
        $this->_cart_contents['customer'] = $data;
        $this->_save_cart();
    }
    
    function set_shipping($method, $price, $code)
    {
        if(!is_numeric($price))
        {
            return FALSE;
        }
        
        $this->_cart_contents['shipping'] = array('method'=>$method, 'price'=> (float) $price, 'code'=>$code);
        
        //update cart - recalculate
        $this->_save_cart();
    }
    
    function shipping_method()
    {
        return $this->_cart_contents['shipping'];
    }
    
    function shipping_cost()
    {
        return $this->_cart_contents['shipping']['price'];
    }
    
    function shipping_code()
    {
        return $this->_cart_contents['shipping']['code'];
    }
    
    function set_payment($module, $description='')
    {
        $this->_cart_contents['payment'] = array('module'=>$module, 'description'=>$description);
        
        // save cart - no recalculation necessary
        $this->_save_cart(false);
    }
    
    function payment_method()
    {
        return $this->_cart_contents['payment'];
    }
    
    //--------------------------------------------------------------------
    // !ITEM METHODS
    //--------------------------------------------------------------------
    function item($key)
    {
        if(!empty($this->_cart_contents['items'][$key]))
        {
            return $this->_cart_contents['items'][$key];
        }
        else        
        {
            return FALSE;
        }
    }
    
    function item_qty($key)
    {
        $item = $this->item($key);
        if($item != FALSE)
        {
            return $item->qty;
        }
        
        return 0;
    }
    
    function items()
    {      
        if(count($this->_cart_contents['items']) == 0)
        {
            return FALSE;
        }
             
        if($this->_reset_required == TRUE)
        {
            $save_cart = FALSE;
            $has_error = FALSE;
            
            //remove unavailble items
            $pm = $this->CI->products_model;
            foreach($this->_cart_contents['items'] as $key => $item)
            {
                //remove invalid products from cart
                $product = $pm->find($item->product_id, 2);
                if(!$product)
                {
                    unset($this->_cart_contents['items'][$key]);
                    $save_cart = TRUE;
                    continue;
                }
                
                //remove invalid options from item
                if($item->hasOptions())
                {
                    foreach($item->options as $opt_id => $opt_value)
                    {
                        if(!$product->validateOption($opt_id, $opt_value))
                        {
                            unset($item->options[$opt_id]);
                            $save_cart = TRUE;
                        }
                    }
                }
                    
                //check if item has all required options
                if($product->hasMissingOptions($item->options))
                {
                    $has_error = TRUE;
                    
                    $item->errors['product_has_required_options'] = sprintf(lang('product_has_required_options'), $product->name);
                    Template::set_message(sprintf(lang('product_has_required_options'), $product->name), 'error');    
                }   
                else
                {
                    unset($item->errors['product_has_required_options']);
                }
                
                if(!$product->validateQty($item->qty))
                {
                    $has_error = TRUE;
                    
                    $item->errors['product_qty_invalid'] = sprintf(lang('product_qty_invalid'), $product->name);
                    Template::set_message(sprintf(lang('product_qty_invalid'), $product->name), 'error');    
                }
                else
                {
                    unset($item->errors['product_qty_invalid']);
                }
                    
                $item->product = $product;
                $item->name    = $product->name;
            }
            
            if($has_error)
            {          
                $this->set_additional_detail('errors', '1');
            }
            else
            {
                $this->remove_additional_detail('errors');
            }
             
            //reset update flag for next time
            $this->_reset_required = FALSE;
                        
            if($save_cart)
            {
                $this->_save_cart();
            }
        }
                   
        return $this->_cart_contents['items'];
    }  
    
    //--------------------------------------------------------------------
    // !URL METHODS
    //--------------------------------------------------------------------
    function get_url()
    {
        return get_cart_url();
    }
    
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------
    private function _insert($item)
    {
        //on update clear the payments & shipping
        $this->clear_payment();
        $this->clear_shipping();
        
        // Was any cart data passed? No? Bah...
        if ( ! is_array($item) OR count($item) == 0)
        {
            return FALSE;
        }
        
        //record the quantity
        $quantity    = isset($item['qty']) ? $item['qty'] : 1;
        $item['qty'] = ceil($quantity);
        
        // Generate our row ID by the entire item array
        if (isset($item['options']) AND count($item['options']) > 0 && !empty($item['options']))
        {
            $newkey = md5($item['product_id'] . $item['sku'] . json_encode($item['options']));
        }
        else
        {
            // No options were submitted so we simply MD5 the product ID.
            // Technically, we don't need to MD5 the ID in this case, but it makes
            // sense to standardize the format of array indexes for both conditions
            $newkey = md5($item['product_id'] . $item['sku']);
        }  
        
        //check to see if the item already exists in the cart
        //if it does, add the new quantity to the existing quantity
        //if it does not, add it as a new item
        
        //this is for non-edited products (except for quantity)
        if(isset($this->_cart_contents['items'][$newkey]))
        {
            $this->_cart_contents['items'][$newkey]->qty += $item['qty'];
        }
        else
        {
            // add our item to the items list
            $this->_cart_contents['items'][$newkey] = new Item($item);
        }

        // Woot!
        return TRUE;
    }
    
    private function _remove($key) 
    {
        if(!isset($this->_cart_contents['items'][$key])) return FALSE;
        
        // Remove the item from our items list
        unset($this->_cart_contents['items'][$key]);
        
        // Remove from db
        $this->CI->carts_model->delete_item($key);
        
        return true;
    }
    
    private function _update($cartkey, $quantity)
    {
        //on update clear the payments & shipping
        $this->clear_payment();
        $this->clear_shipping();
        
        if(!isset($this->_cart_contents['items'][$cartkey]))
        {
            return FALSE;
        }
        
        // update cart, fixed quantity items restricted to 1
        $this->_cart_contents['items'][$cartkey]->qty = ceil($quantity);
        
        return true;
    }
    
    private function _insert_coupon($coupon_code)
    {
        if(!$coupon_code) return FALSE;
        
        $cpm = $this->CI->coupons_model;
        $subtotal = $this->_cart_contents['subtotal'];
        $coupon   = $cpm->find_by_code($coupon_code);

        // Is the code valid?
        if($coupon != FALSE) 
        {
            // Make sure they can't submit the same coupon more than once
            if($this->_cart_contents['coupon_code'] != $coupon_code)
            {
                // check validity
                if(($cpm->validate($coupon->coupon_id)) 
                    && (($coupon->total_amount > 0) && ($subtotal >= $coupon->total_amount))) 
                {
                    $this->_cart_contents['coupon_code'] = $coupon_code;
                    
                    // message coupon added but not applied
                    Template::set_message(lang('coupon_applied_success'), 'success');                    
                    return TRUE;
                } 
                else 
                {
                    // message coupon no longer valid
                    Template::set_message(lang('coupon_invalid'), 'error');
                    return FALSE;
                }
            } 
            else 
            {
                // message coupon already applied
                Template::set_message(lang('coupon_already_applied'), 'error');
                return FALSE;
            }
        } 
        else 
        {
            // invalid code error message
            Template::set_message(lang('coupon_code_invalid'), 'error');
            return FALSE;
        }    
    }
    
    // Calculate the best possible discount within the product instance limitations for the whole cart
    // return the discount amount
    private function _calculate_coupon_discount()
    {
        $total_discount = 0;
                
        // Calculate whole order discount
        //  If a customer has more than one whole order coupon and enters them,
        //  we only want to use the one that results in the best discount (no doubling, etc)
            
        if($this->_cart_contents['coupon_code'])
        {
            $cpm = $this->CI->coupons_model;
            $subtotal       = $this->_cart_contents['subtotal'];
            $coupon_code    = $this->_cart_contents['coupon_code'];
            $coupon         = $cpm->find_by_code($coupon_code);
            
            $can_apply = TRUE;
            
            //check coupon is still valid
            if($cpm->validate($coupon->coupon_id)) 
            {
                if($coupon->total_amount > 0 && $coupon->total_amount > $subtotal)
                {
                    $can_apply = FALSE;                        
                }
            } 
            else 
            {                 
                //$this->_cart_contents['coupon_code'] = false;                
                $can_apply = false; 
            }
            
            $discount_amount = 0;    
            if($can_apply) {
                if($coupon->type == "F")
                {
                    $discount_amount = $coupon->discount;
                } else 
                {
                    //we need to swap percentages
                    //ex 20% discount needs to return an 80% price
                    $reduction_ammount  = $coupon->discount;
                    $discount_amount    = $subtotal * ($reduction_ammount / 100);
                }
            }
            // coupon discounts and whole order discounts can be cumulated
            $total_discount += $discount_amount;
        }
        
        $this->_cart_contents['discounted_subtotal'] = $this->_cart_contents['subtotal'] - $total_discount;
        $this->_cart_contents['coupon_discount']     = $total_discount;
    }
    
    /**
     * Save the cart array to the session DB
     *
     * @access    private
     * @return    bool
     */
    private function _save_cart($recalculate=TRUE, $save_db=TRUE)
    {
        // Once in the check-out stage, we no longer need to keep recalculating totals
        if($recalculate) 
        {
            // Reset totals
            $this->_init_properties(true);
            
            // Lets add up the individual prices and set the cart sub-total
            $total        = 0;
            $taxable      = 0;
            $coupon_total = 0;
            $total_qty    = 0;
            
            foreach ($this->_cart_contents['items'] as $key => $item)
            {
                $this_price = $item->price;
                $total     += ($this_price * $item->qty);
                $total_qty += $item->qty;
                
                // set product subtotal (NOT accounting for coupon discount yet)
                $item->subtotal = ($this_price * $item->qty);
            }
            
            // total products in the cart
            $this->_cart_contents['total_items'] = count($this->_cart_contents['items']);    
            $this->_cart_contents['total_qty']   = $total_qty;    
            
            // Set the cart price totals ...
            $this->_cart_contents['subtotal'] = $total;
            
            // Calculate / set total coupon discount amounts
            $this->_calculate_coupon_discount();
            
            $total = $total - $this->_cart_contents['coupon_discount'];
            $this->_cart_contents['cart_total'] = $total>0?$total:0;
        }   
        
        if($save_db) {
            //save db    
            $this->_save_cart_db();     
        }
            
        // Save up
        $this->CI->session->set_userdata(array('cart_contents' => $this->_cart_contents));
        
        // Woot!
        return TRUE;    
    }
    
    private function _save_cart_db()
    {
        //check customer is logged in
        if(!$this->CI->customer_auth->is_logged_in()) return;
        $customer_id = $this->CI->customer_auth->customer_id();
        
        //get customer's current cart id and save cart data
        $cart    = $this->_cart_contents;
        $cart_id = $this->CI->carts_model->get_customer_cart_id($customer_id);
                               
        $this->CI->carts_model->save($cart_id, $customer_id, $cart);
    }
    
    private function _remove_cart()
    {
        if($this->CI->customer_auth->is_logged_in())
        {
            $customer_id = $this->CI->customer_auth->customer_id();
            $cart_id = $this->CI->carts_model->get_customer_cart_id($customer_id);
        
            $this->CI->carts_model->delete($cart_id);   
        }
        
        $this->_remove_coupon();
        $this->destroy(TRUE, FALSE);
    }
    
    private function _remove_coupon()
    {
        $this->_cart_contents['coupon_discount'] = 0;
        $this->_cart_contents['coupon_code']     = FALSE;
    }
    
    private function _build_request_params($paycode)
    {
        $items = array();
        foreach($this->_cart_contents['items'] as $it)
        {
            $item = array();
            $item['name'] = $it->name;
            $item['sku']  = $it->sku;  
            $item['price']= $it->price;
            $item['qty']  = $it->qty;
            
            $items[] = $item;
        }
        
        $params = array();
        $params['currency'] = settings_item('site.default_currency');
        $params['discount'] = -$this->coupon_discount();
        $params['subtotal'] = $this->subtotal();
        $params['shipping'] = $this->shipping_cost();
        $params['amount']   = $this->total();
        $params['items']    = $items;
        $params['return_url'] = site_url('checkout/confirm');
        $params['cancel_url'] = site_url('checkout/cancel');
        
        return $params;
    }
}
// END Cart Class

/* End of file Cart.php */
/* Location: ./system/libraries/Cart.php */