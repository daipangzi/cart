<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//TODO
class Format {
    
    private static $currency = FALSE;
    private static $language = FALSE;

    /**
     * An instance of the CI super object.
     *
     * @access private
     * @static
     *
     * @var object
     */
    private static $ci;
    
    public function __construct()
    {
        self::$ci =& get_instance();
        
        $currency_code  = self::$ci->config->item('currency');
        $language_code  = self::$ci->config->item('language');
        
        self::$language = self::$ci->languages_model->find_by_code($language_code);
        self::$currency = self::$ci->currencies_model->find_by_code(settings_item('site.default_currency'));
    }
    
    function format_price($value)
    {
        $price_str = self::$currency->symbol_left;        
        $price_str .= number_format($value, 2, '.', ',');
        $price_str .= self::$currency->symbol_right;
        return $price_str;
    }
    
    function format_date_time($time)
    {
        if(!is_int($time))
        {
            $time = strtotime($time);
        }
        
        $str = date(self::$language->date_format.' '.self::$language->time_format, $time);
        return $str;
    }
    
    function format_date($time)
    {
        if(!is_int($time))
        {
            $time = strtotime($time);
        }
        $str = date(self::$language->date_format, $time);
        return $str;
    }
    
    function format_time($time)
    {
        if(!is_int($time))
        {
            $time = strtotime($time);
        }
        $str = date(self::$language->time_format, $time);
        return $str;
    }
    
    function format_sales_percent($percent)
    {
        return sprintf(lang('ed_sales'), $percent);
    }
}
// END Product Class

/* End of file Cart.php */
/* Location: ./system/libraries/Product.php */