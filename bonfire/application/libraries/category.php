<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//TODO
class Category {
    private $hasChildrens = FALSE;
    private $childrens = FALSE;
    
    function __construct($item=FALSE)
    {
        if($item == FALSE) return $this;
        
        foreach($item as $key => $value){
            $this->{$key} = $value;
        }
    }    
    
    function getUrl()
    {
        return get_clean_url($this->category_id, 'category');
    }
    
    function hasChildrens()
    {
        return $this->hasChildrens==0?FALSE:TRUE;
    }
    
    function getChildrens()
    {
        if($this->childrens == FALSE && $this->hasChildrens != 0)
        {
            $CI = & get_instance();
            return $CI->categories_model->get_childrens($this->category_id, 2);    
        }
        
        return $this->childrens;
    }
}
// END Product Class

/* End of file Cart.php */
/* Location: ./system/libraries/Product.php */