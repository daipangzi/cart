<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Bonfire
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   Bonfire
 * @author    Bonfire Dev Team
 * @copyright Copyright (c) 2011 - 2012, Bonfire Dev Team
 * @license   http://guides.cibonfire.com/license.html
 * @link      http://cibonfire.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Form Validation
 *
 * This class extends the CodeIgniter core Form_validation library to add
 * extra functionality used in Bonfire.
 *
 * @package    Bonfire
 * @subpackage Libraries
 * @category   Libraries
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/core/form_validation.html
 *
 */
class MY_Pagination extends CI_Pagination
{
    var $current_rows = 0;
    
    public function get_page_status()
    {  
        $current_page = 0;
        $CI =& get_instance();  
            
        $current_page = $CI->uri->segment($this->uri_segment);
        $current_page = (int) $current_page;
        
        if($this->total_rows > 0)
        {
            return sprintf(lang('page_status'), $current_page+1, $current_page+$this->current_rows ,$this->total_rows);
        }
        else
        {
            return '';
        }
    }
}	
/* Author :  http://net.tutsplus.com/tutorials/php/6-codeigniter-hacks-for-the-masters/ */
/* End of file : ./libraries/MY_Pagination.php */