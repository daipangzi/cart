<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//TODO
class Post_Category {
    private $hasChildrens = FALSE;
    private $childrens = FALSE;
    
    function __construct($item=false)
    {
        if($item == false) return;
        
        foreach($item as $key => $value){
            $this->{$key} = $value;
        }
    }    
    
    function getUrl()
    {
        return get_clean_url($this->category_id, 'post_category');
    }
    
    function getDescription()
    {
        return $this->description;
    }
    
    function hasChildrens()
    {
        return $this->hasChildrens==0?FALSE:TRUE;
    }
    
    function getChildrens()
    {
        if($this->childrens == FALSE && $this->hasChildrens != 0)
        {
            $CI = & get_instance();
            return $CI->post_categories_model->get_childrens($this->category_id, 2);    
        }
        
        return $this->childrens;
    }
}
// END Product Class

/* End of file Cart.php */
/* Location: ./system/libraries/Product.php */