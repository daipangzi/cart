<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//TODO
class Product {
    public $product_id  = 0;
    public $name        = '';
    public $sku         = '';
    public $price       = 0;
    public $special_price = 0;
    public $slug        = '';
    public $manufacturer_id = 0;
    public $start_date  = '';
    public $end_date    = '';
    public $qty         = 0;
    public $status      = 0;
    public $note        = '';
    public $ordered     = 0;
    public $visited     = 0;
    public $images      = FALSE;
    public $options     = FALSE;
    public $short_description = '';
    public $description = '';
    public $keywords    = '';
    public $tags        = '';
    public $page_title  = '';
    public $meta_keywords = '';
    public $meta_description = '';
    
    function __construct($item=FALSE)
    {
        if($item == FALSE) return;
        
        foreach($item as $key => $value){
            $this->{$key} = $value;
        }
    } 
    
    function validate()
    {
        //check status is enabled
        if($this->status == 0) 
        {
            return FALSE;
        }
          
        //check valid duration
        $now = date('Y-m-d', time());
        if($this->start_date != '' && $this->start_date > $now)
        {
            return FALSE;    
        }
        if($this->end_date != '' && $this->end_date < $now)
        {
            return FALSE;    
        }
              
        return TRUE;
    }
    
    function validateOptions($input_options)
    {
        if(empty($input_options))
        {
            $input_options = array();
        }
        
        foreach($this->options as $opt_id => $opt)
        {
            //check required options 
            if($opt->required == 1 && !key_exists($opt_id, $input_options))
            {
                return FALSE;
            }            
        } 
        
        //check all optins from product page are valid
        foreach($input_options as $input_opt_id => $input_opt_value)
        {
            if(!$this->validateOption($input_opt_id, $input_opt_value))
            {
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
        
    function validateOption($id, $value)
    {
        if(!key_exists($id, $this->options))
        {
            return FALSE;
        }
        
        $opt = $this->options[$id];
        $opt_values = explode(',', $opt->values);
        if(!in_array($value, $opt_values))
        {
            return FALSE;
        }    
        
        return TRUE;
    }
    
    function validateQty($required_qty)
    {
        if($required_qty > $this->qty)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    function hasOptions()
    {
        if(!empty($this->options) && count($this->options) > 0)
        {
            return TRUE;
        }
        
        return FALSE;
    }
    
    function hasMissingOptions($item_options)
    {
        if(empty($item_options) && empty($this->options)) 
        {
            return FALSE;
        }
        
        $item_options = (array)$item_options;        
        $options = $this->options;
        foreach($options as $opt)
        {
            if($opt->required == 1 && !key_exists($opt->option_id, $item_options))
            {
                return TRUE;
            }
        }
        
        return FALSE;
    }
    
    //get methods
    function getPrice()
    {
        return $this->special_price;
    }
    
    function getUrl()
    {
        return get_clean_url($this->product_id, 'product');
    }
    
    function getCartUrl()
    {
        return site_url('/cart/add/product/'.$this->product_id);   
    }
    
    function getImageUrl()
    {
        return media_file($this->image);
    }    
    
    function getThumbnailUrl()
    {
        return media_file($this->thumbnail);
    }    
    
    function getShortDescription()
    {
        return $this->short_description;
    }
    
    function getDescription()
    {
        return $this->description;
    }
    
    function getMinimalQty()
    {
        return 1;
    }
    
    function getKey()
    {
        if($this->hasOptions())
        {
            return md5($this->product_id . $this->sku . json_encode($this->options));
        }
        else
        {
            return md5($this->product_id . $this->sku);
        }
    }
    
    function getSalesPercent()
    {
        $percent = $this->special_price*10 / $this->price;
        
        $precission = 0;
        if(abs($percent - round($percent)) > 0)
        {
            $precission = 1;
        }
        
        return round($percent, $precission);
    }
    
    function getSavedPrice()
    {
        return $this->price - $this->special_price;
    }
}
// END Product Class

/* End of file Cart.php */
/* Location: ./system/libraries/Product.php */