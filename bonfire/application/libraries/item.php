<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//TODO
class Item {
    public $item_id = 0;
    public $cart_id = 0;
    public $product_id = 0;
    public $name    = '';
    public $price   = 0;
    public $qty     = 0;
    public $sku     = '';
    public $options = FALSE;
    public $product = FALSE;
    
    public $errors = array();
    
    function __construct($item=FALSE)
    {
        if($item == FALSE) return;
        
        foreach($item as $key => $value){
            $this->{$key} = $value;
        }

        $item = (array)$item;
        if(isset($item['product_name']))
        {
            $this->name = $item['product_name'];
        }
    }
    
    function getUrl() 
    {
        return $this->product->getUrl();
    }
    
    function getThumbnail()
    {
        return $this->product->thumbnail;
    }
    
    function getDeleteUrl() 
    {
        $suffix = '/';
        if ($this->hasOptions())
        {
            $newkey = md5($this->product_id . $this->sku . json_encode($this->options));
        }
        else
        {
            $newkey = md5($this->product_id . $this->sku);
        }  
        $suffix .= $newkey;
        
        return site_url('/cart/delete'.$suffix);
    }
    
    function getOptionHtml()
    {
        if(!$this->hasOptions()) return '';
        
        $html = '';
        $html .= "( ";
        
        $options = $this->options;
        $i = 0;
        foreach($options as $k => $v) 
        {
            $pf = '';
            if($i != 0) $pf = ',';
            $html .= $pf.$this->product->options[$k]->name . ":" . $v . " "; 
        }
        $html .= ")";
        
        return $html;
    }
    
    function getErrorHtml($prefix='<p>', $suffix='</p>')
    {
        if(!$this->hasErrors()) return '';
        
        $html = '';
        foreach($item->errors as $error)
        {
            $html .= "$prefix$error$suffix";
        }
        return $html;
    }
    
    function hasOptions()
    {
        if(!empty($this->options) && count($this->options) > 0)
        {
            return TRUE;
        }
        return FALSE;
    }
    
    function hasErrors()
    {
        return count($this->errors)>0?TRUE:FALSE;
    } 
}
// END Product Class

/* End of file Cart.php */
/* Location: ./system/libraries/Product.php */