<div class="content_wrap"><div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
        <div class="content"><!-- Content -->
            <?php echo form_open($product->getCartUrl(), 'class="product-form"'); ?>
            <input type="hidden" name="product_id" value="<?php echo $product->product_id; ?>"/>
            <div class="top_product main_bordercolor"><!-- main product -->
                <h1 class="main_color">今日团购推荐</h1>
                <p class="p_desc"><?php echo $product->getShortDescription(); ?></p>
                
                <div class="detail"><!-- Product Detail -->
                    <div class="price main_bgcolor">
                        <span><?php echo format_price($product->special_price); ?></span><!-- Price -->
                        <!--<a class="view_btn" href="<?php echo $product->getUrl(); ?>"></a>-->
                        <input type="submit" class="view_btn" value="Buy" />
                    </div>
                    
                    <div class="subdetail main_bordercolor">
                        <?php if($product->hasOptions()) : ?>
                        <div class="p_option main_bordercolor">
                            <?php        
                            $options = $product->options;
                            foreach($options as $o) {
                                echo "<div>";
                                $option_values = explode(",", $o->values);
                                
                                $i = 0;
                                
                                if($o->required == 0)
                                    echo $o->name . " :  ";
                                else
                                    echo $o->name . "* :  ";
                                foreach($option_values as $v) {
                                    $i++;
                                    $v = trim($v);
                            ?>
                                <!--&nbsp;<input type="radio" id="options_<?php echo $product->product_id.$o->option_id.'_'.$i?>" name="options[<?php echo $o->option_id; ?>]" class="" value="<?php echo $v; ?>" <?php echo $o->required=="1"?"required":""; ?> ><label for="options_<?php echo $product->product_id.$o->option_id.'_'.$i?>"> <?php echo $v; ?> </label>-->
                                
                                &nbsp;<a href="#this" class="opt_<?php echo $o->option_id; ?>" onclick="onSelectOption('<?php echo $o->option_id; ?>', '<?php echo $v; ?>', this)"><?php echo $v; ?></a>&nbsp;
                            <?php 
                                }
                                if($o->required == 0)
                                    echo "<input type='hidden' id='options_" . $o->option_id . "' name='options[" . $o->option_id . "]' value='' class='unrequired' />";
                                else
                                    echo "<input type='hidden' id='options_" . $o->option_id . "' name='options[" . $o->option_id . "]' value='' class='required'/>";
                                echo "</div>";
                            } 
                             ?>
                        </div>
                        <?php endif; ?>
                        
                        <div class="p_price main_bordercolor">
                            <div><?php echo lang('ed_market_price'); ?><br><span class="yuan_price"><?php echo format_price($product->price); ?></span></div>
                            <div><?php echo lang('ed_discount'); ?><br><span class="main_color"><?php echo format_sales_percent($product->getSalesPercent()); ?></span></div>
                            <div><?php echo lang('ed_saved'); ?><br><span class="main_color"><?php echo format_price($product->getSavedPrice()); ?></span></div>
                            <div class="clear"></div>
                        </div>
                        <div class="p_time main_bordercolor"><?php echo lang('ed_time_remaining'); ?>
                            <div>
                                <span id="p_hour">1251</span><?php echo lang('ed_hour'); ?> <span id="p_min">31</span><?php echo lang('ed_minute'); ?> <span id="p_sec">52</span><?php echo lang('ed_second'); ?>
                            </div>
                        </div>
                        <div class="p_goucount"><?php echo sprintf(lang('products_soldout'), $product->ordered); ?></div>
                        <div class="p_goutext"><span><?php echo lang('product_limited'); ?></span>&nbsp;下手要快哦</div>
                    </div>
                    
                    <div class="sub_desc"><?php echo $product->getDescription(); ?></div>
                </div>
                
                <div class="p_thumb"><div class="p_thumb_wrap">
                    <a class="fancybox" href="<?php echo media_file($product->image, 'product'); ?>" data-fancybox-group="gallery" title=""><img id="main_product_image" src="<?php echo media_file($product->image, 'product', 500); ?>"/></a>
                    <div id="product_gallery">
                    <?php if( is_array($product_images) && count($product_images)>0 ) { ?>
                        <?php foreach( $product_images as $imgs ) { ?>
                        <a href="#this" data-image="<?php echo media_file($imgs->image, 'product', 500); ?>" zoom-image="<?php echo media_file($imgs->image, 'product'); ?>">
                            <img src="<?php echo media_file($imgs->image, 'product', 150); ?>" />
                        </a>
                    <?php } } ?>
                    </div>
                </div></div>
                
                <div class="clear"></div>
            </div><!-- main product -->
            
            <div class="display:none">
            <?php if( is_array($product_images) && count($product_images)>0 ) { ?>
                <?php foreach( $product_images as $imgs ) { ?>
                <img src="<?php echo media_file($imgs->image, 'product', 500); ?>" style="display:none" />
            <?php } } ?>
            </div>
            
            <?php if( is_array($related_products) && count($related_products)>0 ) { ?>
            <div class="related_products main_bordercolor"><!-- Related Products -->
                <h4 class="main_bordercolor"><?php echo lang('ed_related_products'); ?></h4>
                <ul id="grid" class="products grid effect-2">
                    <?php foreach( $related_products as $pro ) { ?>
                    <li class="item">
                        <div class="item_wrap">
                            <div class="product_title"><a href="<?php echo $pro->getUrl(); ?>"><?php echo $pro->name; ?></a></div>
                            <div class="product_image"><a href="<?php echo $pro->getUrl(); ?>"><img src="<?php echo media_file($pro->image, 'product', 400); ?>" /></a></div>
                            <div class="product_detail">
                                <div class="p1_price">
                                    <span><?php echo format_price($pro->special_price); ?></span>
                                </div>
                                <div class="p2_price">
                                    <a href="<?php echo $pro->getUrl(); ?>" class="view_btn"><?php echo lang('ed_view'); ?></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div><!-- Related Products -->
            <?php } ?>
            
            <div class="more_detail main_bordercolor">
                <div class="more_header main_bordercolor">
                    <ul>
                        <li class="main_bordercolor"><a href="#spec1"><?php echo lang('ed_specs'); ?></a></li>
                        <li class="main_bordercolor"><a href="#spec3"><?php echo lang('ed_product_shots'); ?></a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="sub_more" id="spec1">
                    <h3 class="main_color main_bordercolor"><?php echo lang('ed_specs'); ?></h3>
                    <table width="100%" cellpadding="0" cellspacing="0">
                    <colgroup>
                        <col width="20%" />
                        <col width="" />
                    </colgroup>
                    <tr>
                        <td align="center" class="main_bordercolor"><strong class="main_color">【<?php echo lang('ed_important'); ?>】</strong></td>
                        <td align="left" class="main_bordercolor">本网站为易天团购系统演示网站，提供的所有商品(包括：依莎15D亮丝时尚靓丽连裤袜
                            （酒红色）)都用到同一个商品介绍，仅供参看版面，此版块内容后台可以自我编辑。
                            易天团购系统又名EDayShop团购系统是由易天科技参考聚美优品设计制作的优质团购网
                            站源码,真诚帮助大家实现自己的团购平台梦想。</td>
                    </tr>
                    </table>
                </div>
                
                <div class="sub_more" id="spec3">
                    <h3 class="main_color main_bordercolor"><?php echo lang('ed_product_shots'); ?></h3>
                    <?php if( is_array($product_photos) && count($product_photos)>0 ) { ?>
                    <?php foreach( $product_photos as $photo ) { ?>
                    <img src="<?php echo media_file($photo->image, 'product'); ?>" />
                    <?php } } ?>
                </div>
            </div>
            
            <div class="related_products main_bordercolor spec_border"><!-- Related Products -->
                <h4 class="main_bordercolor"><?php echo lang('ed_comments'); ?></h4>
                <div>
                    <a href="#this">共有0条口碑评论，点击查看</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#this">我要评论</a>
                </div>
            </div>
            
            <div class="related_products main_bordercolor spec_border"><!-- Related Products -->
                <h4 class="main_bordercolor">口碑评价</h4>
                <div>
                    <a href="#this">共有0条收货评价，点击查看</a>
                </div>
            </div>
            
            <?php echo form_close(); ?>
            
        </div><!-- Content -->