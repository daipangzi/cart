<div class="content_wrap">
    <div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
        <div class="category"><!-- Content -->
            <h2 class="category_title"><?php echo $category->name; ?></h2>
            <?php if( is_array($products) && count($products)>0 ) { ?>
            <ul id="grid" class="products grid effect-1"><!-- PRODUCT LIST -->
            <?php foreach( $products as $pro ) { ?>
                <li class="item">
                    <div class="item_wrap">
                        <div class="product_image"><a href="<?php echo $pro->getUrl(); ?>"><img src="<?php echo media_file($pro->image, 'product', 400); ?>" /></a></div>
                        <div class="product_title"><a href="<?php echo $pro->getUrl(); ?>"><?php echo $pro->name; ?></a></div>
                        <div class="product_detail">
                            <div class="p1_price">
                                <span><?php echo format_price($pro->special_price); ?></span>
                                <a href="<?php echo $pro->getUrl(); ?>" class="view_btn"><?php echo lang('ed_view'); ?></a>
                            </div>
                            <div class="p2_price">
                                <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><?php echo lang('ed_market_price'); ?></td>
                                    <td><?php echo lang('ed_discount'); ?></td>
                                    <td><?php echo lang('ed_saved'); ?></td>
                                </tr>
                                <tr>
                                    <td class="origin"><?php echo format_price($pro->price); ?></td>
                                    <td><?php echo format_sales_percent($pro->getSalesPercent()); ?></td>
                                    <td><strong><?php echo format_price($pro->getSavedPrice()); ?></strong></td>
                                </tr>
                                </table>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="product_goucount">
                            <?php echo sprintf(lang('products_soldout'), $pro->ordered); ?>
                        </div>
                    </div>
                </li>
             <?php } ?>
            </ul>
            <?php } else { ?>
            <div><br />&nbsp;&nbsp;<?php echo lang('no_products_in_selection'); ?></div>
            <?php } ?>
        </div><!-- Content -->
        
        