<div class="container-fluid">

	<div class="errorContainer">
		<div class="page-header">
			<h1 class="center">404 <small>error</small></h1>
		</div>

		<h2 class="center"><?php echo lang('page_not_found'); ?></h2>

		<div class="center">
			<a href="javascript: history.go(-1)" class="btn" style="margin-right:10px;"><span class="icon16 icomoon-icon-arrow-left-10"></span>Go back</a>
			<a href="<?php echo site_url(SITE_AREA); ?>" class="btn"><span class="icon16 icomoon-icon-screen"></span>Dashboard</a>
		</div>

	</div>

</div><!-- End .container-fluid -->