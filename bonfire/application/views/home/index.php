<div class="content_wrap">
    <div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
        <div class="banner"><!-- Banner Area -->
            <div class="slide">
                <ul class="bxslider">
                  <li><img src="<?php echo Template::theme_url('images/demo/banner1.jpg') ?>" /></li>
                  <li><img src="<?php echo Template::theme_url('images/demo/banner2.jpg') ?>" /></li>
                  <li><img src="<?php echo Template::theme_url('images/demo/banner1.jpg') ?>" /></li>
                  <li><img src="<?php echo Template::theme_url('images/demo/banner2.jpg') ?>" /></li>
                </ul>
            </div>
            <div class="top_contact">
                <div class="in_cont main_bordercolor">
                    <img src="<?php echo Template::theme_url('images/demo/advertise1.jpg') ?>" id="special_adv"/>
                    <div class="contact_detail">
                        <p><?php echo lang('service_online'); ?>:&nbsp;&nbsp;<?php echo lang('every_day'); ?>09:00-22:00</p>
                        <div class="ph_detail main_color"><?php echo settings_item('site.system_phone'); ?><br /><span><?php echo lang('service_online_duration'); ?></span></div>
                        <div class="xian main_color main_bordercolor"><?php echo lang('ed_qq') . lang('online_service'); ?><span><img src="<?php echo Template::theme_url('images/qqicon.png') ?>" /></span></div>
                    </div>
                </div>
            </div>
            
            <div class="clear"></div>
        </div><!-- Banner Area -->
        
        <div class="content"><!-- Content -->
            <div class="top_product main_bordercolor"><!-- top product -->
                <h5 class="main_bgcolor">今日特卖推荐</h5>
                <p class="p_desc"><a href="<?php echo $top_product->getUrl(); ?>"><?php echo $top_product->name; ?></a></p>
                
                <div class="detail"><!-- Product Detail -->
                    <div class="price main_bgcolor">
                        <span><?php echo format_price($top_product->special_price); ?></span><!-- Price -->
                        <a class="view_btn" href="<?php echo $top_product->getUrl(); ?>"><?php echo lang('ed_view'); ?></a>
                    </div>
                    
                    <div class="subdetail main_bordercolor">
                        <div class="p_price main_bordercolor">
                            <div><?php echo lang('ed_market_price'); ?><br><span class="yuan_price"><?php echo format_price($top_product->price); ?></span></div>
                            <div><?php echo lang('ed_discount'); ?><br><span class="main_color"><?php echo format_sales_percent($top_product->getSalesPercent()); ?></span></div>
                            <div><?php echo lang('ed_saved'); ?><br><span class="main_color"><?php echo format_price($top_product->getSavedPrice()); ?></span></div>
                            <div class="clear"></div>
                        </div>
                        <div class="p_time main_bordercolor"><?php echo lang('ed_time_remaining'); ?>
                            <div>
                                <span id="p_hour">1251</span><?php echo lang('ed_hour'); ?> <span id="p_min">31</span><?php echo lang('ed_minute'); ?> <span id="p_sec">52</span><?php echo lang('ed_second'); ?>
                            </div>
                        </div>
                        <div class="p_goucount"><?php echo sprintf(lang('products_soldout'), $top_product->ordered); ?></div>
                        <div class="p_goutext"><span><?php echo lang('product_limited'); ?></span>&nbsp;下手要快哦</div>
                    </div>
                    
                    <div class="sub_desc"><?php echo $top_product->description; ?></div>
                </div>
                
                <div class="p_thumb"><div class="p_thumb_wrap">
                    <a href="<?php echo $top_product->getUrl(); ?>" title="<?php echo $top_product->name; ?>"><img src="<?php echo media_file($top_product->image, 'product', 500); ?>" /></a>
                </div></div>
                <div class="clear"></div>
            </div><!-- top product -->
            
            <ul id="grid" class="products grid effect-1"><!-- PRODUCT LIST -->
            <?php foreach( $products as $pro ) { ?>
                <li class="item">
                    <div class="item_wrap">
                        <div class="product_image"><a href="<?php echo $pro->getUrl(); ?>" title="<?php echo $pro->name; ?>"><img src="<?php echo media_file($pro->image, 'product', 400); ?>" /></a></div>
                        <div class="product_title"><a href="<?php echo $pro->getUrl(); ?>"><?php echo $pro->name; ?></a></div>
                        <div class="product_detail">
                            <div class="p1_price">
                                <span><?php echo format_price($pro->special_price); ?></span>
                                <a href="<?php echo $pro->getUrl(); ?>" class="view_btn"><?php echo lang('ed_view'); ?></a>
                            </div>
                            <div class="p2_price">
                                <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><?php echo lang('ed_market_price'); ?></td>
                                    <td><?php echo lang('ed_discount'); ?></td>
                                    <td><?php echo lang('ed_saved'); ?></td>
                                </tr>
                                <tr>
                                    <td class="origin"><?php echo format_price($pro->price); ?></td>
                                    <td><?php echo format_sales_percent($pro->getSalesPercent()); ?></td>
                                    <td><strong><?php echo format_price($pro->getSavedPrice()); ?></strong></td>
                                </tr>
                                </table>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="product_goucount">
                            <?php echo sprintf(lang('products_soldout'), $pro->ordered); ?>
                        </div>
                    </div>
                </li>
             <?php } ?>
            </ul>
            
            <div class="geng_wrap"><a class="gengduo main_color" href="#this"><?php echo lang('ed_more'); ?></a></div>
        </div><!-- Content -->
        
        