<?php
    $settings = (array)Settings_lib::find_all();
    
    $store_url  = base_url();
    $site_name  = $settings['site.title'];
    $logo_url   = $settings['site.email_logo_url'];
    $support_email = $settings['site.system_email'];
    $support_phone = $settings['site.system_phone'];
?>

<body style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <!-- [ header starts here] -->
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">
            <tr>
                <td valign="top"><a href="<?php echo $store_url; ?>"><img src="<?php echo $logo_url; ?>" alt="<?php echo $site_name; ?>" style="margin-bottom:10px;" border="0"/></a></td
            </tr>
            <!-- [ middle starts here] -->
            <tr>
                <td valign="top">
                    <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;"><?php echo sprintf(lang('email_header'), $order->customer_name); ?></h1>
                    <p style="font-size:12px; line-height:16px; margin:0 0 10px 0;">
                        <?php echo sprintf(lang('email_ord_status'), $order->order_no, lang($new_status)); ?>
                    </p>
                    
                    <?php if($order->customer_id != '') :?>
                    <p style="font-size:12px; line-height:16px; margin:0 0 10px 0;"><?php echo sprintf(lang('email_ord_you_can_check'), $store_url); ?></p>
                    <?php endif; ?>
                    
                    <p style="font-size:12px; line-height:16px; margin:0 0 10px 0;"><?php echo $msg; ?></p>
                    <p style="font-size:12px; line-height:16px; margin:0;">
                        <?php echo sprintf(lang('email_ord_if_you_question'), $support_email, $support_email, $support_phone); ?>
                    </p>
                </td>
            </tr>
            <tr>
                <td bgcolor="#EAEAEA" align="center" style="background:#EAEAEA; text-align:center;"><center><p style="font-size:12px; margin:0;"><?php echo sprintf(lang('email_footer'), $site_name); ?></p></center></td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>