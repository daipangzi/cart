<?php
$ship_address = $order->ship_address;
$items = $order->order_items;
?>

<div class="content_wrap"><div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
    <div class="cartpage successpage"><!-- Content -->
        <div class="cartheader main_bordercolor">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>1. <?php echo lang('ed_my_cart'); ?></td>
                <td><img src="<?php echo Template::theme_url("images/narrow.png"); ?>" width="28" height="21"/></td>
                <td>2. <?php echo lang('ed_checkout'); ?></td>
                <td><img src="<?php echo Template::theme_url("images/narrow.png"); ?>" width="28" height="21"/></td>
                <td class="selected">3. <?php echo lang('ed_complete_order'); ?></td>
            </tr>
            </table>
        </div>
        
        <?php if( Template::message() != '' ) { ?>
        <div class="system_message">
            <?php echo Template::message(); ?>
        </div>
        <?php } ?>
        
        <div class="checkout_wrap main_bordercolor">
            <div class="checkout_content">
                <h2 class="main_color"><?php echo lang('ed_order_thanks'); ?></h2>
                <p class="order_number"><span class="main_color"><?php echo lang('ed_own_order_no'); ?> : </span><?php echo $order->order_no; ?></p>
            </div>
            
            <div class="checkout_head main_bordercolor"><?php echo lang('ed_order_info'); ?></div>
            <div class="checkout_content">                
                <div class="success_info sub_bordercolor">
                    <h4><?php echo lang('ed_shipping_address'); ?></h4>
                    <p><?php echo "{$ship_address->province_name} {$ship_address->city_name} {$ship_address->address} {$ship_address->email} {$ship_address->phone}"; ?></p>
                </div><br />
                
                <div class="success_info sub_bordercolor">
                    <h4><?php echo lang('ed_shipping_method'); ?></h4>
                    <p><?php echo $order->shipping_method; ?>&nbsp;&nbsp;<span class="main_color"><?php echo format_price($order->shipping); ?></span></p>
                </div><br />
                
                <div class="success_info sub_bordercolor">
                    <h4><?php echo lang('ed_payment_method'); ?></h4>
                    <p><?php echo lang($order->payment_method); ?></p>
                </div><br />
                
                <div class="success_info sub_bordercolor">
                    <h4><?php echo lang('ed_order_items'); ?></h4>
                    <table border="0" cellpadding="0" cellspacing="0" class="checkout_pros">
                    <colgroup>
                        <col width="56%" />
                        <col width="15%" />
                        <col width="10%" />
                        <col width="" />
                    </colgroup>
                    <thead>
                    <tr>
                        <td align="left"><?php echo lang('ed_name'); ?></td>
                        <td align="right"><?php echo lang('ed_price'); ?></td>
                        <td align="right"><?php echo lang('ed_qty'); ?></td>
                        <td align="right"><?php echo lang('ed_subtotal'); ?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($items as $item) : $item = new Item($item); ?>
                    <tr>
                        <td align="left">
                            <a target="_blank" href="#this"><?php echo $item->name; ?></a>
                        </td>
                        <td align="right"><?php echo format_price($item->price); ?></td>
                        <td align="right"><?php echo $item->qty; ?></td>
                        <td align="right"><?php echo format_price($item->price*$item->qty); ?></td>
                    </tr>                  
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" align="right">
                                <strong><?php echo lang('ed_subtotal'); ?>:</strong>&nbsp;
                                <span><?php echo format_price($order->subtotal);?></span>
                            </td>
                        </tr>
                        <?php if(!empty($order->coupon_code)) : ?>
                        <tr>
                            <td colspan="4" align="right">
                                <strong><?php echo sprintf(lang('counpon_summary'), $order->coupon_code); ?>:</strong>&nbsp;
                                <span><?php echo format_price($order->coupon_discount); ?></span>
                            </td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                            <td colspan="4" align="right">
                                <strong><?php echo lang('ed_shipping'); ?>:</strong>&nbsp;
                                <?php echo format_price($order->shipping); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right"><strong><?php echo lang('ed_total'); ?>:</strong>&nbsp;<span class="main_color"><?php echo format_price($order->total);?></span></td>
                        </tr>
                    </tfoot>
                    </table>
                </div><br />
                
                <div class="success_info sub_bordercolor">
                    <h4><?php echo lang('ed_order_note'); ?></h4>
                    <p><?php echo $order->note; ?></p>
                </div><br />
            </div>
            
        </div>
    </div><!-- Content -->