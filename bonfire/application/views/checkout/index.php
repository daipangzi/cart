<div class="content_wrap"><div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
    <div class="cartpage"><!-- Content -->
        <div class="cartheader main_bordercolor">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>1. <?php echo lang('ed_my_cart'); ?></td>
                <td><img src="<?php echo Template::theme_url("images/narrow.png"); ?>" width="28" height="21"/></td>
                <td class="selected">2. <?php echo lang('ed_checkout'); ?></td>
                <td><img src="<?php echo Template::theme_url("images/narrow.png"); ?>" width="28" height="21"/></td>
                <td>3. <?php echo lang('ed_complete_order'); ?></td>
            </tr>
            </table>
        </div>
        
        <?php if( Template::message() != '' ) { ?>
        <div class="system_message">
            <?php echo Template::message(); ?>
        </div>
        <?php } ?>
        
        <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
        <div class="checkout_wrap main_bordercolor">
            <!-- 1. Address -->
            <div class="checkout_head main_bordercolor"><?php echo lang('checkout_receiver_form_title'); ?></div>
            <div class="checkout_content">
                <table border="0" cellpadding="0" cellspacing="0" class="checkout_address">
                <colgroup>
                    <col width="20%" />
                    <col width="3%" />
                    <col width="" />
                </colgroup>
                <tr>
                    <td align="right"><label for="name"><?php echo lang('ed_name'); ?></label><span><?php echo lang('label_required'); ?></span></td>
                    <td>&nbsp;</td>
                    <td><input type="text" id="name" name="customer[name]" size="30" value="<?php echo set_value('name', isset($customer) ? $customer->name : '') ?>" class="sub_bordercolor" />
                        <?php if( form_error('customer[name]') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('customer[name]'); ?>
                        </div>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td align="right"><label for="province"><?php echo lang('ed_province_city'); ?></label></td>
                    <td>&nbsp;</td>
                    <td>
                        <select id="province" name="customer[province]" class="sub_bordercolor">
                            <?php foreach($provinces as $code => $name) { ?>
                                <option value="<?=$code?>" <?php echo set_select('province', $code, isset($customer_address->province) && $customer_address->province == $code ? TRUE : FALSE); ?>><?=$name;?></option>
                            <?php } ?>
                        </select>&nbsp;&nbsp;
                        <select id="city" name="customer[city]" class="sub_bordercolor">
                            <?php foreach( $cities as $code => $name ) { ?>
                                <option value="<?=$code?>" <?php echo set_select('city', $code, isset($customer_address->city) && $customer_address->city == $code ? TRUE : FALSE) ?>><?=$name?></option>
                            <?php } ?>
                        </select>
                        <?php if( form_error('customer[province]') || form_error('customer[city]') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('customer[province]'); ?>
                            <?php echo form_error('customer[city]'); ?>
                        </div>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td align="right"><label for="address"><?php echo lang('ed_address'); ?><span><?php echo lang('label_required'); ?></span></label></td>
                    <td>&nbsp;</td>
                    <td><input type="text" id="address" name="customer[address]" value="<?php echo set_value('address', isset($customer_address->address) ? $customer_address->address : '') ?>" class="sub_bordercolor" size="60" />
                    <?php if( form_error('customer[address]') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('customer[address]'); ?>
                        </div>
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td align="right"><label for="email"><?php echo lang('ed_email'); ?><span><?php echo lang('label_required'); ?></span></label></td>
                    <td>&nbsp;</td>
                    <td><input type="text" id="email" name="customer[email]" value="<?php echo set_value('email', isset($customer) ? $customer->email : '') ?>" class="sub_bordercolor" size="30" />
                    <?php if( form_error('customer[email]') ) { ?>
                        <div class="alert alert-error fade in">
                            <button class="close" type="button" data-dismiss="alert">×</button>
                            <?php echo form_error('customer[email]'); ?>
                        </div>
                    <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td align="right"><label for="phone"><?php echo lang('ed_phone'); ?></label></td>
                    <td>&nbsp;</td>
                    <td><input type="text" id="phone" value="<?php echo set_value('phone', isset($customer->phone) ? $customer->phone : '') ?>" name="customer[phone]" class="sub_bordercolor" size="30" />
                    </td>
                </tr>
                </table>
            </div><!-- 1. Address -->
            
            <!-- 2. Cart -->
            <div class="checkout_head main_bordercolor"><?php echo lang('checkout_confirm_items_title'); ?></div>
            <div class="checkout_content">
                <table border="0" cellpadding="0" cellspacing="0" class="checkout_pros">
                <colgroup>
                    <col width="56%" />
                    <col width="15%" />
                    <col width="10%" />
                    <col width="" />
                </colgroup>
                <thead>
                <tr>
                    <td align="left"><?php echo lang('checkout_confirm_items_title'); ?></td>
                    <td align="right"><?php echo lang('ed_price'); ?></td>
                    <td align="right"><?php echo lang('ed_qty'); ?></td>
                    <td align="right"><?php echo lang('ed_subtotal'); ?></td>
                </tr>
                </thead>
                <tbody>
                <?php if(!$cart->is_empty()) : $items = $cart->items(); ?>
                <?php 
                foreach($items as $item): $options = $item->options; 
                    $product = $item->product;
                ?>
                <tr>
                    <td align="left">
                        <a target="_blank" href="<?php echo $product->getUrl(); ?>"><?php echo $product->name; ?></a>&nbsp;
                        <?php if($item->hasOptions()) : ?>(
                        <?php 
                            $i = 0;
                            foreach($options as $k => $v) 
                            {
                                if($i!=0) echo ", ";
                                echo $product->options[$k]->name.":".$v; 
                                $i++;
                            } 
                        ?>
                        )
                        <?php endif; ?>
                    </td>
                    <td align="right"><?php echo format_price($item->price); ?></td>
                    <td align="right"><?php echo $item->qty; ?></td>
                    <td align="right"><?php echo format_price($item->subtotal); ?></td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>                    
                </tbody>
                <tfoot>
                <?php 
                $sum = $cart->summary();
                foreach($sum as $k => $v) {
                ?>
                <tr>
                    <td colspan="4" align="right"><strong><?php  echo $k; ?>:</strong>&nbsp;<span><?php echo format_price($v);?></span></td>
                </tr>
                <?php } ?>
                <tr>
                    <td colspan="4" align="right"><strong><?php echo lang('ed_total'); ?>:</strong>&nbsp;<span class="main_color"><?php echo format_price($cart->total());?></span></td>
                </tr>
                </tfoot>
                </table>
            </div><!-- 2. Cart -->
                
            <!-- 3. Shipping Method -->
            <div class="checkout_head main_bordercolor"><?php echo lang('checkout_shipping_title'); ?></div>
            <div class="checkout_content">
                <table border="0" cellpadding="0" cellspacing="0" class="checkout_shipping">
                <colgroup>
                    <col width="15%" />
                    <col width="1%" />
                    <col width="8%" />
                    <col width="1%" />
                    <col width="" />
                </colgroup>
                <?php if( count($paymethods)>0 && count($shipmethods)>0 ) : ?>
                    <?php foreach($shipmethods as $s) : ?>
                    <tr>
                        <td><input type="radio" <?php echo count($shipmethods)==1?'checked="checked"':''; ?> id="ship_<?php echo $s->method_id; ?>" name="order[shipmethod]" value="<?php echo $s->method_id; ?>" />&nbsp;<label for="ship_<?php echo $s->method_id; ?>"><?php echo $s->name; ?></label></td>
                        <td>&nbsp;</td>
                        <td><span class="main_color"><?php echo format_price($s->price); ?></span></td>
                        <td>&nbsp;</td>
                        <td><?php echo $s->description; ?></td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php echo lang('shipmethod_not_available'); ?>
                <?php endif; ?>
                </table>
            </div><!-- 3. Shipping Method -->
            
            <!-- 4. Payment Method -->
            <div class="checkout_head main_bordercolor"><?php echo lang('checkout_payment_title'); ?></div>
            <div class="checkout_content">
                <?php if(count($paymethods) > 0 && !empty($paymethods)) : ?>
                <?php foreach($paymethods as $p) : ?>
                <div class="checkout_payment">
                    <input type="radio" name="order[paymethod]" id="<?php echo $p->code; ?>" value="<?php echo $p->code; ?>" <?php echo count($paymethods)==1?'checked="checked"':''; ?>>
                    <label for="<?php echo $p->code; ?>"><?php echo lang($p->code); ?></label>
                    <?php if($p->description != '') : ?><p><?php echo $p->description; ?></p><?php endif; ?>
                </div>
                <?php endforeach; ?>
                <?php else: ?>
                    <?php echo lang('paymethod_not_available'); ?>
                <?php endif; ?>
            </div><!-- 4. Payment Method -->
            
            <!-- 5. Description -->
            <div class="checkout_head main_bordercolor"><?php echo lang('checkout_note_title'); ?></div>
            <div class="checkout_content">
                <textarea id="note" name="order[note]" class="sub_bordercolor"></textarea>
            </div>
        </div>
        <div class="cart_submit_wrap">
            <input type="submit" id="place_order" name="place_order" class="main_cartbtn" value="<?php echo lang('ed_complete_order'); ?>" />&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <?php echo form_close(); ?>
    </div><!-- Content -->