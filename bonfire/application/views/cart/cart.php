<div class="content_wrap"><div class="wrap"><div class="wrapcon"><!-- Content Wrap-->
    <div class="cartpage"><!-- Content -->
        <div class="cartheader main_bordercolor">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="selected">1. <?php echo lang('ed_my_cart'); ?></td>
                <td><img src="<?php echo Template::theme_url("images/narrow.png"); ?>" width="28" height="21"/></td>
                <td>2. <?php echo lang('ed_checkout'); ?></td>
                <td><img src="<?php echo Template::theme_url("images/narrow.png"); ?>" width="28" height="21"/></td>
                <td>3. <?php echo lang('ed_complete_order'); ?></td>
            </tr>
            </table>
        </div>
        <div class="cart_wrap">
            <div class="carttitle main_color main_bordercolor"><?php echo lang('ed_my_cart'); ?></div>
            <?php if(!$cart->is_empty()) :
                $items = $cart->items();
                echo form_open(get_cart_url('/update')); ?>
            <table width="100%" cellpadding="0" cellspacing="0">
            <colgroup>
                <col width="10%" />
                <col width="30%" />
                <col width="10%" />
                <col width="12%" />
                <col width="" />
                <col width="14%" />
                <col width="10%" />
            </colgroup>
            <thead>
            <tr>
                <td colspan="2"><?php echo lang('ed_product'); ?></td>
                <td><?php echo lang('ed_sku'); ?></td>
                <td><?php echo lang('ed_price'); ?></td>
                <td><?php echo lang('ed_qty'); ?></td>
                <td><?php echo lang('ed_subtotal'); ?></td>
                <td><?php echo lang('ed_remove'); ?></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach($items as $rowid => $item) { ?>
            <tr>
                <td align="center"><a href="<?php echo $item->getUrl(); ?>"><img src="<?php echo media_file($item->getThumbnail(), 'product', 150); ?>" /></a></td>
                <td><a href="<?php echo $item->getUrl(); ?>" class="main_color"><?php echo $item->name; ?></a> 
                    <?php echo $item->getOptionHtml(); ?>
                    <?php echo $item->getErrorHtml(); ?>
                </td>
                <td align="center"><strong><?php echo $item->sku; ?></strong></td>
                <td align="center"><strong><?php echo format_price($item->price); ?></strong></td>
                <td align="center"><div class="qty_wrap">
                    <input type="button" class="minus_btn main_color" value="-" />
                    <input type="text" class="qty_input" id="qty" name="cart[<?php echo $rowid; ?>]" value="<?php echo $item->qty; ?>" />
                    <input type="button" class="plus_btn main_color" value="+" />
                    <div class="clear"></div>
                </div></td>
                <td align="center"><strong><?php echo format_price($item->subtotal); ?></strong></td>
                <td align="center"><a href="<?php echo $item->getDeleteUrl(); ?>" class="cart_delbtn main_color"><?php echo lang('ed_remove'); ?></a></td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="7">
                    <div class="coupon_wrap">
                        <?php echo lang('ed_coupon_code'); ?> :&nbsp; 
                        <input type="text" class="coupontext" name="coupon" />
                        <input type="submit" id="coupon_btn" name="coupon_apply" class="cart_btn" value="<?php echo lang('ed_apply'); ?>"/>
                        <input type="submit" id="update_btn" name="update" class="cart_btn" value="<?php echo lang('ed_update'); ?>"/>
                        <input type="submit" id="clear_btn" name="clear" class="cart_btn" value="<?php echo lang('ed_clear'); ?>"/>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <div class="coupon_wrap">
                         <?php
                        $summary = $cart->summary();
                        foreach($summary as $k => $v) {
                            echo $k . ":  " . format_price($v) . "<br>";
                        }
                        ?>
                        <strong><?php echo lang('ed_total'); ?></strong>: <span class="main_color"><?php echo format_price($cart->total());?></span>
                    </div>
                </td>
            </tr>
            </tbody>
            </table>
            <?php if(!$cart->has_errors()): ?>
            <div class="cart_submit_wrap">
                <a href="<?php echo site_url('checkout');?>" class="main_cartbtn">Checkout</a>
            </div>
            <?php endif; ?>
            
            <?php else: ?>
            <div style="padding:20px"><?php echo lang('cart_has_no_items'); ?></div>
            <?php endif; ?>
            <?php echo form_close(); ?>
        </div>
    </div><!-- Content -->