<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Region_model extends BF_Model {

	protected $table		= "region";
	protected $key			= "region_id";
	protected $soft_deletes	= true;
	protected $date_format	= "datetime";
	protected $set_created	= false;
	protected $set_modified = false;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";
    
    function province_dropdown_list($empty_row=TRUE, $empty_text='')
    {
        $language = 'chinese';//global_language();
        $records = $this
            ->select("region_id, name_{$language} name")
            ->where(array('level' => 0, 'LENGTH(region_id) =' => '2'))
            ->order_by('region_id', 'asc')
            ->find_all();
        
        $result = array();
        if($empty_row)
        {
            $result[''] = $empty_text;
        }
        
        foreach($records as $r)
        {
            $result[$r->region_id] = $r->name;
        }
            
        return $result;
    }
    
    function city_dropdown_list($province='00', $empty_row=TRUE, $empty_text='')
    {
        $language = 'chinese';//global_language();
        $records = $this
            ->select("region_id, name_{$language} name")
            ->where(array('level' => 1, 'LENGTH(region_id) =' => '4', 'region_id LIKE' => $province.'%'))
            ->order_by('region_id', 'asc')
            ->find_all();
                 
        $result = array();
        if($empty_row)
        {
            $result[''] = $empty_text;
        }
        
        if(!empty($records)) 
        {
            foreach($records as $r)
            {
                $result[$r->region_id] = $r->name;
            }
        }
            
        return $result;
    }
}
