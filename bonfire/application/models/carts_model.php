<?php

class Carts_model extends BF_Model {
    
    protected $table        = "carts";
    protected $key          = "cart_id";
    protected $soft_deletes = false;
    protected $date_format  = "datetime";
    protected $set_created  = true;
    protected $set_modified = true;
    
    private $CI;
    function __construct() 
    {
        $this->CI =& get_instance();
    }
    
    public function find_cart($customer_id)
    {
        $cart = $this->find_by('customer_id', $customer_id);
              
        if(!empty($cart)) 
        {
            $cart_id = $cart->cart_id;
                
            $cart->items = $this->get_items($cart_id);
            return $cart;
        }
        
        return FALSE;
    }
    
    public function delete($cart_id)
    {
        $this->delete_items($cart_id);
        
        return parent::delete($cart_id);    
    }
    
    public function save($cart_id, $customer_id, $cart)
    {      
        $data = array();
        $data['cart_id']            = $cart_id;
        $data['subtotal']           = $cart['subtotal'];
        $data['cart_total']         = $cart['cart_total'];
        $data['total_items']        = $cart['total_items'];
        $data['total_qty']          = $cart['total_qty'];
		$data['coupon_code']        = $cart['coupon_code'];
		$data['coupon_discount']    = $cart['coupon_discount'];
        $data['discounted_subtotal']= $cart['discounted_subtotal'];
        $data['customer_id']        = $customer_id;
        $data['remote_ip']          = $this->CI->input->ip_address();
        
        if($this->_is_exist($cart_id))
        {
            $this->update($cart_id, $data);
        }
        else
        {
            $cart_id = $this->insert($data);
        }

        if(is_numeric($cart_id))
        {
            $items = $cart['items'];
            if(count($items) > 0) 
            {
                foreach($items as $key => $item)
                {
                    $data = array();
                    $data['cart_id']    = $cart_id;
                    $data['product_id'] = $item->product_id;
                    $data['price']      = $item->price;
                    $data['qty']        = $item->qty;
                    $data['sku']        = $item->sku;
                    $data['options']    = (isset($item->options) && empty($item->options))?'':json_encode($item->options);
                    
                    if($this->exist_item($key))
                    {
                        if($item->qty == 0)
                        {
                            $this->delete_item($key);
                        }
                        else
                        {
                            $data['modified_on'] = date('Y-m-d H:i:s', time());
                            $this->update_item($key, $data);
                        }
                    }
                    else
                    {
                        $data['created_on'] = date('Y-m-d H:i:s', time());
                        $this->insert_item($data);
                    }
                }
            } 
            else
            {
                $this->delete_items($cart_id);
            } 
        }
            
        return true;
    }
    
    //--------------------------------------------------------------------
    // !ITEM METHODS
    //--------------------------------------------------------------------   
    public function get_items($cart_id)
    {
        $records = $this->db
            ->where(array('cart_id' => $cart_id))
            ->get('carts_items')
            ->result();
           
        if(!$records) return FALSE;
        
        $result = array();
        foreach($records as $record)
        {
            if(!empty($record->options) && count($record->options) > 0)
            {
                $key = md5($record->product_id . $record->sku . $record->options);
            }
            else
            {
                $key = md5($record->product_id . $record->sku);
            }
            $record->options = (array)json_decode($record->options);
            
            $result[$key] = new Item($record);
        }
        
        return $result;
    }
     
    public function delete_items($cart_id)
    {
        $this->db->where('cart_id', $cart_id)->delete('carts_items');
    }
    
    public function delete_item($key)
    {
        $this->db->where('MD5(CONCAT(product_id, sku, options)) = ', $key)->delete('carts_items');
    }
    
    public function update_item($key, $data)
    {
        $this->db->where('MD5(CONCAT(product_id, sku, options)) = ', $key)->update('carts_items', $data);
    }
    
    public function exist_item($key)
    {                                                  
        $exist = $this->db->where('MD5(CONCAT(product_id, sku, options)) = ', $key)->count_all_results('carts_items');
        
        if($exist == 0) return FALSE;
        return TRUE;
    } 
    
    public function insert_item($item)
    {
        $this->db->insert('carts_items', $item);
        return $this->db->insert_id();       
    }
    
    //--------------------------------------------------------------------
    // !COMMON METHODS
    //--------------------------------------------------------------------
    public function get_customer_cart_id($customer_id)
    {
        $cart = $this->find_by('customer_id', $customer_id);
        if(empty($cart)) {
            $data = array();
            $data['customer_id'] = $customer_id;
            
            return $this->insert($data);
        } else {
            return $cart->cart_id;
        }
    }
    
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------
    private function _is_exist($cart_id)
    {
        $exist = $this->count_by('cart_id', $cart_id);
        return (bool)$exist;
    }
}