<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('media_file') )
{
    function media_file($filename, $type='product', $width=0, $height=0)
    {
        $media_path = "./media/";
        
        $target = MEDIAPATH . "/{$type}/{$filename}";
        if($type == "product") {
            $f = substr($filename, 0, 1);
            $s = substr($filename, 1, 1);
            $target = MEDIAPATH . "/{$type}/{$f}/{$s}/{$filename}";
        }
        
        if(!file_exists($target) || trim($filename) == "") 
        {
            if(file_exists("{$media_path}/placeholder/{$width}x{$height}.gif")) return base_url("media/placeholder/{$width}x{$height}.gif");
            else return "http://placehold.it/{$width}x{$height}";
        }
        
        return base_url("images/{$filename}?type={$type}&amp;width={$width}&amp;height={$height}&amp;force=yes");
    }
}

if ( ! function_exists('is_backend'))
{
    function is_backend()
    {       
        return Template::get('is_admin')=='admin'?true:false;
    }
}

if( !function_exists('get_clean_url') ) {
    /**
    * put your comment there...
    * 
    * @param mixed $router_id
    */
    function get_clean_url($entry_id, $type='product') {
        $url = '';
        
        switch($type)
        {
            case 'manufacturer':
            case 'category':
            case 'product':
                if($entry_id != 0)
                {
                    $url = '/catalog/' . $type . '/' . $entry_id;
                }
                else
                {
                    $url = '/catalog/' . $type . '/';
                }
                break;
            default:
                break;
        }
        
        return site_url($url);
    }
}

///////////////////////////////////////////////////////////
// upload functions
/////////////////////////////////////////////////////////// 
if( !function_exists('unique_file_name') )
{
    function unique_file_name($filename)
    {
        list($name, $ext) = explode(".", $filename);
        return $name.time().".".$ext;
    }
}

if( !function_exists('init_upload_config') ) {
    /**
    * init and get upload configurations
    * 
    * @param mixed $type : for category, product, manufacturer, avatar images
    * @param mixed $file_exts : 'image', 
    */
    function init_upload_config($type, $file_type='image') {
        //check dir
        $sub_folder = '';        
        switch($type) {
            case 'avatar':
                $sub_folder = 'avatar';
                break;
            case 'banner':
                $sub_folder = 'banner';
                break;
            case 'manufacturer':
                $sub_folder = 'manufacturer';
                break;
            case 'category':
                $sub_folder = 'category';
                break;
            case 'product':
            default:
                $sub_folder = 'product';
                break;                
        }
        
        if(!file_exists(MEDIAPATH))                     mkdir(MEDIAPATH);
        if(!file_exists(MEDIAPATH."/{$sub_folder}"))    mkdir(MEDIAPATH."/{$sub_folder}");
        if(!file_exists(MEDIAPATH.'/tmp'))              mkdir(MEDIAPATH.'/tmp');
        if(!file_exists(MEDIAPATH."/tmp/{$sub_folder}"))   mkdir(MEDIAPATH."/tmp/{$sub_folder}");
        
        //get upload cofigurations
        $config = array();
        switch($file_type) {
            case 'image':
                $config['allowed_types']= 'gif|jpg|png';
                break;
        }
        
        $config['upload_path']  = MEDIAPATH . '/tmp/' . $sub_folder;
        $config['max_size']     = config_item('upload.max_size');
        $config['max_width']    = config_item('upload.max_width');
        $config['max_height']   = config_item('upload.max_height');
        
        return $config;
    }
}

///////////////////////////////////////////////////////////
// sort functions
/////////////////////////////////////////////////////////// 
if( !function_exists('sort_classes') )
{
    function sort_classes($ordered_field, $orderby, $order)
    {
        if($order != "" && $ordered_field == $orderby)
            return "sorted {$order}";
        else
            return "sortable desc";
    }
}

if( !function_exists('sort_direction') )
{
    function sort_direction($ordered_field, $orderby, $order)
    {
        if($order == "" || $order == "desc" || $ordered_field != $orderby)
            return "asc";
        else
            return "desc";
    }
}

///////////////////////////////////////////////////////////
// format functions
/////////////////////////////////////////////////////////// 
if( !function_exists('format_price') ) {
    function format_price($price) {      
        return Format::format_price($price);
    }
}

if( !function_exists('format_date_time') ) {
    function format_date_time($date) {      
        return Format::format_date_time($date);      
    }
}

if( !function_exists('format_date') ) {
    function format_date($date) {
        return Format::format_date($date);
    }
}

if( !function_exists('format_time') ) {
    function format_time($date) {      
        return Format::format_time($date);        
    }
}

if( !function_exists('format_sales_percent') ) {
    function format_sales_percent($percent) {      
        return Format::format_sales_percent($percent);        
    }
}
    
///////////////////////////////////////////////////////////
// common functions
///////////////////////////////////////////////////////////    
if( !function_exists('array_column') ) {
    function array_column($array , $column_key, $index_key=FALSE) 
    {      
        if(!is_array($array) || empty($array) || empty($column_key)) 
        {
            return FALSE;
        }
        
        $result = array();
        foreach($array as $a) 
        {
            //convert to general array
            $temp = (array)$a; 
            
            $new_item = null;
            if(is_array($column_key)) 
            {
                //copy items to new array item
                $new_item = null;
                if(is_object($a))
                {
                    $new_item = new stdClass();
                }
                else
                {
                    $new_item = array();
                }
                
                foreach($column_key as $key) 
                {
                    if(is_object($a))
                    {
                        $new_item->{$key} = $temp[$key];
                    }
                    else
                    {
                        $new_item[$key] = $temp[$key];
                    }
                }
            }
            else
            {
                if(isset($temp[$column_key]))
                {
                    $new_item = $temp[$column_key];
                }
            }
            
            if($index_key != FALSE && isset($temp[$index_key]))
            {
                $result[$temp[$index_key]] = $new_item;
            }
            else
            {
                $result[] = $new_item;
            }
        }
        return $result;
    }
}

///////////////////////////////////////////////////////////
// cart functions
///////////////////////////////////////////////////////////
if ( ! function_exists('get_cart_url'))
{
    function get_cart_url($uri)
    {
        return site_url('/cart'.$uri);
    }
}

if ( ! function_exists('get_cart_items'))
{
    function get_cart_items()
    {
        $ci =& get_instance();
        return $ci->cart->total_items();
    }
}

if ( ! function_exists('get_cart_qty'))
{
    function get_cart_qty()
    {
        $ci =& get_instance();
        return $ci->cart->total_qty();
    }
}

if ( ! function_exists('generate_order_number'))
{
    function generate_order_number($order_id)
    {
        $base_number = 1000000000;
        $order_no    = $base_number + $order_id;
        return $order_no;    
    }
}

if ( ! function_exists('get_mass_order_actions'))
{
    function get_mass_order_actions()
    {
        $result = array();
        $result['']       = '';
        $result['cancel'] = lang('order_cancel');
        $result['hold']   = lang('order_hold');
        $result['unhold'] = lang('order_unhold');
        
        return $result;
    }
}

if ( ! function_exists('get_order_status'))
{
    function get_order_status()
    {
        $result = array();
        $result['']         = '';
        $result['canceled'] = lang('canceled');
        $result['completed']= lang('completed');
        $result['expired']  = lang('expired');
        $result['holded']   = lang('holded');
        $result['pending']  = lang('pending');
        $result['processing'] = lang('processing');
        $result['refunded'] = lang('refunded');
        
        return $result;
    }
}

///////////////////////////////////////////////////////////
// language functions
///////////////////////////////////////////////////////////
if ( ! function_exists('get_language_id_by_code'))
{
    function get_language_id_by_code($code)
    {
        $ci =& get_instance();
        
        return $ci->languages_model->get_id_by_code($code);
    }
}

if ( ! function_exists('get_currency_id_by_code'))
{
    function get_currency_id_by_code($code)
    {
        $ci =& get_instance();
        
        return $ci->currencies_model->get_id_by_code($code);
    }
}

if ( ! function_exists('global_language'))
{
    function global_language()
    {
        $ci =& get_instance();
        
        return $ci->config->item('language');
    }
}

if ( ! function_exists('global_language_id'))
{
    function global_language_id()
    {
        //return get_language_id_by_code(global_language());
        $ci =& get_instance();
        
        return $ci->config->item('language_id');
    }
}

if ( ! function_exists('filter_language'))
{
    function filter_language($lang)
    {
        $return = strtolower($lang);
        switch($lang)
        {
            case 'english':
            case 'chinese':
            case 'japanese':
            case 'korean':
                $return = $lang;
                break;
            case 'en':
                $return = 'english';
                break;
            case 'cn':
                $return = 'chinese';
                break;
            case 'jp':
                $return = 'japanese';
                break;
            case 'ko':
                $return = 'korean';
                break;
            default:
                $return = 'english';
                break;
        }
        
        return $return;
    }
}