<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('create_banner_slider') )
{
    //TODO
    function create_banner_slider($width=0, $height=0)
    {
        $ci =& get_instance();
        $ci->load->model('banners/banners_model', null, true);
        
        $banners = $ci->banners_model->find_all();
        if(empty($banners)) return;
        
        echo '<div class="slide"><ul class="bxslider">';
        foreach($banners as $banner)
        {
            echo '<li><img src="' . media_file($banner->image, "banner") . '" alt="' . $banner->description . '" /></li>';
        }
        echo '</ul></div>';
    }
}