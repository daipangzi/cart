<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Bonfire
 *
 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications
 *
 * @package   Bonfire
 * @author    Bonfire Dev Team
 * @copyright Copyright (c) 2011 - 2012, Bonfire Dev Team
 * @license   http://guides.cibonfire.com/license.html
 * @link      http://cibonfire.com
 * @since     Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Base Controller
 *
 * This controller provides a controller that your controllers can extend
 * from. This allows any tasks that need to be performed sitewide to be
 * done in one place.
 *
 * Since it extends from MX_Controller, any controller in the system
 * can be used in the HMVC style, using modules::run(). See the docs
 * at: https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/wiki/Home
 * for more detail on the HMVC code used in Bonfire.
 *
 * @package    Bonfire\Core\Controllers
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Base_Controller extends MX_Controller
{
    public $breadcrumb;

	/**
	 * Stores the previously viewed page's complete URL.
	 *
	 * @var string
	 */
	protected $previous_page;

	/**
	 * Stores the page requested. This will sometimes be
	 * different than the previous page if a redirect happened
	 * in the controller.
	 *
	 * @var string
	 */
	protected $requested_page;
    
    //--------------------------------------------------------------------

	/**
	 * Class constructor
	 *
	 */
	public function __construct()
	{
		Events::trigger('before_controller', get_class($this));

		parent::__construct();
        
		// Load Models
        $this->load->model('activities/Activity_model', 'activity_model');
        $this->load->model('languages/languages_model');
        $this->load->model('currencies/currencies_model');
        
        //reset interface language
        if(isset($_POST['interface_language']))
        {
             $lang = filter_language($_POST['interface_language']);
             $this->set_customer_language($lang);
        }
        
        //or from get
        if(isset($_GET['language']))
        {
             $lang = filter_language($_GET['language']);
             $this->set_customer_language($lang);
        }
                 
        // set site language
        if($this->session->userdata('current_language') != '')
        {
            $lang = $this->session->userdata('current_language');
            
            $this->config->set_item('language', $lang);
            $this->config->set_item('language_id', get_language_id_by_code($lang));
        }

		// load the application lang file here so that the users language is known
        $this->lang->load('shop');
        
        //load libraries
        $this->load->library('form_validation');
        $this->load->library('template');
        $this->load->library('assets');
        $this->load->library('format');
        
        //load helpers
        $this->load->helper('form');

		/*
			Performance optimizations for production environments.
		*/
		if (ENVIRONMENT == 'production')
		{
		    $this->db->save_queries = FALSE;

		    $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		}

		// Testing niceties...
		else if (ENVIRONMENT == 'testing')
		{
			$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		}

		// Development niceties...
		else if (ENVIRONMENT == 'development')
		{
			if ($this->settings_lib->item('site.show_front_profiler') AND has_permission('Bonfire.Profiler.View'))
			{
				// Profiler bar?
				if ( ! $this->input->is_cli_request() AND ! $this->input->is_ajax_request())
				{
					$this->load->library('Console');
					$this->output->enable_profiler(TRUE);
				}

			}

			$this->load->driver('cache', array('adapter' => 'dummy'));
		}

		// Auto-migrate our core and/or app to latest version.
		if ($this->config->item('migrate.auto_core') || $this->config->item('migrate.auto_app'))
		{
			$this->load->library('migrations/migrations');
			$this->migrations->auto_latest();
		}

		// Make sure no assets in up as a requested page or a 404 page.
		if ( ! preg_match('/\.(gif|jpg|jpeg|png|css|js|ico|shtml)$/i', $this->uri->uri_string()))
		{
			$this->previous_page = $this->session->userdata('previous_page');
			$this->requested_page = $this->session->userdata('requested_page');
		}

		// Pre-Controller Event
		Events::trigger('after_controller_constructor', get_class($this));
        
        // get languages
        Template::set('bar_languages', $this->languages_model->dropdown_list(false));               
        Template::set('languages', $this->languages_model->find_all(1));               
	}//end __construct()
    
    protected function _go_back_request()
    {
        Template::redirect($this->requested_page);
    }
    
    protected function _go_back()
    {
        Template::redirect($this->previous_page);
    }
    
    protected function _redirect($url)
    {
        header("Location: {$url}");
        exit(); 
    }
    
    protected function set_customer_language($lang) 
    {
        $this->session->set_userdata('current_language', $lang);
    }
    
    protected function get_customer_language() 
    {
        $this->session->userdata('current_language');
    }                            
}//end Base_Controller


//--------------------------------------------------------------------

/**
 * Front Controller
 *
 * This class provides a common place to handle any tasks that need to
 * be done for all public-facing controllers.
 *
 * @package    Bonfire\Core\Controllers
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Front_Controller extends Base_Controller
{        
	//--------------------------------------------------------------------
    /**
     * Stores the current customer's details, if they've logged in.
     *
     * @var object
     */
    protected $current_customer = NULL;

	/**
	 * Class constructor
	 *
	 */
	public function __construct()
	{                 
		parent::__construct();
        
        Events::trigger('before_front_controller');
        
        Template::set('is_admin', 'front');                
        Template::set_theme($this->config->item('default_theme')); 
        
        // Pagination config
        $this->pager = array();
        $this->pager['full_tag_open']    = '<div class="pagination pagination-right"><ul>';
        $this->pager['full_tag_close']    = '</ul></div>';
        $this->pager['next_link']         = '&rarr;';
        $this->pager['prev_link']         = '&larr;';
        $this->pager['next_tag_open']    = '<li>';
        $this->pager['next_tag_close']    = '</li>';
        $this->pager['prev_tag_open']    = '<li>';
        $this->pager['prev_tag_close']    = '</li>';
        $this->pager['first_tag_open']    = '<li>';
        $this->pager['first_tag_close']    = '</li>';
        $this->pager['last_tag_open']    = '<li>';
        $this->pager['last_tag_close']    = '</li>';
        $this->pager['cur_tag_open']    = '<li class="active"><a href="#">';
        $this->pager['cur_tag_close']    = '</a></li>';
        $this->pager['num_tag_open']    = '<li>';
        $this->pager['num_tag_close']    = '</li>';

        //set default limit
        $this->limit = $this->settings_lib->item('site.list_limit');   
        
        //load model
        $this->load->helper('shop_func');
        
        //load model
        $this->load->library('cart');
        
        //load models
        $this->load->model('categories/categories_model', null, true);
        $this->load->model('products/products_model', null, true);
        $this->load->model('manufacturers/manufacturers_model', null, true);
        
        // Customer Auth setup
        $this->load->model('customers/Customer_model', 'customer_model');
        $this->load->library('customers/customer_auth');

        // Load our current logged in customer so we can access it anywhere.
        if ($this->customer_auth->is_logged_in())
        {
            $this->current_customer = $this->customer_model->find($this->customer_auth->customer_id());
            $this->current_customer->customer_id = (int)$this->current_customer->customer_id;
        }        

        // Make the current user available in the views
        $this->load->vars( array('current_customer' => $this->current_customer) );
        Template::set('current_customer', $this->current_customer);
        //end Customer Auth setup
        
        //set interface language
        if($this->session->userdata('current_language') == '')
        {                  
            if(isset($this->current_customer->language))  $lang = $this->current_customer->language;
            else  $lang = settings_item('site.default_language');
            
            $this->config->set_item('language', $lang);
            $this->config->set_item('language_id', get_language_id_by_code($lang));
        }        
        Template::set('current_language', $this->config->item('language'));
        //end interface language
        
        // get categories
        $categories = $this->categories_model->find_all_tierd(1, 2);   
        Template::set('categories', $categories);
        
        //get manufacturers list
        $manufacturers = $this->manufacturers_model->find_all(1);
        Template::set('manufacturers', $manufacturers);
        
        //add js
        Assets::add_js( array(
            'jquery-ui.min.js',
            'codeigniter-csrf.js',
            'modernizr.custom.js',
            'masonry.pkgd.min.js',
            'imagesloaded.js',
            'classie.js',
            'AnimOnScroll.js',
            'main.js'
        ));
        
        //reset interface language
        if(isset($_POST['interface_language']))
        {
             $lang = $_POST['interface_language'];
             $this->set_customer_language($lang);
        }
        
        //or from get
        if(isset($_GET['language']))
        {
             $lang = filter_language($_GET['language']);
             $this->set_customer_language($lang);
        }
        
        Events::trigger('after_front_controller');
	}//end __construct()
    //--------------------------------------------------------------------
    protected function set_customer_language($lang) {
        $lang = filter_language($lang);
        parent::set_customer_language($lang);
        
        if ($this->auth->is_logged_in())
        {
            $this->customer_model->save_customer_language($this->current_customer->customer_id, $lang);
        }        
    }
}//end Front_Controller


//--------------------------------------------------------------------

/**
 * Authenticated Controller
 *
 * Provides a base class for all controllers that must check user login
 * status.
 *
 * @package    Bonfire\Core\Controllers
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Authenticated_Controller extends Base_Controller
{
    /**
     * Stores the current user's details, if they've logged in.
     *
     * @var object
     */
    protected $current_user = NULL;
    
    //--------------------------------------------------------------------

	/**
	 * Class constructor setup login restriction and load various libraries
	 *
	 */
	public function __construct()
	{        
		parent::__construct();

        // Auth setup For Admin Users
        $this->load->model('users/user_model');
        $this->load->library('users/auth');
            
        // Load our current logged in user so we can access it anywhere.
        if ($this->auth->is_logged_in())
        {
            $this->current_user = $this->user_model->find($this->auth->user_id());
            $this->current_user->id = (int)$this->current_user->id;
            //$this->current_user->user_img = gravatar_link($this->current_user->email, 22, $this->current_user->email, "{$this->current_user->email} Profile");
        }

        // Make the current user available in the views
        $this->load->vars( array('current_user' => $this->current_user) );
        
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->CI =& $this;	// Hack to make it work properly with HMVC

		Template::set_theme($this->config->item('default_theme'));

        //set interface language
        if($this->session->userdata('current_language') == '')
        {                  
            if(isset($this->current_user->language))  $lang = $this->current_user->language;
            else  $lang = settings_item('site.default_language');
            
            $this->config->set_item('language', $lang);
            $this->config->set_item('language_id', get_language_id_by_code($lang));
        }        
        Template::set('current_language', $this->config->item('language'));
        
        //reset interface language
        if(isset($_POST['interface_language']))
        {
             $lang = $_POST['interface_language'];
             $this->set_customer_language($lang);
        }
        
        //or from get
        if(isset($_GET['language']))
        {
             $lang = filter_language($_GET['language']);
             $this->set_customer_language($lang);
        }
	}//end construct()

	//--------------------------------------------------------------------
    protected function set_customer_language($lang) {
        $lang = filter_language($lang);
        parent::set_customer_language($lang);
        
        if (isset($this->current_user))
        {
            $this->user_model->save_user_language($this->current_user->id, $lang);
        }        
    }

}//end Authenticated_Controller


//--------------------------------------------------------------------

/**
 * Admin Controller
 *
 * This class provides a base class for all admin-facing controllers.
 * It automatically loads the form, form_validation and pagination
 * helpers/libraries, sets defaults for pagination and sets our
 * Admin Theme.
 *
 * @package    Bonfire
 * @subpackage MY_Controller
 * @category   Controllers
 * @author     Bonfire Dev Team
 * @link       http://guides.cibonfire.com/helpers/file_helpers.html
 *
 */
class Admin_Controller extends Authenticated_Controller
{

	//--------------------------------------------------------------------

	/**
	 * Class constructor - setup paging and keyboard shortcuts as well as
	 * load various libraries
	 *
	 */
	public function __construct()
	{                  
		parent::__construct();
        
        // Make sure we're logged in.
        $this->auth->restrict();
        
        //set admin flag
        Template::set('is_admin', 'admin');        
        
        //load language
        $this->lang->load('application');
        
        //load helpers
		$this->load->helper('application');

        //load librarties
		$this->load->library('ui/contexts');
        $this->load->library('upload'); 

		// Pagination config
		$this->pager = array();
		$this->pager['full_tag_open']	= '<div class="pagination pagination-right"><ul>';
		$this->pager['full_tag_close']	= '</ul></div>';
		$this->pager['next_link'] 		= '&rarr;';
		$this->pager['prev_link'] 		= '&larr;';
		$this->pager['next_tag_open']	= '<li>';
		$this->pager['next_tag_close']	= '</li>';
		$this->pager['prev_tag_open']	= '<li>';
		$this->pager['prev_tag_close']	= '</li>';
		$this->pager['first_tag_open']	= '<li>';
		$this->pager['first_tag_close']	= '</li>';
		$this->pager['last_tag_open']	= '<li>';
		$this->pager['last_tag_close']	= '</li>';
		$this->pager['cur_tag_open']	= '<li class="active"><a href="#">';
		$this->pager['cur_tag_close']	= '</a></li>';
		$this->pager['num_tag_open']	= '<li>';
		$this->pager['num_tag_close']	= '</li>';

		$this->limit = $this->settings_lib->item('site.list_limit');

		// load the keyboard shortcut keys
		$shortcut_data = array(
			'shortcuts' => config_item('ui.current_shortcuts'),
			'shortcut_keys' => $this->settings_lib->find_all_by('module', 'core.ui'),
		);
		Template::set('shortcut_data', $shortcut_data);

		// Profiler Bar?
		if (ENVIRONMENT == 'development')
		{
			if ($this->settings_lib->item('site.show_profiler') AND has_permission('Bonfire.Profiler.View'))
			{
				// Profiler bar?
				if ( ! $this->input->is_cli_request() AND ! $this->input->is_ajax_request())
				{
					$this->load->library('Console');
					$this->output->enable_profiler(TRUE);
				}
			}
		}

		// Basic setup
		Template::set_theme($this->config->item('template.admin_theme'), 'junk');
        
        //add js
        Assets::add_js( array(
            'jquery-ui.min.js',
            'jquery-migrate-1.2.1.min.js',
            'bootstrap.js',
            'jquery.mousewheel.js',
            'codeigniter-csrf.js',
            'libs/jRespond.min.js',
            plugin_path().'misc/qtip/jquery.qtip.min.js',
            plugin_path().'misc/totop/jquery.ui.totop.min.js',
            plugin_path().'forms/uniform/jquery.uniform.min.js',
            plugin_path().'forms/tiny_mce/jquery.tinymce.js',
            'main.js'
        )); 
        
        $this->_init_params();
	}//end construct()
    
    // Remap the 404 error functions
    public function _remap($method, $params = array())
    {                  
        // Check if the requested route exists
        if (method_exists($this, $method))
        {
            // Method exists - so just continue as normal
            return call_user_func_array(array($this, $method), $params);
        }
        else
        {
            Template::set_view('admin/error/error404');
            Template::render('blank');
        }
    }

	//--------------------------------------------------------------------    
    private function _init_params() 
    {
        //save offset
        $offset = $this->input->post('offset')?$this->input->post('offset'):0;
        Template::set('offset', $offset);
        
        //save selected tab
        $selected_tab = $this->input->post('selected_tab');
        Template::set('selected_tab', $selected_tab);
        
        //build order
        $params['order']    = isset($_POST['order'])?$_POST['order']:'';
        $params['orderby']  = isset($_POST['orderby'])?$_POST['orderby']:'';
        Template::set('params', $params);
    }
    
    public function upload_image($field) 
    {
        $result = array();
        if (!$this->upload->do_upload($field)) {
            $str = "";
            $result['errors'] = $this->upload->display_errors('','');
        } else {
            $upload_data        = $this->upload->data();
            $result['image']    = $upload_data['file_name'];
        }
        return $result;
    }
    
    /**
    * return moved file name
    * 
    * @param mixed $file_name
    * @param mixed $type : 'category', 'product', 'manufacturer' ...
    * @return mixed
    */
    public function get_moved_image_name($file_name, $type, $delete_folder=true) 
    {
        if(($file_name == "") || ($file_name == "deleted")) return '';
              
        $temp_sub_folder = '';
        $sub_folder      = '';
        switch($type) {
            case 'avatar':
                $sub_folder = '/avatar/';
                break;
            case 'banner':
                $sub_folder = '/banner/';
                break;
            case 'manufacturer':
                $sub_folder = '/manufacturer/';
                break;
            case 'category':
                $sub_folder = '/category/';
                break;           
            case 'product':
            default:
                $sub_folder = '/product/';
                
                $first_prefix   = substr($file_name, 0, 1);
                $second_prefix  = substr($file_name, 1, 1);
                
                if(!file_exists(MEDIAPATH . $sub_folder . $first_prefix)) mkdir(MEDIAPATH . $sub_folder . $first_prefix);
                if(!file_exists(MEDIAPATH .$sub_folder . $first_prefix . '/' . $second_prefix)) mkdir(MEDIAPATH .$sub_folder . $first_prefix . '/' . $second_prefix);
                
                $temp_sub_folder = $first_prefix . '/' . $second_prefix . '/';
                break;                
        }
        
        $target_path    = MEDIAPATH . $sub_folder . $temp_sub_folder;
        $temp_path      = MEDIAPATH . '/tmp' . $sub_folder;
        
        $new_file_name  = unique_file_name($file_name);
              
        if(!file_exists($temp_path . $file_name)) return FALSE;
           
        if(copy($temp_path . $file_name, $target_path . $new_file_name)) {
            if ( ! function_exists('delete_files')) {
                $this->load->helper('file');
            }
            
            if($delete_folder)
                delete_files($temp_path);
            else 
                delete_files($temp_path.$file_name);
            
            return $new_file_name;
        } else {
            return FALSE;
        }    
                
        return FALSE;        
    }
}//end Admin_Controller

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
