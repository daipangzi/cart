<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['upload.max_size']  = 500;
$config['upload.max_width'] = 1500;
$config['upload.max_height']= 1500;

$config['page_templates'] = array('default', 'right-sidebar', 'left-sidebar');

$config['payment_options'] = array(
    'manual'            => '',
    'paypal_express'    => array(
        array(
            'field'     => 'merchant_email', 
            'lang'      => 'lang:ed_merchant_email',
            'rule'      => 'required|trim|max_length[255]|strip_tags|xss_clean|valid_email'
        ) 
    ),
);