<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Checkout extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
                             
        $this->load->model('payment_methods/payment_methods_model');
        $this->load->model('shipping_methods/shipping_methods_model');
        $this->load->model('orders/orders_model');
        $this->load->model('region_model');
                                  
        $this->load->library('cart');
    }
    
	public function index()
	{
        if($this->input->post('place_order'))
        {
            if($this->_place_order())
            {
                Template::redirect('checkout/success');
            }    
        }
       
        //if cart is empty, then redirect to cart page
        if($this->cart->is_empty() || $this->cart->has_errors())
        {
            $this->_redirect(site_url('cart'));
        }   
        Template::set('cart', $this->cart);
         
        //****************************************//
        //chec customer info from cart at first
        //****************************************//
        Assets::add_module_js('customers','customers.js');
        
        //get customer's address
        $customer_province = '00';
        if(isset($this->current_customer))
        {
            $customer_id = $this->current_customer->customer_id;
            
            $customer_address = $this->customer_model->find_address($customer_id);
            if($customer_address != FALSE)
            {
                $customer_province = $customer_address->province;
            }
            Template::set('customer',  $this->current_customer);
            Template::set('customer_address', $customer_address);
        }
        
        $provinces = $this->region_model->province_dropdown_list(true, lang('ed_select'));
        Template::set('provinces', $provinces);
        
        $cities = $this->region_model->city_dropdown_list($customer_province, true, lang('ed_select'));
        Template::set('cities', $cities);
        
        // get payment methods
        $paymethods = $this->payment_methods_model->find_all();
        Template::set('paymethods', $paymethods);
        
        // get shipping methods
        $shipmethods = $this->shipping_methods_model->find_all();
        Template::set('shipmethods', $shipmethods);
        
		Template::render('no_sidebar');
	}
    
    public function confirm()
    {
        if(!$this->cart->has_prepared_order())
        {
            $this->_redirect(site_url('cart'));
        }    
        
        if(!$this->cart->save_order())
        {
            Template::set_message($this->cart->message, 'error');
            $this->_redirect(site_url('cart'));
        }
         
        $this->_redirect(site_url('checkout/success'));
    }
    
    public function success()
    {
        if(!$this->cart->has_completed_order())
        {
            Template::redirect('/cart');
        }
        
        $order = $this->cart->get_short_order();
        Template::set('order', $order);
        Template::render();
    }
    
    public function cancel()
    {
        Template::set_message(lang('order_payment_canceled'), 'info');
        $this->_redirect(site_url('cart'));        
    }
    
    public function failure()
    {
        //TODO
        exit;
    }
    
    //////////////////////////////////////////////
    // Private Methods
    //////////////////////////////////////////////
    private function _place_order()
    {
        // check if cart is empty
        if($this->cart->is_empty())
        {
            Template::redirect('/cart');
        } 
        
        // validation check
        // customer validation here
        $this->form_validation->set_rules('customer[name]','lang:ed_name','required|trim|min_length[1]|max_length[100]|strip_tags');
        $this->form_validation->set_rules('customer[email]','lang:ed_email','required|trim|valid_email|max_length[120]|xss_clean|strip_tags');
        $this->form_validation->set_rules('customer[phone]','lang:ed_phone','trim|xss_clean|strip_tags');
        $this->form_validation->set_rules('customer[province]','lang:ed_province','callback_province_check');
        $this->form_validation->set_rules('customer[city]','lang:ed_city','callback_city_check');
        $this->form_validation->set_rules('customer[address]','lang:ed_address','required|trim|min_length[2]|max_length[100]|xss_clean|strip_tags');
        
        if ($this->form_validation->run($this) === FALSE)
        {
            return FALSE;
        } 
          
        $customer_info  = $this->input->post('customer');   
        $order_info     = $this->input->post('order');
        
        //build shipping address
        $ship = array();
        $ship['name']    = $customer_info['name'];
        $ship['email']   = $customer_info['email'];            
        $ship['phone']   = $customer_info['phone'];
        $ship['province']= $customer_info['province'];
        $ship['city']    = $customer_info['city'];
        $ship['address'] = $customer_info['address'];
        //$ship['zipcode'] = $customer_info['zipcode'];        
        
        // save customer info to cart
        $customer = array();
		if(isset($this->current_customer->email))
		{
            $cust = $this->current_customer;
			$customer['customer_id']	= $cust->customer_id;            
			$customer['customer_name']  = $cust->name;            
			$customer['customer_email'] = $cust->email; 
            
            //if this customer doesn't have address
            if(!$this->customer_model->has_address($cust->customer_id))
            {
                $this->customer_model->save_address($cust->customer_id, $ship);    
            }
		}
		else
		{
			$customer['customer_name']  = $customer_info['name'];
			$customer['customer_email'] = $customer_info['email'];            
		}
		$customer['ship_address'] = $ship;
		
        $this->cart->save_customer($customer);
        // end
        
        // check shipping method if valid
        $shipmethod_id = isset($order_info['shipmethod'])?$order_info['shipmethod']:'';            
        if(!$this->shipping_methods_model->is_valid($shipmethod_id))
        {
            Template::set_message(lang('shipmethod_invalid'), 'error');
            return FALSE;
        }
        else
        {
            $m = $this->shipping_methods_model->find($shipmethod_id);
            
            $this->cart->set_shipping($m->name, $m->price, $m->method_id);
        }
           
        // check payment method if valid
        $paymethod_code = isset($order_info['paymethod'])?$order_info['paymethod']:'';        
        if(!$this->payment_methods_model->is_valid($paymethod_code))
        {
            Template::set_message(lang('paymethod_invalid'), 'error');
            return FALSE;
        }
        else
        {
            $this->cart->set_payment($paymethod_code);
        }
		
		if(isset($order_info['note']))
		{
			$this->cart->set_additional_detail('note', $order_info['note']);
		}
        
        // place order data 
        return $this->cart->place_order();
    }
    
    function province_check($str)
    {
        if($str == '') 
        {
            $this->form_validation->set_message('province_check', lang('province_required'));
            return FALSE;
        }
        else if(strlen($str) != 2)
        {
            $this->form_validation->set_message('province_check', lang('province_invalid'));
            return FALSE;
        }
        else if(!preg_match( '/^[0-9]+$/', $str))
        {
            $this->form_validation->set_message('province_check', lang('province_invalid'));
            return FALSE;
        }
        
        return TRUE;
    }
    
    function city_check($str)
    {
        if($str == '') 
        {
            $this->form_validation->set_message('city_check', lang('city_required'));
            return FALSE;
        }
        else if(strlen($str) != 4)
        {
            $this->form_validation->set_message('city_check', lang('city_invalid'));
            return FALSE;
        }
        else if(!preg_match( '/^[0-9]+$/', $str))
        {
            $this->form_validation->set_message('city_check', lang('city_invalid'));
            return FALSE;
        }
        
        return TRUE;
    }
}//end class