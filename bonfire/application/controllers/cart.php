<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* Checkout Controller
*/

class Cart extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('cart');
        $this->load->library('item');
    }
    
    public function index()
    {
        $this->cart();
    }
    
    public function cart()
    {
        Assets::add_js('customs/cart.js');
        Template::set('cart', $this->cart);
        Template::set_view('cart/cart');
        Template::render('no_sidebar');
    }
    
    public function add()
    {
        $product_id  = $this->input->post('product_id');  
        if(!$this->products_model->is_exist($product_id))
        {
            $product_id = $rowid = $this->uri->segment(5);
        }
            
        //get product object
        $product = $this->products_model->find($product_id, 2);
        if(empty($product))
        {
            Template::set_message(lang('product_invalid'), 'error');            
        }
        else
        {
            $this->requested_page = $product->getUrl();
        }
                                                                       
        if($product->validate() == FALSE) //if the product_id is not valid
        { 
            Template::set_message(lang('product_invalid_to_cart'), 'error');           
            $this->_go_back(); 
        } 
        else 
        {
            $qty    = is_numeric($this->input->post('qty'))?$this->input->post('qty'):$product->getMinimalQty();
            $options= $this->input->post('options');        
             
            //validate product options
            if(!$product->validateOptions($options)) //missing options    
            { 
                Template::set_message(lang('product_option_invalid'), 'error');
                $this->_go_back(); 
            } 
            else 
            {
                $can_add    = TRUE;
                $key        = $product->getKey();
                $total_qty  = $this->cart->item_qty($key) + $qty;
               
                if($product->validateQty($total_qty))
                {                
                    $item = array();
                    $item['product_id'] = $product_id;
                    $item['qty']        = $qty==0?$product->getMinimalQty():$qty;
                    $item['price']      = $product->getPrice();
                    $item['name']       = $product->name;
                    $item['options']    = $options;
                    $item['sku']        = $product->sku;
                                        
                    //add item to cart
                    if($this->cart->insert($item)) 
                    {
                        Template::set_message(sprintf(lang('product_added_success'), $product->name), 'success');
                    } 
                    else 
                    {
                        Template::set_message(sprintf(lang('product_add_failed'), $product->name), 'error');
                        $this->_go_back(); 
                    }
                }
                else
                {
                    Template::set_message(sprintf(lang('product_qty_invalid'), $product->name), 'error');
                    $this->_go_back(); 
                }
            }
        }
            
        $this->_redirect();
    }
    
    public function clear() 
    {
        $this->cart->destroy();
        $this->_redirect();
    }
    
    public function update() 
    {
        if($this->input->post('clear')) {
            $this->cart->destroy();
        } else if($this->input->post('update')) {
            $items = $this->input->post('cart');
            $this->cart->update_cart($items);
        } else if($this->input->post('coupon_apply')) {
            $code = $this->input->post('coupon');
            $this->cart->update_cart(FALSE, $code);
        }
        
        $this->_redirect();
    }
    
    public function delete()
    {
        $rowid = $this->uri->segment(3);

        if($rowid) $this->cart->update_cart(array($rowid => 0));            
        
        $this->_redirect();
    }
    
    public function coupon()
    {
        $code = $this->input->post('coupon');
        if($code) $this->cart->update_cart(FALSE, $code);
        
        $this->_redirect();
    }
    
    protected function _redirect()
    {
        $url = site_url('cart');
        parent::_redirect($url);
    }
}//end class