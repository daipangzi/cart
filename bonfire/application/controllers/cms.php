<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends Front_Controller
{
	public function index()
	{
	}

    //to render pages
	public function page($id=0)
    {
        Assets::add_js('customs/cms.js');
        
        if( $id == 0 ) {
            // go to 404 page
        }
        
        Template::render('no_popup');
    }
}//end class