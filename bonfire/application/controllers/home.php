<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Front_Controller
{
    //--------------------------------------------------------------------
    // !PAGE METHODS
    //--------------------------------------------------------------------
        
    //to render homepage
    public function index()
    {
        Assets::add_js('jquery.bxslider.min.js');
        Assets::add_js('jquery.easing.1.3.js');
        Assets::add_js('customs/home.js');
        
        // get top product
        $top_product = $this->products_model
            ->order_by_saled()
            ->limit(1)
            ->find_all(2);
        
        // get product list
        $product_list = $this->products_model
            ->order_by_end_date()
            ->exclude_products($top_product[0]->product_id)
            ->find_all(2);
            
        Template::set("top_product", $top_product[0]);
        Template::set("products", $product_list);
        Template::render();
    }//end index()
}//end class