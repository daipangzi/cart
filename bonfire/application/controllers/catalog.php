<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Catalog extends Front_Controller
{
    //to render category pages
    public function category($id)
    {
        Assets::add_js('customs/category.js');
        
        // get products
        $products = $this->products_model
                    ->filter_category($id)
                    ->limit(10)
                    ->find_all(2);
        Template::set("products", $products);
        
        // get random products
        $random_products = $this->products_model
                    ->order_by_rand()
                    ->limit(6)
                    ->find_all(2);
        Template::set("random_products", $random_products);
        
        // get category by id
        $category = $this->categories_model->find($id, 2);
        Template::set("category", $category);
        
        Template::render('index_category');
    }
    
    //to render manufacturer pages
    public function manufacturer($id)
    {
        Assets::add_js('customs/category.js');
        
        //get manufacturer
        $manufacturer = $this->manufacturers_model->find($id, 2);
        Template::set('manufacturer', $manufacturer);
        
        // get products
        $products = $this->products_model
                    ->filter_manufacturer($id)
                    ->limit(10)
                    ->find_all(2);
        Template::set("products", $products);
        
        // get random products
        $random_products = $this->products_model
                    ->order_by_rand()
                    ->limit(6)
                    ->find_all(2);
        Template::set("random_products", $random_products);
        
        Template::render('index_category');
    }
    
    //to render product pages
    public function product($id)
    {
        Assets::add_js('jquery.validate');
        Assets::add_css('jquery.fancybox.css');
        Assets::add_js('jquery.fancybox.js');
        
        // get product        
        $product = $this->products_model->find($id, 2);
        if(!$product)
        {
            //go to 404 page
        }
        Template::set('product', $product);
        
        // get photos for the product
        $product_photos = $this->products_model->get_samples($id);
        Template::set('product_photos', $product_photos);
        
        // get images for the product
        $product_images = $this->products_model->get_images($id);
        Template::set('product_images', $product_images);
                
        // get related products
        $product_categories = $this->products_model->get_categories($id);
        $related_products = $this->products_model
                    ->get_related_products($id, 2);
        Template::set('related_products', $related_products);
        
        if( is_array($related_products) && count($related_products)>0 ) {
            echo "<script>var is_related = 0;</script>"; 
        } else { 
            echo "<script>var is_related = 1;</script>"; 
        }
        Assets::add_js('customs/product.js');
        
        Template::render();
    }

}//end class