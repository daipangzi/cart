<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// no translate
$lang['english'] = 'English';
$lang['chinese'] = 'Chinese';

//Left Menu
$lang['ed_menu_list']       = 'Menus';
$lang['ed_customer_mng']    = 'Customers';
$lang['ed_product_mng']     = 'Products';
$lang['ed_category_mng']    = 'Categories';
$lang['ed_manufacturer_mng']= 'Manufacturers';
$lang['ed_coupon_mng']      = 'Coupons';
$lang['ed_order_mng']       = 'Orders';
$lang['ed_invoice_mng']     = 'Invoices';
$lang['ed_shipment_mng']    = 'Shipments';
$lang['ed_refund_mng']      = 'Refunds';
$lang['ed_shipping_methods']= 'Shipping Methods';
$lang['ed_payment_methods'] = 'Payment Methods';
$lang['ed_currency_mng']    = 'Currencies';
$lang['ed_langauge_mng']    = 'Languages';
$lang['ed_banner_mng']      = 'Banners';
$lang['ed_page_mng']        = 'Pages';
$lang['ed_post_mng']        = 'Posts';
$lang['ed_post_category_mng']= 'Post Categories';

//Payments
$lang['manual']         = 'Manual';
$lang['paypal_express'] = 'Paypal Express';

//Order
$lang['canceled']   = 'Canceled';
$lang['closed']     = 'Closed';
$lang['completed']  = 'Completed';
$lang['expired']    = 'Expired';
$lang['holded']     = 'Holded';
$lang['paid']       = 'Paid';
$lang['pending']    = 'Pending';
$lang['processing'] = 'Processing';
$lang['refunded']   = 'Refunded';
$lang['shipped']    = 'Processing';
$lang['payment_pending'] = 'Payment Pending';
$lang['paymentreview']   = 'Payment Review';
$lang['payer_id']       = 'Payer ID';
$lang['transaction_id'] = 'Transaction ID';

$lang['order_cancel']     = 'Cancel';
$lang['order_hold']       = 'Hold';
$lang['order_unhold']     = 'Unhold';
$lang['order_invoice']    = 'Invoice';
$lang['order_ship']       = 'Ship';
$lang['order_refund']     = 'Refund';

// Attribute Words
$lang['ed_yes']     = 'Yes';
$lang['ed_no']      = 'No';
$lang['ed_any']     = 'Any';
$lang['ed_status']  = 'Status';
$lang['ed_enable']  = 'Enabled';
$lang['ed_disable'] = 'Disabled';
$lang['ed_edit']    = 'Edit';
$lang['ed_select']  = 'Select';
$lang['ed_id']      = 'ID';
$lang['ed_name']    = 'Name';
$lang['ed_description']= 'Description';
$lang['ed_data']       = 'Data';
$lang['ed_slug']       = 'Slug';
$lang['ed_keyword']    = 'Keyword';
$lang['ed_sort_order'] = 'Sort Order';
$lang['ed_image']      = 'Image';
$lang['ed_thumbnail']  = 'Thumbnail';
$lang['ed_label']      = 'Label';
$lang['ed_keywords']   = 'Keywords';
$lang['ed_title']      = 'Title';
$lang['ed_from']       = 'From';
$lang['ed_to']         = 'To';
$lang['ed_start_date'] = 'Start Date';
$lang['ed_end_date']   = 'End Date';
$lang['ed_note']       = 'Note';
$lang['ed_default']    = 'Default';
$lang['ed_exclude']    = 'Exclude';
$lang['ed_required']   = 'Required';
$lang['ed_remove']     = 'Remove';
$lang['ed_no_image']   = 'No Image';
$lang['ed_url']        = 'Url';
$lang['ed_general']    = 'General';
$lang['ed_meta_info']  = 'Meta Info';
$lang['ed_attributes'] = 'Attributes';
$lang['ed_images']     = 'Images';
$lang['ed_view']       = 'View';
$lang['ed_tables']     = 'Tables';
$lang['ed_all']        = 'All';
$lang['ed_more']       = 'More';
$lang['ed_version']    = 'Version';
$lang['ed_author']     = 'Author';
$lang['ed_actions']    = 'Actions';
$lang['ed_send']       = 'Send';
$lang['ed_save']       = 'Save';
$lang['ed_search']     = 'Search';
$lang['ed_contact']    = 'Contact';
$lang['ed_contact_us'] = 'Contact Us';
$lang['ed_language']   = 'Language';
$lang['ed_copyright']  = 'Copyright';
$lang['ed_top']        = 'Top';
$lang['ed_page_title'] = 'Page Title';
$lang['ed_meta_keywords']   = 'Meta Keywords';
$lang['ed_meta_description']= 'Meta Description';

$lang['ed_product']        = 'Product';
$lang['ed_product_name']   = 'Product Name';
$lang['ed_product_detail'] = 'Product Detail';
$lang['ed_price']          = 'Price';
$lang['ed_special_price']  = 'Special Price';
$lang['ed_tags']           = 'Tags';
$lang['ed_allow_review']   = 'Allow Review';
$lang['ed_sku']            = 'SKU';
$lang['ed_quantity']       = 'Quantity';
$lang['ed_qty']            = 'Qty';
$lang['ed_stock_status']   = 'Stock Status';
$lang['ed_out_stock']      = 'Out of Stock';
$lang['ed_in_stock']       = 'In Stock';
$lang['ed_tax_class']      = 'Tax Class';
$lang['ed_shippingable']   = 'Shippingable';
$lang['ed_downloadable']   = 'Downloadable';
$lang['ed_add_option']     = 'Add Option';
$lang['ed_option_name']    = 'Option Name';
$lang['ed_option_values']   = 'Option Values';
$lang['ed_related_products']= 'Related Products';
$lang['ed_samples']         = 'Samples';
$lang['ed_options']         = 'Options';
$lang['ed_example']         = 'Example';
$lang['ed_short_description']= 'Short Description';

$lang['ed_category']    = 'Category';
$lang['ed_categories']  = 'Categories';
$lang['ed_path']        = 'Path';
$lang['ed_parent']      = 'Parent';

$lang['ed_manufacturer']   = 'Manufacturer';
$lang['ed_manufacturers']  = 'Manufacturers';

$lang['ed_coupon_name'] = 'Coupon Name';
$lang['ed_coupon_code'] = 'Coupon Code';
$lang['ed_code']        = 'Code';
$lang['ed_type']        = 'Type';
$lang['ed_discount']    = 'Discount';
$lang['ed_total_amount']    = 'Total Amount';
$lang['ed_uses_per_coupon'] = 'Uses Per Coupon';
$lang['ed_percentage']      = 'Percentage';
$lang['ed_fix_amount']      = 'Fix Amount';
$lang['ed_uses_per_customer'] = 'Uses Per Customer';

$lang['ed_order']        = 'Order';
$lang['ed_orders']       = 'Orders';
$lang['ed_subtotal']     = 'Subtotal';
$lang['ed_total']        = 'Total';
$lang['ed_total_qty']    = 'Total Qty';
$lang['ed_total_refunded']= 'Total Refunded';
$lang['ed_shipping']     = 'Shipping';
$lang['ed_payment']      = 'Payment';
$lang['ed_purchased_on'] = 'Purchased On';
$lang['ed_receiver']     = 'Receiver';
$lang['ed_message']      = 'Message';
$lang['ed_comment_history'] = 'Comment History';
$lang['ed_notify_customer'] = 'Notify Customer By Email';
$lang['ed_visible_front']   = 'Visible on Frontend';
$lang['ed_amount']       = 'Amount';
$lang['ed_invoice']      = 'Invoice';
$lang['ed_shipment']     = 'Shipment';
$lang['ed_refund']       = 'Refund';
$lang['ed_refundment']   = 'Refundment';
$lang['ed_print']        = 'Print';
$lang['ed_date_ordered'] = 'Date Ordered';
$lang['ed_date_invoiced']= 'Date Invoiced';
$lang['ed_date_shipped'] = 'Date Shipped';
$lang['ed_date_refunded']= 'Date Shipped';
$lang['ed_viewing']      = 'Viewing';

$lang['ed_rate']         = 'Rate';
$lang['ed_symbol_left']  = 'Left Symbol';
$lang['ed_symbol_right'] = 'Right Symbol';
$lang['ed_symbol']       = 'Symbol';
$lang['ed_as_default']   = 'Set as Default';
$lang['ed_currency']     = 'Currency';
$lang['ed_date_format']  = 'Date Format';
$lang['ed_time_format']  = 'Time Format';

$lang['ed_cart']        = 'Cart';
$lang['ed_my_cart']     = 'My Cart';
$lang['ed_cart_items']  = '<span>%s</span> items in cart';
$lang['ed_apply']       = 'Apply';
$lang['ed_update']      = 'Update';
$lang['ed_clear']       = 'Clear';
$lang['ed_checkout']    = 'Checkout';

$lang['ed_template']    = 'Template';
$lang['ed_created']     = 'Created';
$lang['ed_content']     = 'Content';
$lang['ed_homepage']    = 'Homepage';        
$lang['ed_content_heading'] = 'Content Heading';
$lang['default']        = 'Default';
$lang['right-sidebar']  = 'Right Sidebar';
$lang['left-sidebar']   = 'Left Sidebar';

$lang['ed_sales']       = '<strong>%s</strong>Down';
$lang['ed_links']       = 'Links';
$lang['ed_market_price']= 'Old';
$lang['ed_saved']       = 'Saved';
$lang['ed_specs']       = 'Specs';
$lang['ed_coupon']      = 'Coupon';
$lang['ed_product_shots']= 'Photos';
$lang['ed_comments']    = 'Comments';
$lang['ed_add_cart']    = 'Buy';
$lang['ed_zipcode']     = 'Zipcode';
$lang['ed_important']   = 'Important';
$lang['ed_hour']        = 'H';
$lang['ed_minute']      = 'M';
$lang['ed_second']      = 'S';
$lang['ed_time_remaining']  = 'Remaining';
$lang['ed_shipping_address']= 'Shipping Address';
$lang['ed_complete_order']  = 'Order';

$lang['ed_customer']   = 'Customer';
$lang['ed_customers']  = 'Customers';
$lang['ed_customer_since']= 'Customer Since';
$lang['ed_province_city']= 'Region';
$lang['ed_province']    = 'Province';
$lang['ed_city']        = 'City';
$lang['ed_address']     = 'Address';
$lang['ed_email']       = 'Email';
$lang['ed_full_name']   = 'Full Name';
$lang['ed_password']    = 'Password';
$lang['ed_password_confirm'] = 'Password Confirm';
$lang['ed_phone']       = 'Phone';
$lang['ed_guest']       = 'Guest';
$lang['ed_registered']  = 'Registered Customer';
$lang['ed_qq']          = 'QQ';
$lang['ed_avatar']      = 'Avatar';
$lang['ed_newsletter']  = 'Newsletter';
$lang['ed_gender']      = 'Gender';
$lang['ed_man']         = 'Man';
$lang['ed_woman']       = 'Woman';
$lang['ed_register']    = 'Register';
$lang['ed_login']       = 'Login';
$lang['ed_logout']      = 'Logout';
$lang['ed_profile']     = 'Profile';
$lang['ed_my_account']  = 'My Account';
$lang['ed_account']     = 'Account';
$lang['ed_welcome']     = 'Welcome %s!';
$lang['ed_loggedin']    = 'Logged In';
$lang['ed_registration']= 'Registration';
$lang['ed_my_orders']   = 'My Orders';

$lang['ed_own_order_no'] = 'Your Order Number';
$lang['ed_order_thanks'] = 'Thanks for your order!';
$lang['ed_order_info']   = 'Order Information';
$lang['ed_order_items']  = 'Products';
$lang['ed_order_note']   = 'Order Notes';
$lang['ed_shipping_method'] = 'Shipping Method';
$lang['ed_payment_method']  = 'Payment Method';

//Cart Messages
$lang['cart_has_no_items']      = 'You have no cart items. Please go on shopping.';
$lang['product_invalid']        = 'You are trying to add invalid product. Please select right one.';
$lang['product_invalid_to_cart']= 'Product is unavailable to purchase now.';
$lang['product_option_invalid'] = 'Invalid Product options or Product has required options.';
$lang['product_added_success']  = '"%s" added to your cart.';
$lang['product_add_failed']     = "We can't add %s to cart due to error. Please try again later.";
$lang['product_qty_invalid']    = 'The requested quantity for "%s" is not available.';
$lang['products_has_required_options'] = 'Some of the products below do not have all the required options. Please edit them and configure all the required options.';
$lang['product_has_required_options']  = 'The product has required options';
$lang['coupon_applied_success'] = 'Your coupon discount has been applied!';
$lang['coupon_invalid']         = "Coupon is either invalid, expired or reached it's usage limit!";
$lang['coupon_already_applied'] = "Your coupon is already applied.";
$lang['coupon_code_invalid']    = "Invalid coupon code!";
$lang['counpon_summary']        = 'Coupon[%s]';
$lang['shipmethod_not_available']= 'There is no available shipping methods.';
$lang['paymethod_not_available']= 'There is no available payment methods.';
$lang['shipmethod_invalid']     = 'Invalid Shipping Method.';                
$lang['paymethod_invalid']      = 'Invalid Payment Method.';
$lang['order_has_problem_on_saving'] = 'Sorry, there is a problem on your order. Please try again.';
$lang['order_payment_canceled'] = 'Payment has been canceled.';

$lang['page_status']    = '&nbsp;(Showing %s-%s of %s)';
$lang['fix_errors']     = 'Please fix the following errors';
$lang['no_record']      = 'No records found.';
$lang['page_not_found'] = 'The Page cannot be found.';
$lang['invalid_action'] = 'Invalid Action';
$lang['ed_comma_seperated'] = 'comma separated';

//Email
$lang['email_header'] = 'Dear %s';
$lang['email_footer'] = 'Thank you again, <strong>%s</strong>';

//Added for Frontend
$lang['search_enter_keyword']   = 'Search here...';
$lang['search_popular_terms']   = 'Popular Tags';

$lang['help_center']    = 'Help Center';
$lang['service_online'] = 'Service Online';
$lang['online_service'] = 'Service';
$lang['every_day']      = 'Every Day';
$lang['customers_say']  = 'Customers Say';
$lang['service_online_duration'] = '24 Hours';
$lang['products_soldout']   = '<span class="main_color">%s</span> items sold out';
$lang['product_limited']    = 'Limited Number';
$lang['confirm_codes']      = 'Confirm Codes';
$lang['no_products_in_selection'] = 'There are no products matching the selection.';

$lang['checkout_receiver_form_title'] = '1. Fill out and check receiver information';
$lang['checkout_confirm_items_title'] = '2. Confirm the items';
$lang['checkout_shipping_title']= '3. Choose Shipping Method';
$lang['checkout_payment_title'] = '4. Choose Payment Method';
$lang['checkout_note_title']    = '5. Notes (if you have special instructions , you can remark on this)';