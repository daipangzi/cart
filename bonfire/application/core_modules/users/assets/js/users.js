event = $.browser.msie?'click':'change'; 
$('#avatar_image').live(event, function() { 
    $("#errors").html('');
    $("#loading").html('<img src="'+theme_path+'/admin/images/loaders/circular/054.gif" alt="Uploading...."/>');
    $("#user-form").ajaxForm({
        url: admin_path+'/catalog/customers/upload_avatar',
        success:  showResponse,
    }).submit();
}); 

$('#remover').live('click', function() {
    $("#avatarImage").attr("src", media_path+"placeholder/100x90.gif");  
    $("#image_name").val(''); 
    $("#image_action").val("deleted"); 
    $(this).hide();
});

function showResponse(responseText, statusText, xhr, $form)  { 
    json = $.parseJSON(responseText);
    if(json.image) {
        $("#avatarImage").attr("src", base_path+"images/"+json.image+"?type=tmp/avatar&width=100&height=90&force=yes");
        $("#errors").html("");
        $("#loading").html('');
        $('#remover').show();
        $("#image_name").val(json.image);
        $("#image_action").val("changed"); 
    } else {
        $("#loading").html("");
        $("#errors").html(json.errors);
    }
    //$('#manufacturer_form').unbind('submit').find('input:submit,input:image,button:submit').unbind('click'); 
    $('#user-form').unbind('submit'); 
}