$("input, textarea, select").not('.nostyle').uniform();
$("#loginForm").validate({
    rules: {
        login: {
            required: true
        },
        password: {
            required: true
        }  
    },
    messages: {
        login: {
            required: "Fill me please"
        },
        password: {
            required: "Please provide a password"
        }
}   
});
