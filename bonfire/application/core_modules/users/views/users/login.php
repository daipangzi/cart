<?php if (auth_errors() || validation_errors()) : ?>
<div class="row-fluid"><div class="span12">
    <div class="alert alert-error fade in">
      <a data-dismiss="alert" class="close">&times;</a>
      <?php echo auth_errors() . validation_errors(); ?>
    </div>
</div></div>
<?php endif; ?>

<div class="container-fluid">
    <div id="header"><div class="row-fluid">
        <div class="navbar">
            <div class="navbar-inner">
              <div class="container">
                    <a class="manufacturer" href="<?php echo site_url('/'); ?>"><?php echo $this->settings_lib->item('site.title'); ?>.<span class="slogan">admin</span></a>
              </div>
            </div><!-- /navbar-inner -->
        </div></div><!-- End .row-fluid -->
    </div><!-- End #header -->
</div><!-- End .container-fluid -->    

<div class="container-fluid">
    <div class="login-box"><div class="wrapper">
        <div class="loginMessages"><?php echo Template::message(); ?></div>
        
        <div class="loginContainer">
            <?php echo form_open($this->uri->uri_string(), array('id' => 'loginForm', 'class' => "form-horizontal", 'autocomplete' => 'on')); ?>
            
                <div class="form-row row-fluid <?php echo iif( form_error('login') , 'error') ;?>"><div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span12" for="username">
                            <?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_login_type_both') : ucwords($this->settings_lib->item('auth.login_type')) ?>:
                            <span class="icon16 icomoon-icon-user right gray marginR10"></span>
                        </label>
                        <input class="span12" id="login" type="text" name="login" value="<?php echo set_value('login'); ?>" tabindex="1"/>
                    </div>
                </div></div>

                <div class="form-row row-fluid <?php echo iif( form_error('password') , 'error') ;?>"><div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span12" for="password">
                            <?php echo lang('ed_password'); ?>:
                            <span class="icon16 icomoon-icon-lock right gray marginR10"></span>
                            <span class="forgot"><?php echo anchor('/forgot_password', lang('us_forgot_your_password')); ?></span>
                        </label>
                        <input class="span12" id="password" type="password" name="password" value="" tabindex="2"/>
                    </div>
                </div></div>
                
                <div class="form-row row-fluid"><div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span12 controls">
                                <?php if ($this->settings_lib->item('auth.allow_remember')) : ?>
                                <label for="remember_me">
                                    <input type="checkbox" id="remember_me" value="1" class="styled" name="remember_me" tabindex="3"/> 
                                    <?php echo lang('us_remember_note'); ?>
                                </label>
                                <?php endif; ?>
                                
                                <button type="submit" class="btn btn-info right" id="submit" name="submit" value="1" tabindex="4"><span class="icon16 icomoon-icon-enter white"></span> <?php echo lang('ed_login'); ?></button>
                            </div>
                        </div>
                    </div>
                </div> </div>
            <?php echo form_close(); ?>
        </div>
    </div></div>
</div><!-- End .container-fluid -->