<div class="container-fluid">
    <div id="header"><div class="row-fluid">
        <div class="navbar">
            <div class="navbar-inner">
              <div class="container">
                    <a class="manufacturer" href="<?php echo site_url('/'); ?>"><?php echo $this->settings_lib->item('site.title'); ?>.<span class="slogan">admin</span></a>
              </div>
            </div><!-- /navbar-inner -->
        </div></div><!-- End .row-fluid -->
    </div><!-- End #header -->
</div><!-- End .container-fluid -->    

<div class="container-fluid">
    <div class="login-box"><div class="wrapper">
        <div class="loginMessages"><?php echo Template::message(); ?></div>
        
        <div class="loginContainer">
            <?php echo form_open($this->uri->uri_string(), array('id' => 'loginForm', 'class' => "form-horizontal", 'autocomplete' => 'off')); ?>
                <div class="form-row row-fluid <?php echo iif( form_error('email') , 'error') ;?>"><div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span12" for="email">
                            <?php echo lang('bf_email'); ?>:
                            <span class="icon16 entypo-icon-email right gray marginR10"></span>
                        </label>
                        <input class="span12" id="email" type="email" name="email" value="<?php echo set_value('email') ?>"/>
                    </div>
                </div></div>
                
                <div class="form-row row-fluid"><div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span12 controls">
                                <button type="submit" class="btn btn-info right" id="submit" name="submit" value="1"><span class="icon16 icomoon-icon-mail-send"></span>Send Password</button>
                                <button type="button" class="btn right" onclick="document.location.href='<?php echo site_url('login'); ?>';" style="margin-right:5px"><span class="icon16 entypo-icon-back"></span>Back</button>
                            </div>
                        </div>
                    </div>
                </div> </div>
            <?php echo form_close(); ?>
        </div>
    </div></div>
</div><!-- End .container-fluid -->