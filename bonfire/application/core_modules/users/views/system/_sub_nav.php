<?php if (has_permission('Bonfire.Users.Manage')):?>
<li>
    <a href="#"><span class="icon16 icomoon-icon-user"></span>User Management</a>
    <ul class="sub">
        <li><a href="<?php echo site_url(SITE_AREA.'/system/users'); ?>"><span class="icon16 icomoon-icon-arrow-right-3"></span>Users</a></li>
        <li><a href="<?php echo site_url(SITE_AREA.'/system/users/create'); ?>"><span class="icon16 icomoon-icon-arrow-right-3"></span>New User</a></li>
    </ul>
</li>
<?php endif;?>