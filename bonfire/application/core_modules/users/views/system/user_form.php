<div class="row-fluid messages-area">
<?php if (isset($user) && $user->banned) : ?>
<div class="span4"><div class="alert alert-warning fade in">
	<h4 class="alert-heading"><?php echo lang('us_banned_admin_note'); ?></h4>
</div></div>
<?php endif; ?>
</div>

<div class="admin-box">
    <div class="row-fluid">
        <div class="span12">
            <?php echo form_open($this->uri->uri_string(), 'id="user-form" class="form-horizontal" autocomplete="off"'); ?>
            <input type="hidden" name="image_name" id="image_name" value="<?php echo set_value('image_name', isset($image_name)?$image_name:''); ?>"/>
            <input type="hidden" name="image_action" id="image_action" value=""/>
    
            <fieldset>
                <legend><?php echo lang('us_account_details') ?></legend>        
                
                <div class="form-row row-fluid <?php echo form_error('email') ? 'error' : '' ?>">
                    <div class="span12">
                        <label class="form-label span3" for="email"><?php echo lang('bf_email').lang('bf_form_label_required') ?>:</label>
                        <input type="email" class="span4" name="email" id="email" value="<?php echo set_value('email', isset($user) ? $user->email : '') ?>">
                        <?php if (form_error('email')) echo '<span class="help-inline">'. form_error('email') .'</span>'; ?>
                    </div>
                </div>
                
                <div class="form-row row-fluid <?php echo form_error('username') ? 'error' : '' ?>"> 
                    <div class="span12">
                        <label class="form-label span3" for="username"><?php echo lang('bf_username').lang('bf_form_label_required') ?>:</label>
                        <input type="text" class="span4" name="username" id="username" value="<?php echo set_value('username', isset($user) ? $user->username : '') ?>">
                        <?php if (form_error('username')) echo '<span class="help-inline">'. form_error('username') .'</span>'; ?>
                    </div>
                </div>
                   
                <div class="form-row row-fluid">
                    <div class="span12">
                        <label class="form-label span3" for="username"><?php echo lang('ed_avatar'); ?>:</label>
                        <span class="imgwrapper"><span class="image marginB10 w100_h90">
                            <img src="<?php echo media_file(isset($img)?$img:'', 100, 90, isset($img_path)?$img_path:''); ?>" alt="" id="avatarImage" class="" width="100" height="90"/>
                            <span id="remover" class="remove" <?php if($img=='') echo 'style="display:none;"';?>><i class="icon-remove">&nbsp;</i></span>
                        </span></span>
                        <span id="errors" class="help-inline" style="color:#B94A48;"><?php echo isset($image_error)?$image_error:''; ?></span>
                        <input type="file" name="avatar_image" id="avatar_image" class="" value=""/>
                        <span id="loading" class="help-inline"></span>
                    </div>
                </div>
                
                <div class="form-row row-fluid <?php echo form_error('display_name') ? 'error' : '' ?>">
                    <div class="span12">
                        <label class="form-label span3" for="display_name"><?php echo lang('bf_display_name') ?>:</label>
                        <input type="text" class="span4" name="display_name" id="display_name" value="<?php echo set_value('display_name', isset($user) ? $user->display_name : '') ?>" />
                        <?php if (form_error('display_name')) echo '<span class="help-inline">'. form_error('display_name') .'</span>'; ?>
                    </div>
                </div>
                
                <div class="form-row row-fluid <?php echo form_error('password') ? 'error' : '' ?>">
                    <div class="span12">
                        <label class="form-label span3" for="password"><?php echo lang('bf_password') ?>:</label>
                        <input type="password" class="span4" id="password" name="password" placeholder="Enter your password" value="" />
                        <?php if (form_error('password')) echo '<span class="help-inline">'. form_error('password') .'</span>'; ?>
                    </div>
                </div>

                <div class="form-row row-fluid <?php echo form_error('pass_confirm') ? 'error' : '' ?>">
                    <div class="span12">
                        <label class="form-label span3" for="pass_confirm"><?php echo lang('bf_password_confirm') ?>:</label>
                        <input type="password" class="span4" id="pass_confirm" name="pass_confirm" placeholder="Enter your password again" />
                        <?php if (form_error('pass_confirm')) echo '<span class="help-inline">'. form_error('pass_confirm') .'</span>'; ?>
                    </div>
                </div>

                <div class="form-row row-fluid <?php echo form_error('role_id') ? 'error' : '' ?>">
                    <div class="span12">
                        <label class="form-label span3" for="textarea"><?php echo lang('us_role'); ?></label>
                        <div class="span2 controls">
                            <select name="role_id" id="role_id" class="chzn-select nostyle">
                            <?php if (isset($roles) && is_array($roles) && count($roles)) : ?>
                                <?php foreach ($roles as $role) : ?>

                                    <?php if (has_permission('Permissions.'. ucfirst($role->role_name) .'.Manage')) : ?>
                                    <?php
                                        // check if it should be the default
                                        $default_role = FALSE;
                                        if ((isset($user) && $user->role_id == $role->role_id) || (!isset($user) && $role->default == 1))
                                        {
                                            $default_role = TRUE;
                                        }
                                    ?>
                                    <option value="<?php echo $role->role_id ?>" <?php echo set_select('role_id', $role->role_id, $default_role) ?>>
                                        <?php e(ucfirst($role->role_name)) ?>
                                    </option>

                                    <?php endif; ?>

                                <?php endforeach; ?>
                            <?php endif; ?>
                            </select>
                            <?php if (form_error('role_id')) echo '<span class="help-inline">'. form_error('role_id') .'</span>'; ?>
                        </div>
                    </div>  
                </div>
                            
                <!--<div class="form-row row-fluid">        
                    <div class="span12">
                        <div class="form-actions lrborder">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" name="submit" class="btn btn-info marginR10" value="1"><?php echo lang('bf_action_save') .' '. lang('bf_user') ?></button>
                                <?php echo anchor(SITE_AREA .'/system/users', lang('bf_action_cancel'), 'class="btn btn-danger"'); ?>
                            </div>
                        </div>
                    </div>   
                </div>-->
                
                <div class="form-buttons">
                    <?php echo anchor(SITE_AREA .'/system/users', '<span class="icon-arrow-left"></span>&nbsp;'.lang('bf_action_cancel'), 'class="btn"'); ?>
                    <button type="reset" name="reset" class="btn" value="Reset">&nbsp;Reset&nbsp;</button>&nbsp;
                    <button type="submit" name="save" class="btn" value="1"><span class="icon-ok"></span>&nbsp;<?php echo lang('bf_action_save') .' '. lang('bf_user') ?></button>
                </div>
            </fieldset>    
            <?php echo form_close(); ?>
          
        </div><!-- End .span12 -->

    </div><!-- End .row-fluid -->
</div>
