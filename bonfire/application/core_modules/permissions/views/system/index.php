<p class="intro"><?php e(lang('permissions_intro')) ?></p>

<div class="admin-box">
    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
    <fieldset><legend>Permissions</legend></fieldset>
        
    <div class="form-buttons">
        <?php if (has_permission('Bonfire.Roles.Manage')):?>
        
        <?php echo anchor(SITE_AREA .'/system/roles/permission_matrix', '<span class="typ-icon-grid"></span>&nbsp;'.lang('bf_menu_permission_martrix'), 'class="btn"'); ?>                    
        
        <?php if (isset($results) && is_array($results) && count($results)) : ?>
        <button type="submit" name="delete" class="btn" id="delete-me" value="" onclick="return confirm('<?php echo lang('permissions_delete_confirm'); ?>')"><i class="icon-remove">&nbsp;</i>&nbsp;<?php echo lang('bf_action_delete') ?></button>
        <?php endif; ?>
        
        <?php echo anchor(SITE_AREA .'/system/permissions/create', '<span class="icon-plus-sign"></span>&nbsp;'.lang('bf_action_insert'), 'class="btn"'); ?>                    
        <?php endif; ?>
    </div>
        
    <div class="responsive">
    <table class="table table-striped lrborder checkAll display">
        <thead>
            <tr>
                <th class="column-check"><input class="check-all" type="checkbox" /></th>
                <th><?php echo lang('permissions_id'); ?></th>
                <th><?php echo lang('permissions_name'); ?></th>
                <th><?php echo lang('permissions_description'); ?></th>
                <th><?php echo lang('permissions_status'); ?></th>
            </tr>
        </thead>

        <tbody>

        <?php if (isset($results) && is_array($results) && count($results)) : ?>
        <?php foreach ($results as $record) : ?>
        <?php     $record = (array)$record; ?>
            <tr>
                <td>
                    <input type="checkbox" name="checked[]" value="<?php echo $record['permission_id'] ?>" />
                </td>
                <td><?php echo $record['permission_id'] ?></td>
                <td>
                    <a href="<?php echo site_url(SITE_AREA .'/system/permissions/edit/'. $record['permission_id']) ?>">
                        <?php echo $record['name'] ?>
                    </a>
                </td>
                <td><?php e($record['description']) ?></td>
                <td><?php e(ucfirst($record['status'])) ?></td>
            </tr>
        <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="6">No permissions found.</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    </div>
    <?php echo form_close(); ?>
    <?php echo $this->pagination->create_links(); ?>
</div>