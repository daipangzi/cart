<li>
    <a href="#"><span class="icon16 icomoon-icon-equalizer-2"></span>Permissions</a>
    <ul class="sub">
        <li><a href="<?php echo site_url(SITE_AREA.'/system/permissions'); ?>"><span class="icon16 icomoon-icon-arrow-right-3"></span>Permissions</a></li>
        <?php if(has_permission('Bonfire.Roles.Add')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/system/permissions/create'); ?>"><span class="icon16 icomoon-icon-arrow-right-3"></span>New Permission</a></li>
        <?php endif;?>
    </ul>
</li>