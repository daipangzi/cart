<p class="intro"><?php echo lang('em_template_note'); ?></p>

<div class="admin-box"><div class="row-fluid">
    <?php Template::block('sub_nav'); ?>          
    
    <div class="span10">
		<?php echo form_open(SITE_AREA .'/system/emailer/template'); ?>
	
		<fieldset>
			<legend><?php echo lang('em_header'); ?></legend>
			<div class="clearfix">
				<div class="input">
					<textarea name="header" rows="15" style="width: 99%" class="tinymce"><?php echo htmlspecialchars_decode($this->load->view('email/_header', null, true)) ;?></textarea>
				</div>
			</div>
		</fieldset>
	
		<fieldset>
			<legend><?php echo lang('em_footer'); ?></legend>
	
			<div class="clearfix">
				<div class="input">
					<textarea name="footer" rows="15" style="width: 99%" class="tinymce"><?php echo htmlspecialchars_decode($this->load->view('email/_footer', null, true)) ;?></textarea>
				</div>
			</div>
		</fieldset>
	
		<div class="form-actions lrborder">
			<button type="submit" name="submit" id="submit" class="btn btn-info" value="Save Template">Save Template</button>&nbsp;<?php echo anchor(SITE_AREA .'/system/emailer', lang('bf_action_cancel'), 'class="btn"'); ?>
		</div>
	
	    <?php echo form_close(); ?>
    </div>
</div></div>
