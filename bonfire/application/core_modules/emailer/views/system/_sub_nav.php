<div class="span2">
    <div class="well left-sub-menu">
        <ul class="nav nav-list">
            <li class="nav-header"><?php echo lang('ed_menu_list'); ?></li>
            <li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/system/emailer') ?>"><?php echo lang('bf_context_settings'); ?></a>
            </li>
            <li <?php echo $this->uri->segment(4) == 'template' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/system/emailer/template') ?>">Template</a>
            </li>
            <li <?php echo $this->uri->segment(4) == 'queue' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/system/emailer/queue') ?>">Queue</a>
            </li>
            <li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/system/emailer/create')?>"><?php echo lang('em_create_email'); ?></a>
            </li>
        </ul>
    </div><!-- End .email-nav-->
</div><!-- End .span2-->
