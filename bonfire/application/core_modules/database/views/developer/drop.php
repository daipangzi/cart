<div class="admin-box"><div class="row-fluid">
        <?php Template::block('sub_nav'); ?>          
        
        <div class="span10">
            <fieldset><legend><?php echo lang('bf_action_delete'); ?> <?php echo lang('db_database'); ?> <?php echo lang('db_tables'); ?></legend></fieldset>

            <?php echo form_open(SITE_AREA .'/developer/database/drop'); ?>

	            <?php if (isset($tables) && is_array($tables) && count($tables) > 0) : ?>
		            <?php foreach ($tables as $table) : ?>
			            <input type="hidden" name="tables[]" value="<?php e($table) ?>" />
		            <?php endforeach; ?>


		            <h3><?php echo lang('db_drop_confirm'); ?></h3>

		            <ul>
		            <?php foreach($tables as $file) : ?>
			            <li><?php e($file) ?></li>
		            <?php endforeach; ?>
		            </ul>

		            <div class="attention png_bg">
			            <?php echo lang('db_drop_attention'); ?>
		            </div>

		            <div class="actions">
			            <button type="submit" name="submit" class="btn btn-danger"><?php echo lang('bf_action_delete'); ?> <?php echo lang('db_tables'); ?></button>&nbsp;
			            <a href="<?php echo site_url(SITE_AREA .'/developer/database/backups') ?>" class="btn"><?php echo lang('bf_action_cancel'); ?></a>
		            </div>

	            <?php endif; ?>

            <?php echo form_close(); ?>
        </div>
</div></div>
        