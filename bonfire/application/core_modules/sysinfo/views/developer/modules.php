<div class="admin-box"><div class="row-fluid">
    <?php Template::block('sub_nav'); ?>          
     
    <div class="span10">
        <fieldset><legend><?php echo lang('si.installed_mods'); ?></legend></fieldset>
        <?php if (isset($modules) && is_array($modules) && count($modules)) : ?>
        <div class="responsive">
            <table class="table table-striped lrborder">
            <colgroup>
                <col width="160"/>
                <col width="80"/>
                <col width=""/>
                <col width="150"/>
            </colgroup>
            <thead>
                <tr>
                    <th><?php echo lang('ed_name'); ?></th>
                    <th><?php echo lang('ed_version'); ?></th>
                    <th><?php echo lang('ed_description'); ?></th>
                    <th><?php echo lang('ed_author'); ?></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($modules as $module => $config) : ?>
                <tr>
                    <td><?php echo $config['name'] ?></td>
                    <td><?php echo isset($config['version']) ? $config['version'] : '---'; ?></td>
                    <td><?php echo isset($config['description']) ? $config['description'] : '---'; ?></td>
                    <td><?php echo isset($config['author']) ? $config['author'] : '---'; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>