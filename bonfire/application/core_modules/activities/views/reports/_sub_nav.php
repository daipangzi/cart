<div class="span2">
    <div class="well left-sub-menu">
        <ul class="nav nav-list">
            <li class="nav-header"><?php echo lang('ed_menu_list'); ?></li>
            <li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/reports/activities') ?>"><?php echo lang('activity_home'); ?></a>
            </li>
            <li <?php echo $this->uri->segment(4) == 'activity_own' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_own') ?>"><?php echo lang('activity_own'); ?></a>
            </li>
            <li <?php echo $this->uri->segment(4) == 'activity_user' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_user') ?>"><?php echo lang('activity_user'); ?></a>
            </li>
            <li <?php echo $this->uri->segment(4) == 'activity_module' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_module') ?>"><?php echo lang('activity_modules') ?></a>
            </li>
            <li <?php echo $this->uri->segment(4) == 'activity_date' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/reports/activities/activity_date') ?>"><?php echo lang('activity_date') ?></a>
            </li>
        </ul>
    </div><!-- End .email-nav-->
</div><!-- End .span2-->