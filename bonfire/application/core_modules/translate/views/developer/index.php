<style>
	form { margin: 0; }
</style>
<div class="well">
	
	<?php echo form_open(); ?>
		<div class="span noLeftMargin" style="line-height:28px;"><?php e(lang('tr_current_lang')); ?>&nbsp;&nbsp;</div>
		
		<select name="trans_lang" id="trans_lang" class="nostyle">
		<?php foreach ($languages as $lang) :?>
			<option value="<?php e($lang) ?>" <?php echo isset($trans_lang) && $trans_lang == $lang ? 'selected="selected"' : '' ?>><?php e(ucfirst($lang)) ?></option>
		<?php endforeach; ?>
			<option value="other"><?php e(lang('tr_other')); ?></option>
		</select>
		&nbsp;&nbsp;
		<input type="text" name="new_lang" id="new_lang" style="display: none" class="nostyle" value="" />
	    &nbsp;&nbsp;
		<button type="submit" name="select_lang" class="btn btn-info vtop" value="<?php e(lang('tr_select_lang')); ?>"><?php e(lang('tr_select_lang')); ?></button>
	<?php echo form_close(); ?>
</div>

<!-- Core -->
<div class="admin-box"><div class="row-fluid"><div class="span12">
    <fieldset><legend><?php echo lang('tr_core') ?></legend></fieldset>
    
    <table class="table lrborder">
	    <tbody>
	    <?php foreach ($lang_files as $file) :?>
		    <tr>
			    <td>
				    <a href="<?php echo site_url(SITE_AREA .'/developer/translate/edit/'. $trans_lang .'/'. $file) ?>">
					    <?php e($file); ?>
				    </a>
			    </td>
		    </tr>
	    <?php endforeach; ?>
	    </tbody>
    </table>
    
    <fieldset><legend><?php echo lang('tr_modules') ?></legend></fieldset>
    <table class="table lrborder">
        <tbody>
        <?php if (isset($modules) && is_array($modules) && count($modules)) : ?>
        <?php foreach ($modules as $file) :?>
            <tr>
                <td>
                    <a href="<?php echo site_url(SITE_AREA .'/developer/translate/edit/'. $trans_lang .'/'. $file) ?>">
                        <?php e($file); ?>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php else : ?>
            <tr>
                <td>
                    <div class="alert alert-info fade in">
                        <a class="close" data-dismiss="alert">&times;</a>        
                        <?php echo lang('tr_no_modules'); ?>
                    </div>
                </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
