<li>
    <a href="#"><span class="icon16 icomoon-icon-users"></span>Role Management</a>
    <ul class="sub">
        <li><a href="<?php echo site_url(SITE_AREA.'/system/roles'); ?>"><span class="icon16 icomoon-icon-arrow-right-3"></span>Roles</a></li>
        <?php if(has_permission('Bonfire.Roles.Add')):?>
        <li><a href="<?php echo site_url(SITE_AREA.'/system/roles/create'); ?>"><span class="icon16 icomoon-icon-arrow-right-3"></span>New Role</a></li>
        <?php endif;?>
        <li><a href="<?php echo site_url(SITE_AREA.'/system/roles/permission_matrix'); ?>"><span class="icon16 icomoon-icon-arrow-right-3"></span>Permission Martrix</a></li>
    </ul>
</li>