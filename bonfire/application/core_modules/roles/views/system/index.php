<p class="intro"><?php e(lang('role_intro')) ?></p>

<?php if (isset($role_counts) && is_array($role_counts) && count($role_counts)) : ?>

<div class="admin-box">
    <div class="form-horizontal">
        <fieldset>
            <legend>Roles</legend>
            <div class="form-buttons">
                <?php if ($this->auth->has_permission('Bonfire.Roles.Add')) : ?>
                <?php echo anchor(SITE_AREA .'/system/roles/create', '<span class="icon-plus-sign"></span>&nbsp;'.lang('bf_action_insert'), 'class="btn"'); ?>                    
                <?php endif;?>
                
                <?php echo anchor(SITE_AREA .'/system/roles/permission_matrix', '<span class="typ-icon-grid"></span>&nbsp;'.lang('bf_menu_permission_martrix'), 'class="btn"'); ?>                    
            </div>
        </fieldset>

        <table class="table table-striped lrborder">
            <thead>
                <tr>
                    <th style="width: 10em"><?php echo lang('role_account_type'); ?></th>
                    <th class="text-center" style="width: 5em"># <?php echo lang('bf_users'); ?></th>
                    <th><?php echo lang('role_description') ?></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($roles as $role) : ?>
                <tr>
                    <td><?php echo anchor(SITE_AREA .'/system/roles/edit/'. $role->role_id, $role->role_name) ?></td>
                    <td class="text-center"><?php
                            $count = 0;
                            foreach ($role_counts as $r)
                            {
                                if ($role->role_name == $r->role_name)
                                {
                                    $count = $r->count;
                                }
                            }

                            echo $count;
                        ?>
                    </td>
                    <td><?php e($role->description) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php endif; ?>
