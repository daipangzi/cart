<?php if ($log_threshold == 0) : ?>
    <div class="alert alert-warning fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <?php e(lang('log_not_enabled')); ?>
    </div>
<?php endif; ?>

<p class="intro"><?php e(lang('log_intro'))  ?></p>

<div class="admin-box"><div class="row-fluid">
    <?php Template::block('sub_nav'); ?>          
    
    <div class="span10">
        <fieldset><legend>Logs</legend></fieldset>        
        <?php if (isset($logs) && is_array($logs) && count($logs) && count($logs) > 1) : ?>
        <?php echo form_open(); ?>
            <table class="table lrborder checkAll">
            <colgroup>
                <col width="40"/>
                <col width="150"/>
                <col width=""/>
            </colgroup>
            <thead>
                <tr>
                    <th class="column-check"><input class="check-all" type="checkbox" /></th>
                    <th style="width: 15em;"><?php e(lang('log_date')) ?></th>
                    <th><?php e(lang('log_file')) ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3">
                        <div class="span6">
                            <?php echo lang('bf_with_selected'); ?>:
                            <button type="submit" name="action_delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete') ?>"  onclick="return confirm('<?php echo lang('logs_delete_confirm'); ?>')"><?php echo lang('bf_action_delete') ?></button>
                        </div>
                        <div class="span6"><?php echo $this->pagination->create_links(); ?></div>
                    </td>
                </tr>
            </tfoot>
            <tbody>
            <?php foreach ($logs as $log) :?>
                <?php if ($log != 'index.html') : ?>
                <tr>
                    <td class="column-check">
                        <input type="checkbox" value="<?php e($log) ?>" name="checked[]" />
                    </td>
                    <td>
                        <a href="<?php e(site_url(SITE_AREA .'/developer/logs/view/'. $log)) ?>">
                            <b><?php e(date('F j, Y', strtotime(str_replace('.php', '', str_replace('log-', '', $log)))) ); ?></b>
                        </a>
                    </td>
                    <td><?php e($log) ?></td>
                </tr>
                <?php endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo form_close(); ?>  
        
        <fieldset><legend><?php echo lang('log_delete_button'); ?></legend></fieldset>
        <?php echo form_open(); ?>
            <div class="alert alert-warning fade in">
                <a class="close" data-dismiss="alert">&times;</a>
                <?php echo lang('log_delete_note'); ?>
            </div>

            <div class="form-actions lrborder">
                <button type="submit" name="action_delete_all" class="btn btn-danger" onclick="return confirm('logs_delete_all_confirm')"><i class="icon-white icon-trash">&nbsp;</i>&nbsp;<?php echo lang('log_delete_button'); ?></button>
            </div>
        <?php echo form_close(); ?>
        
        <?php else : ?>
            <div class="alert alert-info fade in ">
                <a class="close" data-dismiss="alert">&times;</a>
                <?php echo lang('log_no_logs'); ?>
            </div>
        <?php endif; ?>
        
    </div>
</div></div>