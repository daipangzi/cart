<div class="span2">
    <div class="well left-sub-menu">
        <ul class="nav nav-list">
            <li class="nav-header"><?php echo lang('ed_menu_list'); ?></li>
            <li  <?php echo $this->uri->segment(4) != 'settings' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/developer/logs') ?>"><?php echo lang('log_logs'); ?></a>
            </li>
            <li <?php echo $this->uri->segment(4) == 'settings' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url(SITE_AREA .'/developer/logs/settings') ?>"><?php echo lang('log_settings'); ?></a>
            </li>
        </ul>
    </div><!-- End .email-nav-->
</div><!-- End .span2-->