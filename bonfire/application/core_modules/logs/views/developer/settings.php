<?php if ($log_threshold == 0) : ?>
    <div class="alert alert-warning fade in">
        <a class="close" data-dismiss="alert">&times;</a>                
        <?php echo lang('log_not_enabled'); ?>
    </div>
<?php endif; ?>
<div class="alert alert-info fade in">
    <a class="close" data-dismiss="alert">&times;</a>        
    <?php echo lang('log_big_file_note'); ?>
</div>

<div class="admin-box"><div class="row-fluid">
    <?php Template::block('sub_nav'); ?>          
    
    <div class="span10">
        <?php echo form_open(site_url(SITE_AREA .'/developer/logs/enable'), 'class="form-horizontal"'); ?>

        <fieldset>
            <legend><?php echo lang('log_settings') ?></legend>

            <div class="control-group">
                <label for="log_threshold" class="control-label"><?php echo lang('log_the_following'); ?></label>
                <div class="controls">
                    <select name="log_threshold" id="log_threshold" style="width: auto; max-width: none;" class="nostyle">
                        <option value="0" <?php echo ($log_threshold == 0) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_0'); ?></option>
                        <option value="1" <?php echo ($log_threshold == 1) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_1'); ?></option>
                        <option value="2" <?php echo ($log_threshold == 2) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_2'); ?></option>
                        <option value="3" <?php echo ($log_threshold == 3) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_3'); ?></option>
                        <option value="4" <?php echo ($log_threshold == 4) ? 'selected="selected"' : ''; ?>><?php echo lang('log_what_4'); ?></option>
                    </select>

                    <p class="help-block"><?php echo lang('log_what_note'); ?></p>
                </div>
            </div>
        </fieldset>

        <div class="form-actions lrborder">
            <button type="submit" name="submit" class="btn btn-info" value="<?php echo lang('log_save_button'); ?>"><?php echo lang('log_save_button'); ?></button>
        </div>

        <?php echo form_close(); ?>
    </div>
</div>