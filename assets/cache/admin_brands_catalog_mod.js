//--------------------------------------------------------------------   
$(document).ready(function() {    
    event = $.browser.msie?'click':'change'; 
    $('#manufacturer_image').live(event, function() { 
        $("#errors").html('');
        $("#loading").html('<img src="'+theme_path+'/admin/images/loaders/circular/054.gif" alt="Uploading...."/>');
        $("#manufacturer_form").ajaxForm({
            url: admin_path+'/catalog/manufacturers/upload_manufacturer_image',
            success:  showResponse,
        }).submit();
    }); 
    
    $('#remover').live('click', function() {
        $("#manufacturerImage").attr("src", media_path+"placeholder/120x60.gif");  
        $("#manufacturer_image_name").val(''); 
        $("#manufacturer_image_action").val("deleted"); 
        $(this).hide();
    });
});

function showResponse(responseText, statusText, xhr, $form)  { 
    json = $.parseJSON(responseText);
    if(json.image) {
        $("#manufacturerImage").attr("src", base_path+"images/"+json.image+"?type=tmp/manufacturer&width=120&height=60&force=yes");
        $("#errors").html("");
        $("#loading").html('');
        $('#remover').show();
        $("#manufacturer_image_name").val(json.image);
        $("#manufacturer_image_action").val("changed"); 
    } else {
        $("#loading").html("");
        $("#errors").html(base64_decode(json.errors));
    }
    //$('#manufacturer_form').unbind('submit').find('input:submit,input:image,button:submit').unbind('click'); 
    $('#manufacturer_form').unbind('submit'); 
}
