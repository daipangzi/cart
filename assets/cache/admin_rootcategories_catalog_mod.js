$(document).ready(function() {     
    event = $.browser.msie?'click':'change'; 
    $('#category_image').live('click change', function() { 
        $("#errors").html('');
        $("#loading").html('<img src="'+theme_path+'/admin/images/loaders/circular/054.gif" alt="Uploading...."/>');
        $("#category_form").ajaxForm({
            url: admin_path+'/catalog/rootcategories/upload_category_image',
            success:       showResponse,
        }).submit();
    }); 

    $('#remover').live('click', function() {
        $("#categoryImage").attr("src", media_path+"placeholder/120x60.gif");   
        $("#category_image_name").val(""); 
        $("#category_image_action").val("deleted"); 
        $(this).hide();
    }); 
});

function showResponse(responseText, statusText, xhr, $form)  { 
    json = $.parseJSON(responseText);
    if(json.image) {
        $("#categoryImage").attr("src", base_path+"images/"+json.image+"?type=tmp/category&width=120&height=60&force=yes");
        $("#errors").html("");
        $("#loading").html('');
        $('#remover').show();
        $("#category_image_name").val(json.image);
        $("#category_image_action").val("changed"); 
    } else {
        $("#loading").html("");
        $("#errors").html(base64_decode(json.errors));
    }
    $('#category_form').unbind('submit'); 
}
