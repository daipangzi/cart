$(document).ready(function() {     
    event = $.browser.msie?'click':'change'; 
    $('#category_image').live('click change', function() { 
        $("#errors").html('');
        $("#loading").html('<img src="'+theme_path+'/admin/images/loaders/circular/054.gif" alt="Uploading...."/>');
        $("#category_form").ajaxForm({
            url: 'categories/upload_category_image',
            success: function(responseText) { 
                json = $.parseJSON(responseText);
                if(json.image) {
                    $("#categoryImage").attr("src", base_path+"images/"+json.image+"?type=tmp/category&width=120&height=60&force=yes");
                    $("#errors").html("");
                    $("#loading").html('');
                    $('#remover').show();
                    $("#image_name").val(json.image);
                    $("#image_action").val("changed"); 
                } else {
                    $("#loading").html("");
                    $("#errors").html(json.errors);
                }
                $('#category_form').unbind('submit'); 
            },
        }).submit();
    }); 

    $('#remover').live('click', function() {
        $("#categoryImage").attr("src", media_path+"placeholder/120x60.gif");   
        $("#image_name").val(""); 
        $("#image_action").val("deleted"); 
        $(this).hide();
    }); 
});


