/* http://keith-wood.name/timeEntry.html
   Turkish initialisation for the jQuery time entry extension
   Written by Vural DinÃ§er */
(function($) {
	$.timeEntry.regional['tr'] = {show24Hours: true, separator: ':',
		ampmPrefix: '', ampmNames: ['AM', 'PM'],
		spinnerTexts: ['ÅŸu an', 'Ã¶nceki alan', 'sonraki alan', 'arttÄ±r', 'azalt']};
	$.timeEntry.setDefaults($.timeEntry.regional['tr']);
})(jQuery);
